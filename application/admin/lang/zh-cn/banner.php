<?php

return [
    'Title'      => '图片名',
    'App_image'  => 'app图片',
    'Pc_image'   => 'pc图片',
    'Weigh'      => '排序',
    'Status'     => '状态',
    'Status 1'   => '显示',
    'Status 0'   => '隐藏',
    'Createtime' => '添加时间',
    'Updatetime' => '更新时间'
];
