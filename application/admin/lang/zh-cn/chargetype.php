<?php

return [
    'Name'       => '充值类型名',
    'Image'      => '图片',
    'Weigh'      => '排序',
    'Status'     => '状态',
    'Status 1'   => '正常',
    'Status 0'   => '禁止',
    'Createtime' => '添加时间',
    'Updatetime' => '更新时间'
];
