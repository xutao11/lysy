<?php

return [
    'Cycle_id'   => '周期id',
    'User_id'    => '用户',
    'Startdate'  => '起始时间',
    'Enddate'    => '终止时间',
    'Xz_money'   => '下注金额',
    'Fd_money'   => '返点',
    'Hd_money'   => '活动金额',
    'Jg_money'   => '奖金',
    'Yx_count'   => '有效会员',
    'Yk_count'   => '盈亏',
    'Money'      => '分红金额',
    'Status'     => '状态',
    'Status 0'   => '待审核',
    'Status 1'   => '通过',
    'Status 2'   => '未通过',
    'Remark'     => '备注',
    'Ratio'      => '分红比例',
    'Createtime' => '创建时间'
];
