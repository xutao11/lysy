<?php

return [
    'Status'     => '状态',
    'Status 1'   => '正常',
    'Status 0'   => '禁止',
    'Title'      => '公告名',
    'Remark'     => '简介',
    'Content'    => '类容',
    'Createtime' => '发布时间',
    'Updatetime' => '更新时间'
];
