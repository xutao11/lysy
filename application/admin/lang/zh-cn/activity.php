<?php

return [
    'Status'     => '状态',
    'Status 1'   => '推荐',
    'Status 0'   => '正常',
    'Remark'     => '简介',
    'Title'      => '活动名',
    'App_image'  => 'app图片',
    'Pc_image'   => 'pc图片',
    'Content'    => '内容',
    'Createtime' => '发布时间',
    'Updatetime' => '更新时间'
];
