<?php

return [
    'Num'       => '周期数',
    'Startdate' => '周期起始时间',
    'Enddate'   => '周期结束时间',
    'Status'    => '状态',
    'Status 1'  => '已出账',
    'Status 0'  => '未出账',
    'Status 2'  => '已发放'
];
