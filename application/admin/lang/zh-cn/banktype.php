<?php

return [
    'Image'      => '图片',
    'Name'       => '银行卡名称',
    'Code'       => '银行卡编码',
    'Status'     => '状态',
    'Status 1'   => '正常',
    'Status 0'   => '禁用',
    'Createtime' => '添加时间',
    'Updatetime' => '更新时间'
];
