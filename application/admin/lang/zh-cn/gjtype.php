<?php

return [
    'Name'       => '类型名',
    'Status'     => '状态',
    'Status 1'   => '启用',
    'Status 2'   => '禁止',
    'Createtime' => '创建时间 ',
    'Updatetime' => '更新时间'
];
