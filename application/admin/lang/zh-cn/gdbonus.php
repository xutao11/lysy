<?php

return [
    'User_id'    => '用户',
    'Date'       => '日期',
    'Xz_num'     => '投注量',
    'Xz_money'   => '投注额',
    'Hd_money'   => '活动',
    'Jg_money'   => '奖金',
    'Fd_money'   => '返点',
    'Ratio'      => '分红比例',
    'Money'      => '分红金额',
    'Createtime' => '计算时间'
];
