<?php

return [
    'Ordercode'     => '订单号',
    'User_id'       => '付款用户',
    'Status'        => '状态',
    'Status 1'      => '代付款',
    'Status 2'      => '已付款',
    'Status 3'      => '未付款',
    'Status 4'      => '待确认',
    'Pay_pt'        => '支付平台',
    'Code'          => '卡号/识别码',
    'Pay_name'      => '支付名',
    'Pay_id'        => '付款方式',
    'Money'         => '充值金额',
    'Donation'      => '赠送比例',
    'Service_redio' => '手续费比例',
    'Remark'        => '备注',
    'Createtime'    => '充值时间',
    'Updatetime'    => '更新时间'
];
