<?php

return [
    'Image'          => 'app图片',
    'Pcimage'          => 'PC图片',
    'Lotterytype_id' => '分类',
    'Name'           => '彩票名',
    'Status'         => '状态',
    'Status 1'       => '正常',
    'Status 0'       => '禁用',
    'Url_b'          => '备用',
    'Url'            => '接口',
    'Createtime'     => '创建时间',
    'Updatetime'     => '修改时间'
];
