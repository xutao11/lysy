<?php

return [
    'Id'           => 'id',
    'Type'         => '平台',
    'Image'         => '下载二维码',
    'Type ios'     => '苹果',
    'Type android' => '安卓',
    'Versionname'  => '版本号',
    'Is_qz'        => '是否强制更新',
    'Is_qz 1'      => '是',
    'Is_qz 0'      => '否',
    'Remark'       => '更新内容',
    'Url'          => '下载地址',
    'Version'      => '版本号',
    'Createtime'   => '发布时间'
];
