<?php

return [
    'Chargetype_id' => '支付类型',
    'Name'          => '支付方式名',
    'Max_money'     => '单次最高充值金额',
    'Min_money'     => '单次最低充值金额',
    'Service'       => '手续费比例',
    'Pay_pt'        => '收款平台',
    'Status'        => '状态',
    'Status 1'      => '开放',
    'Status 0'      => '禁止',
    'Code'          => '收款识别码',
    'Createtime'    => '添加时间',
    'Updatetime'    => '更新时间'
];
