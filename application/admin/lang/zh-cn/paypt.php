<?php

return [
    'Chargetype_id' => '支付类型',
    'Name'          => '支付名称',
    'Bank_name'     => '银行卡名',
    'Bank_code'     => '卡号',
    'Relaname'      => '持卡人名',
    'Type'          => '类型',
    'Type 1'        => '收款',
    'Type 2'        => '出款',
    'Min_money'     => '最低充值金额',
    'Max_money'     => '最大充值金额',
    'Pay_type'      => '金额类型',
    'Pay_type 1'    => '大额',
    'Pay_type 2'    => '小额',
    'Status'        => '状态',
    'Status 1'      => '正常',
    'Status 0'      => '禁用',
    'Createtime'    => '创建时间',
    'Updatetime'    => '更新时间'
];
