<?php

return [
    'User_id'     => '所属用户',
    'Banktype_id' => '银行卡类型',
    'Realname'    => '真实姓名',
    'Branch_name' => '支行名称',
    'Bank_code'   => '银行卡号',
    'Createtime'  => '创建时间',
    'Updatetime'  => '更新时间'
];
