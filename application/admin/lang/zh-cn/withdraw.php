<?php

return [
    'Txcode'        => '订单号',
    'User_id'       => '提现人',
    'Bank_type'     => '银行名',
    'Bank_name'     => '银行卡名(支行名)',
    'Code'          => '银行编码',
    'Bank_code'     => '卡号',
    'Relaname'      => '收款人姓名',
    'Money'         => '提现金额',
    'Service_money' => '手续费',
    'New_money'     => '提现后剩余金额',
    'Status'        => '状态',
    'Status 1'      => '待审核',
    'Status 2'      => '驳回',
    'Status 3'      => '待出款',
    'Status 4'      => '已出款',
    'Status 5'      => '财务提交',
    'Txtype_id'     => '出款渠道',
    'Remark'        => '备注',
    'Createtime'    => '申请时间',
    'Updatetime'    => '更新时间'
];
