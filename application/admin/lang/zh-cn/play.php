<?php

return [
    'Lottery_id'      => '彩票种类',
    'Play_id'         => '所属玩法组',
    'Paramjson'       => '参数',
    'Name'            => '玩法名',
    'Is_ygj'          => '是否支持云挂机',
    'Is_ygj 1'        => '支持',
    'Is_ygj 0'        => '不支持',
    'Is_group'        => '是否为玩法组',
    'Is_group 1'      => '玩法组',
    'Is_group 0'      => '玩法',
    'Status'          => '状态',
    'Status 1'        => '开放',
    'Status 0'        => '禁用',
    'Min_money_award' => '单倍最低奖金',
    'Max_money_award' => '单倍最高奖金',
    'Money'           => '单注金额',
    'Remark'          => '介绍',
    'Createtime'      => '创建时间',
    'Updatetime'      => '更新时间'
];
