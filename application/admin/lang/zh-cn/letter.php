<?php

return [
    'User_id'    => '用户id',
    'Admin_id'   => '发送人',
    'Content'    => '内容',
    'Status'     => '状态',
    'Status 0'   => '未读',
    'Status 1'   => '已读',
    'Createtime' => '发送时间',
    'Updatetime' => '更新时间'
];
