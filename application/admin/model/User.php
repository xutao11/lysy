<?php

namespace app\admin\model;

use think\Model;


class User extends Model
{

    

    

    // 表名
    protected $name = 'user';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'is_dali_text',
        'is_pay_text',
        'status_text',
        'is_jz_text',
        'login_time_text'
    ];
    

    
    public function getIsDaliList()
    {
        return ['0' => __('Is_dali 0'), '1' => __('Is_dali 1')];
    }

    public function getIsPayList()
    {
        return ['0' => __('Is_pay 0'), '1' => __('Is_pay 1')];
    }

    public function getStatusList()
    {
        return ['1' => __('Status 1'), '0' => __('Status 0'), '2' => __('Status 2')];
    }

    public function getIsJzList()
    {
        return ['1' => __('Is_jz 1'), '0' => __('Is_jz 0')];
    }

    public function getIsZzList()
    {
        return ['1' => '开通', '0' => '未开通'];
    }

    public function getIsDaliTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['is_dali']) ? $data['is_dali'] : '');
        $list = $this->getIsDaliList();
        return isset($list[$value]) ? $list[$value] : '';
    }

    //设置密码
    public function setPasswordAttr($value){

        return md5($value);
    }


    public function getIsPayTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['is_pay']) ? $data['is_pay'] : '');
        $list = $this->getIsPayList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getStatusTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['status']) ? $data['status'] : '');
        $list = $this->getStatusList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getIsJzTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['is_jz']) ? $data['is_jz'] : '');
        $list = $this->getIsJzList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getLoginTimeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['login_time']) ? $data['login_time'] : '');
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }

    protected function setLoginTimeAttr($value)
    {
        return $value === '' ? null : ($value && !is_numeric($value) ? strtotime($value) : $value);
    }


}
