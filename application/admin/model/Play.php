<?php

namespace app\admin\model;

use think\Model;


class Play extends Model
{

    

    

    // 表名
    protected $name = 'play';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'is_ygj_text',
        'is_group_text',
        'status_text'
    ];
    

    
    public function getIsYgjList()
    {
        return ['1' => __('Is_ygj 1'), '0' => __('Is_ygj 0')];
    }

    public function getIsGroupList()
    {
        return ['1' => __('Is_group 1'), '0' => __('Is_group 0')];
    }

    public function getStatusList()
    {
        return ['1' => __('Status 1'), '0' => __('Status 0')];
    }


    public function getIsYgjTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['is_ygj']) ? $data['is_ygj'] : '');
        $list = $this->getIsYgjList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getIsGroupTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['is_group']) ? $data['is_group'] : '');
        $list = $this->getIsGroupList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getStatusTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['status']) ? $data['status'] : '');
        $list = $this->getStatusList();
        return isset($list[$value]) ? $list[$value] : '';
    }




}
