<?php

namespace app\admin\model;

use think\Model;


class Hangplan extends Model
{

    

    

    // 表名
    protected $name = 'hangplan';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'status_text',
        'unit_text',
        'betting_jk_text',
        'pt_type_text',
        'fp_type_text',
        'zsyk_y_text',
        'zsyk_k_text'
    ];
    

    
    public function getStatusList()
    {
        return ['1' => __('Status 1'), '0' => __('Status 0')];
    }

    public function getUnitList()
    {
        return ['元' => __('Unit 元'), '角' => __('Unit 角'), '分' => __('Unit 分'), '厘' => __('Unit 厘')];
    }

    public function getBettingJkList()
    {
        return ['0' => __('Betting_jk 0'), '1' => __('Betting_jk 1')];
    }

    public function getPtTypeList()
    {
        return ['1' => __('Pt_type 1'), '2' => __('Pt_type 2')];
    }

    public function getFpTypeList()
    {
        return ['1' => __('Fp_type 1'), '2' => __('Fp_type 2')];
    }

    public function getZsykYList()
    {
        return ['1' => __('Zsyk_y 1'), '0' => __('Zsyk_y 0')];
    }

    public function getZsykKList()
    {
        return ['1' => __('Zsyk_k 1'), '0' => __('Zsyk_k 0')];
    }


    public function getStatusTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['status']) ? $data['status'] : '');
        $list = $this->getStatusList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getUnitTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['unit']) ? $data['unit'] : '');
        $list = $this->getUnitList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getBettingJkTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['betting_jk']) ? $data['betting_jk'] : '');
        $list = $this->getBettingJkList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getPtTypeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['pt_type']) ? $data['pt_type'] : '');
        $list = $this->getPtTypeList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getFpTypeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['fp_type']) ? $data['fp_type'] : '');
        $list = $this->getFpTypeList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getZsykYTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['zsyk_y']) ? $data['zsyk_y'] : '');
        $list = $this->getZsykYList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getZsykKTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['zsyk_k']) ? $data['zsyk_k'] : '');
        $list = $this->getZsykKList();
        return isset($list[$value]) ? $list[$value] : '';
    }




}
