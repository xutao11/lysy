<?php

namespace app\admin\model;

use think\Model;


class Betting extends Model
{

    

    

    // 表名
    protected $name = 'betting';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'is_jz_text',
        'status_text'
    ];
    

    
    public function getIsJzList()
    {
        return ['1' => __('Is_jz 1'), '0' => __('Is_jz 0')];
    }

    public function getStatusList()
    {
        return ['1' => __('Status 1'), '2' => __('Status 2'), '3' => __('Status 3'), '4' => __('Status 4'), '5' => __('Status 5')];
    }


    public function getIsJzTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['is_jz']) ? $data['is_jz'] : '');
        $list = $this->getIsJzList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getStatusTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['status']) ? $data['status'] : '');
        $list = $this->getStatusList();
        return isset($list[$value]) ? $list[$value] : '';
    }
    //用户关联
    public function bettingUser()
    {
        return $this->hasOne('User','id','user_id')->bind('account');
    }

    public function bettingLottery()
    {
        return $this->hasOne('Lottery','id','lottery_id');
    }




}
