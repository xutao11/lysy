<?php

namespace app\admin\model;

use think\Model;


class Appup extends Model
{

    

    

    // 表名
    protected $name = 'appup';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'type_text',
        'is_qz_text'
    ];
    

    
    public function getTypeList()
    {
        return ['ios' => __('Type ios'), 'android' => __('Type android')];
    }

    public function getIsQzList()
    {
        return ['1' => __('Is_qz 1'), '0' => __('Is_qz 0')];
    }


    public function getTypeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['type']) ? $data['type'] : '');
        $list = $this->getTypeList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getIsQzTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['is_qz']) ? $data['is_qz'] : '');
        $list = $this->getIsQzList();
        return isset($list[$value]) ? $list[$value] : '';
    }




}
