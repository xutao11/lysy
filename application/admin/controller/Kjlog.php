<?php

namespace app\admin\controller;

use app\common\controller\Backend;
use think\Db;

/**
 * 彩票开奖记录
 *
 * @icon fa fa-circle-o
 */
class Kjlog extends Backend
{
    
    /**
     * Kjlog模型对象
     * @var \app\admin\model\Kjlog
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\Kjlog;

    }
    
    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $total = $this->model
                ->where($where)
                ->order($sort, $order)
                ->count();

            $list = $this->model
                ->where($where)
                ->order('pre_draw_time desc')
                ->limit($offset, $limit)

                ->select();

            $list = collection($list)->toArray();
            if($list){
                foreach ($list as $k=>$v){
                    $lottery = Db::table('lsyl_lottery')->where('id',$v['lottery_id'])->find();
                    $list[$k]['lottery'] = $lottery?$lottery['name']:'';
//                    $list[$k]['js']=$v['is_jiesuan'] == 1?'已结算':'未结算';
                }
            }
            $result = array("total" => $total, "rows" => $list);
            return json($result);
        }
        return $this->view->fetch();
    }
    

}
