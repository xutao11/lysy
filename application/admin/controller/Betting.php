<?php

namespace app\admin\controller;

use app\common\controller\Backend;
use think\Db;

/**
 * 投注管理
 *
 * @icon fa fa-circle-o
 */
class Betting extends Backend
{
    
    /**
     * Betting模型对象
     * @var \app\admin\model\Betting
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\Betting;
        $this->view->assign("isJzList", $this->model->getIsJzList());
        $this->view->assign("statusList", $this->model->getStatusList());
    }
    
    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $where1 = $this->cxtj();
//            dump($where1);
            if(isset($where1['id'])){
                $where1['Betting.id'] = $where1['id'];
                unset($where1['id']);
            }
            if(isset($where1['account'])){
                $where1['User.account'] = $where1['account'];
                unset($where1['account']);
            }
            if(isset($where1['status'])){
                $where1['Betting.status'] = $where1['status'];
                unset($where1['status']);
            }
            if(isset($where1['is_jz'])){
                $where1['Betting.is_jz'] = $where1['is_jz'];
                unset($where1['is_jz']);
            }
            if (isset($where1['createtime'])){
                $where1['Betting.createtime'] = $where1['createtime'];
                unset($where1['createtime']);
            }
            if (isset($where1['updatetime'])){
                $where1['Betting.updatetime'] = $where1['updatetime'];
                unset($where1['updatetime']);
            }

            $total = $this->model
                ->hasWhere('bettingUser')
                ->relation('bettingLottery')
                ->where($where1)
                ->order('Betting.createtime desc')
                ->count();

            $list = $this->model
                ->hasWhere('bettingUser')
                ->relation('bettingLottery')
                ->limit($offset, $limit)
                ->order('Betting.createtime desc')
                ->where($where1)
                ->field('Betting.*,User.account')
                ->select();
            $list = collection($list)->toArray();
            if($list){
                foreach ($list as $k=>$v){


//                    $lottery = Db::name('lottery')->where('id',$v['lottery_id'])->field('name')->find();
//                    $list[$k]['lottery_name'] = $lottery?$lottery['name']:'';
                    $play = getPlay($v['play_id']);
                    $list[$k]['play'] = implode("/", array_reverse($play));
//                    $list[$k]['param'] = paramParsing(json_decode($v['param'],true));




                }
            }
            $result = array("total" => $total, "rows" => $list);

            return json($result);
        }
        return $this->view->fetch();
    }
    

}
