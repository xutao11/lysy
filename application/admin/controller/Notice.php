<?php

namespace app\admin\controller;

use app\common\controller\Backend;
use think\cache\driver\Redis;
use think\Db;

/**
 * 
 *
 * @icon fa fa-circle-o
 */
class Notice extends Backend
{
    
    /**
     * Notice模型对象
     * @var \app\admin\model\Notice
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\Notice;
        $this->view->assign("statusList", $this->model->getStatusList());
    }
    
    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */

    /**
     * 发送
     */
    public function send(){
        $id = input('id');
        $re = Db::name('notice')->where('id',$id)->find();
        if(!$re) return $this->error('该公告不存在');
        if($re['status'] == '0') return $this->error('已禁止');
        $redis = new Redis();
        $redis->redis_zd()->lPush('notice',$id);
        return $this->success('发送成功');

    }
}
