<?php

namespace app\admin\controller;

use app\common\controller\Backend;
use think\cache\driver\Redis;
use think\Db;
use think\exception\PDOException;
use think\exception\ValidateException;

/**
 * 用户管理
 *
 * @icon fa fa-user
 */
class User extends Backend
{

    /**
     * User模型对象
     * @var \app\admin\model\User
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\User;
        $this->view->assign("isDaliList", $this->model->getIsDaliList());
        $this->view->assign("isPayList", $this->model->getIsPayList());
        $this->view->assign("statusList", $this->model->getStatusList());
        $this->view->assign("isJzList", $this->model->getIsJzList());
        $this->view->assign("isZzList", $this->model->getIsZzList());
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    /**
     * 查看
     */
    public function index()
    {


        //设置过滤方法
        $this->request->filter(['strip_tags']);
        if ($this->request->isAjax()) {


            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {

                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();


            $total = $this->model
                ->where($where)
                ->order($sort, $order)
                ->count();

            $list = $this->model
                ->where($where)
                ->order($sort, $order)
                ->order('login_ip desc')
                ->limit($offset, $limit)
                ->select();
//            dump($this->model->getLastSql());
            $list = collection($list)->toArray();
            if ($list) {
                foreach ($list as $k => $v) {
                    $list[$k]['identity'] = userIdentity($v['level'])['name'];
                    //奖金池
                    //推荐人
                    $tuijian = Db::name('user')->where('id', $v['user_id'])->field('account')->find();
                    $list[$k]['p_user'] = $tuijian ? $tuijian['account'] : '';
//                    $list[$k]['fdType'] = fdType($v['ratio']);
                }
            }
            $result = array("total" => $total, "rows" => $list, 'r' => input('t'));
//
            return json($result);
        }

        return $this->view->fetch();
    }
    /**
     * 编辑
     */
    public function edit($ids = null)
    {
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                $params = $this->preExcludeFields($params);
                $result = false;
                Db::startTrans();
                try {
                    //是否采用模型验证
                    if ($this->modelValidate) {
                        $name = str_replace("\\model\\", "\\validate\\", get_class($this->model));
                        $validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.edit' : $name) : $this->modelValidate;
                        $row->validateFailException(true)->validate($validate);
                    }
                    $result = $row->allowField(true)->save($params);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were updated'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $this->view->assign("row", $row);
        return $this->view->fetch();
    }

    /**
     * 我方支付渠道
     */
    public function ptpay($ids = 0)
    {

//        $this->view->assign("isPayList", $this->model->getIsPayList());
        if (request()->isPost()) {
            $param = input('post.');
            $ids = input('id');
            // 启动事务
            Db::startTrans();
            try {
                $data = [
                    ['user_id' => $ids, 'paypt_id' => $param['max_bank']],
                    ['user_id' => $ids, 'paypt_id' => $param['min_bank']],
                ];
                if ($param['is_pay'] == 1) {
                    Db::table('lsyl_user_ptpay')->where('user_id', $ids)->delete();
                    Db::table('lsyl_user_ptpay')->insertAll($data);
                } else {
                    Db::table('lsyl_user_ptpay')->where('user_id', $ids)->delete();
                }
                Db::table('lsyl_user')->where('id', $ids)->update([
                    'is_pay' => $param['is_pay']
                ]);
                // 提交事务
                Db::commit();
            } catch (\Exception $e) {
                // 回滚事务
                Db::rollback();
                return $this->error('修改失败' . $e->getMessage());
            }
            return $this->success('修改成功');
        } else {
            $user = Db::table('lsyl_user')->where('id', $ids)->field('id,is_pay')->find();
            $bank = [
                'min' => 0,
                'max' => 0
            ];
            if ($user['is_pay'] == 1) {
                $ptbank = Db::table('lsyl_user_ptpay')->alias('up')->where('up.user_id', $user['id'])->join('lsyl_paypt pt', 'pt.id = up.paypt_id')->where('pt.type', '1')->field('pt.*')->select();
                foreach ($ptbank as $k => $v) {
                    if ($v['pay_type'] == 1) {
                        //大额
                        $bank['max'] = $v['id'];
                    }
                    if ($v['pay_type'] == 2) {
                        //小额
                        $bank['min'] = $v['id'];
                    }
                }
            }
            $this->assign('bank', $bank);
            $this->assign('user', $user);
            //大额收款
            $de_money = Db::table('lsyl_paypt')->where('type', 1)->where('pay_type', 1)->select();
            $this->assign('max_bank', $de_money);
            //小额
            $min_bank = Db::table('lsyl_paypt')->where('type', 1)->where('pay_type', 2)->select();
            $this->assign('min_bank', $min_bank);
            $this->assign('ids', $ids);

            return $this->fetch();
        }


    }

    /**
     * 用户等级查看
     */
    public function user_level($ids)
    {
        $user = Db::table('lsyl_user')->where('id', $ids)->find();
        $this->assign('id', $ids);
        if (!$user) return $this->error('该用户不存在');
        return $this->fetch();

    }

    /**
     * 用户等级数据获取
     */
    public function user_level_data($ids)
    {


        $list = user_tj($ids);
        $list = array_reverse($list);

        if ($list) {
            foreach ($list as $k => $v) {

                $list[$k]['identity'] = userIdentity($v['level'])['name'];
                //奖金池
                $list[$k]['fdType'] = prizePool($v['ratio']);
                $list[$k]['login_time'] = date('Y-m-d H:i:s', $v['login_time']);

            }
        }
        $data = [
            'code' => 0,
            'msg' => '',
            'count' => count($list),
            'data' => $list
        ];
        return json($data);
    }

    /**
     * 用户登录日志
     */
    public function userlogin($ids)
    {


        $this->assign('id', $ids);
        return $this->fetch();


    }

    /**
     * 登录日志获取
     */
    public function user_login_data()
    {
        $param = input('param.');
        $user_id = $param['ids'];
        $limit = $param['limit'];
        $page = $param['page'];
        $list = Db::name('userlogin')->where('user_id', $user_id)->field('status,ip,kl,createtime')->limit($limit)->page($page)->select();
        if ($list) {
            foreach ($list as $k => $v) {
                $list[$k]['createtime'] = date('Y-m-d H:i:s', $v['createtime']);
            }
        }
        $count = Db::name('userlogin')->where('user_id', $user_id)->count();
        $data = [
            'code' => 0,
            'msg' => '',
            'count' => $count,
            'data' => $list
        ];
        return json($data);

    }

    /**
     * 资金修正
     */
    public function moneyxz($ids = 0){
        if ($this->request->isPost()) {
            $param= input('param.');
            $remark = $param['remark'];
            if($remark == '') return $this->error('备注不能为空');
            $user = Db::table('lsyl_user')->where('id',$param['id'])->find();
            unset($param['id']);
            $type = $param['type'];
            if(!in_array($type,[1,2])) return $this->error('参数错误');
            if($param['money']<=0) return $this->error('金额必需大于0');

            if($param['lx'] == 4 && $type == '2') return $this->error('活动不能减少金额');
            if($type == 1){

            }else{
                $param['money'] = bcsub(0,$param['money'],3);
            }
            // 启动事务
            Db::startTrans();
            try{
                updateUser($user['id'], $param['money'], $param['lx'], 0, time(), $remark);
                // 提交事务
                Db::commit();

            } catch (\Exception $e) {
                // 回滚事务
                Db::rollback();
                $this->error('修正失败:'.$e->getMessage());
            }
            $this->success('修正成功');
        }else{
            $param= input('param.');
            $ids = $param['ids'];
            $user = Db::table('lsyl_user')->where('id',$ids)->find();
            $this->assign('id',$ids);
            if(!$user) return $this->error('该用户不存在');
            return $this->fetch();
        }

    }

    /**
     * 重置用户密码
     */
    public function resetpwd($ids){
        $user = Db::name('user')->where('id',$ids)->find();
        if(!$user) return $this->error('没有该用户');
        $re = Db::name('user')->where('id',$ids)->update([
           'password'=>md5('ls123456'),
           'updatetime'=>time()
        ]);

        if($re) return $this->success('重置成功');

        return $this->error('重置失败');

    }
    /**
     * 用户报表页面显示
     *
     */
    public function userbb($ids){

        $this->assign('ids',$ids);

        //查询彩种列表
        $lottery = Db::name('lottery')->field('id,name')->select();
        $this->assign('lottery',$lottery);
        return $this->fetch();
    }



    /**
     * 个人报表
     * @param user_id 用户id
     * @param start_date  2010-01-02 00:00:00
     * @param end_date  2030-02-02 23:59:59
     */
    public function user_report(){
        if(!request()->isAjax()) return json(['code'=>0,'msg'=>'非法请求']);
            $user_id = input('user_id');
        $user_ids[] = $user_id;



        $start_date = input('start_date');

        $end_date = input('end_date');
        $end_date =$end_date?$end_date." 23:59:59":'2100-01-01';


        $data = report($user_ids,$start_date,$end_date);
        return json(['code'=>1,'data'=>$data]);
//        return $this->success('个人报表',$data);
    }
    /**
     * 个人投注记录
     * @param user_id 用户id
     * @param start_date_beiing  2010-01-02 00:00:00
     * @param end_date_betting  2030-02-02 23:59:59
     * @param betting_ordercode 订单号
     * @param betting_status '状态:1=待开奖,2=中奖,3=未中奖,4=结算错误,5=已撤销'
     * @param betting_lotteryid 彩种
     * @parame limit
     * @param page
     */
    public function betting_list(){
        $user_id = input('user_id');
        $limit = input('limit');
        $page  = input('page');

        $where = [];
        $where['user_id'] = $user_id;
        //时间范围
        $start_date = input('start_date_beiing');
        $end_date = input('end_date_betting');
        $end_date =$end_date?$end_date." 23:59:59":'2100-01-01';
        $start_date = strtotime($start_date);
        $end_date = strtotime($end_date);
        //状态
        $status = input('betting_status');
        if($status){
            $where['status'] = $status;
        }
        //彩种id
        $lottery_id = input('betting_lotteryid');
        if($lottery_id){
            $where['lottery_id'] = $lottery_id;
        }
        //下注单号
        $ordercode = input('betting_ordercode');
        if($ordercode){
            $where['ordercode'] = ['like',"{$ordercode}%"];
        }

        $betting = Db::name('betting')->where($where)->where('createtime','>=',$start_date)->where('createtime','<=',$end_date)->limit($limit)->page($page)->field('ordercode,pre_draw_issue,param,lottery_id,play_id,jg_money,jg_num,xz_money,status,createtime')->select();
        $count =  Db::name('betting')->where($where)->where('createtime','>=',$start_date)->where('createtime','<=',$end_date)->count();
        if($betting){
            foreach ($betting as $k=>$v){
                $betting[$k]['createtime'] =date('Y-m-d H:i:s',$v['createtime']);
                $betting[$k]['play'] =implode('/',getPlay($v['play_id']));
                $kj = Db::name('kjlog')->where('lottery_id',$v['lottery_id'])->where('pre_draw_issue',$v['pre_draw_issue'])->field('pre_draw_code')->find();
                $betting[$k]['kjcode'] = $kj?$kj['pre_draw_code']:'-';
                $lottery = Db::name('lottery')->where('id',$v['lottery_id'])->field('name')->find();
                $betting[$k]['lottery_name'] = $lottery?$lottery['name']:'-';

                if($v['status']== 2){
                    $betting[$k]['jg'] = bcmul($v['jg_money'],$v['jg_num'],3);
                }else{
                    $betting[$k]['jg'] ='0.000';
                }
            }
        }

        return json([
           'code'=>0,
           'msg'=>'',
           'count'=>$count,
           'data'=>$betting
        ]);
    }

    /**
     * 账变记录
     * @param start_date_beiing  2010-01-02 00:00:00
     * @param end_date_betting  2030-02-02 23:59:59
     * @param type 类型:1=充值,2=提现,3=彩票下注,4=活动,5=转账,6=资金修正,7=返点,8=团队分红,9=浮动工资,10=彩票奖金,11=撤单 0= 全部
     */
    public function money_log(){
        $user_id = input('user_id');
        $limit = input('limit');
        $page  = input('page');

        $where = [];
        $where['user_id'] = $user_id;
        //时间范围
        $start_date = input('start_date_beiing');
        $end_date = input('end_date_betting');
        $end_date =$end_date?$end_date." 23:59:59":'2100-01-01';
        $start_date = strtotime($start_date);
        $end_date = strtotime($end_date);
        //类型
        $type = input('type');
        if($type){
            $where['type'] = $type;
        }

        $money_log = Db::name('moneylog')->where($where)->where('createtime','>=',$start_date)->where('createtime','<=',$end_date)->limit($limit)->page($page)->select();
        $count = Db::name('moneylog')->where($where)->where('createtime','>=',$start_date)->where('createtime','<=',$end_date)->count();
        if($money_log){
            foreach ($money_log as $k=>$v){
                $money_log[$k]['createtime'] = date('Y-m-d H:i:s',$v['createtime']);
            }
        }

        return json([
            'code'=>0,
            'msg'=>'',
            'count'=>$count,
            'data'=>$money_log
        ]);
    }

    /**
     * 用户私信
     */
    public function letter(){
        $user_id = input('user_id');


        $this->assign('ids',$user_id);



        return $this->fetch();

    }
    /**
     * 获取私信列表
     *
     *
     */
    public function letterlist(){
        $user_id = input('user_id');
        $limit = input('limit');
        $page = input('page');
        $lotter = Db::name('letter')->alias('letter')->where('letter.user_id',$user_id)->join('lsyl_admin admin','admin.id = letter.admin_id','left')->order('letter.createtime desc')->limit($limit)->page($page)->field('letter.*,admin.username')->select();
        $count = Db::name('letter')->where('user_id',$user_id)->count();
        if($lotter){
            foreach ($lotter as $k=>$v){
                $lotter[$k]['createtime'] = date('Y-m-d H:i:s',$v['createtime']);
                $lotter[$k]['updatetime'] = date('Y-m-d H:i:s',$v['updatetime']);
            }
        }

        return json([
            'code'=>0,
            'msg'=>'',
            'count'=>$count,
            'data'=>$lotter
        ]);




    }
    /**
     * 发送私信
     */
    public function lettersend(){
        $user_id = input('user_id');
        $user = Db::name('user')->where('id',$user_id)->field('id')->find();
        if(!$user){
            return [
                'code'=>0,
                'msg'=>'没有该用户'
            ];
        }
        $centont = input('content');
        if(empty($centont)) return[
            'code'=>0,
            'msg'=>'内容为空'
        ];
        $admin = session('admin');
        $re = Db::name('letter')->insert([
           'user_id'=>$user_id,
            'admin_id'=>$admin['id'],
            'content'=>$centont,
            'status'=>'0',
            'createtime'=>time(),
            'updatetime'=>time()
        ]);

        //获取私信数量缓存啊
        $redis = new Redis();
        $redis->redis_zd()->lPush('letter',$user_id);
//        $letter =cache('letter');
//        if(isset($letter[$user_id])){
//            $letter[$user_id] ++;
//        }else{
//            $letter[$user_id] = 1;
//        }
//        cache('letter',$letter);

        if($re)return[
            'code'=>1,
            'msg'=>'发送成功'
        ];

        return [
            'code'=>0,
            'msg'=>'发送失败'
        ];




    }
    /**
     * 删除私信
     */
    public function delletter(){
        if(!request()->isAjax()) return[
          'code'=>0,
          'msg'=>'非法请求'
        ];
        $id = input('id');
        $re = Db::name('letter')->where('id',$id)->delete();
        if($re) return [
          'code'=>1,
          'msg'=>'删除成功'
        ];
        return [
            'code'=>0,
            'msg'=>'删除成功'
        ];


    }
    /**
     * 移线
     *
     */
    public function edit_level(){

        $id = input('user_id');
//        dump($id);
        $user = Db::name('user')->where('id',$id)->field('id,account,ratio,user_id')->find();
        if(!$user) return $this->error('没有该用户');
        $user['jgc'] = prizePool($user['ratio']);
        $this->assign('user',$user);
        return $this->fetch();

    }

}
