<?php

namespace app\admin\controller;

use app\common\controller\Backend;
use think\Cache;
use think\Db;
use think\exception\PDOException;
use think\exception\ValidateException;

/**
 * 提现申请管理
 *
 * @icon fa fa-circle-o
 */
class Withdraw extends Backend
{
    
    /**
     * Withdraw模型对象
     * @var \app\admin\model\Withdraw
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\Withdraw;

        $this->statusList = $this->model->getStatusList();

        $this->view->assign("statusList", $this->model->getStatusList());


        //查询出款方式
        $txpay = Db::name('txtype')->where('status',1)->field('id,name')->select();
        $this->assign('txtype',$txpay);

        //根据当管理人角色
        $this->assign('admin_role',$this->admin_role);

    }
    
    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    /**
     * 查看
     */
    public function index()
    {

        $where1=[];

        //风控
        if(in_array(2,$this->admin_role)){
//            $statsu = ['1','2','3'];


            $where1['status'] = ['in',['1','2','3']];
//            unset($this->statusList[3]);
             unset($this->statusList[4]);
             unset($this->statusList[5]);
        }
        //财务
        if(in_array(4,$this->admin_role)){
            $where1['status'] = ['in',['3','4','5']];
            unset($this->statusList[1]);
            unset($this->statusList[2]);
//            unset($this->statusList[3]);
        }
        $this->view->assign("statusList", $this->statusList);
        //设置过滤方法
        $this->request->filter(['strip_tags']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $total = $this->model
                ->where($where)
                ->where($where1)
                ->order($sort, $order)
                ->count();

            $list = $this->model
                ->where($where)
                ->where($where1)
                ->order($sort, $order)
                ->limit($offset, $limit)
                ->select();

            $list = collection($list)->toArray();
            if($list){
                foreach ($list as $k=>$v){
                    $user = Db::name('user')->where('id',$v['user_id'])->field('account')->find();
                    $list[$k]['account'] = $user?$user['account']:'';
                }
            }
            $result = array("total" => $total, "rows" => $list);

            return json($result);
        }
        return $this->view->fetch();
    }
    /**
     * 编辑
     */
    public function edit($ids = null)
    {
//        dump($this->admin_role);
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
//        if($row['status'] != 1) $this->error('该申请已审核');
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($row['status'] == 2) return $this->error('该申请已驳回');
            if ($row['status'] == 4) return $this->error('该申请已出款');

            if ($params) {
                $params = $this->preExcludeFields($params);

                if (isset($params['status']) && $params['status'] == 1) return $this->error('不能修改为当前状态');

                if (isset($params['txtype_id']) && $params['txtype_id'] == '1') {
                    $tx_bank = spBank($row['bank_type']);
                    if ($tx_bank == false) return $this->error('不支持该银行卡');
                    //supay
                } elseif (isset($params['txtype_id']) && $params['txtype_id'] == '3') {
//                    三和
                    $tx_bank = shBank($row['bank_type']);
                    if ($tx_bank == false) return $this->error('不支持该银行卡');


                } elseif (isset($params['txtype_id']) && $params['txtype_id'] == '4') {

                    $tx_bank = happyBank($row['bank_type']);
                    if ($tx_bank == false) return $this->error('不支持该银行卡');


                } elseif (isset($params['txtype_id']) && $params['txtype_id'] == '4') {

                    $tx_bank = happyBank($row['bank_type']);
                    if ($tx_bank == false) return $this->error('不支持该银行卡');
                }


                $result = false;
                Db::startTrans();
                try {
                    //是否采用模型验证
                    if ($this->modelValidate) {
                        $name = str_replace("\\model\\", "\\validate\\", get_class($this->model));
                        $validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.edit' : $name) : $this->modelValidate;
                        $row->validateFailException(true)->validate($validate);
                    }
                    //通过
                    if ($params['status'] == 4) {
                        //查询用户 是否有 今天的盈亏记录
//                        userYkJs('tx_money', $row['user_id'], $row['money']);
                    }
//                    dump($params);
                    //不通过（退还用户的金额）
                    if ($params['status'] == 2) {
//                        Db::table('cp_user')->where('id', $row['user_id'])->setInc('all_money', $row['money']);
//                        $nwe_money = Db::table('cp_user')->where('id', $row['user_id'])->column('all_money');
//                        money_log($row['user_id'], $row['money'], $nwe_money[0], 2, $ids, '用户提现(驳回)', '+');
                        updateUser($row['user_id'],$row['money'],'2',$row['txcode'],time(),'用户提现(驳回)');
                    }
                    if ($params['status'] == '3') {
                        Cache::inc('cw_num');
                    }
                    $result = $row->allowField(true)->save($params);
                    $tx = txPay($ids);
                    // dump($tx);
                    // if($tx && $tx['success'] == false) throw new \think\Exception($tx['msg'], 100006);
//                    if ($tx && $tx['success'] == false) $this->error($tx['msg']);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were updated'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $this->view->assign("row", $row);
        return $this->view->fetch();
    }

    /**
     * 审核
     */
    public function examine($ids){
        //查询当前订单信息
        $withdraw = Db::name('withdraw')->where('id',$ids)->field('id,status,txtype_id')->find();
        $status = $this->statusList;
//        if($withdraw['status'] ==2) return $this->error('该订单已驳回');
        if($withdraw['status'] ==2) return '该订单已驳回';
//        if($withdraw['status'] == 4 || $withdraw['status'] == 5) return $this->error('该订单财务已处理');
        if($withdraw['status'] == 4 || $withdraw['status'] == 5) return '该订单财务已处理';
        //风控
        if(in_array(2,$this->admin_role)){


            unset($status[1]);
            unset($status[4]);
            unset($status[5]);
        }
//        if($withdraw['status'] == 4) return $this->error('该订单已出款');
        if($withdraw['status'] == 4) return '该订单已出款';
        //财务
        if(in_array(4,$this->admin_role)){
//            if($withdraw['status'] == 1) return $this->error('该订单待风控审核');
            if($withdraw['status'] == 1) return '该订单待风控审核';
            unset($status[1]);
            unset($status[2]);
            unset($status[3]);
        }
        $this->assign('status',$status);
        $this->assign('withdraw',$withdraw);
        return $this->fetch();
    }
    /**
     * 审核提交
     */
    public function examinetj(){

        if(!request()->isPost()) return $this->error('非法请求');

        $id = input('id');
        $status = input('status');
        $txtype_id = input('txtype_id');
        $remark = input('remark');
        if($txtype_id == 2){
            //平台出款  订单状态改为已出款
            $status = 4;
        }
        // 启动事务
        Db::startTrans();
        try{
            $withdraw = Db::name('withdraw')->where('id',$id)->lock(true)->find();
            Db::name('withdraw')->where('id',$id)->update([
                'status'=>$status,
                'txtype_id'=>$txtype_id,
                'remark'=>$remark,
                'updatetime'=>time()
            ]);
            if($status == 2){
                //驳回(返回用户金额)
                updateUser($withdraw['user_id'],$withdraw['money'],'2',$withdraw['txcode'],time(),'用户提现(驳回)');
            }

            //提现三方接口
            txPay($id);
            // 提交事务
            Db::commit();
        } catch (\Exception $e) {
            // 回滚事务
            Db::rollback();
            return $this->error('处理失败:'.$e->getMessage());
        }
        return $this->success('处理成功');

    }


    /**
     * 编辑
     */
    public function edit1($ids = null)
    {
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                $params = $this->preExcludeFields($params);
                $result = false;
                Db::startTrans();
                try {
                    //是否采用模型验证
                    if ($this->modelValidate) {
                        $name = str_replace("\\model\\", "\\validate\\", get_class($this->model));
                        $validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.edit' : $name) : $this->modelValidate;
                        $row->validateFailException(true)->validate($validate);
                    }
                    $result = $row->allowField(true)->save($params);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were updated'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $this->view->assign("row", $row);
        return $this->view->fetch();
    }
    

}
