<?php

namespace app\admin\controller;

use app\common\controller\Backend;
use think\Db;
use think\Exception;
use think\exception\PDOException;
use think\exception\ValidateException;

/**
 * 玩法管理
 *
 * @icon fa fa-play
 */
class Play extends Backend
{
    
    /**
     * Play模型对象
     * @var \app\admin\model\Play
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\Play;
        $this->view->assign("isYgjList", $this->model->getIsYgjList());
        $this->view->assign("isGroupList", $this->model->getIsGroupList());
        $this->view->assign("statusList", $this->model->getStatusList());
    }
    
    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $total = $this->model
                ->where($where)
//                ->order($sort, $order)
                ->count();

            $list = $this->model
                ->where($where)
//                ->order($sort, $order)
//                ->limit($offset, $limit)
                ->select();

            $list = collection($list)->toArray();
            if($list){
                foreach ($list as $k=>$v){
                    $lottery = Db::name('lottery')->where('id',$v['lottery_id'])->find();
                    $list[$k]['lottery_id'] = $lottery?$lottery['name']:'';

                }
            }
            $re = tree($list);
            $result = array("total" => $total, "rows" => $re);

            return json($result);
        }
        return $this->view->fetch();
    }
    /**
     * 添加下级玩法
     */
    public function addchild($ids){

        $row = $this->model->get($ids)->toArray();
        if(!$row) return $this->error('没查询到该数据');
        if($row['is_group'] != 1) return $this->error('玩法不能添加下级');
        $lottery = Db::name('lottery')->where('id',$row['lottery_id'])->find();
        if(!$lottery) return $this->error('彩票不存在');

        if(request()->isPost()){
            $params = $this->request->post("row/a");

            $params['lottery_id'] = $row['lottery_id'];
            $params['play_id'] = $row['id'];
            if($params['min_money_award']>=$params['max_money_award']) return $this->error('最低奖金不能大于最高奖金');


            $re = $this->model->save($params);
            if($re) $this->success('添加成功');
            $this->error('添加失败');

        }else{
            //查询彩票种类
            $row['lottery'] = $lottery['name'];
            $isGroupList = [
                0=>'玩法',
                1=>'玩法组'
            ];
            $this->assign('isGroupList',$isGroupList);
            $this->assign("row", $row);
            return $this->fetch();
        }

    }

    /**
     * 编辑
     */
    public function edit($ids = null)
    {
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if($params['min_money_award']>=$params['max_money_award']) return $this->error('最低奖金不能大于最高奖金');
            if ($params) {
                $params = $this->preExcludeFields($params);
                $result = false;
                Db::startTrans();
                try {
                    //是否采用模型验证
                    if ($this->modelValidate) {
                        $name = str_replace("\\model\\", "\\validate\\", get_class($this->model));
                        $validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.edit' : $name) : $this->modelValidate;
                        $row->validateFailException(true)->validate($validate);
                    }
                    $result = $row->allowField(true)->save($params);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were updated'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $this->view->assign("row", $row);
        return $this->view->fetch();
    }



    /**
     * 添加
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                $params = $this->preExcludeFields($params);

                if ($this->dataLimit && $this->dataLimitFieldAutoFill) {
                    $params[$this->dataLimitField] = $this->auth->id;
                }
                $result = false;
                Db::startTrans();
                try {
                    //是否采用模型验证
                    if ($this->modelValidate) {
                        $name = str_replace("\\model\\", "\\validate\\", get_class($this->model));
                        $validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.add' : $name) : $this->modelValidate;
                        $this->model->validateFailException(true)->validate($validate);
                    }
                    $result = $this->model->allowField(true)->save($params);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were inserted'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        return $this->view->fetch();
    }

}
