<?php

namespace app\admin\controller;

use app\common\controller\Backend;
use think\Db;

/**
 * 结算周期管理
 *
 * @icon fa fa-circle-o
 */
class Cycle extends Backend
{
    
    /**
     * Cycle模型对象
     * @var \app\admin\model\Cycle
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\Cycle;
        $this->view->assign("statusList", $this->model->getStatusList());
    }
    
    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */

    /**
     * 主管分红发放
     */
    public function zgff(){

        $id = input('id');
        $cycle =Db::name('cycle')->where('id',$id)->find();

        if(!$cycle) $this->error('该周期不存在');
        if($cycle['status'] == '0') $this->error('该周期未出账');
        if($cycle['status'] == '2') $this->error('该周期已发放');
        // 启动事务
        Db::startTrans();
        try{
            $zgbonus = Db::name('zgbonus')->where('cycle_id',$id)->field('id,user_id,money,startdate,enddate,status')->select();
            if($zgbonus){
                foreach ($zgbonus as $k=>$v){
                    if($v['status'] == '0')  throw new \think\Exception('该周期未有未审核的分红：'.$v['id'], 100006);
                    if($v['money'] > 0 && $v['status'] == '1'){
                        updateUser($v['user_id'], $v['money'], '8', $v['id'],time(), '主管分红：'.$v['startdate'].'~'.$v['enddate']);
                    }



                }
            }
            Db::name('cycle')->where('id',$id)->update([
                'status'=>'2'
            ]);
            // 提交事务
            Db::commit();
        } catch (\Exception $e) {
            // 回滚事务
            Db::rollback();
            return $this->error('发送失败:'.$e->getMessage());
        }
        return $this->success('发放成功');




    }

}
