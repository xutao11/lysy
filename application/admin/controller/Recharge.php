<?php

namespace app\admin\controller;

use app\common\controller\Backend;
use think\Db;

/**
 * 用户充值记录
 *
 * @icon fa fa-circle-o
 */
class Recharge extends Backend
{

    /**
     * Recharge模型对象
     * @var \app\admin\model\Recharge
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\Recharge;
        $this->view->assign("statusList", $this->model->getStatusList());
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $total = $this->model
                ->where($where)
                ->order($sort, $order)
                ->count();

            $list = $this->model
                ->where($where)
                ->order($sort, $order)
                ->limit($offset, $limit)
                ->select();

            $list = collection($list)->toArray();
            if ($list) {
                foreach ($list as $k => $v) {
                    $user = Db::name('user')->where('id', $v['user_id'])->field('account')->find();
                    $list[$k]['account'] = $user ? $user['account'] : '';
                }
            }
            $result = array("total" => $total, "rows" => $list);

            return json($result);
        }
        return $this->view->fetch();
    }


    /**
     * 充值品台支付 审核
     *
     */
    public function examine()
    {
//        return $this->success('确认成功');
        $ids = input('id');
        $rechrge = Db::name('recharge')->where('id', $ids)->find();
        if (!$rechrge) return $this->error('没有该订单');
        if ($rechrge['pay_pt'] != 'paypt') return $this->error('该订单不是平台支付渠道');
        if ($rechrge['status'] != '4') return $this->error('该订单不是待确认状态');


        $re = recharge_js($rechrge['ordercode'],'paypt');
        if($re['status']){
            return $this->success('确认成功');
        }else{
            return $this->error('确认失败:'.$re['msg']);
        }

    }

}
