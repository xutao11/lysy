<?php


namespace app\api\controller;

use think\Config;
use think\Db;
use think\Log;
use think\Request;

/**
 * 充值
 * Class Pay
 * @package app\api\controller
 */
class Pay extends Base
{
    public function __construct(Request $request = null)
    {
        parent::__construct($request);
    }


    /**
     * 获取充值类型
     */
    public function getPaytype()
    {

        $paytype = Db::name('chargetype')->where('status', '1')->order('weigh desc')->field('id,name,image,weigh,status')->select();
        $static_url = \config('static_url');
        if ($paytype) {
            foreach ($paytype as $k => $v) {
                $paytype[$k]['image'] = $static_url . $v['image'];
            }
        }
        return $this->success('充值类型', $paytype);
    }

    /**
     * 获取支付通道
     * @param chargetype_id 充值类型id
     */
    public function getPayList()
    {
        $param = $this->param;
        $userinfo = $this->userinfo;
        $chargetype_id = $param['chargetype_id'];
        $pay = Db::name('pay')->where('chargetype_id', $chargetype_id)->where('status', '1')->field('id,chargetype_id,name,max_money,min_money,service,pay_pt')->select();
        if ($userinfo['is_pay'] == '1' && $chargetype_id == '1') {
            //用户开通了  支付渠道
            //查询品台支付渠道
            $pypay = Db::table('lsyl_user_ptpay')->where('lsyl_user_ptpay.user_id', $userinfo['id'])->join('lsyl_paypt', 'lsyl_paypt.id = lsyl_user_ptpay.paypt_id', 'left')->where('type', '1')->where('lsyl_paypt.status', '1')->field('lsyl_paypt.*')->select();
            if ($pypay) {
                foreach ($pypay as $k => $v) {
                    $pay[] = [
                        'id' => $v['id'],
                        'chargetype_id' => $v['chargetype_id'],
                        'name' => $v['name'],
                        'max_money' => $v['max_money'],
                        'min_money' => $v['min_money'],
                        'service' => '0',
                        'pay_pt' => 'paypt',
                    ];
                }
            }

        }


        return $this->success('支付通道', $pay);
    }

    /**
     * 用户充值
     * @money  充值金额
     * @pay_pt 支付平台
     * @pay_id  支付通道id
     */
    public function recharge()
    {
        $userinfo = $this->userinfo;
        if($userinfo['status'] == '2') return $this->error('虚拟账号不能充值');
        $param = $this->param;

        //充值金额
         $money= $param['money'];
        //支付平台
        $pay_pt = $param['pay_pt'];
        //支付通道id
        $pay_id = $param['pay_id'];
        //平台支付渠道
        if ($pay_pt == 'paypt') {
            if ($userinfo['is_pay'] == '0') return $this->error('您未开通支付渠道');
            //查询用户是否有该支付渠道
            $is_bind = Db::name('user_ptpay')->where('user_id',$userinfo['id'])->where('paypt_id',$pay_id)->find();
            if(!$is_bind) return $this->error('你没有绑定该支付渠道');
            //查询当前平台支付渠道信息
            $pay = Db::name('paypt')->where('id',$pay_id)->find();
            if(!$pay) return $this->error('该平台支付不存在');
            if($pay['status'] == '0') return $this->error('该支付渠道已关闭');
            //平台支付渠道赠送 0.0018
            $donation = '0.0018';
            //手续费比例为0
            $service_redio = '0';
            $code = $pay['bank_code'];
            $status = '4';
            tz('cz_num');
        } else {
            //三方支付渠道
            $pay = Db::name('pay')->where('id',$pay_id)->find();
            if(!$pay) return $this->error('该支付渠道不存在');
            if($pay['status'] == '0') return $this->error('该支付渠道以关闭');
            //平台支付渠道赠送 0
            $donation = '0';
            //手续费比例为三方支付手续费
            $service_redio = $pay['service'];
            $code = $pay['code'];
            $status = '1';
            $pay_pt = $pay['pay_pt'];
        }

        //充值金额范围判断
        if($money>$pay['max_money'] || $money<$pay['min_money']) return $this->error('该通道的支付金额为:'.$pay['min_money'].'~'.$pay['max_money']);
        //订单号生成
        $ordercode = $this->orderCode();


        // 启动事务
        Db::startTrans();
        try{
            $order = Db::name('recharge')->insert([
                'ordercode'=>$ordercode,
                'user_id'=>$userinfo['id'],
                'status'=>$status,
                'pay_pt'=>$pay_pt,
                'code'=>$code,
                'pay_name'=>$pay['name'],
                'pay_id'=>$pay_id,
                'money'=>$money,
                'donation'=>$donation,
                'service_redio'=>$service_redio,
                'createtime'=>time(),
                'updatetime'=>time()
            ]);

            if($pay_pt == 'paypt'){
               $paypt_info = Db::name('paypt')->where('id',$pay_id)->find();
               if(!$paypt_info) throw new \think\Exception('该平台支付渠道不存在', 100006);
               $re_data = [
                   'pay_name'=>$paypt_info['name'],
                   'bank_name'=>$paypt_info['bank_name'],
                   'bank_code'=>$paypt_info['bank_code'],
                   'relaname'=>$paypt_info['relaname'],
                   'min_money'=>$paypt_info['min_money'],
                   'max_money'=>$paypt_info['max_money'],
                   'pay_url'=>''
               ];




            }else{
//                $pay_action =$pay_pt.'_pay';
//                $pay_url = $this->$pay_action($code, $userinfo['id'], $money, $ordercode);
                $pay_url = 'http://www.baidu.com';
                $paypt_info = Db::name('pay')->where('id',$pay_id)->find();
                $re_data = [
                    'pay_name'=>$paypt_info['name'],
                    'bank_name'=>$paypt_info['pay_pt'],
                    'bank_code'=>$paypt_info['code'],
                    'relaname'=>'',
                    'min_money'=>$paypt_info['min_money'],
                    'max_money'=>$paypt_info['max_money'],
                    'pay_url'=>$pay_url
                ];




            }
            // 提交事务
            Db::commit();
        } catch (\Exception $e) {
            // 回滚事务
            Db::rollback();
            return $this->error('充值提交失败:'.$e->getMessage());
        }
        return $this->success('提交成功', $re_data);
    }
    /**
     * supay  支付
     * @param $payMethod 支付方式
     * @param $userId  用户id
     * @param $money  金额
     * @param $bizNum 商户业务单号
     * @return string
     */
    public function supay_pay($payMethod, $userId, $money, $bizNum)
    {
        $payClass = new \pay\Pay();
        $money = $money * 100;
        $url = $payClass->suPay($payMethod, $userId, $money, $bizNum);
        return $url;
    }
    /**
     * fp 支付
     * @param $payMethod 支付方式
     * @param $userId  用户id
     * @param $money  金额
     * @param $bizNum 商户业务单号
     */

    public function fp_pay($payMethod, $userId, $money, $bizNum)
    {
        $payClass = new \pay\Pay();
        $re = $payClass->FpPay($payMethod, $userId, $money, $bizNum);
        $re = json_decode($re, true);
        if ($re['status'] != 1){
            //Db::table('cp_recharge')->where('ordercode',$bizNum)->delete();
//            return $this->error($re['error']);
            Log::write('fp支付(失败)日志信息:'.$re['msg'],'fp支付');
            throw new \think\Exception($re['error'], 100006);

        }
        return $re['payurl'];
    }
    /**
     * ifpay  支付
     * @param $payMethod 支付方式
     * @param $userId  用户id
     * @param $money  金额
     * @param $bizNum 商户业务单号
     */
    public function ifpay_pay($payMethod, $userId, $money, $bizNum){
        $payClass = new \pay\Pay();
        $re = $payClass->ipPay($money,$bizNum,$payMethod);

        if($re['code'] == 000){
            return $re['obj']['h5_url'].'?userId='.$re['userId'].'&id='.$re['obj']['id'];
        }else{

            Log::write('ifpay支付(失败)日志信息:'.$re['msg'],'ifpay支付');
            throw new \think\Exception($re['error'], 100006);
        }


    }
    /**
     * Dora  支付
     *  * @param $payMethod 支付方式
     * @param $userId  用户id
     * @param $money  金额
     * @param $bizNum 商户业务单号
     *
     */
    public function dora_pay($payMethod, $userId, $money, $bizNum){
        $payClass = new \pay\Pay();
        $re = $payClass->doraPay($money,$bizNum,$payMethod,$userId);
        if($re['code'] == 200){
            return $re['data']['scalper_url'];
        }else{
            Log::write('Dora支付(失败)日志信息:'.$re['msg'],'Dora_error支付');
            throw new \think\Exception($re['error'], 100006);
        }
    }
    /**
     * hyt 鸿运通
     *  * @param $payMethod 支付方式
     * @param $userId  用户id
     * @param $money  金额
     * @param $bizNum 商户业务单号
     *
     */
    public function hyt_pay($payMethod, $userId, $money, $bizNum){

        $payClass = new \pay\Pay();
        $re = $payClass->hytPay($money,$bizNum,$payMethod);
        if($re['Code'] == '0'){
            return $re['Url'];
        }else{
            Log::write('鸿运通(失败)日志信息:'.$re['MessageForSystem'],'hyt_error鸿运通');
            throw new \think\Exception($re['error'], 100006);
        }
    }




    /**
     * 生成充值订单号
     */
    protected function orderCode()
    {
        $rand = sprintf("%04d", rand(0, 99999));
        $ordercode = 'CZ' . date('YmdHis', time()) . $rand;
        $count = Db::name('recharge')->where('ordercode', $ordercode)->count();
        if ($count) {
            $this->orderCode();
        } else {
            return $ordercode;
        }
    }
    /**
     * 充值记录
     * @param limit  获取数量 默认10
     * @param page 页数 默认1
     *
     */
    public function payLog(){
        $param= $this->param;
        $userinfo = $this->userinfo;
        $limit = isset($param['limit']) ? $param['limit'] : 10;
        $page = isset($param['page']) ? $param['page'] : 1;
        $list = Db::name('recharge')->where('user_id',$userinfo['id'])->limit($limit)->page($page)->order('createtime desc')->field('pay_id,donation,service_redio',true)->select();
        $count = Db::name('recharge')->where('user_id',$userinfo['id'])->count();
        if($list) {
            foreach ($list as $k => $v) {
                $list[$k]['createtime'] = date('Y-m-d H:i:s', $v['createtime']);
                $list[$k]['updatetime'] = date('Y-m-d H:i:s', $v['updatetime']);
            }

            $data = [
                'count' => $count,
                'list' => $list
            ];
        }
            return $this->success('充值记录',$data);
    }

}