<?php

namespace app\api\controller;
use think\Db;
use think\Request;

/**
 * 登录控制器
 *
 */
class Login extends Base
{
    // 无需登录的接口,*表示全部
    protected $noNeedLogin = ['*'];

    public function __construct(Request $request = null)
    {
        parent::__construct($request);
    }



    /**
     * 登录
     * @param account 账号
     * @param password 密码
     */
    public function login(){
        $param = $this->param;
        $account = $param['account'];
        $password = $param['password'];
        if(empty($account) || empty($password)) return $this->error('账号密码不能为空');
        // 启动事务
        Db::startTrans();
        try{

            $user = Db::name('user')->where('account',$account)->where('password',md5($password))->lock(true)->find();
            if(!$user) throw new \think\Exception('账号或密码错误', 100006);
            if($user['status'] == '0') return $this->error('该账号已被锁定');
            $ip = \request()->ip();
            //更新登录最后时间 和 ip
            Db::name('user')->where('id',$user['id'])->update([
                'login_time'=>time(),
                'login_ip'=>$ip
            ]);
            //记录用户登录记录
            $login_log = Db::name('userlogin')->where('user_id',$user['id'])->select();
            if($login_log){
                //查询当前ip出现的次数
                $count = Db::name('userlogin')->where('user_id',$user['id'])->where('ip',$ip)->count();
                //计算出现的概率
                $kl = bcmul(bcdiv($count+1,count($login_log)+1,6),100,2);
                //判断IP出现的次数小于30% 为异常ip
                if($kl<30){
                    $status = '0';
                }else{
                    $status = '1';
                }
                Db::name('userlogin')->insert([
                    'user_id'=>$user['id'],
                    'status'=>$status,
                    'kl'=>$kl,
                    'ip'=>$ip,
                    'createtime'=>time()
                ]);

            }else{
                //第一次登录
                Db::name('userlogin')->insert([
                    'user_id'=>$user['id'],
                    'status'=>'1',
                    'kl'=>'100',
                    'ip'=>$ip,
                    'createtime'=>time()
                ]);
            }

            //创建token
            $token = $this->getToken($user['id']);
            // 提交事务
            Db::commit();
        } catch (\Exception $e) {
            // 回滚事务
            Db::rollback();
            return $this->error($e->getMessage());

        }
        //是否为初始化密码
        $init_pwd = 0;
        if($password == 'ls123456'){
            $init_pwd = 1;
        }
        $userinfo = Db::name('user')->where('id', $user['id'])->field('id,is_zz,account,avatar,is_dali,is_pay,ratio,money,login_ip,login_time')->find();
        $static_url = config('static_url');
        if ($userinfo['avatar']) {
            $userinfo['avatar'] = $static_url . $userinfo['avatar'];
        }

        //获取奖金池
        $userinfo['prizepool'] = prizePool($userinfo['ratio']);
        $data = [
            'token'=>$token,
            'init_pwd'=>$init_pwd,
            'userinfo' =>$userinfo
        ];
        return $this->success('登录成功',$data);

    }
    /**
     * 更新token
     * @param $user_id
     * @return mixed|string|void
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     */
    public function getToken($user_id){
        //查询该用户是否有token记录
        Db::name('user_token')->where('user_id',$user_id)->delete();
        $token = md5(rand(0,999).time().$user_id);
        //插入
        $re = Db::name('user_token')->insert([
            'token'=>$token,
            'user_id'=>$user_id,
            'createtime'=>time(),
            'expiretime'=>time()+7200
        ]);
        if(!$re) return $this->error('token写入失败');

        if(!$token) return $this->error('token写入失败');

        return $token;
    }


    /**
     * 链接开户接口
     * @param account 账号
     * @param password 密码
     * @param voucher 链接凭证
     */
    public function addLinkUser(){
        $param = $this->param;
        $voucher = $param['voucher'];
        //查询链接
        $link = Db::name('openlink')->where('voucher',$voucher)->find();
        if(!$link) return $this->error('没有查询到该识别码');
        if($link['day']!=0 && time()>$link['expirationtime']) return $this->error('该链接已过期');
        $user = Db::name('user')->where('id',$link['user_id'])->find();
        if(!$user) $this->error('查询到所属用户');
        if($user['status'] == '0') $this->error('创建账号已被锁定');
        $re = $this->addUser($user,$param['account'],$link['ratio'],$link['is_dali'],$param['password']);
        if($re) return $this->success('创建成功');
        return $this->error('创建失败');
    }








}