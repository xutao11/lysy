<?php


namespace app\api\controller;


use app\common\controller\Api;
use think\Db;
use think\Request;

class Base extends Api
{
    public function __construct(Request $request = null)
    {
        parent::__construct($request);
        if(!\request()->isPost()) return $this->error('非法请求');
    }










    /**
     * 创建用户
     */
    protected function addUser($userInfo,$account,$ratio,$is_dali,$password){
        if($userInfo['is_dali'] == '0') return $this->success('您不是代理不能开户');
        if(empty($account)) return $this->error('账号不能为空');
        if($ratio>$userInfo['ratio']) return $this->error('返点比例区间为0~'.$userInfo['ratio']);

        if(!in_array($is_dali,['1','0'])) return $this->error('账户类型错误');
        $is_cunzai = Db::name('user')->where('account',$account)->count();
        if($is_cunzai) return $this->error('该账号已存在');
        //用户层级
        $level = $userInfo['level']+1;
        //查看层级是否有固定返点比例
        $level_re = userIdentity($level);
        if($level_re['ratio'] != 0){
//            $ratio = $level_re['ratio'];
            if($level_re['ratio'] != $ratio) return $this->error('当前账号只能开返点 为：'.$level_re['ratio'].'的账号');
        }


        $re = Db::name('user')->insert([
            'username'=>$account,
            'account'=>$account,
            'is_dali'=>$is_dali,
            'password'=>md5($password),
            'level'=>$level,
            'ratio'=>$ratio,
            'user_id'=>$userInfo['id'],
            'createtime'=>time()
        ]);

        $user_all = Db::name('user')->field('id,user_id')->select();
        cache('userAll',$user_all);


        return $re;

    }



}