<?php

namespace app\api\controller;

use app\common\controller\Api;
use app\common\validate\Bank;
use think\Db;


/**
 * 会员接口
 */
class User extends Base
{
//    protected $noNeedLogin = ['login', 'mobilelogin', 'register', 'resetpwd', 'changeemail', 'changemobile', 'third'];
//    protected $noNeedRight = '*';

    public function _initialize()
    {
        parent::_initialize();
    }

    /**
     * 获取用户信息
     */
    public function userInfo()
    {
        $userinfo = $this->userinfo;
        $user = Db::name('user')->where('id', $userinfo['id'])->field('id,is_zz,username,account,avatar,is_dali,is_pay,ratio,money,login_ip,login_time')->find();
        $static_url = config('static_url');
        if ($user['avatar']) {
            $user['avatar'] = $static_url . $user['avatar'];
        }

        //获取奖金池
        $user['prizepool'] = prizePool($user['ratio']);
        return $this->success('用户信息', $user);
    }

    /**
     * 修改密码
     * @param password 原始密码
     * @param new_password 新密码
     */
    public function editPwd()
    {
        $userinfo = $this->userinfo;
        $param = $this->param;
        $password = $param['password'];
        $new_password = $param['new_password'];
        if (md5($password) != $userinfo['password']) return $this->error('密码错误');
        if (strlen($new_password) < 6) return $this->error('新密码的至少为6为');
        if ($new_password == 'ls123456') return $this->error('新密码不能为初始密码');
        //更新密码
        $re = Db::name('user')->where('id', $userinfo['id'])->update([
            'password' => md5($new_password)
        ]);
        if ($re === false) return $this->error('修改失败');
        return $this->success('密码修改成功');

    }

    /**
     * 修改支付密码
     * @param pay_pwd 原支付密码
     * @param new_pay_pwd  新支付密码
     */
    public function editPaypwd()
    {
        $userinfo = $this->userinfo;
        $param = $this->param;

        $new_pay_pwd = $param['new_pay_pwd'];
        if ($userinfo['pay_pwd'] != '') {
            $pay_pwd = $param['pay_pwd'];
            if ($pay_pwd != $userinfo['pay_pwd']) return $this->error('支付密码错误');
        }

        if (!is_numeric($new_pay_pwd) || strlen($new_pay_pwd) != 6) return $this->error('支付密码为6位的数字');
        $re = Db::name('user')->where('id', $userinfo['id'])->update([
            'pay_pwd' => $new_pay_pwd
        ]);
        if ($re === false) return $this->error('修改失败');
        return $this->success('修改成功');
    }


    /**
     * 开户
     * @param account 账号
     * @param ratio 返点比例
     * @param is_dali 账户类型:0=用户,1=代理
     *
     */
    public function openUser()
    {
        $userInfo = $this->userinfo;
        $param = $this->param;
        $ratio = $param['ratio'];
        $is_dali = $param['is_dali'];

        $account = trim($param['account']);
        $password = 'ls123456';
        $re = $this->addUser($userInfo, $account, $ratio, $is_dali, $password);
        if($re) return $this->success('创建成功',[
            'account'=>$account,
            'pwd'=>$password,
            'is_dali'=>$is_dali,
            'jgc'=> prizePool($ratio),
            'url'=>'http://103.149.27.149:82'
        ]);
        return $this->error('创建失败');


    }

    /**
     * 获取最大返点点比例
     */
    public function getratio(){
        $userinfo = $this->userinfo;
        $level = $userinfo['level']+1;
        $re = userIdentity($level);
        $data = [
            'max_ratio'=>$userinfo['ratio'],
            'gd_ratio'=>'0'
        ];
        if($re['ratio']){
            $data['gd_ratio'] = (string)$re['ratio'];
        }
        return $this->success('最大返点',$data);
    }


    /**
     * 创建开户链接
     * @param ratio 返点比例
     * @param is_dali 账户类型:0=用户,1=代理
     * @param day 有效时间(天,0=永久有效)
     */
    public function addOpenLink()
    {
        $userInfo = $this->userinfo;
        if ($userInfo['is_dali'] == '0') return $this->success('您不是代理不能开户');
        $param = $this->param;
        //用户层级
        $level = $userInfo['level'] + 1;
        //查看层级是否有固定返点比例
        $level_re = userIdentity($level);
        if ($level_re['ratio'] != 0) {
            $ratio = $level_re['ratio'];
        }
        if ($ratio > $userInfo['ratio']) return $this->error('返点比例区间为0~' . $userInfo['ratio']);
        $is_dali = $param['is_dali'];
        if (!in_array($is_dali, ['1', '0'])) return $this->error('账户类型错误');
        $day = $param['day'];

        //过期时间
        $expirationtime = 0;
        if ($day) {
            $expirationtime = strtotime("+{$day} day");
        }
        $re = Db::name('openlink')->insert([
            'user_id' => $userInfo['id'],
            'ratio' => $ratio,
            'is_dali' => $is_dali,
            'voucher' => uniqid(),
            'day' => $day,
            'createtime' => time(),
            'expirationtime' => $expirationtime
        ]);
        if ($re) return $this->success('创建成功');
        return $this->error('创建失败');

    }





    /**
     * 开户链接列表获取
     * @param limit  获取数量 默认10
     * @param page 页数 默认1
     */
    public function getOpenLink()
    {
        $userinfo = $this->userinfo;
        $param = $this->param;
        $limit = isset($param['limit']) ? $param['limit'] : 10;
        $page = isset($param['page']) ? $param['page'] : 1;
        $linkList = Db::name('openlink')->where('user_id', $userinfo['id'])->limit($limit)->page($page)->select();
        $count = Db::name('openlink')->where('user_id', $userinfo['id'])->count();
        $index_pc_url = config('index_url');
        if ($linkList) {
            foreach ($linkList as $k => $v) {
                $linkList[$k]['url'] = $index_pc_url . '/#/logon?voucher=' . $v['voucher'];
                $linkList[$k]['createtime'] = date('Y-m-d H:i:s', $v['createtime']);
                if ($v['day'] != 0) {
                    $linkList[$k]['expirationtime'] = date('Y-m-d H:i:s', $v['expirationtime']);

                }
                $linkList[$k]['expirationtime'] = (string)$linkList[$k]['expirationtime'];
                $linkList[$k]['qrcode'] = request()->domain() . '/api/common/createQrcode?url=' . $linkList[$k]['url'];
            }
        }
        $data = [
            'linkLink' => $linkList,
            'count' => $count
        ];
        return $this->success('开户链接列表', $data);
    }

    /**
     * 删除开户链接
     * @param link_id 链接id
     */
    public function delOpenlink()
    {
        $userInfo = $this->userinfo;
        $param = $this->param;
        $re = Db::name('openlink')->where('id', $param['link_id'])->where('user_id', $userInfo['id'])->delete();
        if ($re) return $this->success('删除成功');
        $this->error('删除失败');
    }

    /**
     * 添加银行卡
     * @param banktype_id 银行卡类型id
     * @param realname 真实姓名
     * @param bank_code 银行卡号
     * @param branch_name 支行名称
     *
     */
    public function addBank()
    {
        $userinfo = $this->userinfo;
        $param = $this->param;
        $data = [
            'banktype_id' => $param['banktype_id'],
            'realname' => $param['realname'],
            'bank_code' => $param['bank_code'],
            'branch_name' => $param['branch_name'],
            'user_id' => $userinfo['id']
        ];
        //c查询用户是否已有绑定银行卡
        $banklist = Db::name('bank')->where('user_id',$userinfo['id'])->find();
        if($banklist){
            if($data['realname'] != $banklist['realname']) return $this->error('真实姓名不一致');
        }
        $bankValidate = new Bank();
        if (!$bankValidate->check($data)) {
            return $this->error($bankValidate->getError());
        }
        $bankModel = new \app\admin\model\Bank();
        $re = $bankModel->save($data);
        if($re) return $this->success('添加成功');
        return $this->error('添加失败');
    }


    /**
     * 个人报表
     * @param $start_date 开始时间 2020-03-03 00:00:00
     * @param $end_date 终止时间 2020-08-01 23:59:59
     */
    public function user_report(){
        $param = $this->param;
        $userinfo = $this->userinfo;
        $start_date = $param['start_date'];
        $end_date = $param['end_date'];
        $user_ids[] = $userinfo['id'];
        //报表数据
        $data = report($user_ids,$start_date,$end_date);
        //余额
        $data['userYe'] = $userinfo['money'];
        return $this->success('个人报表',$data);
    }


    /**
     * 个人帐变记录
//     * @param ordercode  订单号
     * @param start_date    起始时间 2020-03-01 00:00:00
     * @param end_date 2020-03-01 23:59:59
     * @param  type 类型:1=充值,2=提现,3=彩票下注,4=活动,5=转账,6=资金修正,7=返点,8=团队分红,9=浮动工资,10=彩票奖金,11=撤单 0= 全部
     * @param limit  获取数量 默认10
     * @param page 页数 默认1
     */
    public function userMoneyLog(){
        $param = $this->param;
//        $ordercode = isset($param['ordercode'])?$param['ordercode']:"";
        $start_date = isset($param['start_date'])?$param['start_date']:'';
        $end_date = isset($param['end_date'])?$param['end_date']:'';
        $limit = isset($param['limit'])?$param['limit']:'10';
        $page = isset($param['page'])?$param['page']:'1';
        $type = $param['type'];
        $userinfo = $this->userinfo;
        $data = [
            'count'=>0,
            'list'=>[]
        ];
        if($start_date && $end_date){
            $where = [];
            $where['user_id'] = $userinfo['id'];
            if($type){
                $where['type']=$type;
            }
            $start_date = strtotime($start_date);
            $end_date = strtotime($end_date);

            $list = Db::name('moneylog')->where($where)->where('createtime','>=',$start_date)->where('createtime','<=',$end_date)->limit($limit)->page($page)->order('createtime desc')->select();
            $data['count'] = Db::name('moneylog')->where($where)->where('createtime','>=',$start_date)->where('createtime','<=',$end_date)->count();
            if($list){
                foreach ($list as $k=>$v){
                    $list[$k]['createtime'] = date('Y-m-d H:i:s',$v['createtime']);
                }
            }
            $data['list'] = $list;
        }

        return $this->success('个人帐变记录',$data);





    }


    /**
     * 用户修改头像
     * @paeam avatar 头像
     */
    public function editAvatar(){
        $param = $this->param;
        $userinfo = $this->userinfo;
        $avatar = $param['avatar'];
        if(empty($avatar)) return $this->error('头像不能为空');
        $re = Db::name('user')->where('id',$userinfo['id'])->update([
            'avatar'=>$avatar
        ]);
        if($re) return $this->success('修改成功');
        return $this->success('修改失败');

    }


    /**
     * 转账记录
     * @param limit 获取的条数默认10
     * @param page 获取页数 默认1
     * @param start_date  开始时间 2020-03-03 00:00:00
     * @param end_date 终止时间 2020-08-01 23:59:59
     *
     */
    public function recordLog(){
        $param = $this->param;
        $userinfo = $this->userinfo;
        $limit = isset($param['limit'])?$param['limit']:'10';
        $page = isset($param['page'])?$param['page']:'1';
        $start_date = isset($param['start_date'])?$param['start_date']:'0';
        $end_date = isset($param['end_date'])?$param['end_date']:'2120-01-01';
        $start_date = strtotime($start_date);
        $end_date = strtotime($end_date);
        $where= [];
        $where['lsyl_moneylog.user_id']=$userinfo['id'];
        $where['lsyl_moneylog.type']='5';
       $list = Db::table('lsyl_moneylog')->where($where)->join('lsyl_user','lsyl_user.id = lsyl_moneylog.user_id','left')->where('lsyl_moneylog.createtime','>=',$start_date)->where('lsyl_moneylog.createtime','<=',$end_date)->limit($limit)->page($page)->order('lsyl_moneylog.createtime desc')->field('lsyl_moneylog.*,lsyl_user.account')->select();
       $count =  Db::table('lsyl_moneylog')->where($where)->where('createtime','>=',$start_date)->where('createtime','<=',$end_date)->count();
       if($list){
           foreach ($list as $k=>$v){

               $list[$k]['createtime']=date('Y-m-d H:i:s',$v['createtime']);
           }
       }
       $data = [
           'count'=>$count,
           'list'=>$list
       ];
       return $this->success('转账记录',$data);


    }



    /**
     * 我的分红
     */
    public function bounts(){
        $userinfo = $this->userinfo;
//        if($userinfo['level'] != 5){
//            $data = '';
//        }else{
//        dump($userinfo);
            $data = Db::name('zgbonus')->where('user_id',$userinfo['id'])
//                ->whereTime('createtime','today')
                ->order('createtime desc')
                ->field('cycle_id,user_id',true)->find();
            if($data){
                $data['createtime'] = date('Y-m-d H:i:s',$data['createtime']);
                $data['yk_money'] = teamYk($data['jg_money'],$data['fd_money'],$data['xz_money'],$data['hd_money']);
            }

//        }
        return $this->success('我的分红',$data);
    }
    /**
     * 历史分红
     * @param limit 获取条数 默认10
     * @param page  获取页数  默认1
     */
    public function bountsLog(){
        $param = $this->param;
        $userinfo = $this->userinfo;
        $limit = isset($param['limit'])?$param['limit']:10;
        $page = isset($page['page'])?$param['page']:1;
        $data = Db::name('zgbonus')->where('user_id',$userinfo['id'])->order('createtime desc')->limit($limit)->page($page)->field('cycle_id,user_id',true)->select();
        $count = Db::name('zgbonus')->where('user_id',$userinfo['id'])->count();
        if($data){
            foreach ($data as $k=>$v){
                $data[$k]['createtime'] = date('Y-m-d H:i:s',$v['createtime']);
            }


        }
        $re = [
            'count'=>$count,
            'list'=>$data
        ];

        return $this->success('历史分红',$re);

    }


    /**
     * 常用彩种
     * @param num 获取数量
     *
     */
    public function cyLottery(){
        $userinfo = $this->userinfo;
        $param = $this->param;
        $num = $param['num'];
        $lotterys = Db::name('lottery')->where('status','1')->field('id,image,name')->select();
        $static_url = config('static_url');
        if($lotterys){
            foreach ($lotterys as $k=>$v){
                $count =Db::name('betting')->where('lottery_id',$v['id'])->where('user_id',$userinfo['id'])->count();
                $lotterys[$k]['count'] = $count;
                if($v['image']){
                    $lotterys[$k]['image'] = $static_url.$v['image'];
                }


            }
        }
        $cmf_arr = array_column($lotterys, 'count');
          array_multisort($cmf_arr, SORT_DESC, $lotterys);
          $data = array_slice($lotterys,0,$num);



        return $this->success('常用彩种',$data);


    }

    /**
     * 修改昵称
     * @param username 用户昵称
     */
    public function editUsername(){
        $userinfo = $this->userinfo;
        $param = $this->param;
        $useranme = $param['username'];
        if(empty($useranme))  return $this->error('昵称不能为空');
        if($useranme == $userinfo['username']) return $this->error('昵称和原昵称一致');
        if(count($useranme)>20) return $this->error('昵称长度最长为20');
        $re = Db::name('user')->where('id',$userinfo['id'])->update([
           'username'=>$useranme
        ]);
        if($re) return $this->success('修改成功');
        return $this->error('修改失败');



    }

    /**
     * 获取平台通知列表
     */
    public function letterList(){
        $userinfo = $this->userinfo;
        $param = $this->param;
        $limit = isset($param['limit'])?$param['limit']:10;
        $page = isset($page['page'])?$param['page']:1;
        $list = Db::name('letter')->where('user_id',$userinfo['id'])->limit($limit)->page($page)->field('id,content,status,createtime')->order('createtime desc')->select();
        if($list){
            foreach ($list as $k=>$v){
                $list[$k]['createtime'] = date('Y-m-d H:i:s',$v['createtime']);
            }
        }
        $count = Db::name('letter')->where('user_id',$userinfo['id'])->count();
        $data =[
            'data'=>$list,
            'count'=>$count
        ];
        return $this->success('通知列表',$data);
    }
    /**
     * 平台消息已读
     */
    public function lookLetter(){
        $userinfo = $this->userinfo;
        $param = $this->param;
        $id = $param['id'];
        $re = Db::name('letter')->where('user_id',$userinfo['id'])->where('id',$id)->update([
            'status'=>'1',
            'updatetime'=>time()
        ]);
        if($re) return $this->success('ok');
        return $this->error('消息状态修改失败');
    }



}
