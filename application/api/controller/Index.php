<?php

namespace app\api\controller;

use app\common\controller\Api;
use think\Cache;

/**
 * 首页接口
 */
class Index extends Api
{
    protected $noNeedLogin = ['*'];
    protected $noNeedRight = ['*'];

    /**
     * 首页
     *
     */
    public function index()
    {
        $this->success('请求成功');
    }
    /**
     * 推送 cw_num  tx_num
     */
    public function admintongzi()
    {
        $type = input('type');
        Cache::inc($type, 1);
    }
}
