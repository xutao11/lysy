<?php

namespace app\api\controller;

use think\Db;
use think\Exception;
use think\Request;


/**
 * 彩票
 * Class Letter
 * @package app\api\controller
 */
class Lottery extends  Base
{
    protected $noNeedLogin = ['getLongDragon','kjLog','multipleNumbers','getKjyl','zst'];
    public function __construct(Request $request = null)
    {
        parent::__construct($request);
    }
    /**
     * 获取彩票和分类app
     */

    public function getlottery(){
        $lotterytype = Db::name('lotterytype')->order('weigh descc')->field('id,image,name,weigh')->select();
        $static_url = config('static_url');
        if($lotterytype){
            foreach ($lotterytype as $k=>$v){
                $lotterytype[$k]['image'] = $static_url.$v['image'];
                $lottery = Db::name('lottery')->where('lotterytype_id',$v['id'])->where('status','1')->field('id,image,pcimage,name,lotterytype_id')->select();
                if($lottery){
                    foreach ($lottery as $k1=>$v1){
                        $lottery[$k1]['image'] = $v1['image']?$static_url.$v1['image']:'';
                        $lottery[$k1]['pcimage'] = $v1['pcimage']?$static_url.$v1['pcimage']:'';
                    }

                }
                $lotterytype[$k]['lottery'] = $lottery;
            }
        }
        return $this->success('app彩票分类和列表',$lotterytype);
    }

    /**
     * 获取彩票种类
     */
    public function getLotteryType(){
        $lotterytype = Db::name('lotterytype')->order('weigh descc')->field('id,image,name,weigh')->select();
        $static_url = config('static_url');
        if($lotterytype){
            foreach ($lotterytype as $k=>$v){
                $lotterytype[$k]['image'] = $static_url.$v['image'];
            }
        }
        return $this->success('彩票种类',$lotterytype);
    }
    /**
     * 获取彩票列表
     * @param lotterytype_id 彩票种类id
     */
    public function getLotteryList(){
        $param = $this->param;
        $lotterytype_id = $param['lotterytype_id'];
        $static_url = config('static_url');

        if($lotterytype_id == 0){
            $lotterytype = Db::name('lotterytype')->order('weigh descc')->field('id,image,name,weigh')->select();
            if($lotterytype){
                foreach ($lotterytype as $k=>$v){
                    $lotterytype[$k]['image'] = $static_url.$v['image'];
                    $lotteryList = Db::name('lottery')->where('lotterytype_id',$v['id'])->where('status','1')->field('id,image,pcimage,name,lotterytype_id')->select();
                    if($lotteryList){
                        foreach ($lotteryList as $k=>$v){
                            $lotteryList[$k]['image'] = $v['image']?$static_url.$v['image']:'';
                            $lotteryList[$k]['pcimage'] = $v['pcimage']?$static_url.$v['pcimage']:'';
                        }
                    }
                    $lotterytype[$k]['lottery'] = $lotteryList;

                }
            }
            return $this->success('彩票列表pc',$lotterytype);

        }else{
            $lotteryList = Db::name('lottery')->where('lotterytype_id',$lotterytype_id)->where('status','1')->field('id,image,pcimage,name,lotterytype_id')->select();

            if($lotteryList){
                foreach ($lotteryList as $k=>$v){
                    $lotteryList[$k]['image'] = $static_url.$v['image'];
                }
            }
            return $this->success('彩票列表',$lotteryList);
        }







    }


    /**
     * 获取彩票玩法
     * @param lottery_id 彩票id
     * @api_type  前端类别（app端不传）pc端传(pc)
     */
    public function getPlayList(){
        $param = $this->param;
        $lottery_id = $param['lottery_id'];
        //查询 彩票；
        $lottery = Db::name('lottery')->where('id',$lottery_id)->field('id,name,status')->find();
        if(!$lottery) return $this->error('没查询到该彩票');
        $api_type = $param['api_type'];
        if($api_type == 'pc'){

            $palys = getPlayPC($lottery_id);
        }else{
            $palys = getPlayAPP($lottery_id);

        }
        return $this->success($lottery['name'].'玩法',$palys);
    }


    /**
     * 获取计算注数
     * $lottery_id  彩种id
     * $play_id 玩法id
     * 下注参数
     */
    public function getBet(){
        $param = $this->param;
        $userinfo = $this->userinfo;
        try {
            $lottery = new \lottery\Lottery();
            $re = $lottery->getBet($param['lottery_id'],$param,$param['play_id'],$userinfo['ratio']);
            return $this->success('计算注数',$re);
        }catch (Exception $e){

            return $this->error($e->getMessage());
        }

    }
    /**
     * 获取彩票下下期开奖时间
     * $lottery_id  彩种id
     */
    public function gettime(){
        $param = $this->param;
        $lottery_id = $param['lottery_id'];
        try {
            $lottery = new \lottery\Lottery();
            $re = $lottery->getKjtime($lottery_id);
            return $this->success('下期开奖时间',$re);
        }catch (Exception $e){

            return $this->error($e->getMessage());
        }
    }


    /**
     * 彩票下注
     * @param lottery_id  彩种id
     * @param play_id  玩法id
     * @param play_id  玩法id
     * @param param 所选号码
     * @param multiple 倍数
     * @param unit 单位 元 角 分 厘
     * @param hangplan_id 云挂机方案id
     * @param bt_num 直线倍投局数
     * @param gjbt_num 高级倍投局数
     * @param data_js 自动投注号码局数
     */
    public function addOrder()
    {
        $userinfo = $this->userinfo;
        $param = $this->param;

        $data = [
            'user_id'=>$userinfo['id'],
            'lottery_id' => $param['lottery_id'],
            'play_id' => $param['play_id'],
            'param' => [],
            'multiple' => (int)$param['multiple'],
            'unit' => $param['unit'],
//            'hangplan_id'=>isset($param['hangplan_id'])?$param['hangplan_id']:'0',
//            'bt_num'=>isset($param['bt_num'])?$param['bt_num']:'0',
//            'gjbt_num'=>isset($param['gjbt_num'])?$param['gjbt_num']:'0',
//            'data_js'=>isset($param['data_js'])?$param['data_js']:'0',
            'createtime'=>time(),
            'updatetime'=>time()
        ];

        //查询彩种
        $lottery = Db::table('lsyl_play')->where('lsyl_play.id', $data['play_id'])->join('lsyl_lottery', 'lsyl_lottery.id = lsyl_play.lottery_id', 'left')->field('lsyl_play.*,lsyl_lottery.status lottery_status,lsyl_lottery.name lottery_name')->find();
        if ($lottery['lottery_status'] == '0') return $this->error('该彩票已禁止');
        if ($lottery['status'] == '0') return $this->error('该玩法已禁止');

        //解析参数
        $paranjson = json_decode($lottery['paramjson'], true);
        if ($paranjson) {
            foreach ($paranjson as $k => $v) {
                if (isset($param['param'][$k])) {
                    $data['param'][$k] = $param['param'][$k];
                }
            }
        }

        // 启动事务
        Db::startTrans();
        try{
            //获取注数
            $lotteryClass = new \lottery\Lottery();

            $bet = $lotteryClass->getBet($data['lottery_id'], $data['param'], $data['play_id'], $userinfo['ratio']);
            //注数
            $data['xz_num'] = $bet['count'];
            if ($data['xz_num'] < 1) throw new \think\Exception('下注数为0', 100006);

            if ($data['multiple'] < 1) throw new \think\Exception('下注倍数为0', 100006);
            //计算奖金
            $jg = jsMoney($bet['money_award'], $data['unit'], $data['multiple'], $data['xz_num'], $bet['money']);
            $data['jg_money'] = $jg['jg_money'];
            //计算下注
            $data['xz_money'] = $jg['xz_money'];
            //获取下注期数
            $kjtime = $lotteryClass->getKjtime($data['lottery_id']);
            $data['pre_draw_issue'] = $kjtime['qishu'];
            $data['kj_time'] = $kjtime['kj_time'];

            //该期数已出奖
            $pre_draw_issue = $kjtime['qishu'];
            $kj_log = Db::name('kjlog')->where('lottery_id',$data['lottery_id'])->where('pre_draw_issue',$pre_draw_issue)->find();
            if($kj_log) throw new \think\Exception('暂时不能下注', 100006);

            $data['ordercode'] = $this->orderCode();
            //判断用户是否禁止开奖
            $data['is_jz'] = $userinfo['is_jz'];
            //查询改订单是否是开将时间 开始2秒  最后2秒  内的订单  投注金额最大的以单
            $re_max = $this->is_jz_order($data['xz_money'],$data['lottery_id'],$kjtime);
            if($re_max['is_max'] >= 1){
                $data['is_jz'] = 0;
            }
            $data['is_max']=$re_max['is_max'];
            //json化号码
            $data['param'] = json_encode($data['param']);

            //更新用户余额 f返回下注后的余额
           updateUser($data['user_id'],'-'.$data['xz_money'],'3',$data['ordercode'],$data['createtime'],$bet['lottery'].'('.$bet['play_name'].')下注');
            //状态
            $data['status'] = '1';
            Db::name('betting')->insert($data);
            // 提交事务
            Db::commit();
        } catch (\Exception $e) {
            // 回滚事务
            Db::rollback();
            return $this->error('下注失败', $e->getMessage());
        }
        return $this->success('下注成功');
    }


    /**
     * 判断当前订单  是否是倒计时前2秒
     */
    public function is_jz_order($xz_money,$lottery_id,$kj_time){
        $is_max = 0;
        $is_jz = 1;
        //当下注金额小于1000 时不禁止
        if($xz_money < 1000){
            return [
                'is_jz'=>$is_jz,
                'is_max'=>$is_max
            ];
        }

        //  奇趣1  河内1
        if(in_array($lottery_id,['3','6'])){

            $bettingModel = Db::name('betting')->where('lottery_id',$lottery_id);
            //开奖时间
            $kj = strtotime($kj_time['kj_time']);
            //当前时间
            $service_time = strtotime($kj_time['service_time']);
            //开将时间 后2秒   内的订单
            if(bcsub($kj,$service_time) <=2)  {
                $is_jz = 0;
                //查询
                $max_betting = $bettingModel->where('createtime','>=',$kj-2)->where('createtime','<=',$kj)->lock(true)->max('xz_money');
                if(!$max_betting){
                    $is_max = 2;
                }else{
                    if($xz_money > $max_betting){
                        $bettingModel->where('createtime','>=',$kj-2)->where('createtime','<=',$kj)->update([
                            'is_max'=>'0'
                        ]);
                        $is_max = 2;
                    }
                }
            }elseif (bcsub($kj,$service_time) >=58){
                $is_jz = 0;
//                前2秒
                //查询
                $max_betting = $bettingModel->where('createtime','>=',$kj-60)->where('createtime','<=',$kj-58)->lock(true)->max('xz_money');
                if(!$max_betting){
                    $is_max = 1;
                }else{
                    if($xz_money > $max_betting){
                        $bettingModel->where('createtime','>=',$kj-60)->where('createtime','<=',$kj-58)->update([
                            'is_max'=>'0'
                        ]);
                        $is_max = 1;
                    }
                }
            }
        }
        return [
            'is_jz'=>$is_jz,
            'is_max'=>$is_max
        ];
    }


    /**
     * 用户撤单
     * @param betting_id  下注订单id
     */
    public function cancel_order(){
        $param = $this->param;
        $userinfo = $this->userinfo;
        $betting_id = $param['betting_id'];
        //查询订单
        $betting = Db::name('betting')->where('user_id',$userinfo['id'])->where('id',$betting_id)->field('id,user_id,lottery_id,hangplan_id,xz_money,status,kj_time,ordercode')->find();
        if(!$betting) return $this->error('订单不存在');
        $status = [
            1=>'待开奖',2=>'中奖',3=>'未中奖',4=>'结算错误',5=>'已撤销'
        ];
        if($betting['status'] != '1') return $this->error('该订单'.$status[$betting['status']].'不能撤单');
        if($betting['hangplan_id'] != '0') return $this->error('不能撤单');
        //获取当前服务器时间
        $lotteryClass = new \lottery\Lottery();
        $kj_time = $lotteryClass->getKjtime($betting['lottery_id']);
        $kj_time_int = strtotime($betting['kj_time']);
//        dump($kj_time_int);
        $server_time_int =  strtotime($kj_time['service_time']);
//        dump($server_time_int-$kj_time_int);
        if($server_time_int-$kj_time_int >=-5){
            return $this->error('该订单不能撤销');
        }
       // 启动事务
        Db::startTrans();
        try{
            //跟新订单状态
            Db::name('betting')->where('id',$betting_id)->update([
                'status'=>'5',
                'updatetime'=>time()
            ]);
            //查询彩种
            $lottery = Db::name('lottery')->where('id',$betting['lottery_id'])->field('id,name')->find();
            //更新用户的余额
            updateUser($betting['user_id'],$betting['xz_money'],'11',$betting['ordercode'],time(),$lottery['name'].'(撤单)');

            // 提交事务
            Db::commit();
        } catch (\Exception $e) {
            // 回滚事务
            Db::rollback();
            return $this->error('撤单失败:'.$e->getMessage());
        }

        return $this->success('撤单成功');


    }


    /**
     * 个人投注记录
     * @param limit  获取条数 默认10
     * @param page  获取页数 默认1
     * @param status 状态:1=待开奖,2=中奖,3=未中奖,4=结算错误,5=已撤销 0=全部
     * @param start_date  开始时间 2020-03-03 00:00:00
     * @param end_date 终止时间 2020-08-01 23:59:59
     * @param ordercode  订单号
     * @param lottery_id  彩种   0=全部
     */
    public function bettingLog(){
        $param = $this->param;
        $userinfo = $this->userinfo;
        $limit = isset($param['limit']) ? $param['limit'] : 10;
        $page = isset($param['page']) ? $param['page'] : 1;
        $status = isset($param['status']) ? $param['status'] : '0';
        $start_date = isset($param['start_date'])?$param['start_date']:'0';
        $end_date = isset($param['end_date'])?$param['end_date']:'2120-01-01';
        $lottery_id = isset($param['lottery_id'])?$param['lottery_id']:'0';
        $ordercode = isset($param['ordercode'])?$param['ordercode']:'';
        $data = [
            'count'=>0,
            'list'=>[]
        ];
        $where = [];
        $where['lsyl_betting.user_id']=$userinfo['id'];
        if($status){
            $where['lsyl_betting.status']=$status;
        }
        if($lottery_id){
            $where['lsyl_betting.lottery_id']=$lottery_id;
        }
        if($ordercode){
            $where['lsyl_betting.ordercode'] = $ordercode;
        }
//        if(!empty($start_date) && !empty($end_date)){
            $start_date = strtotime($start_date);
            $end_date = strtotime($end_date);
            $list = Db::table('lsyl_betting')->where($where)->where('lsyl_betting.createtime','>=',$start_date)->where('lsyl_betting.createtime',"<=",$end_date)->join('lsyl_lottery','lsyl_lottery.id = lsyl_betting.lottery_id','left')->limit($limit)->page($page)->order('lsyl_betting.createtime desc')->field('lsyl_betting.id,lsyl_betting.ordercode,lsyl_betting.user_id,lsyl_betting.pre_draw_issue,lsyl_betting.param,lsyl_betting.lottery_id,lsyl_betting.play_id,lsyl_betting.jg_money,lsyl_betting.jg_num,lsyl_betting.xz_money,lsyl_betting.xz_num,lsyl_betting.multiple,lsyl_betting.unit,lsyl_betting.kj_time,lsyl_betting.status,lsyl_betting.createtime,lsyl_lottery.name lottery_name')->select();
//          dump( Db::table('lsyl_betting')->getLastSql());
            $count =  Db::table('lsyl_betting')->where($where)->where('lsyl_betting.createtime','>=',$start_date)->where('lsyl_betting.createtime',"<=",$end_date)->count();
            if($list){
                foreach ($list as $k=>$v){

                    $list[$k]['jg'] = bcmul($v['jg_money'],$v['jg_num'],3);
                    //玩法
                    $play = getPlay($v['play_id']);
                    $list[$k]['play'] = implode("/", array_reverse($play));

                    $list[$k]['createtime'] = date('Y-m-d H:i:s',$v['createtime']);

                    //参数解析
                    $param = json_decode($v['param'],true);
                    $list[$k]['param'] = $param;
                    $list[$k]['paramParsing'] = paramParsing($param);
                    //查询开奖号码
                    $kj = Db::name('kjlog')->where('lottery_id',$v['lottery_id'])->where('pre_draw_issue',$v['pre_draw_issue'])->field('pre_draw_code')->find();
                        $list[$k]['pre_draw_code']= $kj?$kj['pre_draw_code']:'';


                }
            }



            $data['count'] = $count;
            $data['list']=$list;


//        }
        return $this->success('个人投注记录',$data);




    }
    /**
     * 获取长龙
     * @param lottery_id 彩种id
     * @param limit 获取条数默认20
     * @param page 获取页数 默认1
     */
    public function getLongDragon(){
        $param = $this->param;
        $lottery_id = $param['lottery_id'];
        $limit = isset($param['limit'])?$param['limit']:20;
        $page = isset($page['page'])?$param['page']:1;

        $count = Db::name('longdragon')->where('lottery_id',$lottery_id)->count();
        $list = Db::name('longdragon')->where('lottery_id',$lottery_id)->order('missing_value desc')->limit($limit)->page($page)->field('number,missing_value,address')->select();
        $data = [
            'count'=>$count,
            'list'=>$list
        ];
        return $this->success('长龙',$data);
    }








        /**
         * 生成充值订单号
         */
        protected function orderCode()
        {
            $rand = sprintf("%05d", rand(0, 99999));
            $ordercode = 'QR' . date('YmdHis', time()) . $rand;
            $count = Db::table('lsyl_betting')->where('ordercode', $ordercode)->count();
            if ($count) {
                $this->orderCode();
            } else {
                return $ordercode;
            }
        }



        /**
         * 开奖记录获取
         * @param lottery_id  彩种id
         * @param limit 获取条数默认10
         * @param page 获取页数 默认1
         */
        public function kjLog(){
            $param = $this->param;
            $lottery_id = $param['lottery_id'];
            $limit = isset($param['limit'])?$param['limit']:20;
            $page = isset($page['page'])?$param['page']:1;

            $list = Db::name('kjlog')->where('lottery_id',$lottery_id)->order('pre_draw_issue desc')->limit($limit)->page($page)->field('pre_draw_issue,pre_draw_code')->select();
            $count = Db::name('kjlog')->where('lottery_id',$lottery_id)->count();
            if($list){
                foreach ($list as $k=>$v){
                    if(in_array($lottery_id,[13,14])){
//                        快三只有3位
                        $list[$k]['play'] = '';
                    }else{
                        $list[$k]['play'] = kj_data_play($v['pre_draw_code']);
                    }


                }
            }
            $data = [
                'list'=>$list,
                'count'=>$count
            ];
            return $this->success('开奖记录',$data);




        }
    /**
     * 多号走势
     * @limmit  获取条数
     * @address (0=>五星，1=>后四，2=>前四，3=后三，4=前三，5=后二，6=前二)
     */
    public function multipleNumbers()
    {
        $param = $this->param;
        $lottery_id = $param['lottery_id'];
        $limit = isset($param['limit'])?$param['limit']:50;

        $kj_data = Db::table('lsyl_kjlog')->where('lottery_id', $lottery_id)->order('pre_draw_issue desc')->limit($limit)->select();
        //倒叙
//        $kj_data = array_reverse($kj_data);
            if (!isset($param['address'])) return $this->error('缺少参数');

        $address = $param['address'];
        if (!in_array($address, [0, 2, 3, 4, 1, 5, 6])) return $this->error('参数错误参数');
        $re_data = [];
        if ($kj_data) {
            foreach ($kj_data as $k => $v) {
                $code = explode(',', $v['pre_draw_code']);


                switch ($address) {
                    case 0:
                        $num = array_slice($code, 0, 5);
                        break;
                    case 1:
                        $num = array_slice($code, 1, 4);
                        break;
                    case 2:
                        $num = array_slice($code, 0, 4);
                        break;
                    case 3:
                        $num = array_slice($code, 2, 3);
                        break;
                    case 4:
                        $num = array_slice($code, 0, 3);
                        break;
                    case 5:
                        $num = array_slice($code, 3, 2);
                        break;
                    case 6:
                        $num = array_slice($code, 0, 2);
                        break;
                }
                $new_num = [];
                foreach ($num as $k1=>$v1){
                    $new_num[] = (string)(int)$v1;
                }
                $re_data[] = [
                    'pre_draw_issue' => $v['pre_draw_issue'],
                    'num' => $new_num,
                    'pre_draw_time' => $v['pre_draw_time'],
                    'pre_draw_code' => $v['pre_draw_code']
                ];
            }
        }
        return $this->success('ok', $re_data);
    }
    /**
     * 走势图
     * @param lottery_id 彩种id
     * @limmit  获取条数
     *
     */

    public function zst(){
        $param = $this->param;
        $lottery = input('lottery_id');
        $limmit = isset($param['limit']) ? $param['limit'] : 50;
        if ($limmit) {
            $kjlog = Db::table('lsyl_kjlog')->where('lottery_id', $lottery)->order('pre_draw_issue desc')->limit($limmit)->select();
        } else {
            $kjlog = Db::table('lsyl_kjlog')->where('lottery_id', $lottery)->order('pre_draw_issue desc')->select();
        }
        $data = $kjlog;
        $re_data = [];
        if ($data) {
            foreach ($data as $k => $v) {
                $code = explode(',', $v['pre_draw_code']);
                $re_data[] = [
                    'pre_draw_issue' => $v['pre_draw_issue'],
                    'pre_draw_time' => $v['pre_draw_time'],
                    'pre_draw_code' =>  $v['pre_draw_code'],
                    'num'=>$code
                ];
            }
        }

        return $this->success('ok', $re_data);
    }

    /**
     * 走势图
     * @num 数量
     */
    public function kjDATA()
    {
        $param = $this->param;
        $limmit = isset($param['limmit']) ? $param['limmit'] : 50;
        if ($limmit) {
            $kjlog = Db::table('cp_kjlog')->where('lottery_id', $this->lottery_id)->order('pre_draw_issue desc')->limit($limmit)->select();
        } else {
            $kjlog = Db::table('cp_kjlog')->where('lottery_id', $this->lottery_id)->order('pre_draw_issue desc')->select();
        }
        $kjlog = array_reverse($kjlog);

        return $kjlog;
    }


    /**
     * 获取彩种的第一个默认玩法
     * @param lottery_id 彩种id
     */
    public function getDefaultPlay(){
        $param = $this->param;
        $lottery_id = $param['lottery_id'];
        $lottery = Db::name('lottery')->where('id',$lottery_id)->find();
        if(!$lottery) return $this->error('彩种不存在');
        if($lottery['status'] == 0) return $this->error('该彩种已禁止');
        $play= Db::name('play')->where('lottery_id',$lottery_id)->where('status','1')->where('is_group','0')->order('id asc')->find();
        $play_name = getPlay($play['id']);
        $play_name= implode("/", array_reverse($play_name));
        $re = [
          'play_id'=>$play['id'],
          'play_name'=>$play_name,
          'lottery_id'=>$lottery_id,
          'lottery_name'=>$lottery['name'],
            'min_money_award'=>$play['min_money_award'],
            'max_money_award'=>$play['max_money_award'],
            'paramjson'=>json_decode($play['paramjson'], true),
            'remark'=>$play['remark'],
            'money'=>$play['money']
        ];
        return $this->success('默认玩法',$re);
    }


    /**
         * 彩种每个位置遗漏值
     * @param lottery_id  彩种id
     */
    public function getKjyl(){
        $param = $this->param;
        $lottery_id = $param['lottery_id'];
        $yl = Db::name('kjyl')->where('lottery_id',$lottery_id)->select();
        $data = [];
        if($yl){
            foreach ($yl as $k=>$v){
                $yl_vlaue = json_decode($v['yl_value'],true);
                if($v['type'] == '1'){
                    $data['zhix'] = $yl_vlaue;
                }else{
                    $data['zhux'] = $yl_vlaue;
                }


            }
        }
        if(isset($data['zhix']) && $data['zhix']){
            foreach ($data['zhix'] as $k=>$v){
                ksort($v);
                $v = array_values($v);


                if($k==1){
                    $data['zhix']['one_num'] =$v;

                }elseif($k==2){
                    $data['zhix']['two_num'] =$v;
                }elseif($k==3){
                    $data['zhix']['three_num'] =$v;
                }elseif($k==4){
                    $data['zhix']['four_num'] =$v;
                }elseif($k==5){
                    $data['zhix']['five_num'] =$v;
                }elseif($k==6){
                    $data['zhix']['six_num'] =$v;
                }elseif($k==7){
                    $data['zhix']['seven_num'] =$v;
                }elseif($k==8){
                    $data['zhix']['eight_num'] =$v;
                }elseif($k==9){
                    $data['zhix']['nine_num'] =$v;
                }elseif($k==10){
                    $data['zhix']['ten_num'] =$v;
                }
                unset($data['zhix'][$k]);
            }

        }
        ksort($data['zhux']);
        $data['zhux'] = array_values($data['zhux']);




        return $this->success('遗漏值',$data);



    }


}