<?php


namespace app\api\controller;

use think\Db;
use think\Request;
use function fast\e;

/**
 * 团队控制器
 * Class Team
 * @package app\api\controller
 */
class Team extends Base
{
    public function __construct(Request $request = null)
    {
        parent::__construct($request);
    }

    /**
     * 获取团队总览
     * @param start_date  开始时间 2020-03-03 00:00:00
     * @param end_date 终止时间 2020-08-01 23:59:59
     *
     */
    public function team_overview(){
        $userinfo = $this->userinfo;
        $param = $this->param;
        $start_date =$param['start_date'];
        $end_date = $param['end_date'];
        //获取所有下级ID
        $user_ids = getChildAll($userinfo['id']);
        //有效会员个数
        $yxUserCount = yxUser($user_ids,$start_date,$end_date);
        //团队充值
        $teamCzMoney = teamCz($user_ids,$start_date,$end_date);
        //团队提现
        $teamTxMoney = teamTx($user_ids,$start_date,$end_date);
        //团队下注
        $teamXzMoney = teamXz($user_ids,$start_date,$end_date);
        //团队中奖
        $teamJgMoney = teamJg($user_ids,$start_date,$end_date);
        //团队返点
        $teamFdMoney = teamFd($user_ids,$start_date,$end_date);
        //团队活动
        $teamHdMoney = teamHd($user_ids,$start_date,$end_date);
        //团队总余额
        $teamAllMoney = teamAllMoney($user_ids);
        //团队工资
        $teamGzMoney = teamGz($user_ids,$start_date,$end_date);
        //团队盈亏
        $teamYkMoney = teamYk($teamJgMoney,$teamFdMoney,$teamXzMoney,$teamHdMoney);
        $data = [
            'userCount'=>(string)count($user_ids),   //团队总人数
            'yxUserCount'=>$yxUserCount,    //有效会员个数
            'teamCzMoney'=>$teamCzMoney,    //团队充值金额
            'teamTxMoney'=>$teamTxMoney,    //团对提现金额
            'teamXzMoney'=>$teamXzMoney,    //团队下注金额
            'teamJgMoney'=>$teamJgMoney,    //团队中奖金额
            'teamFdMoney'=>$teamFdMoney,    //团队返点金额
            'teamHdMoney'=>$teamHdMoney,    //团队活动金额
            'teamAllMoney'=>$teamAllMoney,  //团队总余额
            'teamGzMoney'=>$teamGzMoney,    //团队工资
            'teamYkMoney'=>$teamYkMoney     //团队盈亏
        ];
        return $this->success('团队总览',$data);
    }






    /**
     * 团队报表
//     * @parame account  搜索账号、
     * @param start_date  开始时间 2020-03-03 00:00:00
     * @param end_date 终止时间 2020-08-01 23:59:59
     * @param user_id   用户id
     */
    public function team_report(){
        $userinfo = $this->userinfo;
        $static_url = config('static_url');
        $param = $this->param;
        $start_date =$param['start_date'];
        $end_date = $param['end_date'];
//        $page = isset($param['page'])?$param['page']:1;
//        $limit = isset($param['limit'])?$param['limit']:10;
        $user_id = isset($param['user_id'])?$param['user_id']:0;
        $user_id = $user_id==0?$userinfo['id']:$user_id;

        $re_date = [
            'level'=>[],
        ];
        $userin = Db::name('user')->where('id',$user_id)->find();
        $levels = userLevel($user_id,$userinfo['id']);
        $re_date['level'] = array_reverse($levels);
        //user_id 团队报表
        $user_ids = getChildAll($user_id);
        //有效会员个数
        $yxUserCount = yxUser($user_ids,$start_date,$end_date);
        //团队充值
        $teamCzMoney = teamCz($user_ids,$start_date,$end_date);
        //团队提现
        $teamTxMoney = teamTx($user_ids,$start_date,$end_date);
        //团队下注
        $teamXzMoney = teamXz($user_ids,$start_date,$end_date);
        //团队中奖
        $teamJgMoney = teamJg($user_ids,$start_date,$end_date);
        //团队返点
        $teamFdMoney = teamFd($user_ids,$start_date,$end_date);
        //团队活动
        $teamHdMoney = teamHd($user_ids,$start_date,$end_date);
        //团队总余额
        $teamAllMoney = teamAllMoney($user_ids);
        //团队盈亏
        $teamYkMoney = teamYk($teamJgMoney,$teamFdMoney,$teamXzMoney,$teamHdMoney);
        $avatar = empty($userin['avatar'])?'':$static_url.$userin['avatar'];
        $re_date['team_bb'] = [
            'id'=>$userin['id'],
            'account'=>$userin['account'],
            'is_dali'=>$userin['is_dali'],
            'avatar'=>$avatar,
            'user_count'=>count($user_ids),
            'teamAllMoney'=>$teamAllMoney,
            'yxUserCount'=>$yxUserCount,
            'teamCzMoney'=>$teamCzMoney,
            'teamTxMoney'=>$teamTxMoney,
            'teamXzMoney'=>$teamXzMoney,
            'teamHdMoney'=>$teamHdMoney,
            'teamJgMoney'=>$teamJgMoney,
            'teamFdMoney'=>$teamFdMoney,
            'teamYkMoney'=>$teamYkMoney
        ];




        //个人报表

        //当前user_id 的个人报表
        $user_ids_gr[] = $user_id;
        $userCz = teamCz($user_ids_gr,$start_date,$end_date);
        //提现
        $userTx = teamTx($user_ids_gr,$start_date,$end_date);
        //投注
        $userTz = teamXz($user_ids_gr,$start_date,$end_date);
        //中奖
        $userJg = teamJg($user_ids_gr,$start_date,$end_date);
        //返点
        $userFd = teamFd($user_ids_gr,$start_date,$end_date);
        //活动
        $userHd = teamHd($user_ids_gr,$start_date,$end_date);
        //余额
        $userYe = $userinfo['money'];
        //盈亏
        $userYk = teamYk($userJg,$userFd,$userTz,$userHd);
        $data_user = [
            'id'=>$userin['id'],
            'account'=>$userin['account'],
            'is_dali'=>$userin['is_dali'],
            'user_count'=>count($user_ids),
            'avatar'=>$avatar,
            'userCz'=>$userCz,
            'userTx'=>$userTx,
            'userTz'=>$userTz,
            'userJg'=>$userJg,
            'userFd'=>$userFd,
            'userHd'=>$userHd,
            'userYe'=>$userYe,
            'userYk'=>$userYk
        ];
        $re_date['user_bb'] = $data_user;









        return $this->success('o',$re_date);



//
//
//        $account = isset($param['account'])?$param['account']:'';
//
//        if($user_id == 0 && empty($account)){
//            //当前登录人的直接下级
//            $re_data = $this->team_report_data($userinfo['id'],$start_date,$end_date,$page,$limit);
//        }elseif ($user_id){
//            //点击用户
//
//            //查询当前登录人的所有下级
//            $allUserIds = getChildAll($userinfo['id']);
//            if(!in_array($user_id,$allUserIds)) return $this->error('你无权查看该用户团队');
//            $re_data =  $this->team_report_data($user_id,$start_date,$end_date,$page,$limit);
//        }elseif (!empty($account)){
//            //查询账号
//            $account_user =Db::name('user')->where('account',$account)->field('id')->find();
//            if(!$account_user) return $this->error('账号不存在');
//            //查询当前登录人的所有下级
//            $allUserIds = getChildAll($userinfo['id']);
//            if(!in_array($account_user['id'],$allUserIds)) return $this->error('你无权查看该用户团队');
//            $re_data =  $this->team_report_data($account_user['id'],$start_date,$end_date,$page,$limit);
//
//        }
//
//
//
//
//        //当前user_id 的个人报表
//        $user_ids[] = $user_id;
//        $userCz = teamCz($user_ids,$start_date,$end_date);
//        //提现
//        $userTx = teamTx($user_ids,$start_date,$end_date);
//        //投注
//        $userTz = teamXz($user_ids,$start_date,$end_date);
//        //中奖
//        $userJg = teamJg($user_ids,$start_date,$end_date);
//        //返点
//        $userFd = teamFd($user_ids,$start_date,$end_date);
//        //活动
//        $userHd = teamHd($user_ids,$start_date,$end_date);
//        //余额
//        $userYe = $userinfo['money'];
//        //盈亏
//        $userYk = teamYk($userJg,$userFd,$userTz,$userHd);
//        $data_user = [
//            'userCz'=>$userCz,
//            'userTx'=>$userTx,
//            'userTz'=>$userTz,
//            'userJg'=>$userJg,
//            'userFd'=>$userFd,
//            'userHd'=>$userHd,
//            'userYe'=>$userYe,
//            'userYk'=>$userYk
//        ];
//        $re_data['user_bb'] = $data_user;
//
//        return $this->success('ok',$re_data);

    }

    /**
     * 团队报表中下级
     * @param start_date  开始时间 2020-03-03 00:00:00
     * @param end_date 终止时间 2020-08-01 23:59:59
     * @param user_id   用户id
     * @param page   页数 默认1 、
     * @param limit  获取条数 默认10
     */
    public function team_bb_xj(){
        $userinfo = $this->userinfo;
        $static_url = config('static_url');
        $param = $this->param;
        $start_date =$param['start_date'];
        $end_date = $param['end_date'];
        $page = isset($param['page'])?$param['page']:1;
        $limit = isset($param['limit'])?$param['limit']:10;
        $user_id = isset($param['user_id'])?$param['user_id']:0;
        $user_id = $user_id==0?$userinfo['id']:$user_id;
        //查询直接下级
        $zhisu_user = Db::name('user')->where('user_id',$user_id)->field('id,user_id,account,is_dali,avatar')->page($page)->limit($limit)->select();
        $xj_count =  Db::name('user')->where('user_id',$user_id)->count();
        $re_date['xj_count'] = $xj_count;

        $data_user = [];
        if($zhisu_user){
            foreach ($zhisu_user as $k=>$v){
                $avatar_user = empty($v['avatar'])?'':$static_url.$v['avatar'];
                $user_ids = getChildAll($user_id);
                $user_ids_gr[] = $v['id'];
                $userCz_xj = teamCz($user_ids_gr,$start_date,$end_date);
                //提现
                $userTx_xj = teamTx($user_ids_gr,$start_date,$end_date);
                //投注
                $userTz_xj = teamXz($user_ids_gr,$start_date,$end_date);
                //中奖
                $userJg_xj = teamJg($user_ids_gr,$start_date,$end_date);
                //返点
                $userFd_xj = teamFd($user_ids_gr,$start_date,$end_date);
                //活动
                $userHd_xj = teamHd($user_ids_gr,$start_date,$end_date);
                //余额
                $userYe_xj = $userinfo['money'];
                //盈亏
                $userYk_xj = teamYk($userJg_xj,$userFd_xj,$userTz_xj,$userHd_xj);
                $data_user[] = [
                    'id'=>$v['id'],
                    'user_count'=>count($user_ids),
                    'account'=>$v['account'],
                    'is_dali'=>$v['is_dali'],
                    'avatar'=>$avatar_user,
                    'userCz'=>$userCz_xj,
                    'userTx'=>$userTx_xj,
                    'userTz'=>$userTz_xj,
                    'userJg'=>$userJg_xj,
                    'userFd'=>$userFd_xj,
                    'userHd'=>$userHd_xj,
                    'userYe'=>$userYe_xj,
                    'userYk'=>$userYk_xj
                ];

            }
        }
        $re_date['xj_user'] = $data_user;
        return $this->success('团队报表中下级',$re_date);



    }


    /**
     * 团队报表 数据
     * @param $user_id
     * @param $start_date
     * @param $end_date
     * @param $page
     * @param $limit
     * @return array
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    protected function team_report_data($user_id,$start_date,$end_date,$page,$limit){
        $re_date = [
            'count'=>0,
            'level'=>[],
            'data'=>[]
        ];
        $user = Db::name('user')->where('user_id',$user_id)->field('id,user_id,account,is_dali,avatar')->page($page)->limit($limit)->select();
        $count = Db::name('user')->where('user_id',$user_id)->count();
        $re_date['count'] = $count;
        $static_url = config('static_url');
        $userinfo = $this->userinfo;
        $levels = userLevel($user_id,$userinfo['id']);
        $re_date['level'] = array_reverse($levels);

        if($user){
            foreach ($user as $k=>$v){
                //获取所有下级ID
                $user_ids = getChildAll($v['id']);
                //有效会员个数
                $yxUserCount = yxUser($user_ids,$start_date,$end_date);
                //团队充值
                $teamCzMoney = teamCz($user_ids,$start_date,$end_date);
                //团队提现
                $teamTxMoney = teamTx($user_ids,$start_date,$end_date);
                //团队下注
                $teamXzMoney = teamXz($user_ids,$start_date,$end_date);
                //团队中奖
                $teamJgMoney = teamJg($user_ids,$start_date,$end_date);
                //团队返点
                $teamFdMoney = teamFd($user_ids,$start_date,$end_date);
                //团队活动
                $teamHdMoney = teamHd($user_ids,$start_date,$end_date);
                //团队总余额
                $teamAllMoney = teamAllMoney($user_ids);
                //团队盈亏
                $teamYkMoney = teamYk($teamJgMoney,$teamFdMoney,$teamXzMoney,$teamHdMoney);
                $avatar = empty($v['avatar'])?'':$static_url.$v['avatar'];
                $re_date['data'][] = [
                    'id'=>$v['id'],
                    'account'=>$v['account'],
                    'is_dali'=>$v['is_dali'],
                    'avatar'=>$avatar,
                    'teamAllMoney'=>$teamAllMoney,
                    'yxUserCount'=>$yxUserCount,
                    'teamCzMoney'=>$teamCzMoney,
                    'teamTxMoney'=>$teamTxMoney,
                    'teamXzMoney'=>$teamXzMoney,
                    'teamHdMoney'=>$teamHdMoney,
                    'teamJgMoney'=>$teamJgMoney,
                    'teamFdMoney'=>$teamFdMoney,
                    'teamYkMoney'=>$teamYkMoney

                ];
            }
        }
        return $re_date;
    }



    /**
     * 会员列表
     * @param page   页数 默认1 、
     * @param limit  获取条数 默认10
     * @param account  账号
     */
    public function user_list(){
        $param = $this->param;
        $userinfo = $this->userinfo;
        $page = isset($param['page'])?$param['page']:1;
        $limit = isset($param['limit'])?$param['limit']:10;
        $account = isset($param['account'])?$page['account']:'';
        $where = [];
        //查询所有下级
        $allUserIds = getChildAll($userinfo['id']);
        if(!empty($account)){
            $where['account'] = ['like',"%{$account}%"];
        }
        $where['id'] = ['in',$allUserIds];

        $user = Db::name('user')->where($where)->limit($limit)->page($page)->field('id,avatar,is_dali,account')->select();
        $count =  Db::name('user')->where($where)->count();
        $static_url = config('static_url');
        if($user){
            foreach ($user as $k=>$v){
                $user[$k]['avatar'] = $v['avatar']?$static_url.$v['avatar']:'';
            }
        }
        $data =  [
            'data'=>$user,
            'count'=>$count
        ];
        return $this->success('会员列表',$data);
    }


    /**
     * 在线会员列表
     * @param page   页数 默认1 、
     * @param limit  获取条数 默认10
     */
    public function online_user(){
        $userinfo = $this->userinfo;
        $param = $this->param;
        $page = isset($param['page'])?$param['page']:1;
        $limit = isset($param['limit'])?$param['limit']:10;

        //查询所有下级
        $allUserIds = getChildAll($userinfo['id']);
        $user = Db::table('lsyl_user_token')->where('lsyl_user_token.user_id','in',$allUserIds)->where('lsyl_user_token.expiretime','>',time())->join('lsyl_user','lsyl_user.id = lsyl_user_token.user_id','left')->limit($limit)->page($page)->field('lsyl_user.id,lsyl_user.account,lsyl_user.money,lsyl_user.avatar')->select();
        $count = Db::table('lsyl_user_token')->where('expiretime','>',time())->count();
        if($user){
            $static_url = config('static_url');
            $day = date('Y-m-d',time());
            $start_date = $day.' 00:00:00';
            $end_date = $day.' 23:59:59';
            foreach ($user as $k=>$v){
                $userYk = userYk($v['id'],$start_date,$end_date);
                $user[$k]['xzMoney'] = $userYk['xzMoney'];
                $user[$k]['ykMoney'] = $userYk['ykMoney'];
                $user[$k]['avatar'] = $v['avatar']?$static_url.$v['avatar']:'';
            }
        }

        $data =[
            'data'=>$user,
            'count'=>$count
        ];
        return $this->success('在线会员列表',$data);

    }


    /**
     * 用户上下级列表
     * @parame account  搜索账号、
     * @param user_id   用户id
     * @param page   页数 默认1 、
     * @param limit  获取条数 默认10
     */
    public function user_level(){
        $param = $this->param;
        $userinfo = $this->userinfo;
//        $start_date =$param['start_date'];
//        $end_date = $param['end_date'];
        $user_id = isset($param['user_id'])?$param['user_id']:0;
        $account = isset($param['account'])?$param['account']:'';
        $page = isset($param['page'])?$param['page']:1;
        $limit = isset($param['limit'])?$param['limit']:10;
        if($user_id == 0 && empty($account)){
            //查询下级
            $user_id = $userinfo['id'];


        }elseif ($user_id){
            //查询当前登录人的所有下级
            $allUserIds = getChildAll($userinfo['id']);
            if(!in_array($user_id,$allUserIds)) return $this->error('你无权查看该用户团队');
        }elseif (!empty($account)){
            $account_user =Db::name('user')->where('account',$account)->field('id')->find();
            if(!$account_user) return $this->error('账号不存在');
            //查询当前登录人的所有下级
            $allUserIds = getChildAll($userinfo['id']);
            if(!in_array($account_user['id'],$allUserIds)) return $this->error('你无权查看该用户团队');
            $user_id = $account_user['id'];

        }
        $user = Db::name('user')->where('user_id',$user_id)->limit($limit)->page($page)->field('id,user_id,is_dali,account,money,ratio,login_time,avatar')->select();
        $levels = userLevel($user_id,$userinfo['id']);
        if($user){
            $static_url = config('static_url');
            foreach ($user as $k=>$v){
                $user[$k]['avatar'] = $v['avatar']?$static_url.$v['avatar']:'';
                $user[$k]['login_time'] = $v['login_time']?date('Y-m-d H:i:s',$v['login_time']):'';
                $user_all = getChildAll($v['id']);
                $user[$k]['teamAllmoney'] = (string)Db::name('user')->where('id','in',$user_all)->sum('money');


            }
        }
        $count = Db::name('user')->where('user_id',$user_id)->count();
        $levels = array_reverse($levels);

        $data = [
            'count'=>$count,
            'level'=>$levels,
            'data'=>$user
        ];
        return $this->success('用户上下级列表',$data);




    }


    /**
     * 用户升点
     * @param ratio 返点
     * @param user_id 用户ID
     */
    public function upRatio(){
        $param = $this->param;
        $user_id = $param['user_id'];
        $ratio = $param['ratio'];
        $userinfo = $this->userinfo;
        //查询需要修改的用户
        $user = Db::name('user')->where('id',$user_id)->find();
        if(!$user) return $this->error('该用户不存在');
        if($userinfo['id'] != $user['user_id']) return $this->error('该用户不是你的直属下级');
        //用户身份
        $level = userIdentity($user['level']);
        if($level['ratio'] != 0){
            if($ratio != $level['ratio']) return $this->error('该用户的返点为固定'.$level['ratio']);
        }else{
            if($ratio>$user['ratio']) return $this->error('最高返点为:'.$user['ratio']);
        }

        $re = Db::name('user')->where('id',$user_id)->update([
            'ratio'=>$ratio,
            'updatetime'=>time()
        ]);
        if($re) return $this->success('更新成功');
        return $this->error('更新失败');

    }


    /**
     * 用户转账
     * @param money 转账金额
     * @param user_id 被转账用户
     * @param pay_pwd 支付密码
     */
    public function moneyMove(){
        $param = $this->param;
        $userinfo = $this->userinfo;
        $money = $param['money'];
        $user_id = $param['user_id'];
        $pay_pwd = $param['pay_pwd'];
        //判断用户是否开通转账
        if($userinfo['is_zz'] != '1') return $this->error('你未开通转账功能');
        if(empty($userinfo['pay_pwd'])) return $this->error('请先设置资金密码');
        if($userinfo['pay_pwd'] != $pay_pwd) return $this->error('资金密码错误');
        // 启动事务
        Db::startTrans();
        try{
            $us_money = 0-$money;
            $user = Db::table('lsyl_user')->where('id',$user_id)->field('id,account')->find();
            //更新转账人余额
            updateUser($userinfo['id'],$us_money,'5',$user_id,time(),'转账给:'.$user['account']);
            //更新收钱人
            updateUser($user_id,$money,'5',$userinfo['id'],time(),'转账来源:'.$userinfo['account']);
            // 提交事务
            Db::commit();
        } catch (\Exception $e) {
            // 回滚事务
            Db::rollback();
            return $this->error('转账失败'.$e->getMessage());
        }
        return $this->success('转账成功');




    }

    /**
     * 团队帐变
     * @param user_id 用户ID  当登录人传0
     * @param page 页数 默认1
     * @param limit 获取条数  默认10
     * @param type 类型:1=充值,2=提现,3=彩票下注,4=活动,5=转账,6=资金修正,7=返点,8=团队分红,9=浮动工资,10=彩票奖金,11=撤单 0= 全部
     * @param start_date  开始时间 2020-03-03 00:00:00
     * @param end_date 终止时间 2020-08-01 23:59:59
     *
     */
    public function moneyChange(){
        $param = $this->param;
        $userinfo = $this->userinfo;
        $page = isset($param['page'])?$param['page']:1;
        $limit = isset($param['limit'])?$param['limit']:10;
        $start_date = $param['start_date'];
        $end_date = $param['end_date'];
        $type = $param['type'];




        $user_id = $param['user_id'] !=0?$param['user_id']:$userinfo['id'];
        $userids = getChildAll($user_id);
        $where = [];
        $where['moneylog.user_id'] = ['in',$userids];
        if($type){
            $where['moneylog.type'] = $type;
        }
        if(!in_array($user_id,$userids)) return $this->error('该用户你无权查看');

        $list = Db::name('moneylog')->alias('moneylog')->join('lsyl_user','lsyl_user.id = moneylog.user_id','left')->where('moneylog.createtime',">=",strtotime($start_date))->where('moneylog.createtime','<=',strtotime($end_date))->where($where)->limit($limit)->page($page)->order('moneylog.createtime desc')->field('moneylog.*,lsyl_user.account')->select();
        $count = Db::name('moneylog')->alias('moneylog')->where('moneylog.createtime',">=",strtotime($start_date))->where('moneylog.createtime','<=',strtotime($end_date))->where($where)->count();
        if($list){
            foreach ($list as $k=>$v){
                $list[$k]['createtime'] = date('Y-m-d H:i:s',$v['createtime']);
            }
        }
        $date = [
            'list'=>$list,
            'count'=>$count

        ];
        return $this->success('团队帐变',$date);



    }
    /**
     * 团队投注
     * @param user_id 用户ID  当登录人传0
     * @param page 页数 默认1
     * @param limit 获取条数  默认10
     * @param lottery_id 彩种id 0=全部
     * @param status 状态:1=待开奖,2=中奖,3=未中奖,4=结算错误,5=已撤销  0=全部
     * * @param start_date  开始时间 2020-03-03 00:00:00
     * @param end_date 终止时间 2020-08-01 23:59:59
     */
    public function teamBetting(){
        $param = $this->param;
        $userinfo = $this->userinfo;
        $page = isset($param['page'])?$param['page']:1;
        $limit = isset($param['limit'])?$param['limit']:10;
        $user_id = $param['user_id'] !=0?$param['user_id']:$userinfo['id'];
        $status = $param['status'];
        $start_date = $param['start_date'];
        $end_date = $param['end_date'];

        $userids = getChildAll($user_id);
        $where = [];
        $where['betting.user_id'] = ['in',$userids];
        if($status){
            $where['betting.type'] = $status;
        }
        $lottery_id = $param['lottery_id'];
        if($lottery_id){
            $where['betting.lottery_id']= $lottery_id;
        }
        if(!in_array($user_id,$userids)) return $this->error('该用户你无权查看');
        $betting = Db::name('betting')->alias('betting')->where($where)->where('betting.createtime','>=',strtotime($start_date))->where('betting.createtime','<=',strtotime($end_date))->join('lsyl_lottery','lsyl_lottery.id = betting.lottery_id','left')->join('lsyl_user','lsyl_user.id = betting.user_id','left')->limit($limit)->page($page)->field('betting.id,betting.ordercode,betting.user_id,betting.pre_draw_issue,betting.param,betting.play_id,betting.jg_money,betting.jg_num,betting.xz_money,betting.multiple,betting.unit,betting.kj_time,betting.status,betting.createtime,lsyl_user.account,lsyl_lottery.name lottery_name')->select();
        $count =  Db::name('betting')->alias('betting')->where($where)->where('betting.createtime','>=',strtotime($start_date))->where('betting.createtime','<=',strtotime($end_date))->count();
        if($betting){
            foreach ($betting as $k=>$v){
                $betting[$k]['createtime'] = date('Y-m-d H:i:s',$v['createtime']);
                $play_name = getPlay($v['play_id']);
                $betting[$k]['play_name'] = implode("/", array_reverse($play_name));
                $betting[$k]['param'] = paramParsing(json_decode($v['param'],true));
            }
        }
        $data = [
            'list'=>$betting,
            'count'=>$count
        ];
        return $this->success('团队投注',$data);


    }
    /**
     * 团队充值
     * @param user_id 用户ID  当登录人传0
     * @param page 页数 默认1
     * @param limit 获取条数  默认10
     * @param status 状态:1=代付款,2=已付款,3=未付款,4=待确认 0=全部
     * * @param start_date  开始时间 2020-03-03 00:00:00
     * @param end_date 终止时间 2020-08-01 23:59:59
     */
    public function teamRecharge(){

        $param = $this->param;
        $userinfo = $this->userinfo;
        $page = isset($param['page'])?$param['page']:1;
        $limit = isset($param['limit'])?$param['limit']:10;
        $user_id = $param['user_id'] !=0?$param['user_id']:$userinfo['id'];
        $status = $param['status'];
        $start_date = $param['start_date'];
        $end_date = $param['end_date'];
        $userids = getChildAll($user_id);


        $where = [];
        $where['recharge.user_id'] = ['in',$userids];
        if($status){
            $where['recharge.status'] = $status;
        }
        $list = Db::name('recharge')->alias('recharge')->where($where)->where('recharge.createtime','>=',strtotime($start_date))->join('lsyl_user','lsyl_user.id = recharge.user_id','left')->where('recharge.createtime','<=',strtotime($end_date))->limit($limit)->page($page)->field('recharge.id,recharge.ordercode,recharge.user_id,recharge.status,recharge.pay_pt,recharge.code,recharge.pay_name,recharge.money,recharge.remark,recharge.createtime,lsyl_user.account')->order('recharge.createtime desc')->select();
        $count = Db::name('recharge')->alias('recharge')->where($where)->where('recharge.createtime','>=',strtotime($start_date))->where('recharge.createtime','<=',strtotime($end_date))->count();
        if($list){
            foreach ($list as $k=>$v){
                $list[$k]['createtime'] = date('Y-m-d H:i:s',$v['createtime']);
            }
        }
        $data = [
            'list'=>$list,
            'count'=>$count
        ];
        return $this->success('团队充值',$data);

    }


    /**
     * 团队提现
     * @param user_id 用户ID  当登录人传0
     * @param page 页数 默认1
     * @param limit 获取条数  默认10
     * @param status '状态:1=待审核,2=驳回,3=待出款,4=已出款,5=财务提交 0 =全部',
     * * @param start_date  开始时间 2020-03-03 00:00:00
     * @param end_date 终止时间 2020-08-01 23:59:59
     */
    public function teamTx(){

        $param = $this->param;
        $userinfo = $this->userinfo;
        $page = isset($param['page'])?$param['page']:1;
        $limit = isset($param['limit'])?$param['limit']:10;
        $user_id = $param['user_id'] !=0?$param['user_id']:$userinfo['id'];
        $status = $param['status'];
        $start_date = $param['start_date'];
        $end_date = $param['end_date'];
        $userids = getChildAll($user_id);


        $where = [];
        $where['withdraw.user_id'] = ['in',$userids];
        if($status){
            $where['withdraw.status'] = $status;
        }
        $list = Db::name('withdraw')->alias('withdraw')->where($where)->where('withdraw.createtime','>=',strtotime($start_date))->join('lsyl_user','lsyl_user.id = withdraw.user_id','left')->where('withdraw.createtime','<=',strtotime($end_date))->limit($limit)->page($page)->field('withdraw.id,withdraw.txcode,withdraw.user_id,withdraw.status,withdraw.money,withdraw.remark,withdraw.createtime,lsyl_user.account')->order('withdraw.createtime desc')->select();
        $count = Db::name('withdraw')->alias('withdraw')->where($where)->where('withdraw.createtime','>=',strtotime($start_date))->where('withdraw.createtime','<=',strtotime($end_date))->count();
        if($list){
            foreach ($list as $k=>$v){
                $list[$k]['createtime'] = date('Y-m-d H:i:s',$v['createtime']);
            }
        }
        $data = [
            'list'=>$list,
            'count'=>$count
        ];
        return $this->success('团队充值',$data);
    }










}