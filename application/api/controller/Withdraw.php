<?php


namespace app\api\controller;

use think\Db;
use think\Request;

/**
 * 用户提现控制器
 * Class Withdraw
 * @package app\api\controller
 */
class Withdraw extends Base
{
    public function __construct(Request $request = null)
    {
        parent::__construct($request);
    }


    /**
     * 获取用户的银行卡列表
     */
    public function bank_list()
    {
        $userinfo = $this->userinfo;
        $banklist = Db::table('lsyl_bank')->where('lsyl_bank.user_id', $userinfo['id'])->join('lsyl_banktype', 'lsyl_banktype.id = lsyl_bank.banktype_id', 'left')->where('lsyl_banktype.status', '1')->field('lsyl_bank.id,lsyl_bank.user_id,lsyl_bank.banktype_id,lsyl_bank.realname,lsyl_bank.branch_name,lsyl_bank.bank_code,lsyl_banktype.name,lsyl_banktype.code,lsyl_banktype.image,lsyl_banktype.status')->select();
        if ($banklist) {
            $static_url = config('static_url');
            foreach ($banklist as $k => $v) {
                $banklist[$k]['image'] = $static_url . $v['image'];

            }
        }
        return $this->success('银行卡列表', $banklist);
    }


    /**
     * 用户提现
     * money 提现金额
     * bank_id  银行卡id
     * pay_pwd 提现密码
     *
     *
     */
    public function addTx()
    {
        $userinfo = $this->userinfo;
        if ($userinfo['status'] == '2') return $this->error('虚拟账户禁止提现');
        if ($userinfo['pay_pwd'] == '') return $this->error('请设置支付密码');


        $param = $this->param;
        $money = $param['money'];
        if ($money < 100 || $money > 29999) return $this->error('单次提现最低100，最高29999');
        $pay_pwd = $param['pay_pwd'];
        if (empty($pay_pwd)) return $this->error("提现密码不能为空");

        $bank_id = $param['bank_id'];

        if ($pay_pwd != $userinfo['pay_pwd']) return $this->error('提现密码错误');
//        if ($userinfo['all_money'] < $money) return $this->error('余额不足');

        //银行卡查询
        $bank = Db::table('lsyl_bank')->where('lsyl_bank.id', $bank_id)->join('lsyl_banktype', 'lsyl_banktype.id = lsyl_bank.banktype_id')->field('lsyl_banktype.code,lsyl_banktype.name,lsyl_bank.realname,lsyl_bank.branch_name,lsyl_bank.bank_code,lsyl_bank.user_id')->find();
        if (!$bank) return $this->error('银行卡不存在');
        if ($bank['user_id'] != $userinfo['id']) return $this->error('该银行卡不属于您');
        $withdrawZg = $this->withdrawZg();


        // 启动事务
        Db::startTrans();
        try {
            $txcode = $this->orderCode();

            $data = [
                'txcode' => $txcode,
                'user_id' => $userinfo['id'],
                'money' => $money,
                'bank_type' => $bank['name'],
                'bank_name' => $bank['branch_name'],
                'bank_code' => $bank['bank_code'],
                'code' => $bank['code'],
                'relaname' => $bank['realname'],
                'status' => '1',
                'service_money' => 0,
                'createtime' => time(),
                'updatetime' => time()
            ];

            //计算手续费
            //当日前提现三次免费//日前三次免费，后续规则：提款金额 * 1%，最小手续费 1元，最高手续费 25元
            if ($withdrawZg['withdrawCount'] + 1 > 3) {
                $service_money = bcmul($money, 0.01, 3);
                $data['service_money'] = $service_money > 25 ? 25 : $service_money;
            }
            //写入提现记录表
            //提现金额转为负数
            $money = bcsub(0, $money, 3);
            //更新用户余额
            $new_money = updateUser($userinfo['id'], $money, '2', $txcode, time(), '提现');
            $data['new_money'] = $new_money;
            Db::name('withdraw')->insert($data);
            tz('tx_num');
            // 提交事务
            Db::commit();
        } catch (\Exception $e) {
            // 回滚事务
            Db::rollback();
            return $this->error('申请失败：' . $e->getMessage());
        }
        return $this->success('申请成功');


    }

    /**
     * 生成充值订单号
     */
    protected function orderCode()
    {
        $rand = sprintf("%04d", rand(0, 99999));
        $ordercode = 'TX' . date('YmdHis', time()) . $rand;
        $count = Db::table('lsyl_withdraw')->where('txcode', $ordercode)->count();
        if ($count) {
            $this->orderCode();
        } else {
            return $ordercode;
        }
    }

    /**
     * 提现资格判断
     */
    private function withdrawZg()
    {
        $userInfo = $this->userinfo;

        if ($userInfo['status'] == 2) return $this->error('虚拟账号不能提现');
        //查询总的充值记录

        //查询今日的提现次数
        $withdraw = Db::table('lsyl_withdraw')->where('user_id', $userInfo['id'])->whereTime('createtime', 'today')->count();
        if ($withdraw > 20) return $this->error('每天提现20次');
        $withdrawAllmoney = Db::table('lsyl_withdraw')->where('user_id', $userInfo['id'])->whereTime('createtime', 'today')->sum('money');
        if ($withdrawAllmoney > 1000000) return $this->error('每日提款限额100W');
        $data = [
            'withdrawCount' => (int)$withdraw,
            'withdrawAllmoney' => (string)$withdrawAllmoney
        ];
        return $data;

    }
    /**
     * 提现记录
     * @param limit  获取数量 默认10
     * @param page 页数 默认1
     * @param start_date  开始时间 2020-03-03 00:00:00
     * @param end_date 终止时间 2020-08-01 23:59:59
     * @param txcode  提现单号
     * @param status 状态:1=待审核,2=驳回,3=待出款,4=已出款,5=财务提交 0 =全部
     *
     *
     */
    public function withdrawLog(){
        $param =$this->param;
        $userinfo = $this->userinfo;
        $limit = isset($param['limit']) ? $param['limit'] : 10;
        $page = isset($param['page']) ? $param['page'] : 1;
        $start_date = isset($param['start_date'])?$param['start_date']:'0';
        $end_date = isset($param['end_date'])?$param['end_date']:'2120-01-01';
        $start_date = strtotime($start_date);
        $end_date = strtotime($end_date);
        $txcode = isset($param['txcode'])?$param['txcode']:'';
        $where = [];
        $where['user_id']=$userinfo['id'];
        if(!empty($txcode)){
            $where['txcode']=$txcode;
        }
        $status = isset($param['status'])?$param['status']:'0';
        if($status){
            $where['status'] = $status;
        }

        $list = Db::name('withdraw')->where($where)->where('createtime','>=',$start_date)->where('createtime','<=',$end_date)->limit($limit)->page($page)->order('createtime desc')->field('txtype_id,code',true)->select();
        $count = Db::name('withdraw')->where($where)->where('createtime','>=',$start_date)->where('createtime','<=',$end_date)->count();
        if($list){
            foreach ($list as $k=>$v){
                $list[$k]['createtime'] = date('Y-m-d H:i:s',$v['createtime']);
                $list[$k]['updatetime'] = date('Y-m-d H:i:s',$v['updatetime']);
            }
        }

        $data = [
            'count'=>$count,
            'list'=>$list
        ];
        return $this->success('提现记录',$data);




    }

}