<?php

namespace app\api\controller;

use app\common\controller\Api;
use app\common\model\Area;
use app\common\model\Version;
use fast\Random;
use think\Config;
use think\Db;
use think\Response;
use Endroid\QrCode\QrCode;

/**
 * 公共接口
 */
class Common extends Api
{
    protected $noNeedLogin = ['init', 'getBanner', 'getNotice', 'getActivity', 'createQrcode','getBanktypeList','upload','appUp','downQrcode','activity'];
    protected $noNeedRight = '*';


    /**
     * 获取下载二维码
     */
    public function downQrcode(){
        $android =  Db::table('lsyl_appup')->where('type','android')->order('createtime desc')->field('image')->find();
        $ios =  Db::table('lsyl_appup')->where('type','ios')->order('createtime desc')->field('image')->find();
        $static = \config('static_url');
        $android_qrcode = '';
        if($android && !empty($android['image'])){
            $android_qrcode = $static.$android['image'];
        }
        $ios_qrcode = '';
        if($ios && !empty($ios['image'])){
            $ios_qrcode = $static.$ios['image'];
        }
        $data = [
            'android'=>$android_qrcode,
            'ios'=>$ios_qrcode
        ];
        return $this->success('app下载二维码',$data);



    }
    /**
     *app 更新信息接口
     * @type  平台 android=安卓  iso=苹果
     */
    public function appUp(){
        $type = input('type');
        $re = Db::table('lsyl_appup')->where('type',$type)->order('createtime desc')->find();
        return $this->success('更新信息接口',$re);

    }
    /**
     * 获取银行卡类型列表
     */
    public function getBanktypeList(){

        $banktypeList = Db::name('banktype')->where('status','1')->field('id,name,code')->select();
        return $this->success('银行卡类型列表',$banktypeList);
    }


    /**
     * 二维码生成
     */
    public function createQrcode()
    {
        $param = $this->param;
        $url = $param['url'];

        $text = $this->request->get('text', $url);
        $size = $this->request->get('size', 250);
        $padding = $this->request->get('padding', 15);
        $errorcorrection = $this->request->get('errorcorrection', 'medium');
        $foreground = $this->request->get('foreground', "#ffffff");
        $background = $this->request->get('background', "#000000");
        $logo = $this->request->get('logo');
        $logosize = $this->request->get('logosize');
        $label = $this->request->get('label');
        $labelfontsize = $this->request->get('labelfontsize');
        $labelhalign = $this->request->get('labelhalign');
        $labelvalign = $this->request->get('labelvalign');

        // 前景色
        list($r, $g, $b) = sscanf($foreground, "#%02x%02x%02x");
        $foregroundcolor = ['r' => $r, 'g' => $g, 'b' => $b];

        // 背景色
        list($r, $g, $b) = sscanf($background, "#%02x%02x%02x");
        $backgroundcolor = ['r' => $r, 'g' => $g, 'b' => $b];

        $qrCode = new \Endroid\QrCode\QrCode();
        $qrCode
            ->setText($text)
            ->setSize($size)
            ->setPadding($padding)
            ->setErrorCorrection($errorcorrection)
            ->setForegroundColor($foregroundcolor)
            ->setBackgroundColor($backgroundcolor)
            ->setLogoSize($logosize)
            ->setLabel($label)
            ->setLabelFontSize($labelfontsize)
            ->setLabelHalign($labelhalign)
            ->setLabelValign($labelvalign)
            ->setImageType(QrCode::IMAGE_TYPE_PNG);
        $fontPath = ROOT_PATH . 'public/assets/fonts/SourceHanSansK-Regular.ttf';
        if (file_exists($fontPath)) {
            $qrCode->setLabelFontPath($fontPath);
        }
        if ($logo) {
            $qrCode->setLogo(ROOT_PATH . 'public/assets/img/qrcode.png');
        }
        //也可以直接使用render方法输出结果
        //$qrCode->render();
        return new Response($qrCode->get(), 200, ['Content-Type' => $qrCode->getContentType()]);
    }


    /**
     * 获取banner
     *
     */
    public function getBanner()
    {

//        $bannerList = Db::name('banner')->where('status', '1')->order('weigh desc')->field('app_image,pc_image,title')->select();
        $static_url = \config('static_url');
        $bannerList = Db::name('activity')->where('status', '1')->order('createtime desc')->select();
        if ($bannerList) {
            foreach ($bannerList as $k => $v) {
                $bannerList[$k]['app_image'] = $static_url . $v['app_image'];
                $bannerList[$k]['pc_image'] = $static_url . $v['pc_image'];
                $bannerList[$k]['createtime'] = date('Y-m-d H:i:s', $v['createtime']);
                $bannerList[$k]['updatetime'] = date('Y-m-d H:i:s', $v['updatetime']);

            }
        }
        return $this->success('轮播图', $bannerList);
    }
    /**
     * 活动案详情
     */
    public function activity(){
        $static_url = \config('static_url');
        $id = input('id');
        $activity = Db::name('activity')->where('status', '1')->where('id',$id)->find();
        if(!$activity) return $this->error('活动不存在');
        $activity['app_image'] = $static_url . $activity['app_image'];
        $activity['pc_image'] = $static_url . $activity['pc_image'];
        $activity['createtime'] = date('Y-m-d H:i:s', $activity['createtime']);
        $activity['updatetime'] = date('Y-m-d H:i:s', $activity['updatetime']);
        return $this->success('活动详情',$activity);
    }

    /**
     * 获取公告
     * @param limit  获取数量
     * @param page 获取页数
     */
    public function getNotice()
    {
        $param = $this->param;
        $limit = isset($param['limit']) ? $param['limit'] : 10;
        $page = isset($param['page']) ? $param['page'] : 1;
        $notice = Db::name('notice')->where('status', '1')->order('createtime desc')->limit($limit)->page($page)->select();
        $count = Db::name('notice')->where('status', '1')->count();
        if ($notice) {
            foreach ($notice as $k => $v) {
                $notice[$k]['createtime'] = date('Y-m-d H:i:s', $v['createtime']);
                $notice[$k]['updatetime'] = date('Y-m-d H:i:s', $v['updatetime']);
            }
        }
        $data = [
            'notice' => $notice,
            'count' => $count
        ];
        return $this->success('公告列表', $data);


    }

    /**
     * 获取活动
     * @param limit  获取数量
     * @param page 获取页数
     */
    public function getActivity()
    {
        $param = $this->param;
        $limit = isset($param['limit']) ? $param['limit'] : 10;
        $page = isset($param['page']) ? $param['page'] : 1;
        $activity = Db::name('activity')->where('status', '1')->order('createtime desc')->limit($limit)->page($page)->select();
        $count = Db::name('activity')->where('status', '1')->count();

        $static_url = \config('static_url');
        if ($activity) {
            foreach ($activity as $k => $v) {
                $activity[$k]['createtime'] = date('Y-m-d H:i:s', $v['createtime']);
                $activity[$k]['updatetime'] = date('Y-m-d H:i:s', $v['updatetime']);
                $activity[$k]['app_image'] = $static_url . $v['app_image'];
                $activity[$k]['pc_image'] = $static_url . $v['pc_image'];
            }
        }
        $data = [
            'activity' => $activity,
            'count' => $count
        ];
        return $this->success('活动列表', $data);


    }


    /**
     * 加载初始化
     *
     * @param string $version 版本号
     * @param string $lng 经度
     * @param string $lat 纬度
     */
    public function init()
    {
        if ($version = $this->request->request('version')) {
            $lng = $this->request->request('lng');
            $lat = $this->request->request('lat');
            $content = [
                'citydata' => Area::getCityFromLngLat($lng, $lat),
                'versiondata' => Version::check($version),
                'uploaddata' => Config::get('upload'),
                'coverdata' => Config::get("cover"),
            ];
            $this->success('', $content);
        } else {
            $this->error(__('Invalid parameters'));
        }
    }

    /**
     * 上传文件
     * @ApiMethod (POST)
     * @param File $file 文件流
     */
    public function upload()
    {
        $file = $this->request->file('file');
        if (empty($file)) {
            $this->error(__('No file upload or server upload limit exceeded'));
        }

        //判断是否已经存在附件
        $sha1 = $file->hash();

        $upload = Config::get('upload');

        preg_match('/(\d+)(\w+)/', $upload['maxsize'], $matches);
        $type = strtolower($matches[2]);
        $typeDict = ['b' => 0, 'k' => 1, 'kb' => 1, 'm' => 2, 'mb' => 2, 'gb' => 3, 'g' => 3];
        $size = (int)$upload['maxsize'] * pow(1024, isset($typeDict[$type]) ? $typeDict[$type] : 0);
        $fileInfo = $file->getInfo();
        $suffix = strtolower(pathinfo($fileInfo['name'], PATHINFO_EXTENSION));
        $suffix = $suffix && preg_match("/^[a-zA-Z0-9]+$/", $suffix) ? $suffix : 'file';

        $mimetypeArr = explode(',', strtolower($upload['mimetype']));
        $typeArr = explode('/', $fileInfo['type']);

        //禁止上传PHP和HTML文件
        if (in_array($fileInfo['type'], ['text/x-php', 'text/html']) || in_array($suffix, ['php', 'html', 'htm'])) {
            $this->error(__('Uploaded file format is limited'));
        }
        //验证文件后缀
        if ($upload['mimetype'] !== '*' &&
            (
                !in_array($suffix, $mimetypeArr)
                || (stripos($typeArr[0] . '/', $upload['mimetype']) !== false && (!in_array($fileInfo['type'], $mimetypeArr) && !in_array($typeArr[0] . '/*', $mimetypeArr)))
            )
        ) {
            $this->error(__('Uploaded file format is limited'));
        }
        //验证是否为图片文件
        $imagewidth = $imageheight = 0;
        if (in_array($fileInfo['type'], ['image/gif', 'image/jpg', 'image/jpeg', 'image/bmp', 'image/png', 'image/webp']) || in_array($suffix, ['gif', 'jpg', 'jpeg', 'bmp', 'png', 'webp'])) {
            $imgInfo = getimagesize($fileInfo['tmp_name']);
            if (!$imgInfo || !isset($imgInfo[0]) || !isset($imgInfo[1])) {
                $this->error(__('Uploaded file is not a valid image'));
            }
            $imagewidth = isset($imgInfo[0]) ? $imgInfo[0] : $imagewidth;
            $imageheight = isset($imgInfo[1]) ? $imgInfo[1] : $imageheight;
        }
        $replaceArr = [
            '{year}' => date("Y"),
            '{mon}' => date("m"),
            '{day}' => date("d"),
            '{hour}' => date("H"),
            '{min}' => date("i"),
            '{sec}' => date("s"),
            '{random}' => Random::alnum(16),
            '{random32}' => Random::alnum(32),
            '{filename}' => $suffix ? substr($fileInfo['name'], 0, strripos($fileInfo['name'], '.')) : $fileInfo['name'],
            '{suffix}' => $suffix,
            '{.suffix}' => $suffix ? '.' . $suffix : '',
            '{filemd5}' => md5_file($fileInfo['tmp_name']),
        ];
        $savekey = $upload['savekey'];
        $savekey = str_replace(array_keys($replaceArr), array_values($replaceArr), $savekey);

        $uploadDir = substr($savekey, 0, strripos($savekey, '/') + 1);
        $fileName = substr($savekey, strripos($savekey, '/') + 1);
        //
        $splInfo = $file->validate(['size' => $size])->move(ROOT_PATH . '/public' . $uploadDir, $fileName);
        if ($splInfo) {
            $params = array(
                'admin_id' => 0,
                'user_id' => (int)$this->auth->id,
                'filesize' => $fileInfo['size'],
                'imagewidth' => $imagewidth,
                'imageheight' => $imageheight,
                'imagetype' => $suffix,
                'imageframes' => 0,
                'mimetype' => $fileInfo['type'],
                'url' => $uploadDir . $splInfo->getSaveName(),
                'uploadtime' => time(),
                'storage' => 'local',
                'sha1' => $sha1,
            );
            $attachment = model("attachment");
            $attachment->data(array_filter($params));
            $attachment->save();
            \think\Hook::listen("upload_after", $attachment);
            $static_url = \config('static_url');
            $this->success(__('Upload successful'), [
                'url' => $uploadDir . $splInfo->getSaveName(),
                'http_url'=>$static_url.$uploadDir.$splInfo->getSaveName()
            ]);
        } else {
            // 上传失败获取错误信息
            $this->error($file->getError());
        }
    }
}
