<?php


namespace app\api\controller;
/**
 * 用户云挂机
 */

use think\Db;
use think\Exception;
use think\Request;

class Ygj extends Base
{

    public function __construct(Request $request = null)
    {
        parent::__construct($request);
    }

    /**
     * 高级倍投 方案添加
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function add_gjbt()
    {
        $param = $this->param;
        //倍投方案名
        $name = trim($param['name']);
        if (empty($name)) return $this->error('方案名不能为空');
        $userInfo = $this->userinfo;
        $is_cuznai = Db::name('gjbt')->where('user_id', $userInfo['id'])->where('name', $name)->find();
        if ($is_cuznai) return $this->error('该倍投方案名已存在');
        //写入数据库
        $re = Db::table('lsyl_gjbt')->insert([
            'name' => $name,
            'user_id' => $userInfo['id'],
            'createtime' => time(),
            'updatetime' => time()
        ]);
        if ($re) return $this->success('添加成功');
        return $this->error('添加失败');
    }

    /**
     * 添加高级倍投方案数据
     * @gjbt_id   倍投方案id
     * @number    局数
     * @multiple  倍数
     * @win       中后跳转
     * @no_win    挂后跳转
     *
     */
    public function add_gjbt_data()
    {
        $param = $this->param;
        $userinfo = $this->userinfo;
        $gjbt_id = $param['gjbt_id'];
        //局数
        $number = $param['number'];
        if ($number <= 0) return $this->error('局数需大于等于1');
        //倍数
        $multiple = $param['multiple'];
        if ($multiple < 1) return $this->error('倍数需大于等于1');
        //中后跳转
        $win = $param['win'];
        //挂后跳转
        $no_win = $param['no_win'];
        //查询是否 有该方案
        $gjbt = Db::table('lsyl_gjbt')->where('id', $gjbt_id)->where('user_id', $userinfo['id'])->count();
        if (!$gjbt) return $this->error('为查询到该方案');
        //方案数据（局数）
        $numbers = Db::table('lsyl_gjbt_data')->where('gjbt_id', $gjbt_id)->column('number');
        if (in_array($number, $numbers)) return $this->error('该方案的该局数已存在');
        $numbers[] = $number;
        if (!in_array($win, $numbers) || !in_array($no_win, $numbers)) return $this->error('没有该局数');
        $re = Db::table('lsyl_gjbt_data')->insert([
            'gjbt_id' => $gjbt_id,
            'number' => $number,
            'multiple' => $multiple,
            'win' => $win,
            'no_win' => $no_win,
            'createtime' => time(),
            'updatetime' => time()
        ]);
        if ($re) return $this->success('添加成功');
        return $this->error('添加失败');
    }

    /**
     * 添加云挂机方案
     * @name 方案名
     * @gjtype_id  云挂机方案类型
     * @lottery_id  彩种
     * @play_id  玩法
     * @betting_jk  投注监控:0=关闭,1=开启
     * @jk_gz 监控规则(中为1,挂为0) 001
     * @pt_type 倍投方式:1=直线倍投,2=高级倍投
     * @zx_bt 直线倍投 2 4 6 8 10
     * @gjbt_id  高级倍投
     * @fp_type 翻倍方式:1=中翻倍,2=挂翻倍
     * @zsyk_y 止损盈亏-赢:1=启用,0=不启用
     * @zsyk_y_value 止损盈亏-赢(值)
     * @zsyk_k 止损盈亏-亏:1=启用,0=不启用
     * @zsyk_k_value 止损盈亏-亏(值)
     * @unit 金额模式:元=元,角=角,分=分,厘=厘
     */
    public function add_hangplan()
    {
        $param = $this->param;

        $userinfo = $this->userinfo;
        $name = trim($param['name']);
        if (empty($name)) return $this->error('方案名不能为空');
        $name_cunzai = Db::table('lsyl_hangplan')->where('name', $name)->where('user_id', $userinfo['id'])->count();
        if ($name_cunzai > 0) return $this->error('该方案名已存在');
        $data = $this->ygjValidate($param,$userinfo);
        $re = Db::table('lsyl_hangplan')->insert($data);

        if ($re) return $this->success('创建成功');
        return $this->error('创建失败');
    }


    /**
     * 云挂机 添加  修改 参数验证
     * @param $param
     * @param $userinfo
     * @return array|void
     * @throws Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function ygjValidate($param,$userinfo){
        $name = trim($param['name']);
        if (empty($name)) return $this->error('方案名不能为空');


        $gjtype_id = $param['gjtype_id'];
        //云挂机方案类型

        $is_gjbt_cunzai = Db::table('lsyl_gjtype')->where('id', $gjtype_id)->count();
        if ($is_gjbt_cunzai <= 0) return $this->error('云挂机方案类型不存在');

        $lottery_id = $param['lottery_id'];
        //查询彩种是否开放
        $lottery = Db::table('lsyl_lottery')->where('id', $lottery_id)->find();
        if (!$lottery) return $this->error("该彩种不存在");
        if ($lottery['status'] == 0) return $this->error('该彩种已禁止');
        //玩法
        $play_id = $param['play_id'];
        $play = Db::table('lsyl_play')->where('id', $play_id)->where('lottery_id', $lottery_id)->find();
//        return $this->success('ok',$play);
        if (!$play) return $this->error('该玩法不存在');
        if ($play['is_group'] != 0) return $this->error('不是具体的玩法');
        if ($play['status'] == 0 || $play['is_ygj'] == 0) return $this->error('改玩法已禁止或不支持云挂机');
        //所选号码
        $data_num = $param['data_num'];
        if ($gjtype_id == '1') {
            //定码轮换
            $zhushu = $this->zs($data_num, $lottery_id, $play_id);
            if ($zhushu['count'] <= 0) return $this->error('该号码不能下注1');

        } elseif ($gjtype_id == '2') {
            //高级定码轮换
            if (count($data_num) <= 0) return $this->error('请添加号码');
            //获取所有的局数
            if (!is_array($data_num)) return $this->error('高级定码轮换的data_num参数为数组');
            $numbers = array_column($data_num, 'number');
            if (count($numbers) == 0) return $this->error('高级定码轮换号码格式错误');
            //去重的后的长度与原数组的长度比较
            if (count(array_unique($numbers)) != count($data_num)) return $this->error('局数不能重复');
            //局数必须大于0
            if (min($numbers) <= 0) return $this->error('局数需大于0');
            foreach ($data_num as $k => $v) {
                $zhushu = $this->zs($v['data_num'], $lottery_id, $play_id);
                //判断是有有一局的号码不能下注
                if ($zhushu['count'] <= 0) return $this->error($k . '该号码不能下注');

                $data_num[$k]['zs'] = $zhushu['count'];
                if (!in_array($v['win'], $numbers) || !in_array($v['no_win'], $numbers)) return $this->error('中、挂没有该局数');
                $data_num[$k]['number'] = (string)$v['number'];
                $data_num[$k]['win'] = (string)$v['win'];
                $data_num[$k]['no_win'] = (string)$v['no_win'];
                $data_num[$k]['zs'] = (string)$data_num[$k]['zs'];
            }
        }
        $unit = $param['unit'];
        if (!in_array($unit, ['元', '角', '分', '厘'])) return $this->error('单位错误');
        //投注监控
        $betting_jk = $param['betting_jk'];
        //投注监控规则
        $jk_gz = $param['jk_gz'];
        if ($betting_jk == 1) {
            //开启投注监控时 监控规则不能为空
            if (!is_numeric($jk_gz)) return $this->error('监控规则错误');
        }


        //倍投方式
        $pt_type = $param['pt_type'];
        //直线倍投
        $zx_bt = isset($param['zx_bt']) ? $param['zx_bt'] : '';
        $zx_bt = explode(' ', $zx_bt);
        if ($zx_bt) {
            foreach ($zx_bt as $k => $v) {
                if (is_numeric($v)) {
                    return $this->error('直线倍投输入非法');
                }
            }
        }
        $zx_bt = implode(',', $zx_bt);
        //高级倍投
        $gjbt_id = isset($param['gjbt_id']) ? $param['gjbt_id'] : 0;
        if ($pt_type == 1) {
            //直线倍投
            if (preg_match('/[a-zA-Z]/', $zx_bt) || preg_match('/[\x{4e00}-\x{9fa5}]/u', $zx_bt)) return $this->error('直线倍投格式错误');
            $gjbt_id = 0;
        } elseif ($pt_type == 2) {
            //高级倍投 （判断该高级倍投方案是否存在）
            $gjbt_iscunzai = Db::table('lsyl_gjbt')->where('id', $gjbt_id)->where('user_id', $userinfo['id'])->find();
            if (!$gjbt_iscunzai) return $this->error('该倍投方案不存在');
            $zx_bt = '';

        } else {
            return $this->error('倍投参数错误');
        }
        //翻倍方式:1=中翻倍,2=挂翻倍
        $fp_type = $param['fp_type'];
        //止损盈亏-赢:1=启用,0=不启用
        $zsyk_y = $param['zsyk_y'];
        //止损盈亏-赢(值)
        $zsyk_y_value = $param['zsyk_y_value'];
        //止损盈亏-亏:1=启用,0=不启用
        $zsyk_k = $param['zsyk_k'];
        //止损盈亏-亏(值)
        $zsyk_k_value = $param['zsyk_k_value'];
        if (!is_numeric($zsyk_y_value) || !is_numeric($zsyk_k_value)) return $this->error('止损盈亏的值为数字');
        $data = [
            'name' => $name,
            'gjtype_id' => $gjtype_id,
            'user_id' => $userinfo['id'],
            'status' => '0',
            'lottery_id' => $lottery_id,
            'play_id' => $play_id,
            'unit' => $unit,
            'betting_jk' => $betting_jk,
            'jk_gz' => $jk_gz,
            'pt_type' => $pt_type,
            'zx_bt' => $zx_bt,
            'gjbt_id' => $gjbt_id,
            'fp_type' => $fp_type,
            'zsyk_y' => $zsyk_y,
            'zsyk_y_value' => $zsyk_y_value,
            'zsyk_k' => $zsyk_k,
            'zsyk_k_value' => $zsyk_k_value,
            'remark' => '',
            'data_num' => json_encode($data_num, JSON_UNESCAPED_UNICODE),
            'createtime' => time(),
            'updatetime' => time()
        ];
        return $data;




    }



    /**
     * 修改云挂机方案
     * @id
     * @name 方案名
     * @gjtype_id  云挂机方案类型
     * @lottery_id  彩种
     * @play_id  玩法
     * @betting_jk  投注监控:0=关闭,1=开启
     * @jk_gz 监控规则(中为1,挂为0) 001
     * @pt_type 倍投方式:1=直线倍投,2=高级倍投
     * @zx_bt 直线倍投 2,4,6,8,10
     * @gjbt_id  高级倍投
     * @fp_type 翻倍方式:1=中翻倍,2=挂翻倍
     * @zsyk_y 止损盈亏-赢:1=启用,0=不启用
     * @zsyk_y_value 止损盈亏-赢(值)
     * @zsyk_k 止损盈亏-亏:1=启用,0=不启用
     * @zsyk_k_value 止损盈亏-亏(值)
     * @unit 金额模式:元=元,角=角,分=分,厘=厘
     */
    public function edit_hangplan()
    {
        $param = $this->param;
        $userinfo = $this->userinfo;
        $is_ygj_cunzai = Db::table('lsyl_hangplan')->where('id', $param['id'])->where('user_id', $userinfo['id'])->find();
        if (!$is_ygj_cunzai) return $this->error('该方案不存在');
        if ($is_ygj_cunzai['status'] != '0') return $this->error('该方案运行中不能修改');

        $name = trim($param['name']);
        if (empty($name)) return $this->error('方案名不能为空');
        $name_cunzai = Db::table('lsyl_hangplan')->where('name', $name)->where('id', '<>', $param['id'])->where('user_id', $userinfo['id'])->count();
        if ($name_cunzai > 0) return $this->error('该方案名已存在');
        $data = $this->ygjValidate($param,$userinfo);


        if($data['status'] == '1'){
            ygjLog($param['id'],'1','开启');
        }elseif ($data['status'] == '0'){
            ygjLog($param['id'],'0','修改方案时关闭');
        }
        $re = Db::table('lsyl_hangplan')->where('id', $param['id'])->update($data);
        if ($re) return $this->success('更新成功');
        return $this->error('更新失败');
    }

    /**
     * 更改运行状态
     * @id
     * @status 状态:1=运行,0=禁止
     */
    public function edit_ygj_status()
    {
        $param = $this->param;
        $id = $param['id'];
        $status = $param['status'];
        $userinfo = $this->userinfo;
        $ygj = Db::table('lsyl_hangplan')->where('id', $id)->where('user_id', $userinfo['id'])->find();
        if (!$ygj) return $this->error('改方案不存在');
        $re = Db::table('lsyl_hangplan')->where('id', $id)->update([
            'status' => $status,
            'updatetime' => time()
        ]);
        if ($status == '1') {
            ygjLog($param['id'], '1', '手动开启');
        } elseif ($status == '0') {
            ygjLog($param['id'], '0', '手动关闭');
        }
        if ($re) return $this->success('更新成功');
        return $this->error('更新失败');
    }

    /**
     * 根据彩种获取注数
     * @param $param   号码参数
     * @param $lottery_id 彩种id
     * @param $play_id  玩法id
     */
    public function zs($param, $lottery_id, $play_id)
    {


        try {
            $userinfo = $this->userinfo;
            $lotteryClass = new \lottery\Lottery();
            $re = $lotteryClass->getBet($lottery_id, $param, $play_id, $userinfo['ratio']);
            return $re;
        } catch (Exception $e) {

            return $this->error($e->getMessage());
        }


    }

    /**
     * 获取云挂机方案
     * @param page 分页
     * @lottery_id
     */
    public function getYgjList()
    {
        $userinfo = $this->userinfo;
        $param = $this->param;
        $limit = isset($param['limit']) ? $param['limit'] : 10;
        $page = isset($param['page']) ? $param['page'] : 1;
        $lottery_id = isset($param['lottery_id']) ? $param['lottery_id'] : 0;
        $where = [];
        if ($lottery_id != 0) {
            $where['lottery_id'] = $lottery_id;
        }

        $count = Db::table('lsyl_hangplan')->where($where)->where('user_id', $userinfo['id'])->count();
        $list = Db::table('lsyl_hangplan')->where($where)->where('user_id', $userinfo['id'])->page($page)->limit($limit)->select();
        if ($list) {
            foreach ($list as $k => $v) {
                $log = Db::table('lsyl_hangplan_log')->where('hangplan_id', $v['id'])->order('createtime desc')->find();
                $list[$k]['remarak_log'] = $log ? $log['remark'] : '';

                $list[$k]['createtime'] = date('Y-m-d H:i:s', $v['createtime']);
                $list[$k]['updatetime'] = date('Y-m-d H:i:s', $v['updatetime']);

                //直线倍投
                $zxbt = explode(',', $v['zx_bt']);
                $list[$k]['zx_bt'] = implode(' ', $zxbt);
                //彩票
                $lottery = Db::table('lsyl_lottery')->where('id', $v['lottery_id'])->find();
                $list[$k]['lottery_name'] = isset($lottery['name']) ? $lottery['name'] : '';
                //玩法
                $play = getPlay($v['play_id']);
//                if (count($play) == 4) {
//                    $list[$k]['param'] = $play[0];
//                    unset($play[0]);
//
//                }
                $list[$k]['play'] = implode("/", array_reverse($play));
                $list[$k]['play_ids'] = array_reverse(getPlay($v['play_id']));
                //查询方案类型
                $gjtype = Db::table('lsyl_gjtype')->where('id', $v['gjtype_id'])->find();
                $list[$k]['gjtype_name'] = isset($gjtype['name']) ? $gjtype['name'] : '';
                $data_num = json_decode($v['data_num'], true);
                $list[$k]['data_num'] = $data_num;
                if ($v['gjtype_id'] == '1') {
                    $da = [];
                    //定码轮换
                    $da[] = [
                        'data_num' => $data_num,
                        'data_num_str' => paramParsing($data_num)
                    ];
                    $list[$k]['data_num'] = $da;

                } elseif ($v['gjtype_id'] == '2') {
                    if ($data_num) {
                        foreach ($data_num as $k1 => $v1) {

                            $data_num[$k1]['data_num_str'] = paramParsing($v1['data_num']);
                            $data_num[$k1]['number'] = (string)$v1['number'];
                            $data_num[$k1]['win'] = (string)$v1['win'];
                            $data_num[$k1]['no_win'] = (string)$v1['no_win'];
                            $data_num[$k1]['zs'] = (string)$data_num[$k1]['zs'];
                        }
                    }

                    //高级定码轮换
                    $list[$k]['data_num'] = $data_num;
                }

            }
        }
        $re = [
            'data' => $list,
            'count' => $count
        ];
        return $this->success('云挂机方案', $re);
    }

    /**
     * 获取高级倍投 列表
     */
    public function gjbtList()
    {
        $userinfo = $this->userinfo;
        $param = $this->param;
        $limit = isset($param['limit']) ? $param['limit'] : 10;
        $page = isset($param['page']) ? $param['page'] : 1;
        $count = Db::table('lsyl_gjbt')->where('user_id', $userinfo['id'])->count();
        $data = Db::table('lsyl_gjbt')->where('user_id', $userinfo['id'])->limit($limit)->page($page)->select();
        if ($data) {
            foreach ($data as $k => $v) {
                $data[$k]['createtime'] = date('Y-m-d H:i:s', $v['createtime']);
                $data[$k]['updatetime'] = date('Y-m-d H:i:s', $v['updatetime']);
                //查询方案的数据
                $gjbt_data = Db::table('lsyl_gjbt_data')->where('gjbt_id', $v['id'])->order('number desc')->select();
                if ($gjbt_data) {
                    foreach ($gjbt_data as $k1 => $v1) {
                        $gjbt_data[$k1]['createtime'] = date('Y-m-d H:i:s', $v1['createtime']);
                        $gjbt_data[$k1]['updatetime'] = date('Y-m-d H:i:s', $v1['updatetime']);
                    }
                }
                $data[$k]['gjbt_data'] = $gjbt_data;
            }
        }
        $re = [
            'count' => $count,
            'data' => $data
        ];
        return $this->success('高级倍投列表', $re);
    }

    /**
     * app  获取倍投方案方案列表
     */
    public function gjbtListApp()
    {
        $userinfo = $this->userinfo;
        $param = $this->param;
        $limit = isset($param['limit']) ? $param['limit'] : 10;
        $page = isset($param['page']) ? $param['page'] : 1;
        $count = Db::table('lsyl_gjbt')->where('user_id', $userinfo['id'])->count();
        $data = Db::table('lsyl_gjbt')->where('user_id', $userinfo['id'])->limit($limit)->page($page)->select();
        if ($data) {
            foreach ($data as $k => $v) {
                $data[$k]['createtime'] = date('Y-m-d H:i:s', $v['createtime']);
                $data[$k]['updatetime'] = date('Y-m-d H:i:s', $v['updatetime']);
            }
        }
        $re = [
            'count' => $count,
            'data' => $data
        ];
        return $this->success('高级倍投列表', $re);
    }

    /**
     *  app 获取高级倍投方案具体数据
     * @param gjbt_id  高级倍投方案id
     */

    public function getGjbtData()
    {
        $userinfo = $this->userinfo;
        $param = $this->param;
        $is_cunzai = Db::table('lsyl_gjbt')->where('user_id', $userinfo['id'])->where('id', $param['gjbt_id'])->find();
        if (!$is_cunzai) return $this->error('该方案不存在');
        $data = Db::table('lsyl_gjbt_data')->where('gjbt_id', $param['gjbt_id'])->order('number desc')->select();
        if ($data) {
            foreach ($data as $k => $v) {
                $data[$k]['createtime'] = date('Y-m-d H:i:s', $v['createtime']);
                $data[$k]['updatetime'] = date('Y-m-d H:i:s', $v['updatetime']);
            }
        }
        return $this->success('高级倍投方案具体数据', $data);


    }

    /**
     * 删除高级倍投方案
     * @param gjbt_id  高级倍投方案id
     */
    public function delGjbt()
    {
        $userinfo = $this->userinfo;
        $param = $this->param;
        $gjbt_id = $param['gjbt_id'];
        //查询该高级倍投方案是否有
        $hangplan = Db::table('lsyl_hangplan')->where('user_id', $userinfo['id'])->where('gjbt_id', $gjbt_id)->where('pt_type', '2')->count();
        if ($hangplan > 0) return $this->error('该高级倍投方案，已有挂机方案在使用');
        // 启动事务
        Db::startTrans();
        try {
            Db::table('lsyl_gjbt')->where('id', $gjbt_id)->where('user_id', $userinfo['id'])->delete();
            Db::table('lsyl_gjbt_data')->where('gjbt_id', $gjbt_id)->delete();
            // 提交事务
            Db::commit();

        } catch (\Exception $e) {
            // 回滚事务
            Db::rollback();
            return $this->error('删除失败:' . $e->getMessage());
        }
        return $this->success('删除成功');
    }

    /**
     * 删除云挂机方案
     * @param hangplan_id 云挂机方案id
     */
    public function delYgj()
    {
        $userinfo = $this->userinfo;
        $param = $this->param;
        $hangplan_id = $param['hangplan_id'];
        $re = Db::table('lsyl_hangplan')->where('user_id', $userinfo['id'])->where('id', $hangplan_id)->delete();
        if ($re) return $this->success('删除成功');
        return $this->error('删除失败');
    }

    /**
     * 修改高级倍投方案
     * @param id 高级倍投方案id
     */
    public function editGjbt()
    {
        $userinfo = $this->userinfo;
        $param = $this->param;
        $id = $param['id'];
        $name = $param['name'];
        $is_cuznai = Db::table('lsyl_gjbt')->where('id', '<>', $id)->where('user_id', $userinfo['id'])->where('name', $name)->count();
        if ($is_cuznai) return $this->error('该方案名已存在');
        $re = Db::table('lsyl_gjbt')->where('id', $id)->update([
            'name' => $name,
            'updatetime' => time()
        ]);
        if ($re === false) return $this->error('修改失败');
        return $this->success('修改成功');


    }

    /**
     * 修改高级倍投方案数据
     * id 高级倍投方案数据id
     * gjbt_id  高级倍投id
     * number  局数
     * multiple  倍数
     * win 中后跳转
     * no_win 挂后跳转
     */
    public function editGjbtData()
    {
        $userinfo = $this->userinfo;
        $param = $this->param;
        $id = $param['id'];
        $gjbt_id = $param['gjbt_id'];
        $number = $param['number'];
        $multiple = $param['multiple'];
        $win = $param['win'];
        $no_win = $param['no_win'];
        $is_cunzai = Db::table('lsyl_gjbt')->where('id', $gjbt_id)->where('user_id', $userinfo['id'])->find();
        if (!$is_cunzai) return $this->error('没有该方案');
        $gjbt_data = Db::table('lsyl_gjbt_data')->where('id', '<>', $id)->where('gjbt_id', $gjbt_id)->select();
        $numbers = [];
        $numbers[] = (string)$number;
        if ($gjbt_data) {
            foreach ($gjbt_data as $k => $v) {
                if ($v['number'] == $number) return $this->error('该方案已有该局数');
                $numbers[] = (string)$v['number'];
            }

        }
        if (!in_array($win, $numbers) || !in_array($no_win, $numbers)) return $this->error('没查询到中、挂后跳转的局数');
        $re = Db::table('lsyl_gjbt_data')->where('id', $id)->where('gjbt_id', $gjbt_id)->update([
            'number' => $number,
            'multiple' => $multiple,
            'win' => $win,
            'no_win' => $no_win
        ]);
        if ($re) return $this->success('修改成功');
        return $this->error('修改失败');
    }

    /**
     * 云挂机方案运行记录
     * @hangplan_id  云挂机方案id
     *
     */
    public function ygjLog()
    {
        $userinfo = $this->userinfo;
        $param = $this->param;
        $hangplan_id = $param['hangplan_id'];
        $hangplan = Db::table('lsyl_hangplan')->where('id', $hangplan_id)->where('user_id', $userinfo['id'])->find();
        if (!$hangplan) return $this->error('没有查询到该方案');
        $data = Db::table('lsyl_hangplan_log')->where('hangplan_id', $hangplan_id)->order('createtime desc')->select();
        if ($data) {
            foreach ($data as $k => $v) {
                $data[$k]['createtime'] = date('Y-m-d H:i:s', $v['createtime']);
            }
        }
        return $this->success('云挂机方案运行记录', $data);


    }

}