<?php


namespace app\api\controller;

use think\Db;

/**
 *
 * Class Im
 * @package app\api\controller
 */
class Im extends Base
{
    /**
     * //获取消息列表
     * 好友id recipient_uid
     * @limit   查询条数
     * @page   页数
     *
     *
     */
    public function getMsgList(){
        $userinfo = $this->userinfo;
        $param = $this->param;
        $limit = isset($param['limit']) ? $param['limit'] : 10;
        $page = isset($param['page']) ? $param['page'] : 1;
        $recipient_uid = $param['recipient_uid'];
        //查询登录用户的好友
        $user_ids = Db::table('lsyl_user')->where('user_id',$userinfo['id'])->field('id')->column('id');
        $user_ids[] = $userinfo['user_id'];
        if(!in_array($recipient_uid,$user_ids)) return $this->error('该用户不是你好友');


        $count =  Db::table('lsyl_chatlog')->where('send_uid','in',[$userinfo['id'],$recipient_uid])->where('recipient_uid','in',[$userinfo['id'],$recipient_uid])->order('createtime desc')->count();

        $list = Db::table('lsyl_chatlog')->where('send_uid','in',[$userinfo['id'],$recipient_uid])->where('recipient_uid','in',[$userinfo['id'],$recipient_uid])->order('createtime desc')->limit($limit)->page($page)->select();
        if($list){
            foreach ($list as $k=>$v){
                $list[$k]['createtime'] = date('Y-m-d H:i:s',$v['createtime']);
            }
        }
        //状态变为已读
        Db::table('lsyl_chatlog')->where('send_uid','in',[$userinfo['id'],$recipient_uid])->where('recipient_uid','in',[$userinfo['id'],$recipient_uid])->update([
            'is_look'=>'1'
        ]);

        $data = [
            'list'=>$list,
            'count'=>$count
        ];
        return $this->success('消息',$data);




    }

}