<?php


namespace app\api\controller;


use think\Db;
use think\Log;

class Notify extends Common
{
    protected $noNeedLogin = ['*'];


    /**
     * supay 支付回调
     * status: 交易状态: 1 充值成功;
     * money: ⼈⺠币⾦额, 以分为单位
     * merchantBizNum: 商户业务单号
     * merchantId: 商户ID
     * sysBizNum: Supay 处理流⽔号
     * sign 签名
     */
    public function supayNotify()
    {
        $param = input('param.');
        $myfile = fopen("./notify/recharge_log.txt", "a") or die("Unable to open file!");
        $date = date('Y-m-d H:i:s') . "\n";
        fwrite($myfile, $date);
        $param1 = serialize($param) . "\n";
        fwrite($myfile, $param1);
        fclose($myfile);
        if ($param['status'] == 1) {

            $re = recharge_js($param['merchantBizNum'],'supay');
            //记录

            if($re['status']){
                return 'success';
            }else{
                return 'error';
            }

        }

    }



    /**
     * fp充值
     */
    public function fpNotify()
    {
        $param = input('param.');
        $myfile = fopen("./notify/recharge_log.txt", "a") or die("Unable to open file!");
        $date = date('Y-m-d H:i:s') . "\n";
        fwrite($myfile, $date);
        $param1 = serialize($param) . "\n";
        fwrite($myfile, $param1);
        fclose($myfile);
        if ($param['fxstatus'] == 1) {
             $re = recharge_js($param['fxddh'],'fp');
            //记录

            if($re['status']){
                return 'success';
            }else{
                return 'error';
            }


        }

    }
    /**
     * ifpay 充值回调
     * 序号	名称	类型	必填	备注
    1	userId	number	是	商户ID
    2	id	number	是	支付平台订单编号
    3	type	Stirng	是	receipt为收款单 payment为付款单
    4	state	number	是	订单状态：
    state=1 待买家付款
    state=2 待卖家确认
    state=3 交易完成
    state=4 交易取消
    5	updateTime	String	是	最近更新时间
    6	orderRemark	String	是	商户订单id
    7	orderAmount	BigDecimal	是	订单金额﻿
    注：签名时转字符窜，需去除末尾多余的零
    8	hmac	String(32)	是	验签数据
    详见1.5 HTTP参数签名机制
     */
    public function ifNotify(){

        $param = input('param.');
        $myfile = fopen("./notify/recharge_log.txt", "a") or die("Unable to open file!");
        $date = date('Y-m-d H:i:s') . "\n";
        fwrite($myfile, $date);
        $param1 = serialize($param) . "\n";
        fwrite($myfile, $param1);
        fclose($myfile);
        if ($param['state'] == 3) {
            $re = recharge_js($param['orderRemark'],'ifpay');
            //记录

            if($re['status']){
                return 'success';
            }else{
                return 'error';
            }

        }

    }

    /**
     * dora 支付回调
     */
    public function doraNotify(){
        $param = input('param.');
        $myfile = fopen("./notify/recharge_log.txt", "a") or die("Unable to open file!");
        $date = date('Y-m-d H:i:s') . "\n";
        fwrite($myfile, $date);
        $param1 = serialize($param) . "\n";
        fwrite($myfile, $param1);
        fclose($myfile);

        if ($param['company_order_id']) {
            $re = recharge_js($param['company_order_id'],'dora');
            //记录

            if($re['status']){
                return 'success';
            }else{
                return 'error';
            }


        }

    }
    /**
     * 鸿运通支付回调
     */
    public function hytNotify(){
        $param = input('param.');
        $myfile = fopen("./notify/recharge_log.txt", "a") or die("Unable to open file!");
        $date = date('Y-m-d H:i:s') . "\n";
        fwrite($myfile, $date);
        $param1 = serialize($param) . "\n";
        fwrite($myfile, $param1);
        fclose($myfile);
        $MerchantUniqueOrderId = $param['MerchantUniqueOrderId'];
        if($param['MerchantId'] != '205526') return;
        $re = recharge_js($MerchantUniqueOrderId,'hyt');
        //记录

        if($re['status']){
            return 'success';
        }else{
            return 'error';
        }


    }









    /**
     * supay 提现回调
     */
    public function supayTxNotify()
    {

        $param = input('param.');
        $myfile = fopen("./notify/txNotify.txt", "a") or die("Unable to open file!");
        $date = date('Y-m-d H:i:s') . "\n";
        fwrite($myfile, $date);
        $param1 = serialize($param) . "\n";
        fwrite($myfile, $param1);
        fclose($myfile);
        if ($param['status'] == 1) {
            Db::startTrans();
            try {
                $withdraw = Db::table('lsyl_withdraw')->where('txcode', $param['merchantBizNum'])->where('status', 5)->lock(true)->find();
                if ($withdraw) {
                    //验证签名
                    $is_cunzai = Db::table('lsyl_txsign')->where('withdraw_code', $withdraw['txcode'])->where('sys_biz_num', $param['sysBizNum'])->find();
                    //更改状态
                    if ($is_cunzai) {
                        Db::table('lsyl_withdraw')->where('txcode', $param['merchantBizNum'])->update([
                            'status' => 4,
                            'updatetime' => time()
                        ]);
                        updateUser($withdraw['user_id'], $withdraw['money'], '1', $withdraw['txcode'], time(), '提现');
                        Db::table('lsyl_txsign')->where('withdraw_code', $withdraw['txcode'])->where('sys_biz_num', $param['sysBizNum'])->delete();
                    }
                }
                // 提交事务
                Db::commit();
                return 'success';
            } catch (\Exception $e) {
                // 回滚事务
                Db::rollback();
                Log::write('提现回调日志信息:' . $param['merchantBizNum'] . '(' . $e->getMessage() . ')', '提现回调日志信息');
            }
        }
    }
    /**
     * 三和 提现回调
     */
    public function shTxNotify()
    {
        $param = input('param.');
        $myfile = fopen("./notify/txNotify.txt", "a") or die("Unable to open file!");
        $date = date('Y-m-d H:i:s') . "\n";
        fwrite($myfile, $date);
        $param1 = serialize($param) . "\n";
        fwrite($myfile, $param1);
        fclose($myfile);
        if ($param['retcode'] == '0000') {
            Db::startTrans();
            try {
                $withdraw = Db::table('lsyl_withdraw')->where('txcode', $param['orderNo'])->where('status', 5)->lock(true)->find();
                if ($withdraw) {

                    Db::table('cp_withdraw')->where('txcode', $param['orderNo'])->update([
                        'status' => 4,
                        'updatetime' => time()
                    ]);
                    updateUser($withdraw['user_id'], $withdraw['money'], '1', $withdraw['txcode'], time(), '提现');

                }
                // 提交事务
                Db::commit();

                return 'success';
            } catch (\Exception $e) {
                // 回滚事务
                Db::rollback();
                Log::write('提现回调日志信息:' . $param['merchantBizNum'] . '(' . $e->getMessage() . ')', '提现回调日志信息');
            }
        }
    }
    /**
     * 快乐付  提现回调
     * @return string
     */
    public function happyTxNotify(){

        $param = input('param.');
        $myfile = fopen("./notify/txNotify.txt", "a") or die("Unable to open file!");
        $date = date('Y-m-d H:i:s') . "\n";
        fwrite($myfile, $date);
        $param1 = serialize($param) . "\n";
        fwrite($myfile, $param1);
        fclose($myfile);

        if ($param['status'] == '1') {
            Db::startTrans();
            try {
                $withdraw = Db::table('cp_withdraw')->where('txcode', $param['orderNo'])->where('status', 5)->lock(true)->find();

                if ($withdraw) {

                    Db::table('cp_withdraw')->where('txcode', $param['orderNo'])->update([
                        'status' => 4,
                        'updatetime' => time()
                    ]);
//                    $sq_date = date('Y-m-d',$withdraw['createtime']);
//                    userYkJs('tx_money',$withdraw['user_id'],$withdraw['money'],$sq_date);
                    updateUser($withdraw['user_id'], $withdraw['money'], '1', $withdraw['txcode'], time(), '提现');

                }
                // 提交事务
                Db::commit();

                return 'success';
            } catch (\Exception $e) {
                // 回滚事务
                Db::rollback();
            }

        }
    }
    /**
     * fp提现回调
     */
    public function fpTxNotify(){
        $param = input('param.');
        $myfile = fopen("./notify/txNotify.txt", "a") or die("Unable to open file!");
        $date = date('Y-m-d H:i:s') . "\n";
        fwrite($myfile, $date);
        $param1 = serialize($param) . "\n";
        fwrite($myfile, $param1);
        fclose($myfile);
        if ($param['fxstatus'] == '1') {
            Db::startTrans();
            try {
                $withdraw = Db::table('cp_withdraw')->where('txcode', $param['fxddh'])->where('status', 5)->lock(true)->find();
                if ($withdraw) {
                    Db::table('cp_withdraw')->where('txcode', $param['fxddh'])->update([
                        'status' => 4,
                        'updatetime' => time()
                    ]);
                    updateUser($withdraw['user_id'], $withdraw['money'], '1', $withdraw['txcode'], time(), '提现');
                }
                // 提交事务
                Db::commit();

                return 'success';
            } catch (\Exception $e) {
                // 回滚事务
                Db::rollback();
                Log::write('提现回调日志信息:' . $param['merchantBizNum'] . '(' . $e->getMessage() . ')', '提现回调日志信息');
            }
        }
    }


    /**
     * 万众提现毁掉
     */
    public function wzTxNotify(){
        $param = input('param.');
        $myfile = fopen("./notify/txNotify.txt", "a") or die("Unable to open file!");
        $date = date('Y-m-d H:i:s') . "\n";
        fwrite($myfile, $date);
        $param1 = serialize($param) . "\n";
        fwrite($myfile, $param1);
        fclose($myfile);
        if ($param['code'] == '0' && $param['orderStatus'] == '1') {
            Db::startTrans();
            try {
                $withdraw = Db::table('cp_withdraw')->where('txcode', $param['orderNo'])->where('status', 5)->lock(true)->find();
                if ($withdraw) {
                    Db::table('cp_withdraw')->where('txcode', $param['orderNo'])->update([
                        'status' => 4,
                        'updatetime' => time()
                    ]);
                    updateUser($withdraw['user_id'], $withdraw['money'], '1', $withdraw['txcode'], time(), '提现');
                }
                // 提交事务
                Db::commit();

                return 'OK';
            } catch (\Exception $e) {
                // 回滚事务
                Db::rollback();
                Log::write('提现回调日志信息:' . $param['merchantBizNum'] . '(' . $e->getMessage() . ')', '提现回调日志信息');
            }
        }
    }





}