<?php


namespace app\common\validate;


use think\Validate;

class Bank extends Validate
{

    /**
     * 验证规则
     */
    protected $rule = [
        'realname'=>'require',
        'branch_name'=>'require',
        'bank_code'=>'unique:bank'
    ];
    /**
     * 提示消息
     */
    protected $message = [
        'realname.require'=>'真实姓名不能为空',
        'branch_name.require'=>'支行名不能为空',
        'bank_code.unique'=>'银行卡号已存在'
    ];
    /**
     * 验证场景
     */
    protected $scene = [
        'add'  => [],
        'edit' => [],
    ];

}