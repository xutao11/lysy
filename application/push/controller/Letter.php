<?php


namespace app\push\controller;


use think\Cache;
use think\cache\driver\Redis;
use think\Db;
use think\worker\Server;
use Workerman\Lib\Timer;

/**
 * 私信通知socket
 * Class Letter
 * @package app\push\controller
 */
class Letter extends Server
{
    protected $socket = 'websocket://0.0.0.0:1010';
    protected $processes = 1;
    public static $redis;


    /**
     * 收到信息
     * @param $connection
     * @param $data
     */
    public function onMessage($connection, $data)
    {
        $data = json_decode($data, true);
        if ($data['type'] == 'token') {
            $token = $data['data'];
            //验证token是
            $vali = Db::name('user_token')->where('token', $token)->where('expiretime', '>', time())->find();
            if (!$vali) {

                $msg = [
                    'type' => 'error',
                    'data' => [],
                    'msg' => 'token验证失败'
                ];
                $connection->send(json_encode($msg, JSON_UNESCAPED_UNICODE));


            } else {
                //token验证成功
                $user_id = $vali['user_id'];

                $connection->id = 'user_'.$user_id;

//
////                $letterCliList = cache('letterCliList');
////                self::$redis->redis_zd()->lpush("letterCliList", $connection->id);
//
////                //更新帮绑定
////                $letterCliList[$user_id] = $connection->id;
//                cache('letterCliList_'.$user_id, $connection->id);
                $msg = [
                    'type' => 'bind',
                    'data' => [],
                    'msg' => '绑定成功'
                ];
                $connection->send(json_encode($msg, JSON_UNESCAPED_UNICODE));

            }
        }
    }

    /**
     * 当连接建立时触发的回调函数
     * @param $connection
     */
    public function onConnect($connection)
    {



//        $connection->send('链接成功');
        // 向客户端发送hello $data
//        $connection->send('hello');

    }

    /**
     * 当连接断开时触发的回调函数
     * @param $connection
     */
    public function onClose($connection)
    {
//        $id  = $connection->id;
//        $id = explode('_',$id);
//        if($id[0] == 'user'){
//
//
//            Cache::rm('')
//
//
//        }
//
//        $letterCliList = cache('letterCliList');
//
//        $key = array_search($connection->id, $letterCliList);
//        if (isset($letterCliList[$key])) {
//
//            unset($letterCliList[$key]);
//
//
//            cache('letterCliList', $letterCliList);
//            echo '用户：' . $key . '==============离开' . "\n";
//        }


//        $connection->send('链接');

    }

    /**
     * 当客户端的连接上发生错误时触发
     * @param $connection
     * @param $code
     * @param $msg
     */
    public function onError($connection, $code, $msg)
    {
//        dump($connection);
        echo "error $code $msg\n";
    }


    /**
     * 每个进程启动
     * @param $worker
     */
    public function onWorkerStart($worker)
    {

        self::$redis = new Redis();
        self::$redis->redis_zd()->del('letter');


//        // 获取存储的数据并输出
//        $arList =self::$redis->redis_zd()->lrange("tutorial", 0 ,5);
//        dump($arList);
//        $last = self::$redis->redis_zd()->rPop('tutorial');
//        dump($last);
//        $arList =self::$redis->redis_zd()->lrange("tutorial", 0 ,5);
//        dump($arList);
////dump()

//        cache('letterCliList', '');
        Timer::add(1, function () use ($worker) {
//            $letterCliList = cache('letterCliList');
            //发送私信
            $this->sendLetter($worker);
            //发送通知
            $this->sendNotice($worker);




        });
    }

    /**
     * 发送私信
     */
    public function sendLetter($worker){
        $leng = self::$redis->redis_zd()->lLen('letter');
        if($leng>0){
            //取出最后一个
            $user_id = self::$redis->redis_zd()->rPop('letter');
            $send = false;
            foreach ($worker->connections as $connection){
                $con_id = $connection->id;
                $fj_id= explode('_',$con_id);
                if($fj_id[0] == 'user' && $user_id == $fj_id[1]){
                    $data = [
                        'type' => 'letter',
                        'data' => '1',
                        'msg' => '平台通知'
                    ];
                    $send = true;
                    $connection->send(json_encode($data, JSON_UNESCAPED_UNICODE));
                }
            }
            if($send == false){
                self::$redis->redis_zd()->lPush('letter',$user_id);
            }
        }
    }
    /**
     * 发送皮那台公告
     */
    public function sendNotice($worker){

        $notic = self::$redis->redis_zd()->lLen('notice');
        if($notic>0){
            //取出最后一个
            $da = self::$redis->redis_zd()->rPop('notice');
            $not = Db::name('notice')->field('id,remark')->find();
            if($not){
                foreach ($worker->connections as $connection){
                    $con_id = $connection->id;
                    $fj_id= explode('_',$con_id);
                    if($fj_id[0] == 'user'){
                        $data = [
                            'type' => 'notice',
                            'data' => $da,
                            'msg' => '平台公告',
                            'remark'=>$not['remark']
                        ];
                        $connection->send(json_encode($data, JSON_UNESCAPED_UNICODE));
                    }
                }
            }


        }


    }
}