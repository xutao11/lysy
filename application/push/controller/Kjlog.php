<?php


namespace app\push\controller;

use think\Db;
use think\worker\Server;
use Workerman\Lib\Timer;
use Workerman\MySQL\Connection;

/**
 * 开奖记录抓取
 * Class Worker
 * @package app\push\controller
 */
class Kjlog extends Server
{
    protected $socket = 'websocket://0.0.0.0:9999';
    protected $processes = 3;


    /**
     * 收到信息
     * @param $connection
     * @param $data
     */
    public function onMessage($connection, $data)
    {
    }

    /**
     * 当连接建立时触发的回调函数
     * @param $connection
     */
    public function onConnect($connection)
    {
//        $connection->send('链接成功');
        // 向客户端发送hello $data
//        $connection->send('hello');

    }

    /**
     * 当连接断开时触发的回调函数
     * @param $connection
     */
    public function onClose($connection)
    {
//        $connection->send('链接');

    }

    /**
     * 当客户端的连接上发生错误时触发
     * @param $connection
     * @param $code
     * @param $msg
     */
    public function onError($connection, $code, $msg)
    {
        echo "error $code $msg\n";
    }

    /**
     * 每个进程启动
     * @param $worker
     */
    public function onWorkerStart($worker)
    {  // 每2.5秒执行一次
        global $db;
        $db = new Connection('127.0.0.1', '3306', 'lsyl', 'root', 'lsyl');

        Timer::add(1,function () use ($db,$worker){
            echo $worker->id."进程号".date('Y-m-d H:i:s')."\n";
            if($worker->id == 1){
                //开奖抓取
                $this->getKj($db);
            }elseif ($worker->id ==0){
                //推送
                $newkj = cache('new_kj');
                if(!$newkj){
                    $newkj = $db->query("select lottery_id,pre_draw_issue,pre_draw_code,play_name from lsyl_newkj");
                    cache('new_kj',$newkj);
                }
                foreach ($worker->connections as $connection){
                    $connection->send(json_encode($newkj,JSON_UNESCAPED_UNICODE));
                }
            }elseif ($worker->id == 2){
                //长龙
                $lottery_ids = ['1','2','3','4','5','6','7'];
                foreach ($lottery_ids as $v) {
                    echo('彩种：'.$v."====");
                    //程序运行开始时间
                    $startTime = explode(' ', microtime());
                    longDragonNumber($v, $db);
                    //程序运行结束时间
                    $endTime = explode(' ', microtime());
                    echo '执行耗时：' . round($endTime[0] + $endTime[1] - ($startTime[0] + $startTime[1]), 4) . ' 秒。'."\n";
                }
            }
        });
    }

    /**
     * 开奖抓取
     * @param $db
     */
    public function getKj($db){
        if(date('s',time()) ==20 || date('s',time()) ==10|| date('s',time()) ==30 || date('s',time()) ==40|| date('s',time()) ==50|| date('s',time()) ==0){
            $cp_lottery = $db->select('id,status,url,name,url_b')->from('lsyl_lottery')->where("status= '1'")->query();
//            $cp_lottery = $db->select('id,status,url,name,url_b')->from('lsyl_lottery')->where("id in (13,14)")->query();
            if($cp_lottery){
                foreach ($cp_lottery as $k=>$v){
                    if($v['url_b'] == '') continue;
                    $data = curl_get($v['url_b']);
                    if ($data == false) continue;
                    $data = json_decode($data, true);
//                    dump($data);
                    if(in_array($v['id'],[1,2,3,6,7,9,10,11,13,14])){

                        //多彩
                        //https://www.manycai.com/index/apilist
                        //多彩网账户：ls8888  密码：ls123456多彩网账户：ls8888  密码：ls123456
                        if ($data) {
                            foreach ($data as $k1 => $v1) {
                                $re_data = [];
                                /**
                                 * //重庆时时彩  新疆时时彩 江西11选5
                                 *  {
                                "issue": "20200730042",
                                "opendate": "2020-07-30 18:10:00",
                                "code": "3,0,4,5,4",
                                "lotterycode": "CQSSC",
                                "officialissue": "200730042"
                                },
                                 */
                                if(in_array($v['id'],[1,2,10])){
                                    $re_data = [
                                        'pre_draw_issue' => $v1['issue'],   //期数
                                        'pre_draw_time' => $v1['opendate'],   //开奖时间
                                        'lottery_id' => $v['id'],                                          //彩票种类
                                        'pre_draw_code' => $v1['code'],   //开奖号码
                                        'createtime' => time()
                                    ];


                                }elseif (in_array($v['id'],[3,6,7])){
                                    /**
                                     * 奇趣1 河内1 河内5
                                     *  {
                                    "issue": "20200730-1110",
                                    "opendate": "2020-07-30 18:30:00",
                                    "code": "3,8,5,9,8",
                                    "lotterycode": "PTXFFC",
                                    "officialissue": "20200730-1110",
                                    "cur_online": "314328598"
                                    },
                                     */
                                    $re_data = [
                                        'pre_draw_issue' => $v1['issue'],   //期数
                                        'pre_draw_time' => $v1['opendate'],   //开奖时间
                                        'lottery_id' => $v['id'],                                          //彩票种类
                                        'pre_draw_code' => $v1['code'],   //开奖号码
                                        'createtime' => time()
                                    ];
                                    $pqisu = explode('-',$v1['issue']);
                                    $re_data['pre_draw_issue'] =implode('',$pqisu);


                                }elseif (in_array($v['id'],[9,13])){
                                    /**
                                     * 广东11选5  江苏快三
                                     *  {
                                    "issue": "200730028",
                                    "opendate": "2020-07-30 18:30:00",
                                    "code": "2,1,4,11,6",
                                    "lotterycode": "GD11X5",
                                    "officialissue": "20073028"
                                    },
                                     */
                                    $re_data = [
                                        'pre_draw_issue' => '20'.$v1['issue'],   //期数
                                        'pre_draw_time' => $v1['opendate'],   //开奖时间
                                        'lottery_id' => $v['id'],                                          //彩票种类
                                        'pre_draw_code' => $v1['code'],   //开奖号码
                                        'createtime' => time()
                                    ];
                                }elseif (in_array($v['id'],[14])){
                                    /**
                                     * 广西快三
                                     *     {
                                    "issue": "2020073028",
                                    "opendate": "2020-07-30 18:30:00",
                                    "code": "1,1,2",
                                    "lotterycode": "GXKS",
                                    "officialissue": "20200730028"
                                    },
                                     */
                                    $re_data = [
                                        'pre_draw_issue' => $v1['officialissue'],   //期数
                                        'pre_draw_time' => $v1['opendate'],   //开奖时间
                                        'lottery_id' => $v['id'],                                          //彩票种类
                                        'pre_draw_code' => $v1['code'],   //开奖号码
                                        'createtime' => time()
                                    ];
                                }elseif (in_array($v['id'],[11])){
                                    /**
                                     * 山东11选5
                                     *  {
                                    "issue": "20073031",
                                    "opendate": "2020-07-30 19:01:00",
                                    "code": "9,1,8,7,6",
                                    "lotterycode": "SD11X5",
                                    "officialissue": "20073031"
                                    },
                                     */

                                    $re_data = [
                                        'pre_draw_issue' => '',   //期数
                                        'pre_draw_time' => $v1['opendate'],   //开奖时间
                                        'lottery_id' => $v['id'],                                          //彩票种类
                                        'pre_draw_code' => $v1['code'],   //开奖号码
                                        'createtime' => time()
                                    ];
                                    $qisu =  substr($v1['issue'],6,strlen($v1['issue']));
                                    $re_data['pre_draw_issue'] = '20'.substr($v1['issue'],0,6).sprintf("%03d", $qisu);
                                }
                                $this->dataUp($re_data,$v['id'],$v['name'],$db,'多彩网');
                            }
                        }
                    }else{
                        if (isset($data['data']) && $data['data']) {
                            foreach ($data['data'] as $k1=>$v1){
                                $re_data = [
                                    'pre_draw_issue' => $v1['expect'],   //期数
                                    'pre_draw_time' => $v1['opentime'],   //开奖时间
                                    'lottery_id' => $v['id'],                                          //彩票种类
                                    'pre_draw_code' => $v1['opencode'],   //开奖号码
                                    'createtime' => time()
                                ];
                                $this->dataUp($re_data,$v['id'],$v['name'],$db,'博弈网');

                            }
                        }
                    }
                }
            }
        }
    }



    /**
     * @param $re_date 开奖号码
     * @param $lottery_id  //彩种ID
     * @param $lottery_name //彩种名
     * @param $db //数据库
     * @param $pt 平台
     */
    public function dataUp($re_data,$lottery_id,$lottery_name,$db,$pt){
        $str ="SELECT id,lottery_id,pre_draw_issue,pre_draw_code FROM `lsyl_kjlog` WHERE pre_draw_issue = '".$re_data['pre_draw_issue']."' AND lottery_id = ".$lottery_id;
        $is_cunzai = $db->query($str);
        if (count($is_cunzai) == 0) {
            $db->insert('lsyl_kjlog')->cols($re_data)->query();
            //查询有无
            $kj_new = "SELECT lottery_id,pre_draw_issue,pre_draw_code FROM `lsyl_newkj` WHERE lottery_id = " . $lottery_id;
            $is_cunzai = $db->query($kj_new);
            $data = [
                'pre_draw_issue' => $re_data['pre_draw_issue'],   //期数
                'pre_draw_time' => $re_data['pre_draw_time'],   //开奖时间
                //彩票种类
                'pre_draw_code' => $re_data['pre_draw_code'],   //开奖号码
                'play_name' => kj_data_play($re_data['pre_draw_code'])

            ];
            if (count($is_cunzai) > 0 && $re_data['pre_draw_issue'] > $is_cunzai[0]['pre_draw_issue']) {
                //更新
                $db->update('lsyl_newkj')->cols($data)->where('lottery_id=' . $lottery_id)->query();
                $new_kj = $db->query("select lottery_id,pre_draw_issue,pre_draw_code,play_name from lsyl_newkj");
                cache('new_kj',$new_kj);
                //遗漏值计算
                if(in_array($lottery_id,[1,2,3,4,5,6,7,9,10,11,12])){
                    $this->kjyl($lottery_id,$db);
                    $this->kjyl_zx($lottery_id,$db);
                }

            } else if (count($is_cunzai) == 0) {
                $data['lottery_id'] = $re_data['lottery_id'];  //彩票种类
                //写入
                $db->insert('lsyl_newkj')->cols($data)->query();
                $new_kj = $db->query("select lottery_id,pre_draw_issue,pre_draw_code,play_name from lsyl_newkj");
                cache('new_kj',$new_kj);
                //遗漏值计算
                if(in_array($lottery_id,[1,2,3,4,5,6,7,9,10,11,12])){
                    $this->kjyl($lottery_id,$db);
                    $this->kjyl_zx($lottery_id,$db);
                }
            }
            echo $pt.'----------'.$lottery_name . '------------' . $re_data['pre_draw_issue'] . '-----------' . $re_data['pre_draw_code'] . "-----------" . $re_data['pre_draw_time'] . "\n";
        }

    }


    /**
     * 遗漏值(组选)
     * @param $lottery_id 彩种id
     *
     */
    public function kjyl_zx($lottery_id,$db){

        echo '遗漏(组选)'.$lottery_id."\n";
        //查询彩种
        //查询该彩种最新的开奖号码
        $kjlog = $db->query("select pre_draw_issue,pre_draw_code from lsyl_kjlog where lottery_id = {$lottery_id} order by pre_draw_issue desc limit 1");
        if(!$kjlog) return;
        $kjlog = $kjlog[0];
        //查询遗漏
        $yl = $db->query("select * from lsyl_kjyl where lottery_id = {$lottery_id} and type='0' order by pre_draw_issue desc limit 1");
        if(!$yl) return;
        $yl = $yl[0];
        $yl_value = json_decode($yl['yl_value'], true);
        if (!$yl) return;
        if ($yl['pre_draw_issue'] >= $kjlog['pre_draw_issue']) return;
        //查询相差的期数
        $kjlogs = $db->query("select pre_draw_issue,pre_draw_code from lsyl_kjlog where lottery_id = {$lottery_id} and pre_draw_issue > {$yl['pre_draw_issue']} and pre_draw_issue <= {$kjlog['pre_draw_issue']} order by pre_draw_issue asc");
        if (!$kjlogs) return;
        $pre_draw_issue_new = $yl['pre_draw_issue'];
        foreach ($kjlogs as $k1 => $v1) {
            $pre_draw_issue_new = $v1['pre_draw_issue'];
            $pre_draw_issue = explode(',', $v1['pre_draw_code']);

            //循环遗漏值
            foreach ($yl_value as $k2=>$v2){
                if(in_array($k2,$pre_draw_issue)){
                    $yl_value[$k2]=0;

                }else{
                    $yl_value[$k2]++;
                }



            }

        }
        $yl_value = json_encode($yl_value);
        $yl_value = addslashes($yl_value);
        $str = "UPDATE lsyl_kjyl SET yl_value = '{$yl_value}',pre_draw_issue={$pre_draw_issue_new} WHERE lottery_id = {$lottery_id} and type='0'";

        $db->query($str);



    }

    /**
     * 遗漏值(单号)
     * @param $lottery_id 彩种id
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     */
    public function kjyl($lottery_id,$db){

        echo '遗漏'.$lottery_id."\n";
        //查询彩种
        //查询该彩种最新的开奖号码
        $kjlog = $db->query("select pre_draw_issue,pre_draw_code from lsyl_kjlog where lottery_id = {$lottery_id} order by pre_draw_issue desc limit 1");
        if(!$kjlog) return;
            $kjlog = $kjlog[0];
            //查询遗漏
            $yl = $db->query("select * from lsyl_kjyl where lottery_id = {$lottery_id} and type='1' order by pre_draw_issue desc limit 1");
            if(!$yl) return;
            $yl = $yl[0];
            $yl_value = json_decode($yl['yl_value'], true);
            if (!$yl) return;
            if ($yl['pre_draw_issue'] >= $kjlog['pre_draw_issue']) return;
            //查询相差的期数
            $kjlogs = $db->query("select pre_draw_issue,pre_draw_code from lsyl_kjlog where lottery_id = {$lottery_id} and pre_draw_issue > {$yl['pre_draw_issue']} and pre_draw_issue <= {$kjlog['pre_draw_issue']} order by pre_draw_issue asc");
            if (!$kjlogs) return;
            $pre_draw_issue_new = $yl['pre_draw_issue'];
            foreach ($kjlogs as $k1 => $v1) {
                $pre_draw_issue_new = $v1['pre_draw_issue'];
                $pre_draw_issue = explode(',', $v1['pre_draw_code']);
                foreach ($pre_draw_issue as $k2 => $v2) {
                    foreach ($yl_value[$k2 + 1] as $k3 => $v3) {
                        if ((int)$v2 == (int)$k3) {
                            //遗漏值为0
                            $yl_value[$k2 + 1][$k3] = 0;
                        } else {
                            $yl_value[$k2 + 1][$k3]++;
                        }
                    }
                }
            }
            $yl_value = json_encode($yl_value);
            $yl_value = addslashes($yl_value);
            $str = "UPDATE lsyl_kjyl SET yl_value = '{$yl_value}',pre_draw_issue={$pre_draw_issue_new} WHERE lottery_id = {$lottery_id} and type='1'";

            $db->query($str);



    }

//
//    public function onWorkerStart2($worker)
//    {  // 每2.
//        global $db;
//        $db = new Connection('127.0.0.1', '3306', 'root', 'root', 'lsyl');
//        Timer::add(3, function () use ($worker, $db) {
//            echo date('Y-m-d H:i:s') . "\n";
//            //查询彩票票
//            $lottery = $db->select('id,status,name,url,url_b')->from('lsyl_lottery')->where("status = '1'")->query();
//            if ($lottery) {
//                foreach ($lottery as $k => $v) {
//                    $url = $v['url_b'];
//                    if ($url == '') continue;
//
//                    $data = curl_get($url);
//                    //主线路访问出错 就切换为备用
////                    if ($data == false && !empty($v['url_b'])) {
////                        $data = curl_get($v['url_b']);
////
////                    }
////                    dump($data);
//                    //备用线路出问题就跳出奔驰循环
//                    if ($data == false) continue;
//                    $data = json_decode($data, true);
//
//                    if ($data) {
//                        foreach ($data as $k1 => $v1) {
////                            dump($is_cunzai);
//
//                            /**
//                             * //重庆时时彩  新疆时时彩 江西11选5
//                             *  {
//                            "issue": "20200730042",
//                            "opendate": "2020-07-30 18:10:00",
//                            "code": "3,0,4,5,4",
//                            "lotterycode": "CQSSC",
//                            "officialissue": "200730042"
//                            },
//                             */
//                            if(in_array($v['id'],[1,2,10])){
//                                $re_data = [
//                                    'pre_draw_issue' => $v1['issue'],   //期数
//                                    'pre_draw_time' => $v1['opendate'],   //开奖时间
//                                    'lottery_id' => $v['id'],                                          //彩票种类
//                                    'pre_draw_code' => $v1['code'],   //开奖号码
//                                    'createtime' => time()
//                                ];
//
//
//                            }elseif (in_array($v['id'],[3,6,7])){
//                                /**
//                                 * 奇趣1 河内1 河内5
//                                 *  {
//                                "issue": "20200730-1110",
//                                "opendate": "2020-07-30 18:30:00",
//                                "code": "3,8,5,9,8",
//                                "lotterycode": "PTXFFC",
//                                "officialissue": "20200730-1110",
//                                "cur_online": "314328598"
//                                },
//                                 */
//                                $re_data = [
//                                    'pre_draw_issue' => $v1['issue'],   //期数
//                                    'pre_draw_time' => $v1['opendate'],   //开奖时间
//                                    'lottery_id' => $v['id'],                                          //彩票种类
//                                    'pre_draw_code' => $v1['code'],   //开奖号码
//                                    'createtime' => time()
//                                ];
//                                $pqisu = explode('-',$v1['issue']);
//
//                                $re_data['pre_draw_issue'] =implode('',$pqisu);
//
//
//                            }elseif (in_array($v['id'],[9,13])){
//                                /**
//                                 * 广东11选5  江苏快三
//                                 *  {
//                                "issue": "200730028",
//                                "opendate": "2020-07-30 18:30:00",
//                                "code": "2,1,4,11,6",
//                                "lotterycode": "GD11X5",
//                                "officialissue": "20073028"
//                                },
//                                 */
//                                $re_data = [
//                                    'pre_draw_issue' => '20'.$v1['issue'],   //期数
//                                    'pre_draw_time' => $v1['opendate'],   //开奖时间
//                                    'lottery_id' => $v['id'],                                          //彩票种类
//                                    'pre_draw_code' => $v1['code'],   //开奖号码
//                                    'createtime' => time()
//                                ];
//                            }elseif (in_array($v['id'],[14])){
//                                /**
//                                 * 广西快三
//                                 *     {
//                                "issue": "2020073028",
//                                "opendate": "2020-07-30 18:30:00",
//                                "code": "1,1,2",
//                                "lotterycode": "GXKS",
//                                "officialissue": "20200730028"
//                                },
//                                 */
//                                $re_data = [
//                                    'pre_draw_issue' => $v1['officialissue'],   //期数
//                                    'pre_draw_time' => $v1['opendate'],   //开奖时间
//                                    'lottery_id' => $v['id'],                                          //彩票种类
//                                    'pre_draw_code' => $v1['code'],   //开奖号码
//                                    'createtime' => time()
//                                ];
//                            }elseif (in_array($v['id'],[11])){
//                                /**
//                                 * 山东11选5
//                                 *  {
//                                "issue": "20073031",
//                                "opendate": "2020-07-30 19:01:00",
//                                "code": "9,1,8,7,6",
//                                "lotterycode": "SD11X5",
//                                "officialissue": "20073031"
//                                },
//                                 */
//
//                                $re_data = [
//                                    'pre_draw_issue' => '20'.$v1['issue'],   //期数
//                                    'pre_draw_time' => $v1['opendate'],   //开奖时间
//                                    'lottery_id' => $v['id'],                                          //彩票种类
//                                    'pre_draw_code' => $v1['code'],   //开奖号码
//                                    'createtime' => time()
//                                ];
//
//                                $qisu =  substr($v1['issue'],5,strlen($v1['issue'])-1);
//                                $re_data['pre_draw_issue'] = '20'.substr($v1['issue'],0,5).sprintf("%04d", $qisu);
//
//
//
//
//                            }
//
//
//
//
//
//
//
//
//                            //https://www.zhuyingtj.com
//                            //账号：ls8888   密码：ls123456
//
//
//                            $str = "SELECT id,lottery_id,pre_draw_issue,pre_draw_code FROM `lsyl_kjlog` WHERE pre_draw_issue = '" . $re_data['pre_draw_issue'] . "' AND lottery_id = " . $v['id'];
//                            $is_cunzai = $db->query($str);
//                            if (count($is_cunzai) == 0) {
//                                $db->insert('lsyl_kjlog')->cols($re_data)->query();
//                                //查询有无
//                                $kj_new = "SELECT lottery_id,pre_draw_issue,pre_draw_code FROM `lsyl_newkj` WHERE lottery_id = " . $v['id'];
//                                $is_cunzai = $db->query($kj_new);
//                                if (count($is_cunzai) > 0 && $re_data['pre_draw_issue'] > $is_cunzai[0]['pre_draw_issue']) {
//                                    $data = [
//                                        'pre_draw_issue' => $re_data['pre_draw_issue'],   //期数
//                                        'pre_draw_time' => $re_data['pre_draw_time'],   //开奖时间
//                                        //彩票种类
//                                        'pre_draw_code' => $re_data['pre_draw_code'],   //开奖号码
//
//                                    ];
//                                    $db->update('lsyl_newkj')->cols($data)->where('lottery_id=' . $v['id'])->query();
//                                } else if (count($is_cunzai) == 0) {
//                                    $data = [
//                                        'pre_draw_issue' => $re_data['pre_draw_issue'],   //期数
//                                        'pre_draw_time' => $re_data['pre_draw_time'],   //开奖时间
//                                        'lottery_id' => $re_data['lottery_id'],                                          //彩票种类
//                                        'pre_draw_code' => $re_data['pre_draw_code'],   //开奖号码
//
//                                    ];
//                                    $db->insert('lsyl_newkj')->cols($data)->query();
//                                }
//                                echo $v['name'] . '------------' . $re_data['pre_draw_issue'] . '-----------' . $re_data['pre_draw_code'] . "-----------" . $re_data['pre_draw_time'] . "\n";
//                            }
//                        }
//                    }
//
//
//                }
//
//
//            }
//
//
//        });
//
//
//    }
//
//
//    public function onWorkerStart1($worker)
//    {  // 每2.
//        global $db;
//        $db = new Connection('127.0.0.1', '3306', 'root', 'root', 'lsyl');
//        Timer::add(3, function () use ($worker, $db) {
//            echo date('Y-m-d H:i:s') . "\n";
//            //查询彩票票
//            $lottery = $db->select('id,status,name,url,url_b')->from('lsyl_lottery')->where("status = '1'")->query();
//            if ($lottery) {
//                foreach ($lottery as $k => $v) {
//                    $url = $v['url'] ? $v['url'] : $v['url_b'];
//                    if ($url == '') continue;
//
//                    $data = curl_get($url);
//                    //主线路访问出错 就切换为备用
//                    if ($data == false && !empty($v['url_b'])) {
//                        $data = curl_get($v['url_b']);
//
//                    }
////                    dump($data);
//                    //备用线路出问题就跳出奔驰循环
//                    if ($data == false) continue;
//                    $data = json_decode($data, true);
//                    if (isset($data['data']) && $data['data']) {
//                        foreach ($data['data'] as $k1 => $v1) {
////                            dump($is_cunzai);
//
//                            //https://www.zhuyingtj.com
//                            //账号：ls8888   密码：ls123456
//                            $re_data = [
//                                'pre_draw_issue' => $v1['expect'],   //期数
//                                'pre_draw_time' => $v1['opentime'],   //开奖时间
//                                'lottery_id' => $v['id'],                                          //彩票种类
//                                'pre_draw_code' => $v1['opencode'],   //开奖号码
//                                'createtime' => time()
//                            ];
//
//                            $str = "SELECT id,lottery_id,pre_draw_issue,pre_draw_code FROM `lsyl_kjlog` WHERE pre_draw_issue = '" . $re_data['pre_draw_issue'] . "' AND lottery_id = " . $v['id'];
//                            $is_cunzai = $db->query($str);
//                            if (count($is_cunzai) == 0) {
//                                $db->insert('lsyl_kjlog')->cols($re_data)->query();
//                                //查询有无
//                                $kj_new = "SELECT lottery_id,pre_draw_issue,pre_draw_code FROM `lsyl_newkj` WHERE lottery_id = " . $v['id'];
//                                $is_cunzai = $db->query($kj_new);
//                                if (count($is_cunzai) > 0 && $re_data['pre_draw_issue'] > $is_cunzai[0]['pre_draw_issue']) {
//                                    $data = [
//                                        'pre_draw_issue' => $re_data['pre_draw_issue'],   //期数
//                                        'pre_draw_time' => $re_data['pre_draw_time'],   //开奖时间
//                                        //彩票种类
//                                        'pre_draw_code' => $re_data['pre_draw_code'],   //开奖号码
//
//                                    ];
//                                    $db->update('lsyl_newkj')->cols($data)->where('lottery_id=' . $v['id'])->query();
//                                } else if (count($is_cunzai) == 0) {
//                                    $data = [
//                                        'pre_draw_issue' => $re_data['pre_draw_issue'],   //期数
//                                        'pre_draw_time' => $re_data['pre_draw_time'],   //开奖时间
//                                        'lottery_id' => $re_data['lottery_id'],                                          //彩票种类
//                                        'pre_draw_code' => $re_data['pre_draw_code'],   //开奖号码
//
//                                    ];
//                                    $db->insert('lsyl_newkj')->cols($data)->query();
//                                }
//                                echo $v['name'] . '------------' . $re_data['pre_draw_issue'] . '-----------' . $re_data['pre_draw_code'] . "-----------" . $re_data['pre_draw_time'] . "\n";
//                            }
//                        }
//                    }
//
//
//                }
//
//
//            }
//
//
//        });
//
//
//    }

}