<?php
namespace app\push\controller;
use betting\Betting;
use think\worker\Server;
use Workerman\Lib\Timer;
use ygj\Ygj;

class Timingtask extends Server
{


    protected $socket = '';
    protected $processes = 1;


    /**
     * 收到信息
     * @param $connection
     * @param $data
     */
    public function onMessage($connection, $data)
    {
    }

    /**
     * 当连接建立时触发的回调函数
     * @param $connection
     */
    public function onConnect($connection)
    {
//        $connection->send('链接成功');
        // 向客户端发送hello $data
//        $connection->send('hello');

    }

    /**
     * 当连接断开时触发的回调函数
     * @param $connection
     */
    public function onClose($connection)
    {
//        $connection->send('链接');

    }

    /**
     * 当客户端的连接上发生错误时触发
     * @param $connection
     * @param $code
     * @param $msg
     */
    public function onError($connection, $code, $msg)
    {
        echo "error $code $msg\n";
    }

    /**
     * 每个进程启动
     * @param $worker
     */
    public function onWorkerStart($worker)
    {  // 每2.
        $bettingClass = new Betting();

        $ygjClass = new Ygj();
        Timer::add(2, function () use ($worker,$bettingClass,$ygjClass) {



            echo date('Y-m-d H:i:s') . "\n";
//周期
            createCycle();


            /**
                 * 平台总号（可以投注）
             *
             * 1998+0.05%工资   投注额 *0.05% （每天结算前一天）
             *
             * 线路总号 （可以投注）
             *
             * 1998+0.05%工资   投注额 *0.05% （每天结算前一天）
             */
            wages();
            /**
             *
             * 股东（可以投注）
             *
             * 1996+日亏损分红不累计。  (每天计算 前一天）（所有下级）
             *
             * 投注量        亏损金额（万）                日结亏损分红比例
             * >1                >0                                        0.5%。
             * >1                >10                                      1%
             *
             */
            gdbonus();

            /**
             * 主管 （可以投注）（可以开同级）  用户还是分代理  和普通用户
             *
             * 1994
             *
             * 亏损分红：每月的1号16号发放分红，同ip无效，周期不累计。有效会员：周期内投注有6  天分别超过1000元。
             * 周期消费量（万）                                                        有效会员（个）  分红比例
             * 10                                                                      0                    3%
             * 30                                                                      1                    5%
             * 60                                                                      2                    8%
             * 100                                                                    3                   12%
             * 200                                                                    8                   16%
             * 500                                                                   15                  20%
             * 1000                                                                 20                  25%
             * 2000                                                                 30                  30%
             * 3000                                                                 50                  35%
             * 4000                                                                 80                  40%
             *
             */
            zgbonus();


            /**
             * 计算开奖
             */
            $bettingClass->kjJisuan();
            /**
             * 云挂机
             */
//            $ygjClass->zdtz();






        });


    }

}