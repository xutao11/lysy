<?php


namespace app\push\controller;


use Workerman\MySQL\Connection;
use think\worker\Server;
use Workerman\Lib\Timer;

class Longdragon extends Server
{

    protected $socket = '';
    protected $processes = 2;
    protected $port      = '4567';
    /**
     * 收到信息
     * @param $connection
     * @param $data
     */
    public function onMessage($connection, $data)
    {
    }

    /**
     * 当连接建立时触发的回调函数
     * @param $connection
     */
    public function onConnect($connection)
    {
//        $connection->send('链接成功');
        // 向客户端发送hello $data
//        $connection->send('hello');

    }

    /**
     * 当连接断开时触发的回调函数
     * @param $connection
     */
    public function onClose($connection)
    {
//        $connection->send('链接');

    }

    /**
     * 当客户端的连接上发生错误时触发
     * @param $connection
     * @param $code
     * @param $msg
     */
    public function onError($connection, $code, $msg)
    {
        echo "error $code $msg\n";
    }

    /**
     * 每个进程启动
     * @param $worker
     */
    public function onWorkerStart($worker)
    {  // 每2.

        global $db;
        $db = new Connection('127.0.0.1', '3306', 'root', 'root', 'lsyl');
        Timer::add(2, function () use ($worker,$db) {
            echo date('Y-m-d H:i:s') . "\n";
            $lottery_ids = ['1','2','3','4','5','6','7'];

            foreach ($lottery_ids as $v) {
                echo('彩种：'.$v."====");
                //程序运行开始时间
                $startTime = explode(' ', microtime());
                longDragonNumber($v, $db);
                //程序运行结束时间
                $endTime = explode(' ', microtime());
                echo '执行耗时：' . round($endTime[0] + $endTime[1] - ($startTime[0] + $startTime[1]), 4) . ' 秒。'."\n";
            }

        });
    }

}