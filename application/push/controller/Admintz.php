<?php



namespace app\push\controller;


use think\Cache;
use think\worker\Server;
use Workerman\Lib\Timer;

class Admintz extends Server
{

    protected $socket = 'websocket://0.0.0.0:1001';
    protected $processes = 1;
    public static $fenkong = null;
    public static $caiwu = null;
    public static $admin = null;

    /**
     * 收到信息
     * @param $connection
     * @param $data
     */
    public function onMessage($connection, $data)
    {
        $data = json_decode($data, true);
        switch ($data['message_type']){
            case "fenkong":


                //风控
                if(!in_array($connection,self::$fenkong)){
                    self::$fenkong[] = $connection;
                }


                break;
            case "caifu":
                //风控
                if(!in_array($connection,self::$caiwu)){
                    self::$caiwu[] = $connection;
                }
                break;
            case "admin":
                //总号
                if(!in_array($connection,self::$admin)){
                    self::$admin[] = $connection;
                }
                break;

        }
        echo 'fenkong:'.count(self::$fenkong)."\n";
        echo 'caifu:'.count(self::$caiwu)."\n";
        echo 'zonghao:'.count(self::$admin)."\n";

        $connection->send('');


    }

    /**
     * 当连接建立时触发的回调函数
     * @param $connection
     */
    public function onConnect($connection)
    {

        // 向客户端发送hello $data
        $data['message_type']='Connect';
        $connection->send(json_encode($data));

    }

    /**
     * 当连接断开时触发的回调函数
     * @param $connection
     */
    public function onClose($connection)
    {
        if(in_array($connection,self::$admin)){
            self::$admin = array_diff(self::$admin,[$connection]);
        }elseif (in_array($connection,self::$caiwu)){
            self::$caiwu = array_diff(self::$caiwu,[$connection]);
        }elseif (in_array($connection,self::$fenkong)){
            self::$fenkong = array_diff(self::$fenkong,[$connection]);
        }
        echo 'fenkong:'.count(self::$fenkong)."\n";
        echo 'caifu:'.count(self::$caiwu)."\n";
        echo 'zonghao:'.count(self::$admin)."\n";

    }

    /**
     * 当客户端的连接上发生错误时触发
     * @param $connection
     * @param $code
     * @param $msg
     */
    public function onError($connection, $code, $msg)
    {
        echo "error $code $msg\n";
    }

    /**
     * 每个进程启动
     * @param $worker
     */
    public function onWorkerStart($worker)
    {


        //初始化
        self::$fenkong = [];
        self::$caiwu = [];
        self::$admin = [];
        Timer::add(1,function ()use ($worker){
            $date = date('Y-m-d H:i:s');
            echo $date."\n";

            //回去是否有新充值记录
            $cz_num = Cache::get('cz_num');
            echo 'cz_num:'.$cz_num."\n";
            if($cz_num>0){
                for ($i=1;$i<=$cz_num;$i++){
                    //发送给财务
                    if(self::$caiwu){
                        foreach (self::$caiwu as $k){
                            $data = [
                                'message_type'=>'new_cz',
                                'data'=>'有新的充值记录待确认'
                            ];


                            $k->send(json_encode($data));

                        }
                    }
                    //发送给超级管理员
                    if(self::$admin){
                        foreach (self::$admin as $k){
                            $data = [
                                'message_type'=>'new_cz',
                                'data'=>'有新的充值记录待确认'
                            ];


                            $k->send(json_encode($data));

                        }
                    }
                    Cache::dec('cz_num');


                }
            }
            //财务提现新记录
            $cw_num = Cache::get('cw_num');
            echo 'cw_num:'.$cw_num."\n";
            if($cw_num>0){
                Cache::dec('cw_num');
                //发送给财务
                if(self::$caiwu){
                    foreach (self::$caiwu as $k){
                        $data = [
                            'message_type'=>'new_cz',
                            'data'=>'有新的提现记录待确认'
                        ];


                        $k->send(json_encode($data));

                    }
                }


                //发送给超级管理员
                if(self::$admin){
                    foreach (self::$admin as $k){
                        $data = [
                            'message_type'=>'new_tx',
                            'data'=>'有新的提现记录待确认'
                        ];


                        $k->send(json_encode($data));

                    }
                }
            }


            //风控提现新记录
            $tx_num = Cache::get('tx_num');
            echo 'tx_num:'.$tx_num."\n";
            if($tx_num>0){
                for ($i=1;$i<=$tx_num;$i++){
                    //发送给财务
                    if(self::$fenkong){
                        foreach (self::$fenkong as $k){
                            $data = [
                                'message_type'=>'new_tx',
                                'data'=>'有新的提现记录待确认'
                            ];
                            $k->send(json_encode($data));
                        }
                    }
                    //发送给超级管理员
                    if(self::$admin){
                        foreach (self::$admin as $k){
                            $data = [
                                'message_type'=>'new_tx',
                                'data'=>'有新的提现记录待确认'
                            ];
                            $k->send(json_encode($data));

                        }
                    }

                    Cache::dec('tx_num');

                }

            }

//           echo $date.'======'.Cache::get('name')."\n";



        });





    }


}