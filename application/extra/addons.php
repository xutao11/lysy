<?php

return array (
  'autoload' => false,
  'hooks' => 
  array (
    'upgrade' => 
    array (
      0 => 'kefu',
    ),
    'app_init' => 
    array (
      0 => 'kefu',
    ),
  ),
  'route' => 
  array (
    '/qrcode$' => 'qrcode/index/index',
    '/qrcode/build$' => 'qrcode/index/build',
  ),
);