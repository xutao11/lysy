<?php

// 公共助手函数

use Endroid\QrCode\QrCode;
use think\Db;
use think\Log;
use think\Response;

if (!function_exists('__')) {

    /**
     * 获取语言变量值
     * @param string $name 语言变量名
     * @param array $vars 动态变量值
     * @param string $lang 语言
     * @return mixed
     */
    function __($name, $vars = [], $lang = '')
    {
        if (is_numeric($name) || !$name) {
            return $name;
        }
        if (!is_array($vars)) {
            $vars = func_get_args();
            array_shift($vars);
            $lang = '';
        }
        return \think\Lang::get($name, $vars, $lang);
    }
}

if (!function_exists('format_bytes')) {

    /**
     * 将字节转换为可读文本
     * @param int $size 大小
     * @param string $delimiter 分隔符
     * @return string
     */
    function format_bytes($size, $delimiter = '')
    {
        $units = array('B', 'KB', 'MB', 'GB', 'TB', 'PB');
        for ($i = 0; $size >= 1024 && $i < 6; $i++) {
            $size /= 1024;
        }
        return round($size, 2) . $delimiter . $units[$i];
    }
}

if (!function_exists('datetime')) {

    /**
     * 将时间戳转换为日期时间
     * @param int $time 时间戳
     * @param string $format 日期时间格式
     * @return string
     */
    function datetime($time, $format = 'Y-m-d H:i:s')
    {
        $time = is_numeric($time) ? $time : strtotime($time);
        return date($format, $time);
    }
}

if (!function_exists('human_date')) {

    /**
     * 获取语义化时间
     * @param int $time 时间
     * @param int $local 本地时间
     * @return string
     */
    function human_date($time, $local = null)
    {
        return \fast\Date::human($time, $local);
    }
}

if (!function_exists('cdnurl')) {

    /**
     * 获取上传资源的CDN的地址
     * @param string $url 资源相对地址
     * @param boolean $domain 是否显示域名 或者直接传入域名
     * @return string
     */
    function cdnurl($url, $domain = false)
    {
        $regex = "/^((?:[a-z]+:)?\/\/|data:image\/)(.*)/i";
        $url = preg_match($regex, $url) ? $url : \think\Config::get('upload.cdnurl') . $url;
        if ($domain && !preg_match($regex, $url)) {
            $domain = is_bool($domain) ? request()->domain() : $domain;
            $url = $domain . $url;
        }
        return $url;
    }
}


if (!function_exists('is_really_writable')) {

    /**
     * 判断文件或文件夹是否可写
     * @param string $file 文件或目录
     * @return    bool
     */
    function is_really_writable($file)
    {
        if (DIRECTORY_SEPARATOR === '/') {
            return is_writable($file);
        }
        if (is_dir($file)) {
            $file = rtrim($file, '/') . '/' . md5(mt_rand());
            if (($fp = @fopen($file, 'ab')) === false) {
                return false;
            }
            fclose($fp);
            @chmod($file, 0777);
            @unlink($file);
            return true;
        } elseif (!is_file($file) or ($fp = @fopen($file, 'ab')) === false) {
            return false;
        }
        fclose($fp);
        return true;
    }
}

if (!function_exists('rmdirs')) {

    /**
     * 删除文件夹
     * @param string $dirname 目录
     * @param bool $withself 是否删除自身
     * @return boolean
     */
    function rmdirs($dirname, $withself = true)
    {
        if (!is_dir($dirname)) {
            return false;
        }
        $files = new RecursiveIteratorIterator(
            new RecursiveDirectoryIterator($dirname, RecursiveDirectoryIterator::SKIP_DOTS),
            RecursiveIteratorIterator::CHILD_FIRST
        );

        foreach ($files as $fileinfo) {
            $todo = ($fileinfo->isDir() ? 'rmdir' : 'unlink');
            $todo($fileinfo->getRealPath());
        }
        if ($withself) {
            @rmdir($dirname);
        }
        return true;
    }
}

if (!function_exists('copydirs')) {

    /**
     * 复制文件夹
     * @param string $source 源文件夹
     * @param string $dest 目标文件夹
     */
    function copydirs($source, $dest)
    {
        if (!is_dir($dest)) {
            mkdir($dest, 0755, true);
        }
        foreach (
            $iterator = new RecursiveIteratorIterator(
                new RecursiveDirectoryIterator($source, RecursiveDirectoryIterator::SKIP_DOTS),
                RecursiveIteratorIterator::SELF_FIRST
            ) as $item
        ) {
            if ($item->isDir()) {
                $sontDir = $dest . DS . $iterator->getSubPathName();
                if (!is_dir($sontDir)) {
                    mkdir($sontDir, 0755, true);
                }
            } else {
                copy($item, $dest . DS . $iterator->getSubPathName());
            }
        }
    }
}

if (!function_exists('mb_ucfirst')) {
    function mb_ucfirst($string)
    {
        return mb_strtoupper(mb_substr($string, 0, 1)) . mb_strtolower(mb_substr($string, 1));
    }
}

if (!function_exists('addtion')) {

    /**
     * 附加关联字段数据
     * @param array $items 数据列表
     * @param mixed $fields 渲染的来源字段
     * @return array
     */
    function addtion($items, $fields)
    {
        if (!$items || !$fields) {
            return $items;
        }
        $fieldsArr = [];
        if (!is_array($fields)) {
            $arr = explode(',', $fields);
            foreach ($arr as $k => $v) {
                $fieldsArr[$v] = ['field' => $v];
            }
        } else {
            foreach ($fields as $k => $v) {
                if (is_array($v)) {
                    $v['field'] = isset($v['field']) ? $v['field'] : $k;
                } else {
                    $v = ['field' => $v];
                }
                $fieldsArr[$v['field']] = $v;
            }
        }
        foreach ($fieldsArr as $k => &$v) {
            $v = is_array($v) ? $v : ['field' => $v];
            $v['display'] = isset($v['display']) ? $v['display'] : str_replace(['_ids', '_id'], ['_names', '_name'], $v['field']);
            $v['primary'] = isset($v['primary']) ? $v['primary'] : '';
            $v['column'] = isset($v['column']) ? $v['column'] : 'name';
            $v['model'] = isset($v['model']) ? $v['model'] : '';
            $v['table'] = isset($v['table']) ? $v['table'] : '';
            $v['name'] = isset($v['name']) ? $v['name'] : str_replace(['_ids', '_id'], '', $v['field']);
        }
        unset($v);
        $ids = [];
        $fields = array_keys($fieldsArr);
        foreach ($items as $k => $v) {
            foreach ($fields as $m => $n) {
                if (isset($v[$n])) {
                    $ids[$n] = array_merge(isset($ids[$n]) && is_array($ids[$n]) ? $ids[$n] : [], explode(',', $v[$n]));
                }
            }
        }
        $result = [];
        foreach ($fieldsArr as $k => $v) {
            if ($v['model']) {
                $model = new $v['model'];
            } else {
                $model = $v['name'] ? \think\Db::name($v['name']) : \think\Db::table($v['table']);
            }
            $primary = $v['primary'] ? $v['primary'] : $model->getPk();
            $result[$v['field']] = $model->where($primary, 'in', $ids[$v['field']])->column("{$primary},{$v['column']}");
        }

        foreach ($items as $k => &$v) {
            foreach ($fields as $m => $n) {
                if (isset($v[$n])) {
                    $curr = array_flip(explode(',', $v[$n]));

                    $v[$fieldsArr[$n]['display']] = implode(',', array_intersect_key($result[$n], $curr));
                }
            }
        }
        return $items;
    }
}

if (!function_exists('var_export_short')) {

    /**
     * 返回打印数组结构
     * @param string $var 数组
     * @param string $indent 缩进字符
     * @return string
     */
    function var_export_short($var, $indent = "")
    {
        switch (gettype($var)) {
            case "string":
                return '"' . addcslashes($var, "\\\$\"\r\n\t\v\f") . '"';
            case "array":
                $indexed = array_keys($var) === range(0, count($var) - 1);
                $r = [];
                foreach ($var as $key => $value) {
                    $r[] = "$indent    "
                        . ($indexed ? "" : var_export_short($key) . " => ")
                        . var_export_short($value, "$indent    ");
                }
                return "[\n" . implode(",\n", $r) . "\n" . $indent . "]";
            case "boolean":
                return $var ? "TRUE" : "FALSE";
            default:
                return var_export($var, true);
        }
    }
}

if (!function_exists('letter_avatar')) {
    /**
     * 首字母头像
     * @param $text
     * @return string
     */
    function letter_avatar($text)
    {
        $total = unpack('L', hash('adler32', $text, true))[1];
        $hue = $total % 360;
        list($r, $g, $b) = hsv2rgb($hue / 360, 0.3, 0.9);

        $bg = "rgb({$r},{$g},{$b})";
        $color = "#ffffff";
        $first = mb_strtoupper(mb_substr($text, 0, 1));
        $src = base64_encode('<svg xmlns="http://www.w3.org/2000/svg" version="1.1" height="100" width="100"><rect fill="' . $bg . '" x="0" y="0" width="100" height="100"></rect><text x="50" y="50" font-size="50" text-copy="fast" fill="' . $color . '" text-anchor="middle" text-rights="admin" alignment-baseline="central">' . $first . '</text></svg>');
        $value = 'data:image/svg+xml;base64,' . $src;
        return $value;
    }
}

if (!function_exists('hsv2rgb')) {
    function hsv2rgb($h, $s, $v)
    {
        $r = $g = $b = 0;

        $i = floor($h * 6);
        $f = $h * 6 - $i;
        $p = $v * (1 - $s);
        $q = $v * (1 - $f * $s);
        $t = $v * (1 - (1 - $f) * $s);

        switch ($i % 6) {
            case 0:
                $r = $v;
                $g = $t;
                $b = $p;
                break;
            case 1:
                $r = $q;
                $g = $v;
                $b = $p;
                break;
            case 2:
                $r = $p;
                $g = $v;
                $b = $t;
                break;
            case 3:
                $r = $p;
                $g = $q;
                $b = $v;
                break;
            case 4:
                $r = $t;
                $g = $p;
                $b = $v;
                break;
            case 5:
                $r = $v;
                $g = $p;
                $b = $q;
                break;
        }

        return [
            floor($r * 255),
            floor($g * 255),
            floor($b * 255)
        ];
    }
}

/**
 * 根据返点比例查询奖金池
 *
 */
function prizePool($ratio)
{
    $data = [
        '1998' => 9.9,
        '1996' => 9.8,
        '1994' => 9.7,
        '1992' => 9.6,
        '1990' => 9.5,
        '1988' => 9.4,
        '1986' => 9.3,
        '1984' => 9.2,
        '1982' => 9.1,
        '1980' => 9,
        '1978' => 8.9,
        '1976' => 8.8,
        '1974' => 8.7,
        '1972' => 8.6,
        '1970' => 8.5,
        '1968' => 8.4,
        '1966' => 8.3,
        '1964' => 8.2,
        '1962' => 8.1,
        '1960' => 8,
        '1958' => 7.9,
        '1956' => 7.8,
        '1954' => 7.7,
        '1952' => 7.6,
        '1950' => 7.5,
        '1948' => 7.4,
        '1946' => 7.3,
        '1944' => 7.2,
        '1942' => 7.1,
        '1940' => 7,
        '1938' => 6.9,
        '1936' => 6.8,
        '1934' => 6.7,
        '1932' => 6.6,
        '1930' => 6.5,
        '1928' => 6.4,
        '1926' => 6.3,
        '1924' => 6.2,
        '1922' => 6.1,
        '1920' => 6,
        '1918' => 5.9,
        '1916' => 5.8,
        '1914' => 5.7,
        '1912' => 5.6,
        '1910' => 5.5,
        '1908' => 5.4,
        '1906' => 5.3,
        '1904' => 5.2,
        '1902' => 5.1,
        '1900' => 5,
        '1898' => 4.9,
        '1896' => 4.8,
        '1894' => 4.7,
        '1892' => 4.6,
        '1890' => 4.5,
        '1888' => 4.4,
        '1886' => 4.3,
        '1884' => 4.2,
        '1882' => 4.1,
        '1880' => 4,
        '1878' => 3.9,
        '1876' => 3.8,
        '1874' => 3.7,
        '1872' => 3.6,
        '1870' => 3.5,
        '1868' => 3.4,
        '1866' => 3.3,
        '1864' => 3.2,
        '1862' => 3.1,
        '1860' => 3,
        '1858' => 2.9,
        '1856' => 2.8,
        '1854' => 2.7,
        '1852' => 2.6,
        '1850' => 2.5,
        '1848' => 2.4,
        '1846' => 2.3,
        '1844' => 2.2,
        '1842' => 2.1,
        '1840' => 2,
        '1838' => 1.9,
        '1836' => 1.8,
        '1834' => 1.7,
        '1832' => 1.6,
        '1830' => 1.5,
        '1828' => 1.4,
        '1826' => 1.3,
        '1824' => 1.2,
        '1822' => 1.1,
        '1820' => 1,
        '1818' => 0.9,
        '1816' => 0.8,
        '1814' => 0.7,
        '1812' => 0.6,
        '1810' => 0.5,
        '1808' => 0.4,
        '1806' => 0.3,
        '1804' => 0.2,
        '1802' => 0.1,
        '1800' => 0,
    ];
    $re = array_search((float)$ratio, $data);
    return $re;

}
/**
 * 擦查询用户层级
 */
function user_tj($user_id, $users = [])
{
    $user = Db::table('lsyl_user')->where('id', $user_id)->find();
    $p_user = Db::table('lsyl_user')->where('id', $user['user_id'])->find();
    if ($p_user) {

        $users[] = $p_user;

        $users = user_tj($p_user['id'], $users);

    }
    return $users;
}

/**
 * 用户职位身份
 */

function userIdentity($level)
{
    $identity = [
        '1' => ['name' => '平台总号', 'ratio' => 9.9],
        '2' => ['name' => '线路总号', 'ratio' => 9.9],
        '3' => ['name' => '老板号', 'ratio' => 9.9],
        '4' => ['name' => '股东', 'ratio' => 9.8],
        '5' => ['name' => '主管', 'ratio' => 9.7],

    ];
    if (isset($identity[$level])) return $identity[$level];
    return ['name' => '普通用户', 'ratio' => 0];
}

//递归

function tree($data, $pid = 0, $level = 0)
{
    static $tree1 = [];
    foreach ($data as $v) {
        if ($v['play_id'] == $pid) {
            $v['level'] = $level;
            $v['name'] = str_repeat('=', $level) . $v['name'];
            $tree1[] = $v;
            tree($data, $v['id'], $level + 1);
        }
    }
    return $tree1;
}


/**
 * 获取彩种的玩法
 * lottery_id 彩种id
 */
function getPlayAPP($lottery_id)
{

    $palys = Db::name('play')->where('lottery_id', $lottery_id)->where('status', 1)->where('is_group', 0)->field('id,paramjson,lottery_id,play_id,is_ygj,name,is_group,status,min_money_award,max_money_award,money,remark')->select();
    $re_data = [];
    if ($palys) {
        foreach ($palys as $k => $v) {

            $v['paramjson'] = json_decode($v['paramjson'], true);
            $re = pc_paly($v);

            $re = array_reverse($re);
            $v['name'] = implode('/', $re);
            $re_data[$v['id']] = $v;
        }
    }





    return $re_data;
}

function pc_paly($plays)
{
    $data [] = $plays['name'];
    if ($plays['play_id'] != 0) {
        $p_play = Db::name('play')->where('id', $plays['play_id'])->find();
        if ($p_play) {
            $data = array_merge($data, pc_paly($p_play));
        }

    }
    return $data;
}

function getPlayPC($lottery_id)
{
    $plays = Db::name('play')->where('lottery_id', $lottery_id)->where('status', '1')->select();
    $plays = playTree($plays);
    return $plays;

}

/**
 * 彩票玩法地递归
 */
function playTree($list, $parent_id = 0, $level = 1)
{
    $tree = array();
    foreach ($list as $k => $v) {
//        if($v['paramjson']){
        $v['paramjson'] = json_decode($v['paramjson'], true);
//        }

        if ($v['play_id'] == $parent_id) {
            $v['level'] = $level;
            $tree[$v['id']] = $v;
            $tree[$v['id']]['play'] = playTree($list, $v['id'], $level + 1);
        }
    }
    return $tree;
}


/**
 * 彩排好嘛字典
 */
function paramRemark()
{

    $data = [
        'one_num' => '万位/第一位',
        'two_num' => '千位/第二位',
        'three_num' => '百位/第三位',
        'four_num' => '十位/第四位',
        'five_num' => '个位/第五位',
        'data_num' => '单号（号码组）',
        'double_num' => '二重号',
        'data_address' => '号码位置',
        'one_status' => '万位',
        'two_status' => '千位',
        'three_status' => '百位',
        'four_status' => '十位',
        'five_status' => '个位',
        'six_num' => '第六位',
        'seven_num' => '第七位',
        'eight_num' => '第八位',
        'nine_num' => '第九位',
        'ten_num' => '第十位',
        'sc_num' => '三重号'
    ];
    return $data;
}

/**
 * 参数解析
 */
function paramParsing($param){
    $data = [];
    $param_zw = paramRemark();
    if($param){
        foreach ($param as $k=>$v){

            $data[]=[
               'value'=>$v,
               'key'=>isset($param_zw[$k])?$param_zw[$k]:$k
            ];



        }
    }
    return $data;

}

/**
 * 下注计算奖金 和 下注金额
 * @param $money 金额 元
 * @param $unit 单位 元 角 分  厘
 * @param $multiple 倍数
 * @param $xz_num 下注数
 * @param $xz_money 单注金额
 */
function jsMoney($money, $unit, $multiple, $xz_num, $xz_money)
{

    if ($unit == '角') {
        $be = 10;
    } elseif ($unit == '分') {
        $be = 100;
    } elseif ($unit == '厘') {
        $be = 1000;
    } elseif ($unit == '元') {
        $be = 1;
    } else {
        throw new \think\Exception('下注单位错误', 100006);
    }

    $money = bcdiv($money, $be, 3);
    $money = bcmul($money, $multiple, 3);

    //下注金额
    $xz_money = bcmul($xz_num, $xz_money, 3);
    $xz_money = bcmul($xz_money, $multiple, 3);
    $xz_money = bcdiv($xz_money, $be, 3);

    return [
        'jg_money' => $money,
        'xz_money' => $xz_money
    ];
}


/**
 * 更新用户资金记录(返回变动后的余额)
 * type 类型:1=充值,2=提现,3=彩票下注,4=活动,5=转账,6=资金修正,7=返点,8=团队分红,9=浮动工资,10=彩票奖金,11=撤单'
 *
 *
 */
function updateUser($user_id, $money, $type, $relation, $createtime, $remark = '')
{
    $user = \think\Db::name('user')->where('id', $user_id)->lock(true)->find();
    //计算变变更后的余额
    $new_money = bcadd($money, $user['money'], 3);

    if ($new_money <= 0) throw new \think\Exception('用户余额不足', 100006);
    //更新用户的余额
    $re = \think\Db::name('user')->where('id', $user_id)->update([
        'money' => $new_money,
        'updatetime' => time()
    ]);
    if (!$re) throw new \think\Exception('用户资金更改失败', 100006);
    //记录用户帐变记录
    money_log($money, $new_money, $user_id, $type, $relation, $createtime, $remark);
    return $new_money;
}

/**
 * 返点
 */
function fandianTree($user_id, $xz_money, $ordercode)
{
    $user = \think\Db::name('user')->where('id', $user_id)->lock(true)->field('id,ratio,user_id,updatetime')->find();

    if ($user) {
        //查询上级

        $p_user = \think\Db::name('user')->where('id', $user['user_id'])->lock(true)->field('id,ratio,user_id,updatetime')->find();
        if ($p_user) {
            $fD = bcsub($p_user['ratio'], $user['ratio'], 3);
            if ($fD > 0) {
                //计算返点
                $money = bcmul(bcdiv($fD, 100, 3), $xz_money, 3);
                //更新用户 的金额
                updateUser($p_user['id'], $money, '7', $ordercode, time(), '返点');

                //返点进入个人统计表
//                zj_fd_xz('fd', $money, $p_user['id']);
            }
            if ($p_user['user_id'] != 0) {
                fandianTree($p_user['id'], $xz_money, $ordercode);
            }
        }

    }
}

/**
 * 后台通知
 */
function tz($type){
    $ul = config('static_url');
    $url = $ul.'/api/index/admintongzi?type='.$type;
    if($type){
        file_get_contents($url);
    }


}


/**
 * 用户帐变记录
 * money 变动金额
 * user_id 用户
 * type 类型:1=充值,2=提现,3=彩票下注,4=活动,5=转账,6=资金修正,7=返点,8=团队分红,9=浮动工资,10=彩票奖金,11=撤单'
 * relation 关联id
 * remark 备注
 * createtime  创建时间
 *
 */
function money_log($money, $all_money, $user_id, $type, $relation, $createtime, $remark = '')
{
    $re = \think\Db::name('moneylog')->insert([
        'money' => $money,
        'user_id' => $user_id,
        'type' => $type,
        'relation' => $relation,
        'all_money' => $all_money,
        'remark' => $remark,
        'createtime' => $createtime,
    ]);
    if (!$re) throw new \think\Exception('帐变记录写入失败', 100006);
}

/**
 * 充值计算并更新用户的金额
 *
 * $orcode 充值单号
 *
 */
function recharge_js($orcode,$pt)
{

    $recharge = Db::name('recharge')->where('ordercode', $orcode)->lock(true)->find();
//    dump(Db::name('recharge')->getLastSql());
    if ($recharge) {
        // 启动事务
        Db::startTrans();
        try {

            if($recharge['status'] == '2') throw new \think\Exception('该订单已付款', 100006);
            if($pt == 'paypt'){
                if($recharge['status'] != '4') throw new \think\Exception('该订单不是待确认状态', 100006);
//                $recharge['status'] = '4';
            }else{
//                $where['status'] = '1';
                if($recharge['status'] != '1') throw new \think\Exception('该订单不是代付款状态', 100006);
            }

            if($recharge['pay_pt'] != $pt) throw new \think\Exception('该记录充值平台不匹配', 100006);

            $time = time();
            //更新充值记录状态
            Db::name('recharge')->where('id', $recharge['id'])->update([
                'status' => '2',
                'updatetime' => $time
            ]);
            //实际充值到账金额
            $money = $recharge['money'];
            //手续费金额
            $service_money = 0;
            //查看当前是否有手续费
            if ($recharge['service_redio'] > '0') {

                //计算手续费金额
                $service_money = bcmul($money, $recharge['service_redio'], 3);
                //计算实际充值金额
                $money = bcsub($money, $service_money, 3);

            }
            //赠送金额
            $donation_money = '0';
            //计算赠送金额
            if ($recharge['donation'] > '0') {
                $donation_money = bcmul($money, $recharge['donation'], 3);
            }
            //跟新更充值到账金额并记录
            updateUser($recharge['user_id'], $money, '1', $recharge['ordercode'], $time, '充值');
            //跟新更手续费到账金额并记录
            if ($service_money > 0) {
                updateUser($recharge['user_id'], $service_money, '4', $recharge['ordercode'], $time, '手续费赠送');
            }
            //跟新更赠送金额到账金额并记录
            if ($donation_money > '0') {
                updateUser($recharge['user_id'], $donation_money, '4', $recharge['ordercode'], $time, '充值赠送');
            }
            // 提交事务
            Db::commit();
            return [
                'status'=> true,
                'msg'=>''
            ];
        } catch (\Exception $e) {
            // 回滚事务
            Db::rollback();
            Log::write('充值错误日志信息:' . $orcode . '(' . $e->getMessage() . ')', '充值错误日志信息');
            return [
                'status'=> false,
                'msg'=>$e->getMessage()
            ];
//            return false;
        }
    }
}

/*
getArrSet(array(array(),...))
数组不重复排列集合
*/
function getArrSet($arrs, $_current_index = -1)
{
//总数组
    static $_total_arr;
    //总数组下标计数
    static $_total_arr_index;
    //输入的数组长度
    static $_total_count;
    //临时拼凑数组
    static $_temp_arr;

    //进入输入数组的第一层，清空静态数组，并初始化输入数组长度
    if ($_current_index < 0) {
        $_total_arr = array();
        $_total_arr_index = 0;
        $_temp_arr = array();
        $_total_count = count($arrs) - 1;
        getArrSet($arrs, 0);
    } else {
        //循环第$_current_index层数组
        foreach ($arrs[$_current_index] as $v) {
            //如果当前的循环的数组少于输入数组长度
            if ($_current_index < $_total_count) {
                //将当前数组循环出的值放入临时数组
                $_temp_arr[$_current_index] = $v;
                //继续循环下一个数组
                getArrSet($arrs, $_current_index + 1);

            } //如果当前的循环的数组等于输入数组长度(这个数组就是最后的数组)
            else if ($_current_index == $_total_count) {
                //将当前数组循环出的值放入临时数组
                $_temp_arr[$_current_index] = $v;
                //将临时数组加入总数组
                $_total_arr[$_total_arr_index] = $_temp_arr;
                //总数组下标计数+1
                $_total_arr_index++;
            }

        }
    }
    return $_total_arr;
}

/**
 * 元素排列组合
 * @param $arr
 * @param $size
 * @return array
 */
function numTree($arr, $size)
{
    $len = count($arr);
    $max = pow(2, $len);
    $min = pow(2, $size) - 1;
    $r_arr = array();
    for ($i = $min; $i < $max; $i++) {
        $count = 0;
        $t_arr = array();
        for ($j = 0; $j < $len; $j++) {
            $a = pow(2, $j);
            $t = $i & $a;
            if ($t == $a) {
                $t_arr[] = $arr[$j];
                $count++;
            }
        }
        if ($count == $size) {
            $r_arr[] = $t_arr;
        }
    }
    return $r_arr;
}

/**
 * 获取玩法
 *
 */
function getPlay($play_id, $paly_name = [])
{

    $play = \think\Db::name('play')->where('id', $play_id)->find();

    $paly_name[] = $play['name'];

    if ($play['play_id'] != 0) {

        $paly_name = getPlay($play['play_id'], $paly_name);
    }
    return $paly_name;


}

/**
 * @param $user_id  用户ID
 * 返回所有的下级 下下级 下下下级。。。。。
 */

function getChildAll($user_id)
{
    //获取缓存中的
    $userAll = cache('userAll');
    if(!$userAll){
        $userAll = Db::name('user')->field('id,user_id')->select();
        cache('userAll',$userAll);
    }


    $teamUserIds = getTeamUserids($userAll, $user_id);
    return $teamUserIds;
}

function getTeamUserids($userAll, $user_id, $reUserIds = [])
{
    $reUserIds[] = $user_id;
    if ($userAll) {
        foreach ($userAll as $k => $v) {
            if ($user_id == $v['user_id']) {
                $reUserIds = getTeamUserids($userAll, $v['id'], $reUserIds);
            }
        }
    }
    return $reUserIds;
}


/**
 * 有效会员个数 (周期内投注有6  天分别超过1000元。)
 * @param $user_ids  会员ids
 * @param $start_date 起时间
 * @param $end_date  终止时间
 *
 */
function yxUser($user_ids, $start_date, $end_date)
{
    $yxUserCount = 0;
    $dates = printDates($start_date, $end_date);
    $dates = zqJs($dates);
    if ($dates && $user_ids) {
        //对周期进行循环
        foreach ($dates as $zq_k => $zq_v) {
            //用户循环
            foreach ($user_ids as $k1 => $v1) {
                $days = 0;
                foreach ($zq_v as $d_k => $d_v) {
                    $start = $d_v . ' 00:00:00';
                    $end = $d_v . ' 23:59:59';
                    $all_xz_money = Db::name('betting')->where('user_id', $v1)->where('kj_time', '>=', $start)->where('kj_time', '<=', $end)->where('status', '<>', '5')->sum('xz_money');
                    if ($all_xz_money >= 1000) {
                        $days++;
                    }
                }
                // 周期内投注有6  天分别超过1000元。
                if ($days >= 6) {
                    $yxUserCount++;
                }
            }
        }
    }
    return (string)$yxUserCount;
}

/**
 * 对日期进行归类
 * @param $dates
 * @return array
 * "data": {
 * "202003_1": [
 * "2020-03-10",
 * "2020-03-11",
 * "2020-03-12",
 * "2020-03-13",
 * "2020-03-14",
 * "2020-03-15"
 * ],
 * "202003_2": [
 * "2020-03-16",
 * "2020-03-17",
 * "2020-03-18",
 * "2020-03-19",
 * "2020-03-20"
 * ]
 * }
 */
function zqJs($dates)
{
    $re_date = [];
//    dump($dates);
    if ($dates) {
        foreach ($dates as $k => $v) {
            $time = strtotime($v);

            //周期界限
            $s_date = date('Y-m-', $time) . '01';
            $e_date = date('Y-m-', $time) . '15';
            if ($v >= $s_date && $v <= $e_date) {
                $re_date[date('Ym', $time) . "_1"][] = $v;
            } else {
                $re_date[date('Ym', $time) . "_2"][] = $v;
            }

        }


    }
    return $re_date;

}

/**
 * 获取时间段的 日期
 * @param $start 开始时间
 * @param $end 终止时间
 * @return array
 */
function printDates($start, $end)
{
    $date = [];
    $dt_start = strtotime($start);
    $dt_end = strtotime($end);
    while ($dt_start <= $dt_end) {
//        echo date('Y-m-d',$dt_start)."<br>";
        $date[] = date('Y-m-d', $dt_start);
        $dt_start = strtotime('+1 day', $dt_start);
    }
    return $date;
}

/**
 * 报表
 */
function report($user_ids,$start_date,$end_date){
    $userCz = teamCz($user_ids,$start_date,$end_date);
    //提现
    $userTx = teamTx($user_ids,$start_date,$end_date);
    //投注
    $userTz = teamXz($user_ids,$start_date,$end_date);
    //中奖
    $userJg = teamJg($user_ids,$start_date,$end_date);
    //返点
    $userFd = teamFd($user_ids,$start_date,$end_date);
    //活动
    $userHd = teamHd($user_ids,$start_date,$end_date);

    //盈亏
    $userYk = teamYk($userJg,$userFd,$userTz,$userHd);
    $data = [
        'userCz'=>$userCz,
        'userTx'=>$userTx,
        'userTz'=>$userTz,
        'userJg'=>$userJg,
        'userFd'=>$userFd,
        'userHd'=>$userHd,
//        'userYe'=>$userYe,
        'userYk'=>$userYk
    ];
    return $data;
}


/**
 * 团队充值
 * @param $user_ids  团队ids
 * @param $start_date 起时间
 * @param $end_date  终止时间
 */
function teamCz($user_ids, $start_date, $end_date)
{
    $start_date = strtotime($start_date);
    $end_date = strtotime($end_date);
    $sumCz = Db::name('recharge')->where('user_id', 'in', $user_ids)->where('status', '2')->where('createtime', '>=', $start_date)->where('createtime', '<=', $end_date)->sum('money');
    $sumCz = sprintf('%.3f', $sumCz);
    return $sumCz;
}


/**
 * 团队提现
 * @param $user_ids  团队ids
 * @param $start_date 起时间
 * @param $end_date  终止时间
 */
function teamTx($user_ids, $start_date, $end_date)
{
    $start_date = strtotime($start_date);
    $end_date = strtotime($end_date);
    $sumTx = Db::name('withdraw')->where('user_id', 'in', $user_ids)->where('status', '4')->where('createtime', '>=', $start_date)->where('createtime', '<=', $end_date)->sum('money');
    $sumTx = sprintf('%.3f', $sumTx);
    return $sumTx;
}


/**
 * 团队下注
 * @param $user_ids  团队ids
 * @param $start_date 起时间
 * @param $end_date  终止时间
 */
function teamXz($user_ids, $start_date, $end_date)
{
    $start_date = strtotime($start_date);
    $end_date = strtotime($end_date);
    $sumxz = Db::name('betting')->where('user_id', 'in', $user_ids)->where('status', '<>', '5')->where('createtime', '>=', $start_date)->where('createtime', '<=', $end_date)->sum('xz_money');
    $sumxz = sprintf('%.3f', $sumxz);
    return $sumxz;
}

/**
 * 团队中奖
 * @param $user_ids 团队ids
 * @param $start_date 起时间
 * @param $end_date 终止时间
 * @return float|int
 */
function teamJg($user_ids, $start_date, $end_date)
{
    $start_date = strtotime($start_date);
    $end_date = strtotime($end_date);
    $betting = Db::name('betting')->where('user_id', 'in', $user_ids)->where('status', '2')->where('createtime', '>=', $start_date)->where('createtime', '<=', $end_date)->field('jg_money,xz_num')->select();
    $sumjg = '0.000';
    if($betting){
        foreach ($betting as $k=>$v){
            $jg = bcmul($v['jg_money'],$v['xz_num'],3);
            $sumjg = bcadd($sumjg,$jg,3);

        }
    }
    return $sumjg;
}

/**
 * 团队返点
 * @param $user_ids 团队ids
 * @param $start_date 起时间
 * @param $end_date 终止时间
 */
function teamFd($user_ids, $start_date, $end_date)
{
    $start_date = strtotime($start_date);
    $end_date = strtotime($end_date);
    $sumfd = Db::name('moneylog')->where('user_id', 'in', $user_ids)->where('type', '7')->where('createtime', '>=', $start_date)->where('createtime', '<=', $end_date)->sum('money');
    $sumfd = sprintf('%.3f', $sumfd);
    return $sumfd;
}

/**
 * 团队活动
 * @param $user_ids 团队ids
 * @param $start_date 起时间
 * @param $end_date 终止时间
 */
function teamHd($user_ids, $start_date, $end_date)
{
    $start_date = strtotime($start_date);
    $end_date = strtotime($end_date);
    $sumHd = Db::name('moneylog')->where('user_id', 'in', $user_ids)->where('type', '4')->where('createtime', '>=', $start_date)->where('createtime', '<=', $end_date)->sum('money');
    $sumHd = sprintf('%.3f', $sumHd);
    return $sumHd;

}

/**
 * 团队工资
 * @param $user_ids 团队ids
 * @param $start_date 起时间
 * @param $end_date 终止时间
 */
function teamGz($user_ids, $start_date, $end_date)
{
    $start_date = strtotime($start_date);
    $end_date = strtotime($end_date);
    $sumGz = Db::name('moneylog')->where('user_id', 'in', $user_ids)->where('type', '9')->where('createtime', '>=', $start_date)->where('createtime', '<=', $end_date)->sum('money');
    $sumGz = sprintf('%.3f', $sumGz);
    return $sumGz;

}

/**
 * 团队余额
 * @param $user_ids 团队ids
 */
function teamAllMoney($user_ids)
{
    $teamAllMoney = Db::name('user')->where('id', 'in', $user_ids)->sum('money');
    $teamAllMoney = sprintf('%.3f', $teamAllMoney);
    return $teamAllMoney;
}


/**
 * 盈亏 (中奖金额 + 返点金额 + 活动金额 + 工资 - 下注金额
 * @param $JgMoney 团队中奖金额
 * @param $FdMoney 团队返点金额
 * @param $XzMoney 团队下注金额
 * @param $HdMoney 团队活动金额
 * @param $GzMoney 团队工资
 */
function teamYk($JgMoney, $FdMoney, $XzMoney, $HdMoney, $GzMoney = 0)
{
    $yk = bcsub(bcadd(bcadd(bcadd($JgMoney, $HdMoney, 3), $FdMoney, 3), $GzMoney, 3), $XzMoney, 3);
    return $yk;

}


/**
 * 查询用户层级
 * @userid  查询推荐关系的用户
 * @z_id    当前登录人的id
 */
function userLevel($userid, $z_id, $users = [])
{
    $user = Db::name('user')->where('id', $userid)->field('id,user_id,account')->find();
    if (count($users) == 0) {
        $users[] = $user;
    }
    $p_user = Db::name('user')->where('id', $user['user_id'])->field('id,user_id,account')->find();
    if ($p_user) {
        $users[] = $p_user;
        if ($p_user['id'] == $z_id) {
            return $users;
        }
        $users = userLevel($p_user['id'], $z_id, $users);
    }
    return $users;
}

/**
 * 用户个人盈亏
 * @param $user_id
 * @param $start_date
 * @param $end_date
 */
function userYk($user_id, $start_date, $end_date)
{
    //下注
    $xzMoney = Db::name('betting')->where('user_id', $user_id)->where('kj_time', '>=', $start_date)->where('kj_time', '<=', $end_date)->sum('xz_money');
    $xzMoney = sprintf('%.3f', $xzMoney);
    //奖金
    $jgMoney = Db::name('betting')->where('user_id', $user_id)->where('kj_time', '>=', $start_date)->where('status', '2')->where('kj_time', '<=', $end_date)->sum('jg_money');
    $jgMoney = sprintf('%.3f', $jgMoney);

    $start_time = strtotime($start_date);
    $end_time = strtotime($end_date);
    //返点
    $fdMoney = Db::name('moneylog')->where('user_id', $user_id)->where('type', '7')->where('createtime', '>=', $start_time)->where('createtime', '<=', $end_time)->sum('money');
    $fdMoney = sprintf('%.3f', $fdMoney);
    //活动
    $hdMoney = Db::name('moneylog')->where('user_id', $user_id)->where('type', '4')->where('createtime', '>=', $start_time)->where('createtime', '<=', $end_time)->sum('money');
    $hdMoney = sprintf('%.3f', $hdMoney);
    //盈亏
    $ykMoney = teamYk($jgMoney, $fdMoney, $xzMoney, $hdMoney);
    $ykMoney = sprintf('%.3f', $ykMoney);
    $data = [
        'xzMoney' => $xzMoney,
        'jgMoney' => $jgMoney,
        'fdMoney' => $fdMoney,
        'hdMoney' => $hdMoney,
        'ykMoney' => $ykMoney,
    ];

    return $data;
}

/**
 * 平台总号（可以投注）
 *
 * 1998+0.05%工资   投注额 *0.05% （每天结算前一天）
 *
 * 线路总号 （可以投注）
 *
 * 1998+0.05%工资   投注额 *0.05% （每天结算前一天）
 */
function wages()
{

// 启动事务
    Db::startTrans();
    try {
        $user_ids = Db::table('lsyl_user')->where('level', 'in', ['1', '2'])->field('id,account')->select();
        $date = date('Y-m-d', strtotime('-1 day'));
        $satrt_date = $date . ' 00:00:00';
        $end_date = $date . ' 23:59:59';
        if ($user_ids) {
            foreach ($user_ids as $k => $v) {
                //查询是否已经计算
                $is_cuznai = Db::name('wages')->where('user_id', $v['id'])->where('date', $date)->find();
                if (!$is_cuznai) {
                    //不存在时计算工资
                    $userIds = getChildAll($v['id']);   //获取该用户的团队人
                    //计算总投注量
                    $teamXzMoney = teamXz($userIds, $satrt_date, $end_date);
                    $wagesMoney = '0.000';
                    if ($teamXzMoney > 0) {
                        //计算工资
                        $wagesMoney = bcmul($teamXzMoney, bcdiv('0.05', 100, 6), 3);
                    }
                    Db::name('wages')->insert([
                        'user_id' => $v['id'],
                        'date' => $date,
                        'xz_money' => $teamXzMoney,
                        'wages' => $wagesMoney,
                        'createtime' => time()
                    ]);
                    //更新用户余额
                    if ($wagesMoney > 0) {
                        updateUser($v['id'], $wagesMoney, '9', Db::name('wages')->getLastInsID(), time(), $date . '工资');

                    }
                    echo '日期：' . $date . '=======用户:' . $v['account'] . "({$v['id']})=======工资" . $wagesMoney . "\n";
                }
            }
        }
        // 提交事务
        Db::commit();
    } catch (\Exception $e) {
        // 回滚事务
        Db::rollback();
        errorLog('平台总号-线路总号工资', $e->getMessage());
    }
}

/**
 * @param $type 错误类型
 * @param $contont  错误信息
 */
function errorLog($type, $contont)
{
    Db::name('error')->insert([
        'type' => $type,
        'content' => $contont,
        'createtime' => time()
    ]);


}

/**
 *
 * 股东（可以投注）
 *
 * 1996+日亏损分红不累计。  (每天计算 前一天）（所有下级）
 *
 * 投注量        亏损金额（万）                日结亏损分红比例
 * >1                >0                                        0.5%。
 * >1                >10                                      1%
 *
 */
function gdbonus()
{
    // 启动事务
    Db::startTrans();
    try {
        //查询股用户
        $user_ids = Db::name('user')->where('level', '4')->field('id,account')->select();
        $date = date('Y-m-d', strtotime('-1 day'));
        $satrt_date = $date . ' 00:00:00';
        $end_date = $date . ' 23:59:59';
        if ($user_ids) {
            foreach ($user_ids as $k => $v) {

                //查询是否已经计算
                $is_cuznai = Db::name('gdbonus')->where('user_id', $v['id'])->where('date', $date)->find();
                //不存在时计算工资
                if (!$is_cuznai) {
                    $userIds = getChildAll($v['id']);   //获取该用户的团队人
                    //查询团推投注数量
                    $ratio = 0;
                    $xz_num = Db::name('betting')->where('user_id', 'in', $userIds)->where('status', '<>', '5')->count();
                    //计算总投注量
                    $teamXzMoney = teamXz($userIds, $satrt_date, $end_date);
                    //返点
                    $teamFdMoney = teamFd($userIds, $satrt_date, $end_date);
                    //活动
                    $teamHdMoney = teamHd($userIds, $satrt_date, $end_date);
                    //中奖
                    $teamJgMoney = teamJg($userIds, $satrt_date, $end_date);
                    //盈亏
                    $teamYkMoney = teamYk($teamJgMoney, $teamFdMoney, $teamXzMoney, $teamHdMoney);
                    //计算分红金额
                    $money = 0;
                    if ($teamYkMoney >= 0 || $xz_num < 1) {
                        //但没有亏损  或 没有投注量
                        $ratio = 0;
                    } else {
                        //亏损金额转正数
                        $kyMoney = abs($teamYkMoney);
                        if ($kyMoney > 0 && $kyMoney <= 100000) {
                            $ratio = 0.005;
                        } else {
                            $ratio = 0.01;
                        }
                        $money = bcmul($kyMoney, $ratio, 3);
                    }

                    //记录
                    Db::name('gdbonus')->insert([
                        'user_id' => $v['id'],
                        'date' => $date,
                        'xz_num' => $xz_num,
                        'xz_money' => $teamXzMoney,
                        'yk_money' => $teamYkMoney,
                        'hd_money'=>$teamHdMoney,
                        'jg_money'=>$teamJgMoney,
                        'fd_money'=>$teamFdMoney,
                        'ratio' => $ratio,
                        'money' => $money,
                        'createtime' => time()
                    ]);
                    if ($money > 0) {
                        updateUser($v['id'], $money, '8', Db::name('gdbonus')->getLastInsID(), time(), '股东分红');
                    }
                    echo '日期：' . $date . '=======用户:' . $v['account'] . "({$v['id']})=======股东分红" . $money . "\n";
                }
            }
        }
        // 提交事务
        Db::commit();
    } catch (\Exception $e) {
        // 回滚事务
        Db::rollback();
        errorLog('股东分红', $e->getMessage());
    }


}

/**
 * 主管 （可以投注）（可以开同级）  用户还是分代理  和普通用户
 *
 * 1994
 *
 * 亏损分红：每月的1号16号发放分红，同ip无效，周期不累计。有效会员：周期内投注有6  天分别超过1000元。
 * 周期消费量（万）                                                        有效会员（个）  分红比例
 * 10                                                                      0                    3%
 * 30                                                                      1                    5%
 * 60                                                                      2                    8%
 * 100                                                                    3                   12%
 * 200                                                                    8                   16%
 * 500                                                                   15                  20%
 * 1000                                                                 20                  25%
 * 2000                                                                 30                  30%
 * 3000                                                                 50                  35%
 * 4000                                                                 80                  40%
 *
 */

function zgbonus()
{

// 启动事务
    Db::startTrans();
    try{

        //当前时间
        $new_time = date('Y-m-d H:i:s', time());
        //查询可以计算的周期
        $cycle = Db::name('cycle')->where('enddate', '<', $new_time)->where('status', '0')->select();
        if ($cycle) {
            //查询主管号
            $zg_user = Db::table('lsyl_user')->where('level',5)->field('id,account')->select();
            if ($zg_user) {
                foreach ($cycle as $k => $v) {
                    foreach ($zg_user as $k1 => $v1) {
                        $is_cuznai = Db::name('zgbonus')->where('user_id',$v1['id'])->where('startdate',$v['startdate'])->where('enddate',$v['enddate'])->find();
                        if($is_cuznai) continue;
                        //获取团队ids
                        $teamUserids = getChildAll($v1['id']);
                        $teamXzMoney = teamXz($teamUserids, $v['startdate'], $v['enddate']);
                        $teamYxCount = yxUser($teamUserids, $v['startdate'], $v['enddate']);
                        //返点
                        $teamFdMoney = teamFd($teamUserids, $v['startdate'], $v['enddate']);
                        //活动
                        $teamHdMoney = teamHd($teamUserids, $v['startdate'], $v['enddate']);
                        //中奖
                        $teamJgMoney = teamJg($teamUserids, $v['startdate'], $v['enddate']);
                        //盈亏
                        $teamYkMoney = teamYk($teamJgMoney, $teamFdMoney, $teamXzMoney, $teamHdMoney);
                        ////获取比例
                        $ratio = 0;
                        //分红金额
                        $money = 0;
                        //有亏损
                        if($teamYkMoney<0){
                            $ksMoney = abs($teamYkMoney);

                            $ratio = zgbonus_ratio($teamXzMoney,$teamYxCount);
                            //计算分红
                            $money = bcmul($ksMoney,$ratio,3);
                        }
                        Db::name('zgbonus')->insert([
                            'cycle_id'=>$v['id'],
                            'user_id'=>$v1['id'],
                            'startdate'=>$v['startdate'],
                            'enddate'=>$v['enddate'],
                            'xz_money'=>$teamXzMoney,
                            'yx_count'=>$teamYxCount,
                            'yk_money' => $teamYkMoney,
                            'hd_money'=>$teamHdMoney,
                            'jg_money'=>$teamJgMoney,
                            'fd_money'=>$teamFdMoney,
                            'status'=>'0',
                            'ratio'=>$ratio,
                            'money'=>$money,
                            'createtime'=>time()
                        ]);
                        echo '用户：'.$v1['account'].'======='.$v['startdate'].'~'.$v['enddate'].'======c下注：'.$teamXzMoney.'===有效会员：'.$teamYxCount.'=====分红：'.$money."\n";
                    }
                    Db::name('cycle')->where('id',$v['id'])->update([
                        'status'=>'1'
                    ]);
                }
            }
        }
        // 提交事务
        Db::commit();
    } catch (\Exception $e) {
        // 回滚事务
        Db::rollback();
        echo '主管分红错误',$e->getMessage()."\n";
        errorLog('主管分红错误',$e->getMessage());
    }
}

/**
 * 主管 （可以投注）（可以开同级）  用户还是分代理  和普通用户
 *
 * 1994
 *
 * 亏损分红：每月的1号16号发放分红，同ip无效，周期不累计。有效会员：周期内投注有6  天分别超过1000元。
 * 周期消费量（万）                                        有效会员（个）  分红比例
 * 10                                                                      0                    3%
 * 30                                                                      1                    5%
 * 60                                                                      2                    8%
 * 100                                                                    3                   12%
 * 200                                                                    8                   16%
 * 500                                                                   15                  20%
 * 1000                                                                 20                  25%
 * 2000                                                                 30                  30%
 * 3000                                                                 50                  35%
 * 4000                                                                 80                  40%
 *
 * @param $xz_money   下注量
 * @param $yxCount  有效会员
 */
function zgbonus_ratio($xz_money, $yxCount)
{
    $ratio = 0;
    if ($xz_money > 100000 && $xz_money <= 300000 && $yxCount >= 0) {

        $ratio = 0.03;

    } elseif ($xz_money > 300000 && $xz_money <= 600000 && $yxCount >= 1) {
        $ratio = 0.05;
    } elseif ($xz_money > 600000 && $xz_money <= 1000000 && $yxCount >= 2) {
        $ratio = 0.08;
    } elseif ($xz_money > 1000000 && $xz_money <= 2000000 && $yxCount >= 3) {
        $ratio = 0.12;

    } elseif ($xz_money > 2000000 && $xz_money <= 5000000 && $yxCount >= 8) {
        $ratio = 0.16;
    } elseif ($xz_money > 5000000 && $xz_money <= 10000000 && $yxCount >= 15) {
        $ratio = 0.20;

    } elseif ($xz_money > 10000000 && $xz_money <= 20000000 && $yxCount > 20) {
        $ratio = 0.25;
    } elseif ($xz_money > 20000000 && $xz_money <= 30000000 && $yxCount >= 30) {
        $ratio = 0.30;
    } elseif ($xz_money > 30000000 && $xz_money <= 40000000 && $yxCount >= 50) {
        $ratio = 0.35;

    } elseif ($xz_money > 4000000 && $yxCount >= 80) {
        $ratio = 0.40;
    }
    return $ratio;


}


/**
 * 创建周期数  当前时间大于 最后一个周期数的结束时间  就增加周期数
 */
function createCycle()
{

    //获取当天时间


    //获取当天日期
    $time = time();
    $date_d = date('d', $time);
    if ($date_d >= 1 && $date_d <= 15) {
        $startdate = date('Y-m-', $time) . "01 00:00:00";
        $enddate = date('Y-m-', $time) . '15 23:59:59';
    } elseif ($date_d >= 16) {
        $startdate = date('Y-m-', $time) . "16 00:00:00";
        $enddate = date('Y-m-', $time) . date('t', $time) . ' 23:59:59';
    }

    //查询是否已有周期
    $cycle = Db::name('cycle')->where('startdate', $startdate)->where('enddate', $enddate)->find();
    if (!$cycle) {
        $max_num = Db::name('cycle')->max('num');
        $num = $max_num + 1;
        Db::name('cycle')->insert([
            'num' => $num,
            'startdate' => $startdate,
            'enddate' => $enddate,
            'status' => '0'
        ]);

    }


}


/**
 * 用户开奖
 */
function bettingOpen()
{


}


/**
 *
 * @param $url
 * @return bool|string
 */
function curl_get($url)
{

    $ch = curl_init();

    // set url
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_TIMEOUT, 3);
    //return the transfer as a string
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
//    curl_setopt($ch, CURLOPT_NOSIGNAL,1);    //注意，毫秒超时一定要设置这个
//    curl_setopt($ch, CURLOPT_TIMEOUT_MS,200);
    if (substr($url, 0, 5) == 'https') {
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // 跳过证书检查
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);  // 从证书中检查SSL加密算法是否存在
    }

    // $output contains the output string
    $output = curl_exec($ch);

    // close curl resource to free up system resources
    curl_close($ch);
//    dump($output);
    return $output;
}

//从$input数组中取$m个数的组合算法
function comb($input, $m)
{
    if ($m == 1) {
        foreach ($input as $item) {
            $result[] = array($item);
        }
        return $result;
    }
    for ($i = 0; $i <= count($input) - $m; $i++) {
        $nextinput = array_slice($input, $i + 1);
        $nextresult = comb($nextinput, $m - 1);
        foreach ($nextresult as $one) {
            $result[] = array_merge(array($input[$i]), $one);
        }
    }
    return $result;
}


/**
 * 云挂机启动日志
 * @param $ygj_id 方案id
 * @param $status 状态 1=启动 0= 停止
 * @param $remark 备注
 */
function ygjLog($ygj_id,$status,$remark){
    Db::table('lsyl_hangplan_log')->insert([
        'hangplan_id'=>$ygj_id,
        'status'=>$status,
        'remark'=>$remark,
        'createtime'=>time()
    ]);
}

/**
 * 长龙号码
 */
function longDragonNumber($lottery_id,$db){
    $data = ['01234','01235','01236','01237','01238','01239','01245','01246','01247','01248','01249','01256','01257','01258','01259','01267','01268','01269','01278','01279','01289','01345','01346','01347','01348','01349','01356','01357','01358','01359','01367','01368','01369','01378','01379','01389','01456','01457','01458','01459','01467','01468','01469','01478','01479','01489','01567','01568','01569','01578','01579','01589','01678','01679','01689','01789','02345','02346','02347','02348','02349','02356','02357','02358','02359','02367','02368','02369','02378','02379','02389','02456','02457','02458','02459','02467','02468','02469','02478','02479','02489','02567','02568','02569','02578','02579','02589','02678','02679','02689','02789','03456','03457','03458','03459','03467','03468','03469','03478','03479','03489','03567','03568','03569','03578','03579','03589','03678','03679','03689','03789','04567','04568','04569','04578','04579','04589','04678','04679','04689','04789','05678','05679','05689','05789','06789','12345','12346','12347','12348','12349','12356','12357','12358','12359','12367','12368','12369','12378','12379','12389','12456','12457','12458','12459','12467','12468','12469','12478','12479','12489','12567','12568','12569','12578','12579','12589','12678','12679','12689','12789','13456','13457','13458','13459','13467','13468','13469','13478','13479','13489','13567','13568','13569','13578','13579','13589','13678','13679','13689','13789','14567','14568','14569','14578','14579','14589','14678','14679','14689','14789','15678','15679','15689','15789','16789','23456','23457','23458','23459','23467','23468','23469','23478','23479','23489','23567','23568','23569','23578','23579','23589','23678','23679','23689','23789','24567','24568','24569','24578','24579','24589','24678','24679','24689','24789','25678','25679','25689','25789','26789','34567','34568','34569','34578','34579','34589','34678','34679','34689','34789','35678','35679','35689','35789','36789','45678','45679','45689','45789','46789','56789'];
    $addres = ['万','千','百','十','个'];
    $where['lottery_id']=$lottery_id;
    $updata = [];
    $up_ids = [];
    //擦寻该彩种种的最新开奖
    $newKj_sql = "SELECT id,pre_draw_issue,pre_draw_code FROM lsyl_kjlog WHERE lottery_id={$lottery_id} ORDER BY pre_draw_issue DESC limit 1";
    $newKj = $db->query($newKj_sql);
    if(count($newKj)== 1) {
        foreach ($data as $k=>$v){
            foreach ($addres as $k1=>$v1){

                $where['number']=$v;
                $where['address'] = $v1;
                $is_yl = $db->select('*')->from('lsyl_longdragon')->where("address= '".$v1."' AND number = '".$v."' AND lottery_id= '".$lottery_id."'")->query();
                $pre_draw_code = explode(',',$newKj[0]['pre_draw_code']);
                //没有查询到改遗漏值
                if(count($is_yl)==0){
                    //遗漏值
                    $yl_num = 0;
                    //获取当前位子的号码
                    $number = $pre_draw_code[$k1];
                    $v_arr = str_split($v);
                    if(!in_array($number,$v_arr)){
                        $yl_num++;
                    }else{
                        $yl_num = 0;
                    }
                    // echo '(新)位置：'.$v1.'===== 号码：'.$number.'=====遗漏号码：'.$v.'========开奖号码'.$newKj[0]['pre_draw_code'].'遗漏值：'.$yl_num."\n";
                    $data = [
                        'lottery_id'=>$lottery_id,
                        'number'=>$v,
                        'missing_value'=>$yl_num,
                        'address'=>$v1,
                        'pre_draw_issue'=>$newKj[0]['pre_draw_issue'],
                    ];
                    $db->insert('lsyl_longdragon')->cols($data)->query();
                }else{
                    //查询到记录
                    //遗漏值
                    $yl_num1 = $is_yl[0]['missing_value'];
                    //查询当前最新记录 与 遗漏值 期数之间相差的期数
                    $lasr_qishu = $is_yl[0]['pre_draw_issue'];
                    $kj_log_sql = "SELECT `id`,`pre_draw_issue`,`pre_draw_code` FROM `lsyl_kjlog` WHERE  `lottery_id` = {$lottery_id}  AND ( `pre_draw_issue` > '".$is_yl[0]['pre_draw_issue']."' AND `pre_draw_issue` <= '".$newKj[0]['pre_draw_issue']."' ) ORDER BY `pre_draw_issue` ASC";
                    $kj_log = $db->query($kj_log_sql);

                    foreach ($kj_log as $k2=>$v2){
                        //获取当前位子的号码
                        $pre_draw_code = explode(',',$v2['pre_draw_code']);
                        $number =$pre_draw_code[$k1];
                        $v_arr = str_split($v);
                        if(!in_array($number,$v_arr)){
                            //没有查询到 遗漏值+1
                            $yl_num1++;
                        }else{
                            $yl_num1 = 0;
                        }
                        $lasr_qishu = $v2['pre_draw_issue'];
                    }

                    if($lasr_qishu != $is_yl[0]['pre_draw_issue']){
                        $up_ids[] = $is_yl[0]['id'];
                        $updata['missing_value'][] = $yl_num1;
                        $updata['pre_draw_issue'][] = $lasr_qishu;

                    }


                }
            }

        }

        if($up_ids){

            $sql = sqlZz($up_ids,$updata);
            $sql = $sql.' where lottery_id='.$lottery_id;
            $re = $db->query($sql);

        }
    }




}

function sqlZz($ids,$data){
//        $ids=[1,2,3];
//        $data = [
//            'name'=>['name1','name2','name3'],
//            'remark'=>['remark1','remark2','remark3']
//        ];
//        $data = [
//            ['id'=>1,'name'=>'name1'],
//            ['id'=>2,'name'=>'name2'],
//            ['id'=>3,'name'=>'name3'],
//
//        ];


    $str = [];
    foreach ($data as $k=>$v){
        $s =[];
        foreach ($ids as $k1=>$v1){
            $s[] ="WHEN {$v1} THEN '{$v[$k1]}'";
        }
        $s = implode(' ',$s);
        $str[] =  "`{$k}`= ( CASE id  {$s} END )";
    }
    $str = implode(',',$str);
//        echo $str;
    $sql = "UPDATE `lsyl_longdragon` SET {$str}";
    return $sql;
}
/**
 * 获取玩法
 *
 */
//function getPlay($play_id, $paly_name = [])
//{
//
//    $play = \think\Db::table('lsyl_play')->where('id', $play_id)->find();
//
//    $paly_name[] = $play['name'];
//
//    if ($play['play_id'] != 0) {
//
//        $paly_name = getPlay($play['play_id'], $paly_name);
//    }
//    return $paly_name;
//
//
//}

/**
 * curl 模拟请求
 * @param $url
 * @param string $method
 * @param array $postData
 * @return bool|string
 */

function getHttpContent($url, $method = 'GET', $postData = array())
{
    $data = '';
    $user_agent = $_SERVER ['HTTP_USER_AGENT'];
    $header = array(
        "User-Agent: $user_agent"
    );
    if (!empty($url)) {
        try {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_TIMEOUT, 30); //30秒超时
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
            //curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_jar);
            if (strstr($url, 'https://')) {
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); // https请求 不验证证书和hosts
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
            }

            if (strtoupper($method) == 'POST') {
                $curlPost = is_array($postData) ? http_build_query($postData) : $postData;
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $curlPost);
            }
            $data = curl_exec($ch);

            curl_close($ch);
//            return $data;
        } catch (Exception $e) {
            $data = '';
        }
    }
    return $data;
}
/**
 * 速付提现
 */
function shBank($bank_type)
{
//    $bank_list = ['中国⼯商银⾏', '中国农业银⾏', '中国建设银⾏', '招商银⾏', '中国银⾏', '中国邮政储蓄银⾏', '交通银⾏', '中信银⾏', '中国⺠⽣银⾏', '中国光⼤银⾏', '兴业银⾏', '上海浦东发展银⾏', '⼴发银⾏', '平安银⾏', '华夏银⾏', '北京银⾏', '上海银⾏', '江苏银⾏', '北京农商⾏', '江苏省农村信⽤社联合社', '营⼝银⾏', '友利银⾏', '乌海银⾏', '⻓春经开融丰村镇银⾏', '江⻄省农村信⽤社', '海南省农村信⽤社', '重庆农村商业银⾏', '鞍⼭银⾏', '海⼝联合农商银⾏', '包商银⾏', '常熟农商银⾏', '晋商银⾏', '湖北省农信社', '恒丰银⾏', '莱商银⾏', '⾦华银⾏', '临商银⾏', '银座村镇银⾏', '厦⻔银⾏', '东莞农村商业银⾏', '攀枝花市商业银⾏', '⼴东省农村信⽤社联合社', '珠海华润银⾏', '韩亚银⾏', '浙江⺠泰商业银⾏', '廊坊银⾏', '锦州银⾏', '天津滨海农村商业银⾏', '徽商银⾏', '宁夏⻩河农村商业银⾏', '⻬鲁银⾏', '⽆锡农村商业银⾏', '潍坊银⾏', '企业银⾏', '浙商银⾏', '华融湘江银⾏', '焦作中旅银⾏', '东亚银⾏', '贵州省农村信⽤社联合社', '枣庄银⾏', '张家港农村商业银⾏', '⻘岛银⾏', '安徽省农村信⽤社', '⾃贡银⾏', '渤海银⾏', '湖北银⾏', '顺德农商银⾏', '辽阳银⾏', '⾩新银⾏', '苏州农村商业银⾏', '⼴东南粤银⾏', '⼴⻄北部湾银⾏', '葫芦岛银⾏', '温州银⾏', '内蒙古银⾏', '⼴州农村商业银⾏', '⻘海省农村信⽤社', '⽯嘴⼭银⾏', '乐⼭市商业银⾏', '重庆三峡银⾏', '衡⽔市商业银⾏', '⼤连银⾏', '河北银⾏', '邢台银⾏', '盛京银⾏', '⽹商银⾏', '保定银⾏', '德州银⾏', '⻓沙银⾏', '浙江省农村信⽤社联合社', '杭州银⾏', '晋中银⾏', '中银富登村镇银⾏', '深圳农村商业银⾏', '中原银⾏', '⼭东省农村信⽤社联合社', '⼴州银⾏', '新疆农村信⽤社', '陕⻄省农信社', '吉林银⾏', '泰安银⾏', '⽢肃银⾏', '贵阳银⾏', '⽇照银⾏', '昆仑银⾏', '九江银⾏', '⼭⻄省农村信⽤社', '四川天府银⾏', '浙江稠州商业银⾏', '上海农商银⾏', '遂宁银⾏', '宁夏银⾏', '内蒙古农村信⽤社联合社', '河北省农村信⽤社', '辽宁省农村信⽤社', '东营莱商村镇银⾏', '天津银⾏', '泉州银⾏', '武汉农村商业银⾏', '宁波银⾏', '江⻄银⾏', '张家⼝银⾏', '济宁银⾏', '哈尔滨银⾏', '⻘海银⾏', '宜宾市商业银⾏', '兰州银⾏', '南京银⾏', '晋城银⾏', '台州银⾏', '重庆银⾏', '联合村镇银⾏', '⻓春绿园融泰村镇银⾏', '四川省农村信⽤社联合社', '富滇银⾏', '烟台银⾏', '⼤连农村商业银⾏', '宁波通商银⾏', '上饶银⾏', '河南省农村信⽤社', '苏州银⾏', '云南红塔银⾏', '厦⻔国际银⾏', '承德银⾏', '湖南省农村信⽤社', '⻄安银⾏', '天津农商银⾏', '新韩银⾏', '江南农村商业银⾏', '⼴东华兴银⾏', '乌鲁⽊⻬银⾏', '⻓春朝阳和润村镇银⾏', '绵阳市商业银⾏', '邯郸银⾏', '江苏江阴农村商业银⾏', '梅县客家村镇银⾏', '⿊⻰江省农村信⽤社联合社', '⻓城华⻄银⾏', '东莞银⾏', '威海市商业银⾏', '沧州银⾏', '朝阳银⾏', '⻓安银⾏', '汉⼝银⾏', '洛阳银⾏', '平顶⼭银⾏', '⼴⻄壮族⾃治区农村信⽤社联合社', '绍兴银⾏', '营⼝沿海银⾏', '福建省农村信⽤社联合社', '⻬商银⾏', '赣州银⾏', '湖州银⾏', '曲靖市商业银⾏', '嘉兴银⾏', '昆⼭农村商业银⾏', '福建海峡银⾏', '贵州银⾏', '江苏⻓江商业银⾏', '南海农商银⾏', '浙江泰隆商业银⾏', '富邦华⼀银⾏', '柳州银⾏', '泸州市商业银⾏', '⽢肃省农村信⽤社'];
    $bank_list = [
        '建设银行',
        '农业银行',
        '工商银行',
        '中国银行',
        '浦发银行',
        '光大银行',
        '平安银行',
        '兴业银行',
        '邮政储蓄银行',
        '中信银行',
        '华夏银行',
        '招商银行',
        '广发银行',
        '北京银行',
        '民生银行',
        '交通银行',
        '深圳发展银行',
        '厦门银行',
        '中国建设银行',
        '中国农业银行',
        '中国工商银行',
        '中国光大银行',
        '中国邮政储蓄银行',
    ];
    if (in_array($bank_type, $bank_list)) return true;
    return false;
}

/**
 * supay 提现
 *
 */
function supayTx($money,$withdraw){

    $txPay = new \pay\Txpay();
    $re = $txPay->txPay($money, $withdraw['txcode'], $withdraw['relaname'], $withdraw['bank_type'], $withdraw['bank_name'], $withdraw['bank_code']);
    $e = json_encode($re);
    if ($re['success'] == false) throw new \think\Exception($e, 100006);

    Db::table('lsyl_txsign')->insert([
        'withdraw_code' => $withdraw['txcode'],
        'sign' => $re['data']['sign'],
        'sys_biz_num' => $re['data']['sysBizNum']

    ]);

}
/**
 * 三河银行卡
 */
function spBank($bank_type)
{
    $bank_list = ['中国工商银行', '中国农业银行', '中国银行', '中国建设银行', '交通银行', '中信银行', '中国光大银行', '华夏银行', '广发银行', '平安银行（原深圳发展银行）', '招商银行', '兴业银行', '上海浦东发展银行', '北京银行', '天津银行', '河北银行', '邯郸市商业银行', '邢台银行', '张家口市商业银行', '承德银行', '沧州银行', '廊坊银行', '衡水银行', '晋商银行', '晋城银行', '晋中银行', '内蒙古银行', '包商银行', '乌海银行', '鄂尔多斯银行', '大连银行', '鞍山市商业银行', '锦州银行', '葫芦岛银行', '营口银行', '阜新银行', '吉林银行', '哈尔滨银行', '龙江银行', '南京银行', '江苏银行', '苏州银行', '江苏长江商行', '杭州银行', '宁波银行', '宁波通商银行股份有限公司', '温州银行', '嘉兴银行', '湖州银行', '绍兴银行', '金华银行股份有限公司', '浙江稠州商业银行', '台州银行', '浙江泰隆商业银行', '浙江民泰商业银行', '福建海峡银行', '厦门银行', '泉州银行', '南昌银行', '九江银行股份有限公司', '赣州银行', '上饶银行', '齐鲁银行', '青岛银行', '齐商银行', '枣庄银行', '东营银行', '烟台银行', '潍坊银行', '济宁银行', '泰安市商业银行', '莱商银行', '威海市商业银行', '德州银行', '临商银行', '日照银行', '郑州银行', '中原银行', '洛阳银行', '平顶山银行', '焦作市商业银行', '汉口银行', '湖北银行', '华融湘江银行', '长沙银行', '广州银行', '珠海华润银行', '广东华兴银行', '广东南粤银行', '东莞银行', '广西北部湾银行', '柳州银行', '桂林银行股份有限公司', '成都银行', '重庆银行', '自贡市商业银行', '攀枝花市商业银行', '德阳银行', '绵阳市商业银行', '南充市商业银行', '贵阳银行', '富滇银行', '曲靖市商业银行', '玉溪市商业银行', '西安银行', '长安银行', '兰州银行', '青海银行', '宁夏银行', '乌鲁木齐市商业银行', '昆仑银行', '无锡农村商业银行', '江阴农商银行', '太仓农商行', '昆山农村商业银行', '吴江农村商业银行', '常熟农村商业银行', '张家港农村商业银行', '广州农村商业银行', '顺德农村商业银行', '海口联合农村商业银行', '成都农村商业银行股份有限公司', '重庆农村商业银行', '恒丰银行', '浙商银行', '天津农商银行', '渤海银行', '徽商银行', '北京顺义银座村镇银行', '浙江景宁银座村镇银行', '浙江三门银座村镇银行', '江西赣州银座村镇银行', '东营莱商村镇银行股份有限公司', '深圳福田银座村镇银行', '重庆渝北银座村镇银行', '重庆黔江银座村镇银行', '上海农商银行', '深圳前海微众银行', '上海银行', '北京农村商业银行', '吉林农村信用社', '江苏省农村信用社联合社', '浙江省农村信用社', '鄞州银行', '安徽省农村信用社联合社', '福建省农村信用社', '农村信用社', '山东省农联社', '湖北农信', '武汉农村商业银行', '广东省农信', '深圳农商行', '东莞农村商业银行', '广西农村信用社（合作银行）', '海南省农村信用社', '四川省联社', '贵州省农村信用社联合社', '云南省农村信用社', '陕西信合', '黄河农村商业银行', '中国邮政储蓄银行', '东亚银行（中国）有限公司', '友利银行', '新韩银行中国', '企业银行', '韩亚银行', '厦门国际银行', '富邦华一银行'];
    if (in_array($bank_type, $bank_list)) return true;
    return false;

}

/**
 *  sanhe 提现
 *
 *
 */
function sanheTx($money,$withdraw){
    //三河代付
    $txPay = new \pay\Txpay();
    if($withdraw['bank_type'] != '中国银行' && mb_strpos($withdraw['bank_type'],'中国')>=0){
        $withdraw['bank_type'] = str_replace("中国","",$withdraw['bank_type']);
    }
    $re = $txPay->shPay($withdraw['txcode'], $money, $withdraw['relaname'], $withdraw['bank_code'], '', $withdraw['bank_type'], $withdraw['bank_type'], '用户:' . $withdraw['user_id']);
//    if($re['error']['code'] != 200) return [
//        'success'=>false,
//        'msg' => $re['error']['message']
//    ];
    if($re['error']['code'] != 200) throw new \think\Exception($re['error']['message'], 100006);
//    return [
//        'success'=>true,
//        'msg' => 'success'
//    ];
}

/**
 *  快乐  代付
 * @param $bankname
 * @return bool|string
 */
function happyBank($bankname){

    $bank_list = [
        ['name'=>'中国农业银行','code'=>'ABC'],
        ['name'=>'农业银行','code'=>'ABC'],
        ['name'=>'华夏银行','code'=>'HXB'],
        ['name'=>'交通银行','code'=>'BOCO'],
        ['name'=>'广发银行','code'=>'CGB'],
        ['name'=>'中国邮政银行','code'=>'POST'],
        ['name'=>'邮政银行','code'=>'POST'],
        ['name'=>'中国银行','code'=>'BOC'],
        ['name'=>'兴业银行','code'=>'CIB'],
        ['name'=>'中信银行','code'=>'ECITIC'],
        ['name'=>'招商银行','code'=>'CMBCHINA'],
        ['name'=>'光大银行','code'=>'CEB'],
        ['name'=>'中国光大银行','code'=>'CEB'],
        ['name'=>'建设银行','code'=>'CCB'],
        ['name'=>'中国建设银行','code'=>'CCB'],
        ['name'=>'平安银行','code'=>'PINGANBANK'],
        ['name'=>'浦发银行','code'=>'SPDB'],
        ['name'=>'北京银行','code'=>'BCCB'],
        ['name'=>'民生银行','code'=>'CMBC'],
        ['name'=>'上海银行','code'=>'SHB'],
        ['name'=>'工商银行','code'=>'ABC'],
        ['name'=>'中国工商银行','code'=>'ICBC'],
        ['name'=>'中国农业银行','code'=>'ICBC'],
    ];
    if($bankname == '') return false;
    $bank_code = false;
    foreach ($bank_list as $k=>$v){
        if($v['name'] == $bankname){

            $bank_code = $v['code'];
            break;
        }
    }

    return $bank_code;

}

/**
 * happy 提现
 *
 */
function happyTx($money,$withdraw){
    $txPay = new \pay\Txpay();
    //查询银行卡 编码
    $bank_code = happyBank($withdraw['bank_type']);
    if($bank_code == false) throw new \think\Exception('不支持该银行卡', 100006);
//    if($bank_code == false)  return [
//        'success'=>false,
//        'msg' => '不支持该银行卡'
//    ];
    $re = $txPay->happyPay($withdraw['txcode'],$money,$withdraw['relaname'],$bank_code,$withdraw['bank_name'],$withdraw['bank_code']);
    if(!$re){
        //当返回为空时  就查询
        $re_cx = $txPay->happyCx($withdraw['txcode']);
//        if($re_cx == null) return [
//            'success'=>false,
//            'msg' => '没有查询到订单'
//
        if($re_cx == null) throw new \think\Exception('没有查询到订单', 100006);
        if($re_cx['code'] == '0'){
            if($re_cx['detail']['status'] == '2'){
                throw new \think\Exception('第三方返回:'.$re_cx['remark'], 100006);
//                return [
//                    'success'=>false,
//                    'msg' => '第三方返回:'.$re_cx['remark']
//                ];
            }else if($re_cx['detail']['status'] == '1'){
                //出款成功
                Db::table('lsyl_withdraw')->where('id',$withdraw['id'])->update([
                    'status'=>4,
                    'updatetime'=>time(),
                    'remark'=>$re_cx['detail']['remark']
                ]);

//                return [
//                    'success'=>true,
//                    'msg' => 'success'
//                ];

//            }else if($re_cx['detail']['status'] == '0'){
//                return [
//                    'success'=>true,
//                    'msg' => 'success'
//                ];
            }
        }else{
            throw new \think\Exception('第三方返回:'.$re_cx['msg'], 100006);
//            return [
//                'success'=>false,
//                'msg' => '第三方返回:'.$re_cx['msg']
//            ];
        }

    }else{
        if($re['code'] != '0') throw new \think\Exception($re['msg'], 100006);
//        if($re['code'] == '0'){
//            return [
//                'success'=>true,
//                'msg' => 'success'
//            ];
//        }else{
//            return [
//                'success'=>false,
//                'msg' => $re['msg']
//            ];
//        }
    }

}

/**
 * fp 提现
 */
function fpTx($money,$withdraw){
    $txPay = new \pay\Txpay();
    $orderNo = $withdraw['txcode'];
    $bizAmt = $money;
    $bankBranchName = $withdraw['bank_type'];
    $cardNo = $withdraw['bank_code'];
    $accName = $withdraw['relaname'];
    $re = $txPay->fpTxpay($orderNo,$bizAmt,$accName,$bankBranchName,$cardNo);


    if($re['fxstatus'] == '0'){
        throw new \think\Exception($re['fxmsg'], 100006);
//        return [
//            'success'=>false,
//            'msg' => $re['fxmsg']
//        ];
    }
    if($re['fxbody'][0]['fxstatus'] == '0'){
        throw new \think\Exception($re['fxbody'][0]['fxcode'], 100006);
//        return [
//            'success'=>false,
//            'msg' => $re['fxbody'][0]['fxcode']
//        ];
    }
    if($re['fxbody'][0]['fxstatus'] == '3'){

        Db::table('lsyl_withdraw')->where('id',$withdraw['id'])->update([
            'status'=>4,
            'updatetime'=>time(),
            'remark'=>$re['fxbody'][0]['fxcode']
        ]);
    }



}

/**
 * wz 提现
 */
function wzTx($money,$withdraw){


    //查询银行卡 编码
    $bank_code = wzBank($withdraw['bank_type']);
//    if($bank_code == false)  return [
//        'success'=>false,
//        'msg' => '不支持该银行卡'
//    ];
    if($bank_code == false) throw new \think\Exception('不支持该银行卡', 100006);
    $txPay = new \pay\Txpay();
    $orderNo = $withdraw['txcode'];
    $amount = $money;
    $bankAccountName = $withdraw['relaname'];
    $bankAccountNo = $withdraw['bank_code'];
    $userName = $withdraw['user_id'];
    $re = $txPay->wzTxpay($amount,$bankAccountName,$bankAccountNo,$orderNo,$bank_code,$userName);
    if($re['code'] != 0) throw new \think\Exception($re['message'], 100006);
//    if($re['code'] == 0){
//        return [
//            'success'=>true,
//            'msg' => 'success'
//        ];
//    }else{
//        return [
//            'success'=>false,
//            'msg' => $re['message']
//        ];
//    }




}



/**
 * 提现第三方
 */
function txPay($withdraw_id)
{
//    throw new \think\Exception('第三方代付接口错误',10000);
//    return;
    //查询提现订单status
    $withdraw = Db::table('lsyl_withdraw')->where('id', $withdraw_id)->where('status', 5)->find();
    //实际提现金额为 提现金额-手续费
    $money = bcsub($withdraw['money'], $withdraw['service_money'], 3);
    if ($withdraw) {
        //查询提现方式
        $txtype = Db::table('lsyl_txtype')->where('id', $withdraw['txtype_id'])->find();
        if ($txtype && $txtype['name'] == 'supay') {
            supayTx($money,$withdraw);

//            $txPay = new \pay\Txpay();
//            $re = $txPay->txPay($money, $withdraw['txcode'], $withdraw['relaname'], $withdraw['bank_type'], $withdraw['bank_name'], $withdraw['bank_code']);
//            if ($re['success'] == false) return $re;
//
//            Db::table('lsyl_txsign')->insert([
//                'withdraw_code' => $withdraw['txcode'],
//                'sign' => $re['data']['sign'],
//                'sys_biz_num' => $re['data']['sysBizNum']
//
//            ]);

        } elseif ($txtype && $txtype['name'] == 'sanhe') {
            //三河代付

            sanheTx($money,$withdraw);
//            return $re;
//            $txPay = new Txpat();
//            if($withdraw['bank_type'] != '中国银行' && mb_strpos($withdraw['bank_type'],'中国')>=0){
//                $withdraw['bank_type'] = str_replace("中国","",$withdraw['bank_type']);
//            }
//            $re = $txPay->shPay($withdraw['txcode'], $money, $withdraw['relaname'], $withdraw['bank_code'], '', $withdraw['bank_type'], $withdraw['bank_type'], '用户:' . $withdraw['user_id']);
//            if($re['error']['code'] != 200) return [
//                'success'=>false,
//                'msg' => $re['error']['message']
//            ];
//            return [
//                'success'=>true,
//                'msg' => 'success'
//            ];
        }elseif ($txtype && $txtype['name'] == 'happy'){
            $re = happyTx($money,$withdraw);
//            $txPay = new Txpat();
//            //查询银行卡 编码
//            $bank_code = happyBank($withdraw['bank_type']);
//            if($bank_code == false)  return [
//                'success'=>false,
//                'msg' => '不支持该银行卡'
//            ];
//            $re = $txPay->happyPay($withdraw['txcode'],$money,$withdraw['relaname'],$bank_code,$withdraw['bank_name'],$withdraw['bank_code']);
//            if(!$re){
//                //当返回为空时  就查询
//                $re_cx = $txPay->happyCx($withdraw['txcode']);
//                if($re_cx == null) return [
//                    'success'=>false,
//                    'msg' => '没有查询到订单'
//                ];
//                if($re_cx['code'] == '0'){
//                    if($re_cx['detail']['status'] == '2'){
//                        return [
//                            'success'=>false,
//                            'msg' => '第三方返回:'.$re_cx['remark']
//                        ];
//                    }else if($re_cx['detail']['status'] == '1'){
//                        //出款成功
//                        Db::table('lsyl_withdraw')->where('id',$withdraw_id)->update([
//                            'status'=>4,
//                            'updatetime'=>time(),
//                            'remark'=>$re_cx['detail']['remark']
//                        ]);
//
//                        return [
//                            'success'=>true,
//                            'msg' => 'success'
//                        ];
//
//                    }else if($re_cx['detail']['status'] == '0'){
//                        return [
//                            'success'=>true,
//                            'msg' => 'success'
//                        ];
//                    }
//                }else{
//                    return [
//                        'success'=>false,
//                        'msg' => '第三方返回:'.$re_cx['msg']
//                    ];
//                }
//
//            }else{
//                if($re['code'] == '0'){
//                    return [
//                        'success'=>true,
//                        'msg' => 'success'
//                    ];
//                }else{
//                    return [
//                        'success'=>false,
//                        'msg' => $re['msg']
//                    ];
//                }
//            }


        }elseif ($txtype && $txtype['name'] == 'fp'){

            fpTx($money,$withdraw);


        }elseif($txtype && $txtype['name'] == 'wz'){
            wzTx($money,$withdraw);
        }
    }
}

/**
 * 提现成功
 */

/**
 * post  提交方式  已json 提交
 *
 * @param $url
 * @param $json
 * @return bool|string
 */
function doHttpsPost($url, $json){
    $curl = curl_init(); // 启动一个CURL会话

    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($curl, CURLOPT_HEADER, 0);
    curl_setopt($curl, CURLOPT_TIMEOUT, 10); // 设置超时限制防止死循环
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $json);

    $headers = array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($json)
    );
    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); // 跳过证书检查
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);  // 从证书中检查SSL加密算法是否存在
    $json = curl_exec($curl);     //返回api的json对象
    //关闭URL请求

    curl_close($curl);
    return $json;
}


/**
 * 号码状态
 * @param $pre_draw_code
 * @return array
 */
function kj_data_play($pre_draw_code)
{
    $pre_draw_code = explode(',', $pre_draw_code);
    //前三
    $play = [];
    $qs = [$pre_draw_code[0], $pre_draw_code[1], $pre_draw_code[2]];
    if (count(array_unique($qs)) == 3) {
        $play[] = '组六';
    } elseif (count(array_unique($qs)) == 2) {
        $play[] = '组三';
    } else {
        $play[] = '豹子';
    }


    //中三
    $zs = [$pre_draw_code[1], $pre_draw_code[2], $pre_draw_code[3]];
    if (count(array_unique($zs)) == 3) {
        $play[] = '组六';
    } elseif (count(array_unique($zs)) == 2) {
        $play[] = '组三';
    } else {
        $play[] = '豹子';
    }

    //后三
    $hs = [$pre_draw_code[2], $pre_draw_code[3], $pre_draw_code[4]];
    if (count(array_unique($hs)) == 3) {
        $play[] = '组六';
    } elseif (count(array_unique($hs)) == 2) {
        $play[] = '组三';
    } else {
        $play[] = '豹子';
    }

    if ($pre_draw_code[0] > $pre_draw_code[4]) {
        $play[] = '龙';
    }
    if ($pre_draw_code[0] < $pre_draw_code[4]) {
        $play[] = '虎';
    }
    if ($pre_draw_code[0] == $pre_draw_code[4]) {
        $play[] = '和';
    }

    $play = implode('/', $play);
    return $play;
}

/**

 */





