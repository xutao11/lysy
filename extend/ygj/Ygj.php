<?php


namespace ygj;


use lottery\Lottery;
use think\Db;

class Ygj
{
    /**
     *
     * @user_id 用户id
     *
     */

    public function zdtz(){
        //查询所有的云挂机方案
        $ygj = Db::table('lsyl_hangplan')->where('status', '1')->select();
//        dump($ygj);

        if($ygj){
            foreach ($ygj as $k=>$v){
                //查询最新的开启时间
                $ygj_log = Db::table('lsyl_hangplan_log')->where('hangplan_id', $v['id'])->order('createtime desc')->find();
                if ($ygj_log['status'] == '0') {
                    //最新的记录时关闭
                    Db::table('lsyl_hangplan')->where('id', $v['id'])->update([
                        'status' => '0'
                    ]);
                    continue;
                }
                //判断盈亏
                $yk = $this->ykStop($v,$ygj_log);
                if($yk == false) continue;


                $param_data = [
                    'user_id'=>$v['user_id'],
                    'hangplan_id'=>$v['id'],
                    'play_id'=>$v['play_id'],
                    'unit'=>$v['unit']
                ];
                //获取当前云挂机的彩种的时间
                $lotteryClass = new Lottery();
                $gettime = $lotteryClass->getKjtime($v['lottery_id']);
                //查询当前方案及期数是否已自动投注
                $is_xz = Db::table('lsyl_betting')->where('pre_draw_issue','=',$gettime['qishu'])->where('user_id',$v['user_id'])->where('hangplan_id',$v['id'])->where('status','1')->find();
                if($is_xz) continue;
                //查询开启之后的是否已有投注
                $bettings = Db::table('lsyl_betting')->where([
                    'user_id' => $v['user_id'],
                    'lottery_id' => $v['lottery_id'],
                    'play_id' => $v['play_id'],
                ])->where('createtime', '>=', $ygj_log['createtime'])->where('hangplan_id', 'in', [0, $v['id']])->where('status', '<>', '5')->field('id,status,createtime,lottery_id,hangplan_id,bt_num,play_id,multiple,unit,gjbt_num,data_js')->select();
                //投注监控
                $jk_bool = $this->betting_jz($v, $ygj_log);

                //投注监控不满足 不能投注
                if($jk_bool == false) continue;
                $bt = $this->bt_type($v, $bettings);

                if($bt == false) continue;

                if($v['pt_type'] == '1'){
                    //直线倍投
                    $param_data['bt_num']=$bt['number'];
                }elseif ($v['pt_type'] == '2'){
                    //高级倍投
                    $param_data['gjbt_num']=$bt['number'];
                }
                //倍数
                $param_data['multiple']=$bt['multiple'];
                //获取投注号码
                $data_num = $this->getTzNumber($v,$bettings);
                if($data_num == false) continue;
                //自动投注号码局数
                $param_data['data_js'] = $data_num['data_js'];
                //投注号码
                $param_data['param'] = $data_num['data_num'];


//                $param_data = array_merge($param_data,$data_num['data_num']);
                //查询
                $param_data['lottery_id'] = $v['lottery_id'];
                $addorder = $this->addOrder($param_data);
                if($addorder == false){
                    //最新的记录时关闭
                    Db::table('lsyl_hangplan')->where('id', $v['id'])->update([
                        'status' => '0'
                    ]);
                    Db::table('lsyl_hangplan_log')->insert([
                        'hangplan_id'=>$v['id'],
                        'status'=>'0',
                        'remark'=>$addorder['msg'],
                        'createtime'=>time()
                    ]);
                    continue;

                }else{
                   dump($param_data);
                }
            }
        }
    }
    public function ykStop($ygj,$ygj_log){
        if($ygj['zsyk_y'] == '0' && $ygj['zsyk_k'] == '0')  return true;
        //查询该开启之后下注总额
        $xz_money = Db::table('lsyl_betting')->where('status','in',['2','3','4'])->where('user_id',$ygj['user_id'])->where('hangplan_id',$ygj['id'])->where('createtime','>=',$ygj_log['createtime'])->sum('xz_money');
        //中奖金额
        $jg_money = Db::table('lsyl_betting')->where('status','=','2')->where('user_id',$ygj['user_id'])->where('hangplan_id',$ygj['id'])->where('createtime','>=',$ygj_log['createtime'])->sum('jg_money');
        if($ygj['zsyk_y'] == '1'){
            //盈利
            $yl_money = bcsub($jg_money,$xz_money,3);
            //止损盈亏-赢
            if($yl_money >= $ygj['zsyk_y_value']){
                //更新
                Db::table('lsyl_hangplan')->where('id', $ygj['id'])->update([
                    'status' => '0'
                ]);
                //添加记录
                Db::table('lsyl_hangplan_log')->insert([
                    'hangplan_id'=>$ygj['id'],
                    'status'=>'0',
                    'remark'=>'盈利('.$yl_money.')大于'.$yl_money['zsyk_y_value'],
                    'createtime'=>time()
                ]);
                return false;
            }
        }
        if($ygj['zsyk_k'] == '1'){
            //亏损
            $ks_money = bcsub($xz_money,$jg_money,3);
            //止损盈亏-亏
            if($ks_money>=$ygj['zsyk_k_value']){
                //更新
                Db::table('lsyl_hangplan')->where('id', $ygj['id'])->update([
                    'status' => '0'
                ]);
                //添加记录
                Db::table('lsyl_hangplan_log')->insert([
                    'hangplan_id'=>$ygj['id'],
                    'status'=>'0',
                    'remark'=>'亏损('.$ks_money.')大于'.$ygj['zsyk_k_value'],
                    'createtime'=>time()
                ]);
                return false;
            }

        }
        return true;



    }

    /**
     * 彩票下注
     * @param lottery_id  彩种id
     * @param play_id  玩法id
     * @param play_id  玩法id
     * @param param 所选号码
     * @param multiple 倍数
     * @param unit 单位 元 角 分 厘
     * @param hangplan_id 云挂机方案id
     * @param bt_num 直线倍投局数
     * @param gjbt_num 高级倍投局数
     * @param data_js 自动投注号码局数
     */
    public function addOrder($param)
    {

        $userinfo = Db::name('user')->where('id',$param['user_id'])->find();
        $data = [
            'user_id'=>$param['id'],
            'lottery_id' => $param['lottery_id'],
            'play_id' => $param['play_id'],
            'param' => [],
            'multiple' => (int)$param['multiple'],
            'unit' => $param['unit'],
            'hangplan_id'=>isset($param['hangplan_id'])?$param['hangplan_id']:'0',
            'bt_num'=>isset($param['bt_num'])?$param['bt_num']:'0',
            'gjbt_num'=>isset($param['gjbt_num'])?$param['gjbt_num']:'0',
            'data_js'=>isset($param['data_js'])?$param['data_js']:'0',
            'createtime'=>time(),
            'updatetime'=>time()
        ];
        //查询彩种
        $lottery = Db::table('lsyl_play')->where('lsyl_play.id', $data['play_id'])->join('lsyl_lottery', 'lsyl_lottery.id = lsyl_play.lottery_id', 'left')->field('lsyl_play.*,lsyl_lottery.status lottery_status,lsyl_lottery.name lottery_name')->find();
        if ($lottery['lottery_status'] == '0') return $this->error('该彩票已禁止');
        if ($lottery['status'] == '0') return $this->error('该玩法已禁止');

        //解析参数
        $paranjson = json_decode($lottery['paramjson'], true);
        if ($paranjson) {
            foreach ($paranjson as $k => $v) {
                if (isset($param['param'][$k])) {
                    $data['param'][$k] = $param['param'][$k];
                }
            }
        }
        // 启动事务
        Db::startTrans();
        try{
            //获取注数
            $lotteryClass = new \lottery\Lottery();
            $bet = $lotteryClass->getBet($data['lottery_id'], $data['param'], $data['play_id'], $userinfo['ratio']);
            //注数
            $data['xz_num'] = $bet['count'];
            if ($data['xz_num'] < 1) throw new \think\Exception('下注数为0', 100006);

            if ($data['multiple'] < 1) throw new \think\Exception('下注倍数为0', 100006);
            //计算奖金
            $jg = jsMoney($bet['money_award'], $data['unit'], $data['multiple'], $data['xz_num'], $bet['money']);
            $data['jg_money'] = $jg['jg_money'];
            //计算下注
            $data['xz_money'] = $jg['xz_money'];
            //获取下注期数
            $kjtime = $lotteryClass->getKjtime($data['lottery_id']);
            $data['pre_draw_issue'] = $kjtime['qishu'];
            $data['kj_time'] = $kjtime['kj_time'];
            $data['ordercode'] = $this->orderCode();
            //判断用户是否禁止开奖
//            $data['is_jz'] = $userinfo['is_jz'];
//            //查询改订单是否是开将时间 开始2秒  最后2秒  内的订单  投注金额最大的以单
//            $re_max = $this->is_jz_order($data['xz_money'],$data['lottery_id'],$kjtime);
//            if($re_max['is_max'] >= 1){
//                $data['is_jz'] = 0;
//            }
//            $data['is_max']=$re_max['is_max'];
            //json化号码
            $data['param'] = json_encode($data['param']);

            //更新用户余额 f返回下注后的余额
            updateUser($data['user_id'],'-'.$data['xz_money'],'3',$data['ordercode'],$data['createtime'],$bet['lottery'].'('.$bet['play_name'].')下注');
            //状态
            $data['status'] = '1';
            Db::name('betting')->insert($data);
            // 提交事务
            Db::commit();

        } catch (\Exception $e) {
            // 回滚事务
            Db::rollback();
            return [
                'status'=>false,
                'msg'=>$e->getMessage()
            ];

        }
        return [
            'status'=>true,
            'msg'=>'自动投注成功'
        ];

    }



    /**
     *创建订单
     *
     */
//    public function addOrder($param_data,$lottery_id){
//
//        $url = $this->getApiUrl($lottery_id).'/orderAdd';
//        $param_data['editBetting'] = 'ls7777777';
//        dump($param_data);
//        $re = getCurl($url,'post',$param_data);
//        dump($re);
//        $re = json_decode($re,true);
//
//        if($re['code'] == 1){
//            return [
//                'code'=>200,
//                'data_num'=>$re['data']
//            ];
//        }else{
//            return [
//                'code'=>0,
//                'msg'=>$re['msg']
//            ];
//        }
//
//
//    }




    /**
     * 获取投注的号码
     * data_js  //号码局数
     * data_num  //号码
     */
    public function getTzNumber($ygj, $bettings)
    {

        if ($ygj['gjtype_id'] == '1') {
            //定码轮换
            return [
                'data_js' => 0,
                'data_num' => json_decode($ygj['data_num'],true)
            ];

        } elseif ($ygj['gjtype_id'] == '2') {
            //高级定码轮换
            //获取追后一条记录
            //解析 号码
            $data_nums = json_decode($ygj['data_num'],true);

            $data_nums_new = [];

            foreach ($data_nums as $k=>$v){
                $data_nums_new[$v['number']] = $v;
            }
//            dump($data_nums_new);
            //根据key 排序
            ksort($data_nums_new);
            $count_betting = count($bettings);
            if($count_betting == 0){
                //之前没有投注记录就返回第局数最小的
                $first = reset($data_nums_new);
                $data_num = [
                    'data_js'=>$first['number'],
                    'data_num'=>$first['data_num']
                ];
                return $data_num;
            }else{
                //最后一条投注记录
                $last_betting = $bettings[$count_betting-1];
                if($last_betting['hangplan_id'] == 0 || $last_betting['data_js'] == 0){
                    //返回第一记录
                    $first = reset($data_nums_new);
                    $data_num = [
                        'data_js'=>$first['number'],
                        'data_num'=>$first['data_num']
                    ];
                    return $data_num;
                }else{
                    if(isset($data_nums_new[$last_betting['data_js']])){
                        $new_js_data = $data_nums_new[$last_betting['data_js']];

                        if($last_betting['status'] == '2'){
                            //中奖
                            $next_js_data = $data_nums_new[$new_js_data['win']];
                        }elseif (in_array($last_betting['status'],['3','4'])){
                            //未中奖
                            $next_js_data = $data_nums_new[$new_js_data['no_win']];
                        }else{
                            return false;
                        }
                        $data_num = [
                            'data_js'=>$next_js_data['number'],
                            'data_num'=>$next_js_data['data_num']
                        ];
                        return $data_num;
                    }else{
                        //没有查询到该局数  就返回第一局
                        $first = reset($data_nums_new);
                        $data_num = [
                            'data_js'=>$first['number'],
                            'data_num'=>$first['data_num']
                        ];
                        return $data_num;
                    }
                }
            }
            return $data_num;
        }
    }

    /**
     * 更具倍投获取倍数
     * return [
     * 'number'=>1,  //局数
     * 'multiple'=>$zx_bt[0]  //倍数
     * ];
     */
    public function bt_type($ygj, $bettings)
    {
        $count_betting = count($bettings);

        //直线倍投
        if ($ygj['pt_type'] == '1') {
            //分解直线倍投规则
            $zx_bt = explode(',', $ygj['zx_bt']);

            //直线倍投
            if ($count_betting == 0) {
                //如果没有自动投注
                return [
                    'number' => 1,
                    'multiple' => $zx_bt[0]
                ];
            }
            //获取最后一条记录
            $last_betting = $bettings[$count_betting - 1];
            dump($last_betting);
            if($last_betting['status'] == '1') return false;
            //最后一条记录不是自动投注
            if ($last_betting['hangplan_id'] == 0) {
                return [
                    'number' => 1,
                    'multiple' => $zx_bt[0]
                ];
            }


            if ($ygj['fp_type'] == '1') {
                //中翻倍
                if ($last_betting['status'] == '2') {
                    //判断直线倍投是否已结束一轮
                    if ($last_betting['bt_num'] >= count($zx_bt)) {
                        return [
                            'number' => 1,
                            'multiple' => $zx_bt[0]
                        ];
                    } else {
                        return [
                            'number' => $last_betting['bt_num'] + 1,
                            'multiple' => $zx_bt[$last_betting['bt_num']]
                        ];
                    }
                } elseif ($last_betting['status'] == '3' || $last_betting['status'] == '4') {
                    return [
                        'number' => 1,
                        'multiple' => $zx_bt[0]
                    ];
                }
            } else if ($ygj['fp_type'] == '2') {

                //挂翻倍
                if ($last_betting['status'] == '3' || $last_betting['status'] == '4') {
                    //判断直线倍投是否已结束一轮
                    if ($last_betting['bt_num'] >= count($zx_bt)) {

                        return [
                            'number' => 1,
                            'multiple' => $zx_bt[0]
                        ];
                    } else {

                        return [
                            'number' => $last_betting['bt_num'] + 1,
                            'multiple' => $zx_bt[$last_betting['bt_num']]
                        ];
                    }
                } elseif ($last_betting['status'] == '2') {

                    return [
                        'number' => 1,
                        'multiple' => $zx_bt[0]
                    ];
                }
            }
        } else {
            //查询高级倍投
            //高级倍投
            if ($count_betting == 0) {
                $frist_gjbt = Db::table('lsyl_gjbt_data')->where('gjbt_id', $ygj['gjbt_id'])->where('number', '1')->find();
                //如果没有自动投注

                return [
                    'number' => $frist_gjbt['number'],
                    'multiple' => $frist_gjbt['multiple']
                ];
            }
            //获取最后一条记录
            $last_betting = $bettings[$count_betting - 1];
            if($last_betting['status'] == '1') return false;
            if ($last_betting['hangplan_id'] == '0' || $last_betting['gjbt_num'] == 0) {
                $frist_gjbt = Db::table('lsyl_gjbt_data')->where('gjbt_id', $ygj['gjbt_id'])->where('number', '1')->find();
                //如果没有自动投注

                return [
                    'number' => $frist_gjbt['number'],
                    'multiple' => $frist_gjbt['multiple']
                ];
            }
            //查询最后一次高级倍投的局数
            $new_gjbt_js = Db::table('lsyl_gjbt_data')->where('gjbt_id', $ygj['gjbt_id'])->where('number', $last_betting['gjbt_num'])->find();
            //当前自动投足的局数 及 倍数
            if ($last_betting['status'] == '2') {
                //中奖
                $z_data = Db::table('lsyl_gjbt_data')->where('gjbt_id', $ygj['gjbt_id'])->where('number', $new_gjbt_js['win'])->find();

                if(!$z_data){
                    Db::table('lsyl_hangplan')->where('id', $last_betting['hangplan_id'])->update([
                        'status' => '0'
                    ]);
                    //添加记录
                    Db::table('lsyl_hangplan_log')->insert([
                        'hangplan_id'=>$last_betting['hangplan_id'],
                        'status'=>'0',
                        'remark'=>'高级倍投没有该'.$new_gjbt_js['win'].'局数',
                        'createtime'=>time()
                    ]);
                    return false;
                }else{
                    return [
                        'number' => $z_data['number'],
                        'multiple' => $z_data['multiple']
                    ];
                }

            } elseif (in_array($last_betting['status'], ['3', '4'])) {

                //挂后
                $z_data = Db::table('lsyl_gjbt_data')->where('gjbt_id', $ygj['gjbt_id'])->where('number', $new_gjbt_js['no_win'])->find();
                if(!$z_data){
                    Db::table('lsyl_hangplan')->where('id', $last_betting['hangplan_id'])->update([
                        'status' => '0'
                    ]);
                    //添加记录
                    Db::table('lsyl_hangplan_log')->insert([
                        'hangplan_id'=>$last_betting['hangplan_id'],
                        'status'=>'0',
                        'remark'=>'高级倍投没有该'.$new_gjbt_js['win'].'局数',
                        'createtime'=>time()
                    ]);
                    return false;
                }else{
                    return [
                        'number' => $z_data['number'],
                        'multiple' => $z_data['multiple']
                    ];
                }
            }
        }
    }


    /**
     * 投注监控
     * 可以自动投注 返回 true  不能自东投注 返回 false
     *
     * 投注监控是判断 之后的期数 和方案的投注号码匹配
     */
    public function betting_jz($ygj, $ygj_log)
    {


        if ($ygj['betting_jk'] == '1') {
            //判断该方案 这个时间端中是否一斤开始投注
            $betting_count = Db::table('lsyl_betting')->where('hangplan_id',$ygj['id'])->where('createtime','>=',$ygj_log['createtime'])->count();
            if($betting_count >0) return true;
            //开启监控
            //解析监控规则
            $jk_gz = str_split($ygj['jk_gz']);
            $jk_gz_count = count($jk_gz);
            //查询该方案开启后的开奖记录
            $kj_log = Db::table('lsyl_kjlog')->where('lottery_id',$ygj['lottery_id'])->where('pre_draw_time','>=',date('Y-m-d H:i:s',$ygj_log['createtime']))->order('pre_draw_time desc')->limit($jk_gz_count)->field('pre_draw_time,pre_draw_issue,pre_draw_code')->select();
            if(count($kj_log) != $jk_gz_count) return false;
            //获取该云挂机方案的第一次投注号码
            $data_num = null;
            if($ygj['gjtype_id'] == '1'){
                //定码轮换
                $data_num =  json_decode($ygj['data_num'],true);

            }elseif ($ygj['gjtype_id'] == '2'){
                //高级定码轮换
                $data_nums = json_decode($ygj['data_num'],true);
                $data_nums_new = [];
                foreach ($data_nums as $k=>$v){
                    $data_nums_new[$v['number']] = $v;
                }
                //根据key 排序
                ksort($data_nums_new);
                //获取局数最小的一局
                $first = reset($data_nums_new);
                $data_num = $first['data_num'];

            }
            if($data_num == null) return true;
            $kj_str = '';
            foreach($kj_log as $k=>$v){
                $kj_status = $this->getKjStatus($ygj['play_id'],$data_num,$ygj['lottery_id'],$v['pre_draw_code']);
                if($kj_status['status'] == '3'){
                    //未中奖
                    $kj_str.='0';
                }elseif ($kj_status['status'] =='2'){
                    //中奖
                    $kj_str.='1';
                }else{
                    return false;
                }
            }

            $kj_str = substr($kj_str,-$jk_gz_count);
            if($kj_str == $ygj['jk_gz']) return true;
            return false;
        }else{
            return true;
        }


    }
    public function test(){
        $kj_str = '001001001';
        $jk_gz_count = 1;
        $kj_str = substr($kj_str,-$jk_gz_count);
        echo $kj_str;
//        $play_id = '937';
//        $data_num = [
//            'two_num'=>'1,2,3,4',
//            'three_num'=>'1,2,3,4',
//            'four_num'=>'1,2,3,4',
//            'five_num'=>'1,2,3,4',
//        ];
//        $lottery_id = '2';
//        $pre_draw_code = '0,5,1,9,0';
//        $re = $this->getKjStatus($play_id,$data_num,$lottery_id,$pre_draw_code);
//        dump($re);


    }


    public function getKjStatus($play_id,$data_num,$lottery_id,$pre_draw_code){
        switch ($lottery_id){
            case '1':
                $controller = 'Cqssc';
                $action = 'bet_'.$play_id;
                $pre_draw_code = $this->formatNum(explode(',', $pre_draw_code),9,0);
                break;
            case '2':
                $controller ='Qqyfc';
                $action = 'bet_'.$play_id;
                $pre_draw_code = $this->formatNum(explode(',', $pre_draw_code),9,0);
                break;
            case '4':
                $controller ='Qqwfc';
                $action = 'bet_'.$play_id;
                $pre_draw_code = $this->formatNum(explode(',', $pre_draw_code),9,0);
                break;
            case '5':
                $controller ='Qqsfc';
                $action = 'bet_'.$play_id;
                $pre_draw_code = $this->formatNum(explode(',', $pre_draw_code),9,0);
                break;
            case '8':
                $controller = 'Ylhlwfc';
                $action = 'bet_'.$play_id;
                $pre_draw_code = $this->formatNum(explode(',', $pre_draw_code),9,0);
                break;
            case '9':
                $controller ='Guangdong';
                $action = 'play_'.$play_id;
                $pre_draw_code = $this->formatNum(explode(',', $pre_draw_code),11,1);
                break;
            case '10':
                $controller ='Jiangxi';
                $action = 'play_'.$play_id;
                $pre_draw_code = $this->formatNum(explode(',', $pre_draw_code),11,1);
                break;
            case '11':
                $controller ='Sandong';
                $action = 'play_'.$play_id;
                $pre_draw_code = $this->formatNum(explode(',', $pre_draw_code),11,1);
                break;
            case '13':
                $controller ='Xyft';
                $action = 'bet_'.$play_id;
                $pre_draw_code = $this->formatNum(explode(',', $pre_draw_code),10,1);
                break;
            case '15':
                $controller ='Ylhlyfc';
                $action = 'bet_'.$play_id;
                $pre_draw_code = $this->formatNum(explode(',', $pre_draw_code),9,0);
                break;
            case '20':
                $controller ='Xjssc';
                $action = 'bet_'.$play_id;
                $pre_draw_code = $this->formatNum(explode(',', $pre_draw_code),9,0);
                break;
            case '21':
                $controller ='Jiangsuk3';
                $action = 'play_'.$play_id;
                $pre_draw_code =$this->formatNum(explode(',', $pre_draw_code),6,1);
                break;
            case '22':
                $controller ='Guangxik3';
                $action = 'play_'.$play_id;
                $pre_draw_code =$this->formatNum(explode(',', $pre_draw_code),6,1);
                break;
            default:
                return false;
                break;
        }
        $betting['param']  = json_encode($data_num);
        $re = controller($controller)->$action($betting,$pre_draw_code);
        return $re;

    }

    /**
     * 获取接口地址
     */
    public function getApiUrl($lottery_id){

        $url = 'http://118.193.38.53/api/';
//        $url = 'http://127.0.0.1/api/';
        switch ($lottery_id){
            case '1':
                $controller = 'Cqssc';
                break;
            case '2':
                $controller ='Qqyfc';
                break;
            case '4':
                $controller ='Qqwfc';
                break;
            case '5':
                $controller ='Qqsfc';
                break;
            case '8':
                $controller = 'Ylhlwfc';
                break;
            case '9':
                $controller ='Guangdong';
                break;
            case '10':
                $controller ='Jiangxi';
                break;
            case '11':
                $controller ='Sandong';
                break;
            case '13':
                $controller ='Xyft';
                break;
            case '15':
                $controller ='Ylhlyfc';
                break;
            case '20':
                $controller ='Xjssc';
                break;
            case '21':
                $controller ='Jiangsuk3';
                break;
            case '22':
                $controller ='Guangxik3';
                break;
            default:
                return false;
                break;

        }
        return $url.$controller;
    }

    /**
     * 获取下期开奖时间
     */
    public function gettime($lottery_id = 2){
        $url = $this->getApiUrl($lottery_id).'/getKjtime';
        $param = [
            'editBetting'=>'ls7777777',
            'user_id'=>1
        ];
        $re = getCurl($url,'post',$param);
        $re = json_decode($re,true);
        if($re['code'] == 1){
            return $re['data'];
        }else{
            return false;
        }
    }

    public function formatNum($data,$max=9,$min=0)
    {

        foreach ($data as $k => $v) {
            if ($v > $max || $v < $min ||$v == '') {
                unset($data[$k]);
            } else {
                $data[$k] = (int)sprintf("%02d", $v);
            }
        }
        return $data;
    }

}