<?php


namespace pay;


use think\Log;

class Pay
{


    /**
     *
     * 寻找支付  FP
     *
     *
     */
    public function FpPay($payMethod, $fxdesc, $money, $bizNum)
    {
        $url = config('notifyAddress');
        //密钥
        $key = 'viKWrpclXZUCuZoqrfYefsxVlxgKvpQy';
        //商户id
        $fxid = '2020171';
        //商户订单号
        $fxddh = $bizNum;
        //商品名称
        $fxdesc = $fxdesc;
        //支付金额(元)
        $fxfee = $money;
        //异步通知地址
        $fxnotifyurl = $url . '/api/Notify/fpNotify';
        //同步通知地址
        $fxbackurl = '';
        //异步数据类
//        $fxnotifystyle = '';
        //附加信
        $fxattch = $bizNum;
        //签名
        $fxsign = md5($fxid . $fxddh . $fxfee . $fxnotifyurl . $key); //加密
        //用户支付时设备的 IP 地址
        $fxip = $this->getClientIP();

        $url = 'http://fp.find-pay.com/pay';
        $data = [
            'fxid' => $fxid,
            'fxddh' => $fxddh,
            'fxdesc' => $fxdesc,
            'fxfee' => $fxfee,
            'fxnotifyurl' => $fxnotifyurl,
            'fxbackurl' => $fxbackurl,
            'fxattch' => $fxattch,
            'fxsign' => $fxsign,
            'fxip' => $fxip,
            "fxpay" => $payMethod,
        ];
//        dump($data);
        $re = getHttpContent($url, 'POST', $data);
        return $re;


    }

    /**
     * ifpay
     *$tradeAmt   RMB
     * $orderRemark  商户订单ID
     * $receipt
     */
    public function ipPay($tradeAmt, $orderRemark, $receipt)
    {
        $url = 'https://api.ifpay.gold/api/orderShUserReceipt';
        $userId = '';//商户ID
//        $hmac='ww';  //签名字符串
//        $nbNotifyUrl = 'http://118.193.38.53/api/Notify/ifNotify';   //
        $nbNotifyUrl = config('notifyAddress') . '/api/Notify/ifNotify';

        if (!in_array($receipt, [1, 3, 5])) throw new \think\Exception('ifpay没有该支付通道', 100006);
        if ($receipt == 3) {
            $userId = 2363;
            $key = '1D9D93A29A03E10919E2F184773A485E';
        }
        if ($receipt == 5) {
            $userId = 2362;
            $key = 'CEAD79ACE6CA2FDFEF884F803B3C26A8';
        }
        if ($receipt == 1) {
            $userId = 2364;
            $key = 'E1A93732790F08C65FD179E29A9A068D';
        }

        /**
         * Json数组字符串：
         * [1]指定支付宝；
         * [2]指定微信；
         * [3]指定银行卡；
         * [4]指定PayPal；
         * [5]指定USDT；
         * [6]指定云闪付
         */
        $receipt = '[' . $receipt . ']';
        $param = [
            'userId' => $userId,//商户ID
            'tradeAmt' => $tradeAmt,
//            'hmac'=>$hmac,
            'orderRemark' => $orderRemark,
            'nbNotifyUrl' => $nbNotifyUrl,
            'receipt' => $receipt
        ];
        $hmac_str = $key . $param['nbNotifyUrl'] . $param['orderRemark'] . $param['receipt'] . $param['tradeAmt'] . $param['userId'];
        $param['hmac'] = strtoupper(md5($hmac_str));

        $re = getHttpContent($url, 'POST', $param);
        $re = json_decode($re, true);
        $re['userId'] = $param['userId'];
        return $re;


    }
    /**
     * Dora  支付
     * @param $amount_money //充值金额（支付宝/支付宝原生码 充值金额范围 295.00~5000.00）
     * @param $company_order_id //商户订单号
     * @param $channel_code  //渠道编码 [1 (支付宝)、12（自由码）、16（支付宝原生码）]
     * @param $player_id  ////玩家ID(支付宝会风控玩家ID，请确保每次充值的玩家ID都不相同,如果玩家ID充值时相同，会造成充值失败)
     *
     */
    public function doraPay($amount_money,$company_order_id,$channel_code,$player_id){
        //接口i地址
        $url = 'https://counter.cowge.com:8096/dora/counter/deposit';
        $api_version = '1.6';
        $company_id = '370';    // 商户id（查阅成功开户文档）
//        $notify_url = 'http://118.193.38.53/api/Notify/doraNotify';   //回调地址(这个路径是玩家充值成功后，Dora会根据此路径返回给商户充值成功的数据信息)
//        $sign = '';   //签名(签名是根据除sign外其他9个参数用MD5工具类加密算出来的，然后赋给sign字段)
        $terminal = '1';  //终端类型
        $timestamp = time();  //时间戳
        $key = '2e35c0d5c29ac5568afba67a82a87338';
        $notify_url = config('notifyAddress') .'/api/Notify/doraNotify';

        $data = [
            'amount_money'=>$amount_money,
            'api_version'=>$api_version,
            'channel_code'=>$channel_code,
            'company_id'=>$company_id,
            'company_order_id'=>$company_order_id,
            'notify_url'=>$notify_url,
            'terminal'=>$terminal,
            'timestamp'=>$timestamp,
            'player_id'=>$player_id
        ];
        // //签名(签名是根据除sign外其他9个参数用MD5工具类加密算出来的，然后赋给sign字段)
        $sign_str = "amount_money={$amount_money}&api_version={$api_version}&channel_code={$channel_code}&company_id={$company_id}&company_order_id={$company_order_id}&notify_url={$notify_url}&player_id={$player_id}&terminal={$terminal}&timestamp={$timestamp}&api_Key={$key}";
        $data['sign'] = strtoupper(md5($sign_str));
//        dump($data);
        $param = json_encode($data,JSON_UNESCAPED_UNICODE);
        $re = doHttpsPost($url,$param);
//        $re = getHttpContent($url,'POST',$data);

        $re = json_decode($re,true);
        return $re;
    }
    /**
     * 鸿运通
     * zfb                        支付宝扫码            交易金额500-10000  24小时

    zfb2                       支付宝H5/wap      交易金额500-10000  24小时


    wy                           网关/银联          交易金额100-10000  交易时间8:00-23:00

    kzk                          卡转卡               交易金额100-30000  24小时
     *
     */
    public function hytPay($Amount,$MerchantUniqueOrderId,$PayTypeId){

        $url = 'http://service.88wu1i81r19.ofxtw.com/InterfaceV4/CreatePayOrder';


        //订单金额 单位元，建议格式#.##
//        $Amount = 10.00;
        //ipIP地址
        $Ip = request()->ip();
        //商户ID
        $MerchantId ='205526';
        //商户唯一订单ID
//        $MerchantUniqueOrderId = '';
        //支付结果异步通知地址
//        $NotifyUrl = 'http://118.193.38.53/api/Notify/hytNotify';
        $notifyAddress = config('notifyAddress')."/api/Notify/hytNotify";
        //支付编码（支付方式）
//        $PayTypeId = '';
        //支付结果异同步跳转地址
        $ReturnUrl = '';
        $param = [
            'Amount'=>$Amount,
            'Ip'=>$Ip,
            'MerchantId'=>$MerchantId,
            'MerchantUniqueOrderId'=>$MerchantUniqueOrderId,
            'NotifyUrl'=>$notifyAddress,
            'PayTypeId'=>$PayTypeId,
            'ReturnUrl'=>$ReturnUrl
        ];
        ksort($param);
        $data = '';
        foreach($param as $pk=>$pv){
            $data.="{$pk}={$pv}&";
        }
        //去掉最后一个&字符
        $data = substr($data,0,strlen($data)-1);
        $re = file_get_contents($url.'?'.$data);
        Log::write('鸿运通(失败)日志信息:'.$url.'?'.$data,'hyt_error鸿运通');
        $re = json_decode($re,true);
        return $re;

    }

    /**
     * supay 支付
     * //⽀付⽅式, 值(区分⼤⼩写):
     * //wechat, wechatMobile,
     * //wechatBank, alipay,
     * //alipayH5, bank, ysf,
     * //pddWechat, pddAlipay
     * //● wechat 微信 ● wechatMobile 微信 ⼿机号转账 ● wechatBank 微信银 ⾏卡转账 ● alipay ⽀付宝 ● alipayH5 ⽀付宝H5 ● bank 银⾏卡 ● ysf 云闪付
     * $payMethod = 'wechat';
     * //用户id
     * $userId = 1;
     * //⾦额, 整数, 以分为单位
     * $money = 1;
     * //商户业务单号，由商户⽣成 ，⽤于标识由商户发起的交 易申请流⽔，不可重复
     * $bizNum ='qq25251656';
     * //充值结果通知地址
     * $notifyAddress = '';
     */
    public function suPay($payMethod, $userId, $money, $bizNum)
    {
        //商户API Key
//        $key = 'E071149599174961F92F6583D86F7588'; //测试
        $key = '989B7AB30F894B43E277E0CB40937443';
        //商户ID ，由OTC ⽣成，⽤ 于标识商户
//        $merchantId = '5e68b0278f1b136b5c1e6212';  //测试
        $merchantId = '5e799fd88f2bd07f63562f3c';
        $notifyAddress = config('notifyAddress').'/api/Notify/supayNotify';

        //recharge', 固定值
        $type = 'recharge';
        //签名
        $sign = "bizNum={$bizNum}&merchantId={$merchantId}&money={$money}&notifyAddress={$notifyAddress}&payMethod=$payMethod&type={$type}&userId={$userId}&key={$key}";
        $sign = strtoupper(md5($sign));

        $notifyAddress = urldecode($notifyAddress);
//        $url = "https://test.su-pay.co/b/recharge?merchantId={$merchantId}&bizNum={$bizNum}&money={$money}&payMethod={$payMethod}&notifyAddress={$notifyAddress}&userId={$userId}&type={$type}&sign={$sign}";
        $url = "https://su-pay.co/b/recharge?merchantId={$merchantId}&bizNum={$bizNum}&money={$money}&payMethod={$payMethod}&notifyAddress={$notifyAddress}&userId={$userId}&type={$type}&sign={$sign}";
        return $url;

    }


}