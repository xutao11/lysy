<?php


namespace pay;


use http\Exception\InvalidArgumentException;

class Txpay
{



    /**
     * @param $money 提现金额
     * @param $bizNum 商户业务单号，由商户⽣成 ，⽤于标识由商户发起的交 易申请流⽔，不可重复
     * @param $name 收款人姓名
     * @param $bank 银行卡
     * @param $branch 支行名
     * @param $account 银行卡号
     */
    public function txPay($money, $bizNum, $name, $bank, $branch, $account)
    {
        //商户ID ，本平台⽣成，⽤于标识商户
        $merchantId = '5e80573d2052b96564005107';
        //充值结果通知地址
//    public $notifyAddress = 'http://lishengyule.com/api/Notify/supayTxNotify';
        $notifyAddress = config('notifyAddress') .'/api/Notify/supayTxNotify';
        //key
        $key = '4D7BE4B9EF0EFFF613F5718E6FB3A9C3';
        //接口地址
        $url = 'https://benteng.pro';
        $data = [
            'merchantId' => $merchantId,
            'notifyAddress' => $notifyAddress,
            'bizNum' => $bizNum,
            'name' => $name,
            'bank' => $bank,
            'branch' => $branch,
            'account' => $account,
            'money' => $money * 100

        ];
//        $signStr ="account={$data['account']}&bank={$data['bank']}&bizNum={$data['bizNum']}&branch={$data['branch']}&merchantId={$data['merchantId']}&money=value&name={$data['name']}&notifyAddress={$data['notifyAddress']}&key={$this->key}";


        $str = "account={$data['account']}&bank={$data['bank']}&bizNum={$data['bizNum']}&branch={$data['branch']}&merchantId={$data['merchantId']}&money={$data['money']}&name={$data['name']}&notifyAddress={$data['notifyAddress']}&key={$key}";
        $data['sign'] = strtoupper(md5($str));

        $url = $url . '/api/df/add';
        $re = getHttpContent($url, 'POST', $data);
        $re = json_decode($re, true);
        return $re;
    }


    /**
     * 三河 代付
     */
    public function shPay($orderNo, $amount,$accountName, $accountNo, $mobile,$openBankName, $bankName, $memo = '')
    {
        //array 类型参数列表
        $paras = array();

        $paras['channel'] = '';
        //商户名
        $paras['merchant'] = 'ls7777';
        //商户代付订单号
        $paras['orderNo'] = $orderNo;
        //订单金额
        $paras['amount'] = number_format($amount, 2, '.', '');
        //后台通知URL，订单完成后会通到此URL
        $paras['notifyUrl'] = config('notifyAddress') .'/api/Notify/shTxNotify';
        //付款方式：固定 1
        $paras['payType'] = 1;
        //收款人姓名
        $paras['accountName'] = $accountName;
        //收款人账号
        $paras['accountNo'] = $accountNo;
//        账号类型: 0-对私账号，1-对公账号
        $paras['accountType'] = 0;
        //手机号码
        $paras['mobile'] = $mobile;
        //开户行名称，必须在提供的银行名称列表中。
        //参考：银行列表.txt
        $paras['openBankName'] = $openBankName;
        //银行名称
        $paras['bankName'] = $bankName;

//        $paras['bankCode'] = $bankCode;
//        $paras['businessType'] = $businessType;
        //备注
        $paras['memo'] = $memo;
        // var_dump($paras);

        $json = $this->invoke('createOrder', $paras);



        $obj = json_decode($json,true);


        return $obj;
    }

    /**
     * @param $orderNo  //訂單號
     * @param $bizAmt  //金額 单位：元
     * @param $accName //收款人姓名
     * @param $bankCode //銀行編碼
     * @param $bankBranchName //支行名稱
     * @param $cardNo  //卡號
     * @return mixed
     */
    public function happyPay($orderNo,$bizAmt,$accName,$bankCode,$bankBranchName,$cardNo){
        $url = 'http://khgri4829.com:6084/api/defray/V2';

        $key = '1de1355aa7a54115b57c41c0ec658325'; //秘钥
        $param = [
            'version'=>'V2',  //版本号
            'signType'=>'MD5',  //签名类型
            'merchantNo'=>'API5074783094627413' ,   //商户号 本系统统一分配
            'date'=>date('YmdHis',time()),   //时间
            'channleType'=>'0',      //通道类型
//            'sign'=>'',         //加密串
            'orderNo'=>$orderNo, //訂單號
            'bizAmt'=>(int)$bizAmt,   //金額 单位：元
            'accName'=>$accName,  //收款人姓名
            'bankCode'=>$bankCode,  //銀行編碼
            'bankBranchName'=>$bankBranchName, //支行名稱
            'cardNo'=>$cardNo,  //卡號
            'noticeUrl'=>config('notifyAddress') .'/api/Notify/happyTxNotify',
            'openProvince'=>'北京', // 开户省
            'openCity'=>'北京',  //开户市
            // 'phone'=>'', //手机号
        ];

        //签名
        ksort($param);
        $sign_str = '';
        foreach($param as $pk=>$pv){
            $sign_str.="{$pk}={$pv}&";
        }
        $sign_str = substr($sign_str,0,strlen($sign_str)-1);
        $sign_str = $sign_str.$key;

        $param['sign'] = md5($sign_str);



        $param = json_encode($param,JSON_UNESCAPED_UNICODE);


        $re = $this->doHttpsPost($url, $param);
        $re = json_decode($re, true);
        return $re;

    }

    /**
     * happy  查询订单
     *
     */
    public function happyCx($orderNo){
        $url = 'http://khgri4829.com:6084/api/defray/queryV2';
        $key = '1de1355aa7a54115b57c41c0ec658325'; //秘钥
        $param = [
            'version'=>'V2',  //版本号
            'signType'=>'MD5',  //签名类型
            'merchantNo'=>'API5074783094627413' ,   //商户号 本系统统一分配
            'date'=>date('YmdHis',time()),   //时间
            'orderNo'=>$orderNo, //訂單號
        ];
        ksort($param);
        $sign_str = '';
        foreach($param as $pk=>$pv){
            $sign_str.="{$pk}={$pv}&";
        }

        $sign_str = substr($sign_str,0,strlen($sign_str)-1);
        $sign_str = $sign_str.$key;
        $param['sign'] = md5($sign_str);
        $param = json_encode($param,JSON_UNESCAPED_UNICODE);
        $re = $this->doHttpsPost($url, $param);
        $re = json_decode($re, true);
        return $re;


    }
    /***********************************************************
     * API调用帮助函数
     *
     * @access protected
     * @param string $method 远程函数名
     * @param array $paras 参数列表，关联数组
     *InvalidArgumentException
     * @return string(valid json)
     ***********************************************************/
    protected function invoke($method, $paras)
    {
        $appkey = '2D2CE774-CCE7-F633-EFD5-36ACEB24C8ED';
        $request = new \stdClass;
        $request->method = $method;
        if($paras == null) {
            $request->params = array();
        } elseif (is_array($paras)) {
            $request->params = $paras;
        } else {

            throw new InvalidArgumentException('$paras must be array or object');
        }
        // 签名流程
        {
            ksort($paras);
            //urldecode 防止有中文时被编码，造成验签失败
            $signStr = urldecode(http_build_query($paras)).$appkey;
//            'accountName=测试&accountNo=1478523696332584&accountType=0&amount=10&bankName=中国工商银行&memo=cesium&merchant=ls7777&mobile=13000000000&notifyUrl=http://103.149.26.233/api/Notify/shTxNotify&openBankName=中国工商银行&orderNo=Tx1212121&payType=12D2CE774-CCE7-F633-EFD5-36ACEB24C8ED';
            $request->sign = md5($signStr);
        }
        $post = json_encode($request);

        $api = 'https://www.trp1688.com/gateway/pay.json';
        $resp = $this->doHttpsPost($api, $post);

        $this->_lastRequest = array();
        $this->_lastRequest['request'] = $post;
        $this->_lastRequest['response'] = $resp;
        return $resp;
    }
    // post json data over https
    private function doHttpsPost($url, $json){
        $curl = curl_init(); // 启动一个CURL会话

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_TIMEOUT, 10); // 设置超时限制防止死循环
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $json);

        $headers = array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($json)
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); // 跳过证书检查
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);  // 从证书中检查SSL加密算法是否存在
        $json = curl_exec($curl);     //返回api的json对象
        //关闭URL请求

        curl_close($curl);
        return $json;
    }
    /**
     * @param $orderNo  //訂單號
     * @param $bizAmt  //金額 单位：元
     * @param $accName //收款人姓名
     * @param $bankBranchName //支行名稱
     * @param $cardNo  //卡號
     * @return mixed
     */
    public function fpTxpay($orderNo,$bizAmt,$accName,$bankBranchName,$cardNo){
        $url = 'http://fp.find-pay.com/Pay';
        $key = 'sFbEOTWEqVAWpGBlPpVYNPJiPFsMugLh';
        $fxid = '2020213'; //用户 ID唯一号，由支付平台提供
        $fxaction = 'repay'; // 用户查询动作，这里填写【repay】
        $param = [
            'fxid'=>$fxid, //用户 ID唯一号，由支付平台提供
            'fxaction'=>$fxaction, // 用户查询动作，这里填写【repay】
            'fxnotifyurl'=>config('notifyAddress') .'/api/Notify/fpTxNotify', //异步返回地址
            'fxbody'=>'', // 提交代付订单信息参考下方订单信息域（json 格式字符串）
            'fxsign'=>'', //签名
        ];

        $param_fxbody[] = [
            'fxddh'=>$orderNo, //用户订单号
            'fxdate'=>date('YmdHis',time()), // 格式 YYYYMMDDhhmm
            'fxfee'=>$bizAmt,  //代付金额 单位 0.01 元
            'fxbody'=>$cardNo, //收款人的账户
            'fxname'=>$accName, //收款人的开户名
            'fxaddress'=>$bankBranchName, //收款人的开户行，例如中国银行
            'fxzhihang'=>$bankBranchName, //收款人的开户地址所在支行
            // 'fxsheng'=>'', //收款人的开户地址所在省
            // 'fxshi'=>'', //收款人的开户地址所在市
            // 'fxlhh'=>'', //开户卡对应银行联行号
        ];
        $param['fxbody'] = json_encode($param_fxbody);
        //签名【md5(用户 ID+用户查询动作+订单信息域（json 格式字符串）+用户秘
        //钥)】


        $fxsign = md5($fxid . $fxaction .$param['fxbody']. $key); //加密
        $param['fxsign'] = $fxsign;

        $re = getHttpContent($url,'POST',$param);

        $re = json_decode($re,true);
        $re['fxbody'] = json_decode($re['fxbody'],true);
        return $re;

    }
    /**
     * 万众代付
     * @param $amount  交易金额（元） 纪录到小数点后两位数
     * @param $bankAccountName //收款银行户名
     * @param $bankAccountNo //收款银行帐号
     * @param $bankCode //收款银行代码（请看 参考资料-银行代码表）
     * @param $orderNo ////订单编号 (必须唯一)
     * @param $userName ////商户端用户名 (交易进行者的身份识别码)
     */
    public function wzTxpay($amount,$bankAccountName,$bankAccountNo,$orderNo,$bankCode,$userName){
        $url = 'http://gateway.ajy2018.vip/api/v1.0/Trade/Withdraw';

        $key = '7225debf0c174f1c80593cab28b5b6ae';
        $amount = sprintf("%.2f", $amount);
//        $amount = 500.00;   //交易金额（元） 纪录到小数点后两位数
//        $bankAccountName = ''; //收款银行户名
//        $bankAccountNo = '';  //收款银行帐号
//        $bankCode = '';  //收款银行代码（请看 参考资料-银行代码表）
        $callbackUrl = config('notifyAddress') .'/api/Notify/wzTxNotify'; //回调通知地址,不传值则不通知
        $clientIP = request()->ip(); //商户用户端IP
        $merchantNo = '109249484170'; //商户号
//        $orderNo = ''; //订单编号 (必须唯一)
        $payType = 2002;//交易类型 2002 提現下发
//        $userName = '0'; //商户端用户名 (交易进行者的身份识别码)

        $param = [
            'amount'=>$amount,
            'bankAccountName'=>$bankAccountName,
            'bankAccountNo'=>$bankAccountNo,
            'bankCode'=>$bankCode,
            'callbackUrl'=>$callbackUrl,
            'clientIP'=>$clientIP,
            'merchantNo'=>$merchantNo,
            'orderNo'=>$orderNo,
            'payType'=>$payType,
            'userName'=>$userName
        ];

        ksort($param);
        $sign_str = '';
        foreach($param as $pk=>$pv){
            $sign_str.="{$pk}={$pv}&";
        }

//        $sign_str = substr($sign_str,0,strlen($sign_str)-1);
        $sign_str = $sign_str.'key='.$key;

        $param['sign'] = md5($sign_str);

//        $sign_str="amount={$param['amount']}&bankAccountName={$param['bankAccountName']}&bankAccountNo={$param['bankAccountNo']}&bankCode={$param['bankCode']}&callbackUrl={$param['callbackUrl']}&clientIP={$param['clientIP']}&merchantNo={$param['merchantNo']}&orderNo={$param['orderNo']}&payType={$param['payType  ']}&userName={$param['userName']}&key={$key}";
//        $param['sign'] = md5($sign_str);
        $param['signType'] = 'MD5';

        $param = json_encode($param,JSON_UNESCAPED_UNICODE);

        $re = doHttpsPost($url,$param);
        $re = json_decode($re,true);

        return $re;
    }

}