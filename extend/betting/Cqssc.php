<?php


namespace betting;


use betting\calculation\Calculation;

class Cqssc
{
    use Calculation;


    /**
     * 前四/前四直选/复式
     * 游戏玩法：从万、千、百、十位各选一个号码组成一注。
     * 投注方案：2345*；开奖号码：2345*
     * 投注玩法：从万位、千位、百位、十位中选择一个4位数号码组成一注，所选号码与开奖号码全部相同，且顺序一致，即为中奖
     * @one_num 1,2,3 (万位)
     * @two_num 1,2,3 (千位)
     * @three_num 1,2,3 (百位)
     * @four_num 1,2,3 (十位)
     */
    public function play_1384($param,$pre_draw_code){
        $re = $this->q4zhixfs($param,$pre_draw_code);

        return $re;
    }
    /**
     * 前四/前四直选/单式
     * 投注玩法：手动输入一个4位数号码组成一注，所选号码的万位、千位、百位、十位与开奖号码相同，且顺序一致，即为中奖（每个号码之间用,隔开，每注之间用;隔开）
     * 投注方案：2345*； 开奖号码：2345*，即中前四星直选一等奖
     * data_num 1234 2345
     */
    public function play_1385($param,$pre_draw_code){
        return $this->q4zhixds($param,$pre_draw_code);
    }
    /**
     * 前四/前四直选/组选24
     * 游戏玩法：至少选择四个号码组成一注。
     * 投注方案：选择1234（展开为1234*，1243*，1324*，1342*，1423*，1432*...）,开奖为4321*，即为中奖
     * 投注玩法：前四组选24：选4个或多个，所选号码与开奖万、千、百、十一致顺序不限，即为中奖
     * @data_num 1,2,3,4
     */
    public function play_1386($param,$pre_draw_code){
        return $this->q4zhux24($param,$pre_draw_code);
    }
    /**
     * 前四/前四直选/组选12
     * 投注玩法：从二重号中选择一个或多个，从单号中选择两个或多个，所选号码与开奖万、千、百、十一致顺序不限，即为中奖
     * 投注方案：如投注的二重号是1，单号是23，开奖为1123*，即为中奖
     * @data_num 5,6 (单号)
     * @double_num 5,6 (二重号)
     */
    public function play_1387($param,$pre_draw_code){
        return $this->q4zhux12($param,$pre_draw_code);

    }
    /**
     * 后四/后四直选/复式
     * 游戏玩法：从千、百、十、个位各选一个号码组成一注。
     * 投注方案：*2345；开奖号码：*2345
     * 投注玩法：从千位、百位、十位、个位中选择一个4位数号码组成一注，所选号码与开奖号码全部相同，且顺序一致，即为中奖
     *
     * @two_num 1,2,3 (千位)
     * @three_num 1,2,3 (百位)
     * @four_num 1,2,3 (十位)
     *  @five_num 1,2,3 (个位)
     */
    public function play_1392($param,$pre_draw_code){
        return $this->h4zhixfs($param,$pre_draw_code);

    }
    /**
     * |lottery_id     |是  |string | （彩种）固定值1|
     * /api/lottery/getBet
     * 后四/后四直选/单式
     * 投注玩法：手动输入一个4位数号码组成一注，所选号码的从千位、百位、十位、个位与开奖号码相同，且顺序一致，即为中奖（每个号码之间用,隔开，每注之间用;隔开）
     * 投注方案：*2345； 开奖号码：*2345，即中前四星直选一等奖
     * data_num 1234 2345
     */
    public function play_1393($param,$pre_draw_code){
        return $this->h4zhixds($param,$pre_draw_code);
    }

    /**
     * 后四/后四直选/组选24
     * 游戏玩法：至少选择四个号码组成一注。
     * 投注方案：选择1234（展开为*1234，*1243，*1324，*1342，*1423，*1432...）,开奖为*4321，即为中奖
     * 投注玩法：后四组选24：选4个或多个，所选号码与开奖千、百、十、个一致顺序不限，即为中奖
     * @data_num 1,2,3,4
     */
    public function play_1394($param,$pre_draw_code){
        return $this->h4zhux24($param,$pre_draw_code);
    }
    /**
     * 后四/后四直选/组选12
     * 投注玩法：从二重号中选择一个或多个，从单号中选择两个或多个，所选号码与开奖千、百、十、个一致顺序不限，即为中奖
     * 投注方案：如投注的二重号是1，单号是23，开奖为*1123，即为中奖
     * @data_num 5,6 (单号)
     * @double_num 5,6 (二重号)
     */
    public function play_1395($param,$pre_draw_code){
        return $this->h4zhux12($param,$pre_draw_code);
    }
    /**
     * 前三/前三直选/复式
     * 游戏玩法：从万、千、百位各选一个号码组成一注。
     * 投注方案：234**；开奖号码：234**
     * 投注玩法：从万位、千位、百位中选择一个3位数号码组成一注，所选号码与开奖号码全部相同，且顺序一致，即为中奖
     * @one_num  第一位好嘛 （1,2,3）
     * @two_num  第二位好嘛 （1,2,3）
     * @three_num  第三位好嘛 （1,2,3）
     */
    public function play_1400($param,$pre_draw_code)
    {
        return $this->q3zhixfs($param,$pre_draw_code);
    }
    /**
     * 前三/前三直选/单式   (玩法)
     * 投注玩法：手动输入一个3位数号码组成一注，所选号码的万、千、百与开奖号码相同，且顺序一致，即为中奖（每个号码之间用,隔开，每注之间用;隔开）
     * 投注方案：234**；开奖号码：234**
     * 投注玩法：手动输入一个3位数号码组成一注，所选号码的万、千、百位与开奖号码相同，且顺序一致，即为中奖
     * @data_num  下注号码 123 321
     */
    public function play_1401($param,$pre_draw_code)
    {
        return $this->q3zhixds($param,$pre_draw_code);
    }
    /**
     * 前三/前三组选 /组三
     * 游戏玩法：至少选择三个号码组成一注。
     * 投注方案：选择123（展开为123**，132**，231**，213**，312**，321**）,开奖为321**，即为中奖
     * 投注玩法：前三组六：选3个或多个，所选号码与开奖万、千、百一致顺序不限，即为中奖
     * @data_num   1,2,3
     */
    public function play_1402($param,$pre_draw_code)
    {
        return $this->q3zhuxz3($param,$pre_draw_code);
    }
    /**
     * 前三/前三组选 /组六
     * 游戏玩法：至少选择三个号码组成一注。
     * 投注方案：选择123（展开为123**，132**，231**，213**，312**，321**）,开奖为321**，即为中奖
     * 投注玩法：前三组六：选3个或多个，所选号码与开奖万、千、百一致顺序不限，即为中奖
     * @data_num   1,2,3
     */
    public function play_1403($param,$pre_draw_code)
    {
        return $this->q3zhuxz6($param,$pre_draw_code);
    }
    /**
     * 中三/中三直选/复式
     *   游戏玩法：从千、百、十位各选一个号码组成一注。
     * 投注方案：234**；开奖号码：234**
     * 投注玩法：从千、百、十中选择一个3位数号码组成一注，所选号码与开奖号码全部相同，且顺序一致，即为中奖
     * @two_num  第二位好嘛 1,2,3）
     * @three_num  第三位好嘛 1,2,3）
     *  @four_num  第三位好嘛 （1,2,3）
     */
    public function play_1408($param,$pre_draw_code)
    {
        return $this->z3zhixfs($param,$pre_draw_code);
    }
    /**
     * 中三/中三直选/单式   (玩法)
     * 投注玩法：手动输入一个3位数号码组成一注，所选号码的万、千、百与开奖号码相同，且顺序一致，即为中奖
     * 投注方案：234**；开奖号码：234**
     * 投注玩法：手动输入一个3位数号码组成一注，所选号码的万、千、百位与开奖号码相同，且顺序一致，即为中奖
     * @data_num  下注号码 123 124
     */
    public function play_1409($param,$pre_draw_code)
    {
        return $this->z3zhixds($param,$pre_draw_code);
    }
    /**
     * 中三/中三组选/组三
     * 游戏玩法：至少选择三个号码组成一注。
     * 投注方案：选择123（展开为*123*，*132*，*231*，*213*，*312*，*321*）,开奖为321**，即为中奖
     * 投注玩法：前三组六：选3个或多个，所选号码与开奖千、百、十一致顺序不限，即为中奖
     * @data_num   1,2,3
     */
    public function play_1410($param,$pre_draw_code)
    {
        return $this->z3zhuxz3($param,$pre_draw_code);
    }
    /**
     * 中三/中三组选/组六
     * 游戏玩法：至少选择三个号码组成一注。
     *投注方案：选择12（展开为*122*，*212*，*221* 和 *112*、*121*、*211*）,开奖为*212* 或 *121*，即为中奖
     * 投注玩法：中三组三：选2个或多个，所选号码与开奖千、百、十位一致顺序不限，即为中奖
     * @data_num   1,2,3
     */
    public function play_1411($param,$pre_draw_code)
    {
        return $this->z3zhuxz6($param,$pre_draw_code);
    }
    /**
     * 后三/后三直选/复式
     * 游戏玩法：从百、十、个位各选一个号码组成一注。
     * 投注方案：**345；开奖号码：**345
     * 投注玩法：从百位、十位、个位中选择一个3位数号码组成一注，所选号码与开奖号码全部相同，且顺序一致，即为中奖
     * @three_num  第三位好嘛 （1,2,3）
     * * @four_num  第一位好嘛 （1,2,3
     * @five_num  第二位好嘛 1,2,3
     */
    public function play_1416($param,$pre_draw_code)
    {
        return $this->h3zhixfs($param,$pre_draw_code);
    }
    /**
     * 后三/后三直选/单式   (玩法)
     * 投注玩法：手动输入一个3位数号码组成一注，所选号码的万、千、百与开奖号码相同，且顺序一致，即为中奖（每个号码之间用,隔开，每注之间用;隔开）
     * 投注方案：**345； 开奖号码：**345，即中后三星直选一等奖
     * 投注玩法：手动输入一个3位数号码组成一注，所选号码的百位、十位、个位与开奖号码相同，且顺序一致，即为中奖
     * @data_num  下注号码 123  456
     */
    public function play_1417($param,$pre_draw_code)
    {
        return $this->h3zhixds($param,$pre_draw_code);
    }
    /**
     * 后三/后三组选/组三
     * 游戏玩法：至少选择三个号码组成一注。
     * 投注方案：选择12（展开为**122，**212，**221 和 **112、**121、**211）,开奖为**212 或 **121，即为中奖
     * 投注玩法：后三组三：选2个或多个，所选号码与开奖百、十、个位一致顺序不限，即为中奖
     * @data_num   1,2,3
     */
    public function play_1418($param,$pre_draw_code)
    {
        return $this->h3zhuxz3($param,$pre_draw_code);
    }
    /**
     * 后三/后三组选/组六
     * 游戏玩法：至少选择三个号码组成一注。
     * 投注方案：选择123（展开为**123，**132，**231，**213，**312，**321）,开奖为**321，即为中奖
     * 投注玩法：后三组六：选3个或多个，所选号码与开奖百、十、个位一致顺序不限，即为中奖
     * @data_num   1,2,3
     */
    public function play_1419($param,$pre_draw_code)
    {
        return $this->h3zhuxz6($param,$pre_draw_code);
    }
    /**
     * 二星/直选/前二复式
     * 游戏玩法：从万、千位各选一个号码组成一注。
     * 投注方案：23***；开奖号码：23***
     * 投注玩法：从万位、千位中选择一个2位数号码组成一注，所选号码与开奖号码全部相同，且顺序一致，即为中奖
     * @one_num 1,2
     * @two_num 1,2
     */
    public function play_1424($param,$pre_draw_code)
    {
        return $this->q2zhixfs($param,$pre_draw_code);
    }
    /**
     * 二星/直选/前二单式
     * 游戏玩法：从万、千位各选一个号码组成一注。
     * 投注玩法：手动输入一个2位数号码组成一注，所选号码的万位、千位与开奖号码相同，且顺序一致，即为中奖（每个号码之间用,隔开，每注之间用;隔开）
     * 投注方案：23***； 开奖号码：23***，即中前二星直选一等奖
     * @data_num 01 12
     */
    public function play_1425($param,$pre_draw_code)
    {
        return $this->q2zhixds($param,$pre_draw_code);
    }
    /**
     * 二星/直选/后二复式
     * 游戏玩法：从十、个位各选一个号码组成一注
     * 投注方案：***23；开奖号码：***23
     * 投注玩法：从十位、个位中选择一个2位数号码组成一注，所选号码与开奖号码全部相同，且顺序一致，即为中奖
     * @four_num 1,2,3 (十位)
     *  @five_num 1,2,3 (个位)
     */
    public function play_1426($param,$pre_draw_code)
    {
        return $this->h2zhixfs($param,$pre_draw_code);
    }
    /**
     * 二星/直选/后二单式
     * 游戏玩法：从万、千位各选一个号码组成一注。
     * 投注玩法：手动输入一个2位数号码组成一注，所选号码的十位、个位与开奖号码相同，且顺序一致，即为中奖（每个号码之间用,隔开，每注之间用;隔开）
     * 投注方案：***23； 开奖号码：***23，即中后二星直选一等奖
     * @data_num 0,1;1,2
     */
    public function play_1427($param,$pre_draw_code)
    {
        return $this->h2zhixds($param,$pre_draw_code);
    }
    /**
     * 二星/组选/前二复式
     * 游戏玩法：至少选择两个号码组成一注。
     * 投注方案：选择12（展开为12***，21***）,开奖为12*** 或 21***，即为中奖
     * 投注玩法：前二组选复式：从0-9中选2个或多个号码对万位和千位投注，顺序不限，即为中奖
     * @data_num 1,2,3
     */
    public function play_1428($param,$pre_draw_code)
    {
        return $this->q2zhuxfs($param,$pre_draw_code);
    }
    /**
     * 二星/组选/前二单式
     * 投注玩法：前二组选单式：键盘手动输入购买号码，2个数字为一注，与开奖号码的万位、千位符合且顺序不限，即为中奖（每个号码之间用,隔开；每注之间用;隔开）
     * 投注方案：手动输入12，开奖号码为12*** 或 21*** 即为中奖
     * @data_num 12 23
     */
    public function play_1429($param,$pre_draw_code)
    {
        return $this->q2zhuxds($param,$pre_draw_code);
    }
    /**
     * 二星/组选/后二复式
     * 游戏玩法：至少选择两个号码组成一注。
     * 投注方案：选择12（展开为***12，***21）,开奖为***12 或 ***21，即为中奖
     * 投注玩法：后二组选复式：从0-9中选2个或多个号码对十位和个位投注，顺序不限，即为中奖
     * @data_num 1,2,3
     */
    public function play_1430($param,$pre_draw_code)
    {
        return $this->h2zhuxfs($param,$pre_draw_code);
    }
    /**
     * 二星/组选/后二单式
     * 投注玩法：后二组选单式：键盘手动输入购买号码，2个数字为一注，与开奖号码的十位、个位符合且顺序不限，即为中奖（每个号码之间用,隔开；每注之间用;隔开）
     * 投注方案：手动输入12，开奖号码为***12 或 ***21 即为中奖
     * @data_num 12 23
     */
    public function play_1431($param,$pre_draw_code)
    {
        return $this->h2zhuxds($param,$pre_draw_code);
    }
    /**
     * 定位胆/定位胆/定位胆
     * 投注玩法：从万位、千位、百位、十位、个位任意位置上选择1个或1个以上号码，所选号码与相同位置上的开奖号码一致，即为中奖
     * 投注方案：定万位为1，开奖号码为1****即为中奖
     * @one_num 1,2,3 (万位)
     * @two_num 1,2,3 (千位)
     * @three_num 1,2,3 (百位)
     * @four_num 1,2,3 (十位)
     * @five_num 1,2,3 (个位)
     *
     */
    public function play_1434($param,$pre_draw_code){
        return $this->dwd($param,$pre_draw_code);
    }
    /**
     * 不定位/不定位/前三一码
     * 游戏玩法：至少选择一个号码组成一注。
     * 投注方案：选择不定位1，开出1****、*1***、**1**即为中奖
     * 投注玩法：从0-9中选择1个号码，每注由1个号码组成，只要开奖号码的万位、千位、百位中包含所选号码，即为中奖
     * @data_num 1,2,3
     */
    public function play_1438($param,$pre_draw_code){
        return $this->bddq3ym($param,$pre_draw_code);
    }
    /**
     * 不定位/不定位/中三一码
     * 游戏玩法：至少选择一个号码组成一注。
     * 投注方案：选择不定位1，开出*1***、**1**、***1*即为中奖
     * 投注玩法：从0-9中选择1个号码，每注由1个号码组成，只要开奖号码的千位、百位、十位中包含所选号码，即为中奖
     * @data_num 1,2,3
     */
    public function play_1439($param,$pre_draw_code){
        return $this->bddz3ym($param,$pre_draw_code);
    }
    /**
     * 不定位/不定位/后三一码
     * 游戏玩法：至少选择一个号码组成一注。
     * 投注方案：选择不定位1，开出****1、***1*、**1**即为中奖
     * 投注玩法：从0-9中选择1个号码，每注由1个号码组成，只要开奖号码的百位、十位、个位中包含所选号码，即为中奖
     * @data_num 1,2,3
     */
    public function play_1440($param,$pre_draw_code){
        return $this->bddh3ym($param,$pre_draw_code);
    }
    /**
     * 不定位/不定位/五星一码
     * 游戏玩法：至少选择一个号码组成一注。
     * 投注方案：例如投注3，开奖号34535，中一注。（出现一次和出现两次都只算中一注）
     * 投注玩法：每注号码由一个数字组成，只要开奖号码任意一位出现了投注的号码，即为中奖
     * @data_num 1,2,3
     */
    public function play_1441($param,$pre_draw_code){
        return $this->bdw5x1m($param,$pre_draw_code);
    }
    /**
     * 不定位/不定位/五星二码
     * 游戏玩法：至少选择两个号码组成一注。
     * 投注方案：例如投注36，开奖号码32468，中一注。
     * 投注玩法：每注号码由两个数字组成，只要开奖号码包含投注号码即为中奖
     * @data_num 1,2,3
     */
    public function play_1442($param,$pre_draw_code){
        return $this->bdw5x2m($param,$pre_draw_code);
    }
    /**
     * 不定位/不定位/五星三码
     * 游戏玩法：至少选择三个号码组成一注。
     * 投注方案：例如投注368，开奖号码32468，中一注。
     * 投注玩法：每注号码由三个数字组成，只要开奖号码中包含投注号码即为中奖
     * @data_num 1,2,3
     */
    public function play_1443($param,$pre_draw_code){
        return $this->bdw5x3m($param,$pre_draw_code);
    }
    /**
     * 跨度/跨度/前三跨度
     * 游戏玩法：至少选择一个号码组成一注
     * 投注方案：选择5, 等于开奖号前三位2,5,7的最大数7与最小数字2的差值，即为中奖
     * 投注玩法：选择0-9，若所选号码与开奖号前三位的最大最小数字的差值相等，即为中奖
     * @data_num 1,2,3
     */
    public function play_1448($param,$pre_draw_code){
        return $this->kdq3($param,$pre_draw_code);
    }
    /**
     * 跨度/跨度/中三跨度
     * 游戏玩法：至少选择一个号码组成一注。
     * 投注方案：选择5, 等于开奖号中三位2,5,7的最大数7与最小数字2的差值，即为中奖
     * 投注玩法：选择0-9，若所选号码与开奖号中三位的最大最小数字的差值相等，即为中奖
     * @data_num 1,2,3
     */
    public function play_1449($param,$pre_draw_code){
        return $this->kdz3($param,$pre_draw_code);

    }
    /**
     * 跨度/跨度/后三跨度
     * 游戏玩法：至少选择一个号码组成一注。
     * 投注方案：选择5, 等于开奖号后三位2,5,7的最大数7与最小数字2的差值，即为中奖
     * 投注玩法：选择0-9，若所选号码与开奖号后三位的最大最小数字的差值相等，即为中奖
     * @data_num 1,2,3
     */
    public function play_1450($param,$pre_draw_code){
        return $this->kdh3($param,$pre_draw_code);
    }

    /**
     * 跨度/跨度/前二跨度
     * 游戏玩法：至少选择一个号码组成一注。
     * 投注方案：选择5, 等于开奖号前二位2,7的最大数7与最小数字2的差值，即为中奖
     * 投注玩法：选择0-9，若所选号码与开奖号前二位的最大最小数字的差值相等，即为中奖
     * @data_num 1,2,3
     */
    public function play_1451($param,$pre_draw_code){
        return $this->kdq2($param,$pre_draw_code);
    }
    /**
     * 跨度/跨度/后二跨度
     * 游戏玩法：至少选择一个号码组成一注。
     * 投注方案：选择5, 等于开奖号后二位2,7的最大数7与最小数字2的差值，即为中奖
     * 投注玩法：选择0-9，若所选号码与开奖号后二位的最大最小数字的差值相等，即为中奖
     * @data_num 1,2,3
     */
    public function play_1452($param,$pre_draw_code){
        return $this->kdh2($param,$pre_draw_code);
    }
    /**
     * 任选/任四/复式
     * 游戏玩法：从万、千、百、十、个任四位选一个号码组
     * 投注方案：2*456；开奖号码：2*456
     * 投注玩法：任选四位与开奖号分别对应相同且顺序一致即为中奖
     * @one_num 1,2,3 (万位)
     * @two_num 1,2,3 (千位)
     * @three_num 1,2,3 (百位)
     * @four_num 1,2,3 (十位)
     * @five_num 1,2,3 (个位)
     */
    public function play_1457($param,$pre_draw_code){
        return $this->r4fs($param,$pre_draw_code);

    }
    /**
     * 任选/任三/复式
     * 游戏玩法：从万、千、百、十、个任三位选一个号码组
     * 投注方案：2*4*6；开奖号码：2*4*6
     * 投注玩法：任选三位与开奖号分别对应相同且顺序一致即为中奖
     * @one_num 1,2,3 (万位)
     * @two_num 1,2,3 (千位)
     * @three_num 1,2,3 (百位)
     * @four_num 1,2,3 (十位)
     * @five_num 1,2,3 (个位)
     */
    public function play_1459($param,$pre_draw_code)
    {
        return $this->r3fs($param,$pre_draw_code);

    }

    /**
     * 任选/任三/组三
     * 游戏玩法：至少选择两个号码组成一注，并选择三个对
     * 投注方案：选择12（展开为**122，**212，**221 和 **112、**121、**211）,开奖为**212 或 **121，即为中奖
     * 投注玩法：0-9中任意选择2个号码组成两注，所选号码与开奖号码的对应位相同，且顺序不限，即为中奖
     * @data_address 万,千,百,十,个
     * @data_num 1,2,4
     */
    public function play_1461($param,$pre_draw_code)
    {
        return $this->r3z3($param,$pre_draw_code);
    }
    /**
     * 任选/任三/组六
     * 玩法示意：从0-9中任意选择3个或3个以上号码和任意三个位置
     * 位置选择万、千、百，号码选择012；开奖号码为012**、则中奖
     * 从0-9中任意选择3个或3个以上号码和万、千、百、十、个任意的三个位置，如果组合的号码与开奖号码对应则中奖
     * @data_address 万,千,百,十,个
     * @data_num 1,2,4
     */
    public function play_1462($param,$pre_draw_code)
    {
        return $this->r3z6($param,$pre_draw_code);
    }
    /**
     * 任选/任二/复式
     * 玩法示意：万、千、百、十、个任意2位，开奖号分别对应且顺序一致即中奖
     * 万位买0，千位买1，百位买2，开奖01234，则中奖。
     * 从万、千、百、十、个中至少2个位置各选一个或多个号码，将各个位置的号码进行组合，所选号码的各位与开奖号码相同则中奖。
     * @one_num 1,2,3 (万位)
     * @two_num 1,2,3 (千位)
     * @three_num 1,2,3 (百位)
     * @four_num 1,2,3 (十位)
     * @five_num 1,2,3 (个位)
     */
    public function play_1463($param,$pre_draw_code)
    {
        return $this->r2fs($param,$pre_draw_code);
    }
    /**
     * 任选/任二/单式
     * 游戏玩法：至少选择两个号码组成一注，并选择两个对（每个号码之间用,隔开；每注号码之间用;隔开）
     * 投注方案：选择12,对应位百、十，开奖为**21* 或 **12*，即为中奖
     * 投注玩法：从0-9中选2个号码组成一注，所择号码与开奖号码的对应位相同，顺序不限，即为中奖
     * @data_num 12 34
     * @data_address 万,千,百,十,个
     */
    public function play_1464($param,$pre_draw_code)
    {
        return $this->r2ds($param,$pre_draw_code);

    }
    /**
     * 任选/任二/组选
     * 玩法示意： 从0-9中任意选择2个或2个以上号码和任意两个位置
     * 位置选择万、千，号码选择01；开奖号码为01***、则中奖
     * 从0-9中任意选择2个或2个以上号码和万、千、百、十、个任意的两个位置，如果组合的号码与开奖号码对应则中奖
     * @data_num 1,2,3
     * @data_address 万,千,百,十,个
     *
     * data_num 两两组合的个数  *   data_address 两两组合的个数
     *
     */
    public function play_1465($param,$pre_draw_code)
    {
        return $this->rx_re_zx($param,$pre_draw_code);
    }
    /**
     *大小单双/大小单双/前二
     * 游戏玩法：从万、千位各选一个号码组成一注。
     * 投注方案：万位选择小，千位选择双，开出12***即为中奖
     * 投注玩法：大小单双:对万位和千位的“大（56789）小（01234）、单（13579）双（02468）”形态进行购买。
     * @w_status 大,小,单,双(万位)
     * @q_status 大,小,单,双（千位）
     */
    public function play_1469($param,$pre_draw_code)
    {

        return $this->dxdsq2($param,$pre_draw_code);
    }
    /**
     *大小单双/大小单双/后二
     * 游戏玩法：从十、个位各选一个号码组成一注
     * 投注方案：十位选择小，个位选择双，开出***12即为中奖
     * 投注玩法：大小单双:对十位和个位的“大（56789）小（01234）、单（13579）双（02468）”形态进行购买。
     * @s_status 大,小,单,双(十位)
     * @g_status 大,小,单,双（个位）
     */
    public function play_1468($param,$pre_draw_code)
    {

        return $this->dxdsh2($param,$pre_draw_code);
    }
    /**
     * 和值/直选/前三
     * 游戏玩法：至少选择一个号码组成一注。
     * 投注方案：选择01, 相当于前三位投注0,0,1/0,1,0/1,0,0三组号码，开奖号码前三位号码的和值为1（0,0,1/0,1,0/1,0,0），即为中奖。
     * 投注玩法：选择0-27，若所选和值号码与开奖号前三位的和值相等，即为中奖。
     * @data_num 1,2,3,4
     */
    public function play_1474($param,$pre_draw_code){
        return $this->hzzhixq3($param,$pre_draw_code);
    }
    /**
     * 和值/直选/中三
     * 游戏玩法：至少选择一个号码组成一注。
     * 投注方案：选择01, 相当于中三位投注0,0,1/0,1,0/1,0,0三组号码，开奖号码中三位号码的和值为1（0,0,1/0,1,0/1,0,0），即为中奖。
     * 投注玩法：选择0-27，若所选和值号码与开奖号中三位的和值相等，即为中奖。
     * @data_num 1,2,3,4
     */
    public function play_1475($param,$pre_draw_code){
        return $this->hzzhixz3($param,$pre_draw_code);
    }
    /**
     * 和值/直选/后三
     * 游戏玩法：至少选择一个号码组成一注。
     * 投注方案：选择01, 相当于后三位投注0,0,1/0,1,0/1,0,0三组号码，开奖号码后三位号码的和值为1（0,0,1/0,1,0/1,0,0），即为中奖。
     * 投注玩法：选择0-27，若所选和值号码与开奖号后三位的和值相等，即为中奖。
     * @data_num 1,2,3,4
     */
    public function play_1476($param,$pre_draw_code){
        return $this->hzzhixh3($param,$pre_draw_code);
    }
    /**
     * 和值/直选/前二
     * 游戏玩法：至少选择一个号码组成一注。
     * 投注方案：选择1, 相当于前2位投注0,1/1,0两组号码，开奖号码前2位号码的和值为1（0,1/1,0），即为中奖。
     * 投注玩法：选择0-18，若所选和值号码与开奖号前二位的和值相等，即为中奖。
     * @data_num 1,2,3,4
     */
    public function play_1477($param,$pre_draw_code){
        return $this->hzzhixq2($param,$pre_draw_code);
    }

    /**
     * 和值/直选/后二
     * 游戏玩法：至少选择一个号码组成一注。
     * 投注方案：选择1, 相当于后2位投注0,1/1,0两组号码，开奖号码后2位号码的和值为1（0,1/1,0），即为中奖。
     * 投注玩法：选择0-18，若所选和值号码与开奖号后二位的和值相等，即为中奖。
     * @data_num 1,2,3,4
     */
    public function play_1478($param,$pre_draw_code){
        return $this->hzzhixh2($param,$pre_draw_code);
    }
    /**
     * 和值/尾选/前三
     * 游戏玩法：至少选择一个号码组成一注
     * 投注方案：选择5, 等于开奖号前三位4,5,6和值的尾数，即为中奖
     * 投注玩法：选择0-9，若所选号码与开奖号前三位的和值的尾数相等，即为中奖
     * @data_num 1,2,3,4
     */
    public function play_1479($param,$pre_draw_code){
        return $this->hzwsq3($param,$pre_draw_code);
    }
    /**
     * 和值/尾选/中三
     * 游戏玩法：至少选择一个号码组成一注
     * 投注方案：选择5, 等于开奖号前中位4,5,6和值的尾数，即为中奖
     * 投注玩法：选择0-9，若所选号码与开奖号前中位的和值的尾数相等，即为中奖
     * @data_num 1,2,3,4
     */
    public function play_1480($param,$pre_draw_code){
        return $this->hzwsz3($param,$pre_draw_code);
    }
    /**
     * 和值/尾选/后三
     * 游戏玩法：至少选择一个号码组成一注
     * 投注方案：选择5, 等于开奖号后三位4,5,6和值的尾数，即为中奖
     * 投注玩法：选择0-9，若所选号码与开奖号后三位的和值的尾数相等，即为中奖
     * @data_num 1,2,3,4
     */
    public function play_1481($param,$pre_draw_code){
        return $this->hzwsh3($param,$pre_draw_code);
    }
    /**
     * 趣味/趣味/一帆风顺
     * 游戏玩法：从0-9中任意选择1个以上号码。
     * 投注方案：例如投注3，开奖号34535，中一注。（出现一次和出现两次都只算中一注）
     * 投注玩法：每注号码由一个数字组成，只要开奖号码任意一位出现了投注的号码，即为中奖
     * @data_num 1,2,3
     */
    public function play_1484($param,$pre_draw_code)
    {
        return $this->yffs($param,$pre_draw_code);
    }
    /**
     * 趣味/趣味/好事成双
     * 游戏玩法：从0-9中任意选择1个以上的二重号码。
     * 投注方案：例如投注3，开奖号34573，中一注。
     * 投注玩法：从0-9中任意一个号码组成一注，只要所选号码在开奖号码任意一位出现了2次，即为中奖
     * @data_num 1,2,3
     */
    public function play_1485($param,$pre_draw_code)
    {

        return $this->hscs($param,$pre_draw_code);
    }
    /**
     * 新龙虎/新龙虎/万千
     * 投注方案：例如投注龙，开奖号为64535，即为中奖
     * 投注玩法：开奖号码的万位数字大于千位数字则为龙；万位小于千位则为虎；号码相同则打和。
     * @data_num 龙,虎
     */
    public function play_1490($param,$pre_draw_code)
    {
         $re = $this->lh($param,$pre_draw_code,[0,1]);
        $re['play_name'] = '新龙虎/新龙虎/万千';
        return $re;
    }
    /**
     * 新龙虎/新龙虎/万百
     * 投注方案：例如投注龙，开奖号为64535，即为中奖
     * 投注玩法：开奖号码的万位数字大于百位数字则为龙；万位小于百位则为虎；号码相同则打和。
     * @data_num 龙,虎
     */
    public function play_1491($param,$pre_draw_code)
    {
        $re = $this->lh($param,$pre_draw_code,[0,2]);
        $re['play_name'] = '新龙虎/新龙虎/万百';
        return $re;

    }
    /**
     * 新龙虎/新龙虎/万十
     * 投注方案：例如投注龙，开奖号为64535，即为中奖
     * 投注玩法：开奖号码的万位数字大于十位数字则为龙；万位小于十位则为虎；号码相同则打和。
     * @data_num 龙,虎
     */
    public function play_1492($param,$pre_draw_code)
    {

        $re = $this->lh($param,$pre_draw_code,[0,3]);
        $re['play_name'] = '新龙虎/新龙虎/万十';
        return $re;
    }
    /**
     * 新龙虎/新龙虎/万个
     * 投注方案：例如投注龙，开奖号为64535，即为中奖
     * 投注玩法：开奖号码的万位数字大于个位数字则为龙；万位小于个位则为虎；号码相同则打和。
     * @data_num 龙,虎
     */
    public function play_1493($param,$pre_draw_code)
    {
        $re = $this->lh($param,$pre_draw_code,[0,4]);
        $re['play_name'] = '新龙虎/新龙虎/万个';
        return $re;
    }
    /**
     * 新龙虎/新龙虎/千百
     * 投注方案：例如投注龙，开奖号为64535，即为中奖
     * 投注玩法：开奖号码的千位数字大于百位数字则为龙；千位小于百位则为虎；号码相同则打和。
     * @data_num 龙,虎
     */
    public function play_1494($param,$pre_draw_code)
    {
        $re = $this->lh($param,$pre_draw_code,[1,2]);
        $re['play_name'] = '新龙虎/新龙虎/千百';
        return $re;
    }
    /**
     * 新龙虎/新龙虎/千十
     * 投注方案：例如投注龙，开奖号为64535，即为中奖
     * 投注玩法：开奖号码的千位数字大于十位数字则为龙；千位小于十位则为虎；号码相同则打和。
     * @data_num 龙,虎
     */
    public function play_1495($param,$pre_draw_code)
    {
        $re = $this->lh($param,$pre_draw_code,[1,3]);
        $re['play_name'] = '新龙虎/新龙虎/千十';
        return $re;
    }
    /**
     * 新龙虎/新龙虎/千个
     * 投注方案：例如投注龙，开奖号为64535，即为中奖
     * 投注玩法：开奖号码的千位数字大于个位数字则为龙；千位小于个位则为虎；号码相同则打和。
     * @data_num 龙,虎
     */
    public function play_1496($param,$pre_draw_code)
    {
        $re = $this->lh($param,$pre_draw_code,[1,4]);
        $re['play_name'] = '新龙虎/新龙虎/千个';
        return $re;
    }
    /**
     * 新龙虎/新龙虎/百十
     * 投注方案：例如投注龙，开奖号为64535，即为中奖
     * 投注玩法：开奖号码的百位数字大于十位数字则为龙；百位小于十位则为虎；号码相同则打和。
     * @data_num 龙,虎
     */
    public function play_1497($param,$pre_draw_code)
    {
        $re = $this->lh($param,$pre_draw_code,[2,3]);
        $re['play_name'] = '新龙虎/新龙虎/百十';
        return $re;
    }
    /**
     * 新龙虎/新龙虎/百个
     * 投注方案：例如投注龙，开奖号为64535，即为中奖
     * 投注玩法：开奖号码的百位数字大于个位数字则为龙；百位小于个位则为虎；号码相同则打和。
     * @data_num 龙,虎
     */
    public function play_1498($param,$pre_draw_code)
    {
        $re = $this->lh($param,$pre_draw_code,[2,4]);
        $re['play_name'] = '新龙虎/新龙虎/百个';
        return $re;
    }
    /**
     * 新龙虎/新龙虎/十个
     * 投注方案：例如投注龙，开奖号为64535，即为中奖
     * 投注玩法：开奖号码的十位数字大于个位数字则为龙；十位小于个位则为虎；号码相同则打和。
     * @data_num 龙,虎
     */
    public function play_1499($param,$pre_draw_code)
    {
        $re = $this->lh($param,$pre_draw_code,[3,4]);
        $re['play_name'] = '新龙虎/新龙虎/十个';
        return $re;
    }




    /**
     * 新龙虎/和/万千
     * 投注方案：例如投注龙，开奖号为64535，即为中奖
     * 投注玩法：开奖号码的万位数字大于千位数字则为龙；万位小于千位则为虎；号码相同则打和。
     * @data_num 和
     */
    public function play_1780($param,$pre_draw_code)
    {
        $re = $this->lh($param,$pre_draw_code,[0,1]);
        $re['play_name'] = '新龙虎/和/万千';
        return $re;
    }
    /**
     * 新龙虎/和/万百
     * 投注方案：例如投注龙，开奖号为64535，即为中奖
     * 投注玩法：开奖号码的万位数字大于百位数字则为龙；万位小于百位则为虎；号码相同则打和。
     * @data_num 和
     */
    public function play_1781($param,$pre_draw_code)
    {
        $re = $this->lh($param,$pre_draw_code,[0,2]);
        $re['play_name'] = '新龙虎/和/万百';
        return $re;
    }
    /**
     * 新龙虎/和/万十
     * 投注方案：例如投注龙，开奖号为64535，即为中奖
     * 投注玩法：开奖号码的万位数字大于十位数字则为龙；万位小于十位则为虎；号码相同则打和。
     * @data_num 和
     */
    public function play_1782($param,$pre_draw_code)
    {
        $re = $this->lh($param,$pre_draw_code,[0,3]);
        $re['play_name'] = '新龙虎/和/万十';
        return $re;

    }
    /**
     * 新龙虎/和/万个
     * 投注方案：例如投注龙，开奖号为64535，即为中奖
     * 投注玩法：开奖号码的万位数字大于个位数字则为龙；万位小于个位则为虎；号码相同则打和。
     * @data_num 和
     */
    public function play_1783($param,$pre_draw_code)
    {
        $re = $this->lh($param,$pre_draw_code,[0,4]);
        $re['play_name'] = '新龙虎/和/万个';
        return $re;
    }
    /**
     * 新龙虎/和/千百
     * 投注方案：例如投注龙，开奖号为64535，即为中奖
     * 投注玩法：开奖号码的千位数字大于百位数字则为龙；千位小于百位则为虎；号码相同则打和。
     * @data_num 和
     */
    public function play_1784($param,$pre_draw_code)
    {
        $re = $this->lh($param,$pre_draw_code,[1,2]);
        $re['play_name'] = '新龙虎/和/千百';
        return $re;
    }
    /**
     * 新龙虎/和/千十
     * 投注方案：例如投注龙，开奖号为64535，即为中奖
     * 投注玩法：开奖号码的千位数字大于十位数字则为龙；千位小于十位则为虎；号码相同则打和。
     * @data_num 和
     */
    public function play_1785($param,$pre_draw_code)
    {
        $re = $this->lh($param,$pre_draw_code,[1,3]);
        $re['play_name'] = '新龙虎/和/千十';
        return $re;
    }
    /**
     * 新龙虎/和/千个
     * 投注方案：例如投注龙，开奖号为64535，即为中奖
     * 投注玩法：开奖号码的千位数字大于个位数字则为龙；千位小于个位则为虎；号码相同则打和。
     * @data_num 和
     */
    public function play_1786($param,$pre_draw_code)
    {
        $re = $this->lh($param,$pre_draw_code,[1,4]);
        $re['play_name'] = '新龙虎/和/千个';
        return $re;
    }
    /**
     * 新龙虎/和/百十
     * 投注方案：例如投注龙，开奖号为64535，即为中奖
     * 投注玩法：开奖号码的百位数字大于十位数字则为龙；百位小于十位则为虎；号码相同则打和。
     * @data_num 和
     */
    public function play_1787($param,$pre_draw_code)
    {
        $re = $this->lh($param,$pre_draw_code,[2,3]);
        $re['play_name'] = '新龙虎/和/百十';
        return $re;
    }
    /**
     * 新龙虎/和/百个
     * 投注方案：例如投注龙，开奖号为64535，即为中奖
     * 投注玩法：开奖号码的百位数字大于个位数字则为龙；百位小于个位则为虎；号码相同则打和。
     * @data_num 和
     */
    public function play_1788($param,$pre_draw_code)
    {
        $re = $this->lh($param,$pre_draw_code,[2,4]);
        $re['play_name'] = '新龙虎/和/百个';
        return $re;
    }
    /**
     * 新龙虎/和/十个
     * 投注方案：例如投注龙，开奖号为64535，即为中奖
     * 投注玩法：开奖号码的十位数字大于个位数字则为龙；十位小于个位则为虎；号码相同则打和。
     * @data_num 和
     */
    public function play_1789($param,$pre_draw_code)
    {
        $re = $this->lh($param,$pre_draw_code,[3,4]);
        $re['play_name'] = '新龙虎/和/十个';
        return $re;
    }

    /**
     *  * |lottery_id     |是  |string | （彩种）固定值1|
     * /api/lottery/getBet
     */







    /**
     * 格式化数据
     */
    public function formatNum($data,$max=9,$min=0)
    {

        foreach ($data as $k => $v) {
            if ($v > $max || $v < $min ||$v == '') {
                unset($data[$k]);
            } else {
                $data[$k] = (int)$v;
            }
        }
        return $data;
    }


}