<?php


namespace betting;

use betting\calculation\Cp11x5;

/**
 * 江西11选5 下注开奖计算
 * Class Jx11x5
 * @package betting
 */
class Jx11x5
{
    use Cp11x5;
    /**
     * 三码/前三直选/复式   (玩法)
     * 从01-11共11个号码中选择3个不重复的号码组成一注，所选号码与当期顺序摇出的5个号码中的前3个号码相同，且顺序一致，即为中奖。
     * @pre_draw_code  开奖号码
     * @betting_num 购买号码
     *
     * @one_num  第一位好嘛 （01,02,03）
     * @two_num  第二位好嘛 （01,02,03）
     * @three_num  第三位好嘛 （01,02,03）
     *
     */
    public function play_81($param, $pre_draw_code)
    {
        return $this->q3_zhix_fs($param,$pre_draw_code);
    }


    /**
     * 三码/前三直选/单式   (玩法)
     * 手动输入3个号码组成一注，所输入的号码与当期顺序摇出的5个号码中的前3个号码相同，且顺序一致，即为中奖。
     * @pre_draw_code  开奖号码
     * @betting 购买号码
     *
     * @data_num  下注号码 01,02,03;02,01,03
     */
//    public function play_5($param, $pre_draw_code)
//    {
//        return $this->q3_zhix_ds($param,$pre_draw_code);
//    }


    /**
     * 三码/前三组合/复式   (玩法)
     * 从01-11中共11个号码中选择3个号码，所选号码与当期顺序摇出的5个号码中的前3个号码相同，顺序不限，即为中奖。
     * @pre_draw_code  开奖号码
     * @betting 购买号码
     * @data_num  所选号码 （01,02,03,04）
     */

    public function play_84($param, $pre_draw_code)
    {
        return $this->q3_zhux_fs($param,$pre_draw_code);
    }

    /**
     * 三码/前三组合/单式   (玩法)
     * 手动输入3个号码组成一注，所输入的号码与当期顺序摇出的5个号码中的前3个号码相同，顺序不限，即为中奖。
     * @pre_draw_code  开奖号码
     * @betting 购买号码
     * @data_num  购买号码(1,2,3;1,2,4)
     */

//    public function play_6($param, $pre_draw_code)
//    {
//        return $this->q3_zhux_ds($param, $pre_draw_code);
//    }

    /**
     * 二码/前二直选/复式   (玩法)
     * 从01-11共11个号码中选择2个不重复的号码组成一注，所选号码与当期顺序摇出的5个号码中的前2个号码相同，且顺序一致，即为中奖。
     * @pre_draw_code  开奖号码
     * @betting 购买号码
     * @one_num  第一位好嘛 （01,02,03）
     * @two_num  第二位好嘛 （01,02,03）
     */
    public function play_88($param, $pre_draw_code)
    {
        return $this->q2_zhix_fs($param,$pre_draw_code);
    }

    /**
     * 二码/前二直选/单式   (玩法)
     * 手动输入2个号码组成一注，所输入的号码与当期顺序摇出的5个号码中的前2个号码相同，且顺序一致，即为中奖。
     * @pre_draw_code  开奖号码
     * @betting 购买号码
     * @data_num  下注号码 01,02;02,01
     */
//    public function play_12($param, $pre_draw_code)
//    {
//        return $this->q2_zhix_ds($param,$pre_draw_code);
//
//    }


    /**
     * 二码/前二组合/复式   (玩法)
     * 从01-11中共11个号码中选择2个号码，所选号码与当期顺序摇出的5个号码中的前2个号码相同，顺序不限，即为中奖。
     * @pre_draw_code  开奖号码
     * @betting 购买号码
     * @data_num  开奖号码(01,02,03,04)
     */

    public function play_90($param, $pre_draw_code)
    {

        return $this->q2_zhux_fs($param,$pre_draw_code);

    }

    /**
     * 二码/前二组合/单式   (玩法)
     * 手动输入2个号码组成一注，所输入的号码与当期顺序摇出的5个号码中的前2个号码相同，顺序不限，即为中奖。
     * @pre_draw_code  开奖号码
     * @betting 购买号码
     * @data_num  购买号码(1,2;1,4)
     */

//    public function play_40($param, $pre_draw_code)
//    {
//        return $this->q2_zhux_ds($param,$pre_draw_code);
//    }

    /**
     * 不定胆/前三位   (玩法)
     * 从01-11中共11个号码中选择1个号码，每注由1个号码组成，只要当期顺序摇出的第一位、第二位、第三位开奖号码中包含所选号码，即为中奖。
     * @pre_draw_code  开奖号码
     * @betting 购买号码
     * @data_num  所选号码 01,02,03,02,04
     */

//    public function play_15($param, $pre_draw_code)
//    {
//        return $this->bdw_q3($param,$pre_draw_code);
//    }

    /**
     * 定位胆/定位胆   (玩法)
     * 从第一位，第二位，第三位任意1个位置或多个位置上选择1个号码，所选号码与相同位置上的开奖号码一致，即为中奖。
     *
     * @pre_draw_code  开奖号码 (01,08)  01表示第几位 08表示购买的好嘛
     * @betting 购买号码
     * @one_num  第一位好嘛 （01,02,03）
     * @two_num  第二位好嘛 （01,02,03）
     * @three_num  第三位好嘛 （01,02,03）
     */

    public function play_95($param, $pre_draw_code)
    {

        return $this->dwd($param,$pre_draw_code);


    }

    /**
     * 任选/任选复试/一中一  (玩法)
     * 从01-11共11个号码中选择1个号码进行购买，只要当期顺序摇出的5个开奖号码中包含所选号码，即为中奖。
     * @pre_draw_code  开奖号码
     * @betting 购买号码
     * @data_num  所选号码（01,02,03）
     */

    public function play_107($param, $pre_draw_code)
    {
        return $this->rx_fs_1z1($param,$pre_draw_code);


    }

    /**
     * 任选/任选复试/二中二  (玩法)
     * 从01-11共11个号码中选择2个号码进行购买，只要当期顺序摇出的5个开奖号码中包含所选号码，即为中奖。
     * @pre_draw_code  开奖号码
     * @betting 购买号码
     *  @data_num  所选号码 01,02,03,04
     */


    public function play_108($param, $pre_draw_code)
    {
        return $this->rx_fs_2z2($param,$pre_draw_code);
    }

    /**
     * 任选/任选复试/三中三  (玩法)
     * 从01-11共11个号码中选择3个号码进行购买，只要当期顺序摇出的5个开奖号码中包含所选号码，即为中奖。
     * 从01-11共11个号码中选择3个号码进行购买，只要当期顺序摇出的5个开奖号码中包含所选号码，即为中奖。
     * @pre_draw_code  开奖号码
     * @betting 购买号码
     * @data_num  所选号码 01,02,03,04
     */

    public function play_109($param, $pre_draw_code)
    {
        return $this->rx_fs_3z3($param,$pre_draw_code);
    }

    /**
     * 任选/任选复试/四中四  (玩法)
     * 从01-11共11个号码中选择4个号码进行购买，只要当期顺序摇出的5个开奖号码中包含所选号码，即为中奖。
     * @pre_draw_code  开奖号码
     * @betting 购买号码
     * @data_num  所选号码 01,02,03,04
     */


    public function play_110($param, $pre_draw_code)
    {
        return $this->rx_fs_4z4($param,$pre_draw_code);
    }

    /**
     * 任选/任选复试/五中五  (玩法)
     * 从01-11共11个号码中选择5个号码进行购买，只要当期顺序摇出的5个开奖号码中包含所选号码，即为中奖。
     * @pre_draw_code  开奖号码
     * @betting_num 购买号码
     * @data_num  所选号码 01,02,03,04,05
     */

    public function play_111($param, $pre_draw_code)
    {
        return $this->rx_fs_5z5($param,$pre_draw_code);
    }

    /**
     * 任选/任选复试/六中五  (玩法)
     * 从01-11共11个号码中选择6个号码进行购买，只要当期顺序摇出的5个开奖号码中包含所选号码，即为中奖。
     * @pre_draw_code  开奖号码
     * @data_num  所选号码 01,02,03,04,05,06
     */
    public function play_112($param, $pre_draw_code)
    {
        return $this->rx_fs_6z5($param,$pre_draw_code);

    }

    /**
     * 任选/任选复试/七中五  (玩法)
     * 从01-11共11个号码中选择7个号码进行购买，只要当期顺序摇出的5个开奖号码中包含所选号码，即为中奖。
     * @pre_draw_code  开奖号码
     * @betting 购买号码
     * @data_num  所选号码 01,02,03,04,05,06
     */

    public function play_113($param, $pre_draw_code)
    {
        return $this->rx_fs_7z5($param,$pre_draw_code);

    }

    /**
     * 任选/任选复试/八中五  (玩法)
     * 从01-11共11个号码中选择8个号码进行购买，只要当期顺序摇出的5个开奖号码中包含所选号码，即为中奖。
     * @pre_draw_code  开奖号码
     * @betting_num 购买号码
     * @data_num  所选号码 01,02,03,04,05,06,7,8
     */

    public function play_114($param, $pre_draw_code)
    {
        return $this->rx_fs_8z5($param,$pre_draw_code);


    }
    /**
     * 格式化数据
     */
    public function formatNum($data,$max=11,$min=1)
    {

        foreach ($data as $k => $v) {
            if ($v > $max || $v < $min ||$v == '') {
                unset($data[$k]);
            } else {
                $data[$k] = (int)$v;
            }
        }
        return $data;
    }


}