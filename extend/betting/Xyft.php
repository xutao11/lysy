<?php


namespace betting;


class Xyft
{


    /**
     * 前一/前一/复试
     * 游戏玩法：冠军 投注的1个号码与开奖号码的第一位号
     * 如：第一位选择01，开奖号码为01,*,*,*,*,*,*,*,*,*即为中奖
     * 冠军 投注的1个号码与开奖号码的第一位号码相同且顺序一致，即为中奖
     * @data_num 1,2,3
     */
    public function play_124($param, $pre_draw_code)
    {
        //获取下注的参数
        
        $data_num = explode(',', $param['data_num']);

        $data_num = array_unique($data_num);
        //获取冠军号码

        if(in_array($pre_draw_code[0],$data_num)){
            $re = [
                'count'=>1,
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '前一/前一/复试'
            ];
        }else{
            $re = [
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '前一/前一/复试',
                'count'=>0
            ];
        }
        return $re;
    }
    /**
     * 前二/前二/复试
     * 如：第一位选择01，第二位选择02.开奖号码为：01,02,*,*,*,*,*,*,*,*即为中奖
     * 冠军，亚军 投注的2个号码与开奖号码的第一位、第二位号码相同且顺序一致，即为中奖。
     * @one_num 1,2,3 (冠军)
     * @two_num 1,2,3 (亚军)
     */
    public function play_127($param, $pre_draw_code)
    {
        
        if (!isset($param['one_num']) || !isset($param['two_num'])){
            return [
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '前二/前二/复试',
                'count'=>0
            ];
        }
        //冠军号码格式化
        $one_num = explode(',', $param['one_num']);

        $one_num = array_unique($one_num);
        //亚军号码格式化
        $two_num = explode(',', $param['two_num']);

        $two_num = array_unique($two_num);
        //获取冠开奖号码

        if(in_array($pre_draw_code[0],$one_num) && in_array($pre_draw_code[1],$two_num)){
            $re = [
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '前二/前二/复试',
                'count'=>1
            ];
        }else{
            $re = [
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '前二/前二/复试',
                'count'=>0
            ];
        }
        return $re;
    }
    /**
     * 前二/前二/单式
     * 游戏玩法：手动输入号码，从01-10中任意输入2个号码（每个号码之间用,隔开 每注之间用;隔开）
     * 投注方案：0102； 开奖号码为：01,02,*,*,*,*,*,*,*,*即为中奖
     * 手动输入号码，从01-10中任意输入2个号码组成一注，所选号码与开奖号码的第一位、第二位相同，且顺序一致，即为中奖
     * @data_num 1,2,3 (冠军)
     */
    public function play_128($param, $pre_draw_code)
    {
        
        if (!isset($param['data_num'])){
            return [
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '前二/前二/单式',
                'count'=>0
            ];
        }
        //获取开奖号码

        //前二
        $num = $pre_draw_code[0].','.$pre_draw_code[1];
        $data_num = $param['data_num'];
        $data_num = explode(';',$data_num);
        foreach ($data_num as $k=>$v){
            $v = $this->formatNum(explode(',',$v),10,1);
            $data_num[$k] = implode(',',$v);
        }
        if(in_array($num,$data_num)){
            $re = [
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '前二/前二/单式',
                'count'=>1
            ];
        }else{
            $re = [
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '前二/前二/单式','count'=>0
            ];
        }
        return $re;
    }
    /**
     * 定位胆/定位胆/定位胆
     * 冠军 如：第一位为01，开奖号码为01,*,*,*,*,*,*,*,*,*即为中奖亚军 如：第二位为02，开奖号码为*,02,*,*,*,*,*,*,*,*即为中奖季军 如：第三位为03，开奖号码为*,*,03,*,*,*,*,*,*,*即为中奖第四名 如：第四名位为04，开奖号码为*,*,*,04,*,*,*,*,*,*即为中奖第五名 如：第五名位为05，开奖号码为*,*,*,*,05,*,*,*,*,*即为中奖第六名 如：第六名位为06，开奖号码为*,*,*,*,*,06,*,*,*,*即为中奖第七名 如：第七名位为07，开奖号码为*,*,*,*,*,*,07,*,*,*即为中奖第八名 如：第八名位为08，开奖号码为*,*,*,*,*,*,*,08,*,*即为中奖第九名 如：第九名位为09，开奖号码为*,*,*,*,*,*,*,*,09,*即为中奖第十名 如：第十名位为10，开奖号码为*,*,*,*,*,*,*,*,*,10即为中奖
     * 冠军 从一号位置开始，选择最少一个、最多十个位置，任意1个位置或多个位置上选择1个号码，所选号码与相同位置上的开奖号码一致，即为中奖
     * @one_num 1,2,3 (冠军)
     * @two_num 1,2,3 (亚军)
     * @three_num 1,2,3 (季军)
     * @four_num 1,2,3 (第四名)
     * @five_num 1,2,3 (第5名)
     * @six_num 1,2,3 (第6名)
     * @seven_num 1,2,3 (第7名)
     * @eight_num 1,2,3 (第8名)
     * @nine_num 1,2,3 (第9名)
     * @ten_num 1,2,3 (第10名)
     */
    public function play_1531($param, $pre_draw_code)
    {
        
        $one_num = !isset($param['one_num'])?[]:$this->formatNum(explode(',',$param['one_num']),10,1);
        $two_num = !isset($param['two_num'])?[]:$this->formatNum(explode(',',$param['two_num']),10,1);
        $three_num = !isset($param['three_num'])?[]:$this->formatNum(explode(',',$param['three_num']),10,1);
        $four_num = !isset($param['four_num'])?[]:$this->formatNum(explode(',',$param['four_num']),10,1);
        $five_num = !isset($param['five_num'])?[]:$this->formatNum(explode(',',$param['five_num']),10,1);
        $six_num = !isset($param['six_num'])?[]:$this->formatNum(explode(',',$param['six_num']),10,1);
        $seven_num = !isset($param['seven_num'])?[]:$this->formatNum(explode(',',$param['seven_num']),10,1);
        $eight_num = !isset($param['eight_num'])?[]:$this->formatNum(explode(',',$param['eight_num']),10,1);
        $nine_num = !isset($param['nine_num'])?[]:$this->formatNum(explode(',',$param['nine_num']),10,1);
        $ten_num = !isset($param['ten_num'])?[]:$this->formatNum(explode(',',$param['ten_num']),10,1);
        //获取开奖号码
        $count = 0;
        if(in_array($pre_draw_code[0], $one_num)){
            $count++;
        }
        if(in_array($pre_draw_code[1], $two_num)){
            $count++;
        }
        if(in_array($pre_draw_code[2], $three_num)){
            $count++;
        }
        if(in_array($pre_draw_code[3], $four_num)){
            $count++;
        }
        if(in_array($pre_draw_code[4], $five_num)){
            $count++;
        }
        if(in_array($pre_draw_code[5], $six_num)){
            $count++;
        }
        if(in_array($pre_draw_code[6], $seven_num)){
            $count++;
        }
        if(in_array($pre_draw_code[7], $eight_num)){
            $count++;
        }if(in_array($pre_draw_code[8], $nine_num)){
        $count++;
    }
        if(in_array($pre_draw_code[9], $ten_num)){
            $count++;
        }
        if($count > 0){
            $re = [
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '定位胆/定位胆/定位胆',
                'count' => $count
            ];
        }else{
            $re = [
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '定位胆/定位胆/定位胆',
                'count'=>0
            ];
        }
        return $re;
//
//        if(in_array($pre_draw_code[0],$one_num) || in_array($pre_draw_code[1],$two_num) || in_array($pre_draw_code[2],$three_num) || in_array($pre_draw_code[3],$four_num) || in_array($pre_draw_code[4],$five_num) || in_array($pre_draw_code[5],$six_num) || in_array($pre_draw_code[6],$seven_num) || in_array($pre_draw_code[7],$eight_num) || in_array($pre_draw_code[8],$nine_num) || in_array($pre_draw_code[9],$ten_num)){
//            $re = [
//                'status' => 2,
//                'remark' => '中奖',
//                'play_name' => '定位胆/定位胆/定位胆'
//            ];
//        }else{
//            $re = [
//                'status' => 3,
//                'remark' => '未中奖',
//                'play_name' => '定位胆/定位胆/定位胆'
//            ];
//        }
//        return $re;
    }
    /**
     * 大小/大小/第一名
     * 玩法示意： 选择第一名车号大小为一注。
     * 选择的号码与开奖号码相对应，即为中奖，如第一位购买号码为大，开奖号码为大（6,7,8,9,10）即为中奖。
     * 选择第一名车号大小为一注。
     * @data_num  大,小
     */
    public function play_142($param, $pre_draw_code)
    {
        //开奖号码

        //第一名
        $one_num = $pre_draw_code[0];
        
        $num_type = explode(',',$param['data_num']);
        if($one_num>=6){
            $one_num_type = '大';
        }else{
            $one_num_type = '小';
        }
        if(in_array($one_num_type,$num_type)){
            $re = [
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '大小/大小/第一名',
                'count'=>1
            ];
        }else{
            $re = [
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '大小/大小/第一名',
                'count'=>0
            ];
        }
        return $re;
    }
    /**
     * 大小/大小/第二名
     * 玩法示意： 选择第二名车号大小为一注。
     * 选择的号码与开奖号码相对应，即为中奖，如第二位购买号码为大，开奖号码为大（6,7,8,9,10）即为中奖。
     * 选择第二名车号大小为一注。
     * @data_num  大,小
     */
    public function play_143($param, $pre_draw_code)
    {
        //开奖号码

        //第一名
        $one_num = $pre_draw_code[1];
        
        $num_type = explode(',',$param['data_num']);
        if($one_num>=6){
            $one_num_type = '大';
        }else{
            $one_num_type = '小';
        }
        if(in_array($one_num_type,$num_type)){
            $re = [
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '大小/大小/第二名',
                'count'=>1
            ];
        }else{
            $re = [
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '大小/大小/第二名',
                'count'=>0
            ];
        }
        return $re;
    }
    /**
     * 大小/大小/第三名
     * 玩法示意： 选择第三名车号大小为一注。
     * 选择的号码与开奖号码相对应，即为中奖，如第三位购买号码为大，开奖号码为大（6,7,8,9,10）即为中奖。
     * 选择第三名车号大小为一注。
     * @data_num  大,小
     */
    public function play_144($param, $pre_draw_code)
    {
        //开奖号码

        //第一名
        $one_num = $pre_draw_code[2];
        
        $num_type = explode(',',$param['data_num']);
        if($one_num>=6){
            $one_num_type = '大';
        }else{
            $one_num_type = '小';
        }
        if(in_array($one_num_type,$num_type)){
            $re = [
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '大小/大小/第三名',
                'count'=>1
            ];
        }else{
            $re = [
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '大小/大小/第三名',
                'count'=>0
            ];
        }
        return $re;
    }
    /**
     * 单双/单双/第一名
     * 如：选择单，开奖号码的第一位为01或03或05或07或09，即为中奖
     * 选择第一名车号的单双，1 3 5 7 9为单，2 4 6 8 10为双，若相符即为中奖
     * @data_num 单,双
     */
    public function play_1535($param, $pre_draw_code)
    {
        //开奖号码

        //第一名
        $one_num = $pre_draw_code[0];
        
        $num_type = explode(',',$param['data_num']);
        if($one_num%2 ==0){
            $one_num_type = '双';
        }else{
            $one_num_type = '单';
        }
        if(in_array($one_num_type,$num_type)){
            $re = [
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '单双/单双/第一名',
                'count'=>1
            ];
        }else{
            $re = [
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '单双/单双/第一名',
                'count'=>0
            ];
        }
        return $re;
    }
    /**
     * 单双/单双/第二名
     * 如：选择单，开奖号码的第二位为01或03或05或07或09，即为中奖
     * 选择第二名车号的单双，1 3 5 7 9为单，2 4 6 8 10为双，若相符即为中奖
     * @data_num 单,双
     */
    public function play_1536($param, $pre_draw_code)
    {
        //开奖号码

        //第一名
        $one_num = $pre_draw_code[1];
        
        $num_type = explode(',',$param['data_num']);
        if($one_num%2 ==0){
            $one_num_type = '双';
        }else{
            $one_num_type = '单';
        }
        if(in_array($one_num_type,$num_type)){
            $re = [
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '单双/单双/第二名',
                'count'=>1
            ];
        }else{
            $re = [
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '单双/单双/第二名',
                'count'=>0
            ];
        }
        return $re;
    }
    /**
     * 单双/单双/第三名
     * 如：选择单，开奖号码的第三位为01或03或05或07或09，即为中奖
     * 选择第三名车号的单双，1 3 5 7 9为单，2 4 6 8 10为双，若相符即为中奖
     * @data_num 单,双
     */
    public function play_1537($param, $pre_draw_code)
    {
        //开奖号码

        //第一名
        $one_num = $pre_draw_code[2];
        
        $num_type = explode(',',$param['data_num']);
        if($one_num%2 ==0){
            $one_num_type = '双';
        }else{
            $one_num_type = '单';
        }
        if(in_array($one_num_type,$num_type)){
            $re = [
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '单双/单双/第三名',
                'count'=>1
            ];
        }else{
            $re = [
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '单双/单双/第三名',
                'count'=>0
            ];
        }
        return $re;
    }
    /**
     * 龙虎/龙虎/第一名
     * 如：选择龙，开奖号码的第一位为03，第十位为01，即为中奖
     * 龙：冠军号码大于第十名号码视为“龙”中奖，如冠军开出07，第十名开出03。否则是虎。
     * @data_num 龙,虎
     */
    public function play_1540($param, $pre_draw_code)
    {
        //开奖号码

        //第一名
        $one_num = $pre_draw_code[0];
        //最后一名
        $two_num = $pre_draw_code[9];
        if($one_num>$two_num){
            $one_num_type = '龙';
        }elseif ($one_num<$two_num){
            $one_num_type = '虎';
        }else{
            return [
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '龙虎/龙虎/第一名',
                'count'=>0
            ];
        }
        
        $num_type = explode(',',$param['data_num']);
        if(in_array($one_num_type,$num_type)){
            $re = [
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '龙虎/龙虎/第一名',
                'count'=>1
            ];
        }else{
            $re = [
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '龙虎/龙虎/第一名',
                'count'=>0
            ];
        }
        return $re;
    }

    /**
     * 龙虎/龙虎/第二名
     * 如：选择龙，开奖号码的第二位为03，第九位为01，即为中奖
     * 龙：亚军号码大于第九名号码视为“龙”中奖，如亚军开出07，第九名开出03。否则是虎。
     * @data_num 龙,虎
     */
    public function play_1541($param, $pre_draw_code)
    {
        //开奖号码

        //第一名
        $one_num = $pre_draw_code[1];
        //最后一名
        $two_num = $pre_draw_code[8];
        if($one_num>$two_num){
            $one_num_type = '龙';
        }elseif ($one_num<$two_num){
            $one_num_type = '虎';
        }else{
            return [
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '龙虎/龙虎/第二名',
                'count'=>0
            ];
        }
        
        $num_type = explode(',',$param['data_num']);
        if(in_array($one_num_type,$num_type)){
            $re = [
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '龙虎/龙虎/第二名',
                'count'=>1
            ];
        }else{
            $re = [
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '龙虎/龙虎/第二名','count'=>0
            ];
        }
        return $re;
    }

    /**
     * 龙虎/龙虎/第三名
     * 如：选择龙，开奖号码的第三位为03，第八位为01，即为中奖
     * 龙：季军号码大于第八名号码视为“龙”中奖，如季军开出07，第八名开出03。否则是虎。
     * @data_num 龙,虎
     */
    public function play_1542($param, $pre_draw_code)
    {
        //开奖号码

        //第一名
        $one_num = $pre_draw_code[2];
        //最后一名
        $two_num = $pre_draw_code[7];
        if($one_num>$two_num){
            $one_num_type = '龙';
        }elseif ($one_num<$two_num){
            $one_num_type = '虎';
        }else{
            return [
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '龙虎/龙虎/第三名',
                'count'=>0
            ];
        }
        
        $num_type = explode(',',$param['data_num']);
        if(in_array($one_num_type,$num_type)){
            $re = [
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '龙虎/龙虎/第三名',
                'count'=>1
            ];
        }else{
            $re = [
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '龙虎/龙虎/第三名',
                'count'=>0
            ];
        }
        return $re;
    }

    /**
     * 格式化数据
     */
    public function formatNum($data,$max=10,$min=1)
    {

        foreach ($data as $k => $v) {
            if ($v > $max || $v < $min ||$v == '') {
                unset($data[$k]);
            } else {
                $data[$k] = (int)$v;
            }
        }
        return $data;
    }

}