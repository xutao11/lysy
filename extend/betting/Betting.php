<?php


namespace betting;

use think\Db;
use think\Exception;

class Betting
{


    /**
     * 开奖计算入口
     */
    public function kjJisuan()
    {
        /**
         * 改单
         */
        $this->changeOrder();


        $bettings = Db::table('lsyl_betting')->where('is_jz', '1')->where('status', 1)->field('id,ordercode,user_id,pre_draw_issue,param,lottery_id,is_jz,is_max,play_id,jg_money,xz_money,multiple,unit,status')->select();
//        dump($bettings);
        if ($bettings) {
            foreach ($bettings as $k => $v) {
                // 启动事务
                Db::startTrans();
                try {
                    $this->drawPrize($v);
                    // 提交事务
                    Db::commit();
                } catch (\Exception $e) {
                    // 回滚事务
                    Db::rollback();
                    errorLog('开奖计算错误', $e->getMessage());
                }


            }
        }


    }


    /**
     * 开奖计算
     * @param $betting 下注订单
     */
    public function drawPrize($betting)
    {


//            sleep(5);
        //该订单已结算
        if ($betting['status'] != '1') return;
        //订单已禁止
        if ($betting['is_jz'] == '0') return;

        //查询开奖期数
        $kjlog = Db::name('kjlog')->where('lottery_id', $betting['lottery_id'])->where('pre_draw_issue', $betting['pre_draw_issue'])->field('id,lottery_id,pre_draw_issue,pre_draw_code')->find();

        if (!$kjlog) return;
        //查询彩种
        $lottery = Db::name('lottery')->where('id', $betting['lottery_id'])->field('id,name')->find();

        $lotteryClass = $this->getLotteryClass($betting['lottery_id']);
        $action = 'play_' . $betting['play_id'];
        //解析所选号码
        $param = json_decode($betting['param'], true);

        $pre_draw_code = $this->format_pre_draw_code($kjlog['pre_draw_code']);
        $bet_status = $lotteryClass->$action($param, $pre_draw_code);


        if ($bet_status['status'] == '2') {
            //中奖
            //计算奖金
            $money = bcmul($betting['jg_money'], $bet_status['count'], 3);
            //更新用户余额
            updateUser($betting['user_id'], $money, '10', $betting['ordercode'], time(), $lottery['name'] . "({$bet_status['play_name']})中奖");
            Db::name('betting')->where('id', $betting['id'])->update([
                'jg_num' => $bet_status['count'],
                'status' => '2',
                'updatetime' => time(),
                'remark' => '中奖金额:' . $money
            ]);


        } else {
            Db::name('betting')->where('id', $betting['id'])->update([
                'jg_num' => $bet_status['count'],
                'status' => (string)$bet_status['status'],
                'updatetime' => time(),
                'remark' => $bet_status['remark']
            ]);
        }
        //返点
        fandianTree($betting['user_id'], $betting['xz_money'], $betting['ordercode']);
        echo '下注id:' . $betting['user_id'] . '---------' . $bet_status['remark'] . '----------' . $bet_status['play_name'] . '======方法:' . $action . "\n";



    }


    /**
     * 自动改单 奇趣1   河内1
     * @throws Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     */
    public function changeOrder()
    {
        //查询需要改单的订单
        $betting = Db::table('lsyl_betting')->where('lottery_id', 'in', [3, 6])->where('is_jz', '0')->where('is_max', 'in', ['1', '2'])->field('id,ordercode,user_id,pre_draw_issue,param,lottery_id,is_jz,is_max,play_id,jg_money,xz_money,multiple,unit,status,kj_time')->select();
        if ($betting) {
            foreach ($betting as $k => $v) {
                //开奖时间
                $kj_time = strtotime($v['kj_time']);
                if ($v['is_max'] == '1') {
                    //前两秒 与上一期做比较
                    //获取上期期数
                    $qishu_num = (int)substr($v['pre_draw_issue'], 8);
                    //日期
                    $qishu_date = substr($v['pre_draw_issue'], 0, 8);
                    if ($qishu_num == 1) {
                        $last_qishu = date('Ymd', strtotime('-1 day', strtotime($qishu_date))) . '1440';
                    } else {
                        $last_qishu = $qishu_date . sprintf("%04d", $qishu_num - 1);
                    }
                    //查询上一期是否开奖
                    $kj_data = Db::name('kjlog')->where('lottery_id', $v['lottery_id'])->where('pre_draw_issue', $last_qishu)->field('id,lottery_id,pre_draw_issue,pre_draw_code')->find();
                    if (!$kj_data) continue;
                    Db::name('betting')->where('pre_draw_issue', $v['pre_draw_issue'])->where('lottery_id', $v['lottery_id'])->where('createtime', '>=', $kj_time - 60)->where('createtime', '<=', $kj_time - 58)->update([
                        'is_jz' => '1'
                    ]);
                    $action = 'play_' . $v['play_id'];

                    $pre_draw_code = $this->format_pre_draw_code($kj_data['pre_draw_code']);

                    $param = json_decode($v['param'], true);
                    $lotteryClass = $this->getLotteryClass($v['lottery_id']);
                    $bet_status = $lotteryClass->$action($param, $pre_draw_code);


                    if ($bet_status['status'] == 2) {
                        Db::name('betting')->where('id', $v['id'])->update([
                            'is_jz' => 1,
                        ]);
                        //中奖不修改
                    } elseif ($bet_status['status'] == 3) {
                        //没有中奖 (修改为上一期)
                        Db::name('betting')->where('id', $v['id'])->update([
                            'is_jz' => 1,
                            'kj_time' => date('Y-m-d H:i:s', strtotime('-1 minute', strtotime($v['kj_time']))),
                            'pre_draw_issue' => $last_qishu
                        ]);
                    }


                } elseif ($v['is_max'] == '2') {
                    //后2秒
                    $kj_data = Db::name('kjlog')->where('lottery_id', $v['lottery_id'])->where('pre_draw_issue', $v['pre_draw_issue'])->field('id,lottery_id,pre_draw_issue,pre_draw_code')->find();
                    if (!$kj_data) continue;
                    Db::name('betting')->where('pre_draw_issue', $v['pre_draw_issue'])->where('lottery_id', $v['lottery_id'])->where('createtime', '>=', ($kj_time - 2))->where('createtime', '<=', $kj_time)->update([
                        'is_jz' => '1'
                    ]);
                    $action = 'play_' . $v['play_id'];

                    $pre_draw_code = $this->format_pre_draw_code($kj_data['pre_draw_code']);
                    $param = json_decode($v['param'], true);
                    $lotteryClass = $this->getLotteryClass($v['lottery_id']);
                    $bet_status = $lotteryClass->$action($param, $pre_draw_code);

                    if ($bet_status['status'] == 3) {
                        //不中奖不修改
                        Db::name('betting')->where('id', $v['id'])->update([
                            'is_jz' => 1,
                        ]);
                    } elseif ($bet_status['status'] == 2) {
                        //获取下期期数
                        $qishu_num = (int)substr($v['pre_draw_issue'], 8);
                        //日期
                        $qishu_date = substr($v['pre_draw_issue'], 0, 8);
                        if ($qishu_num == 1440) {
                            $next_qishu = date('Ymd', strtotime('+1 day', strtotime($qishu_date))) . '0001';
                        } else {
                            $next_qishu = $qishu_date . sprintf("%04d", $qishu_num + 1);
                        }
                        //没有中奖 (修改为下一期)
                        Db::name('betting')->where('id', $v['id'])->update([
                            'is_jz' => 1,
                            'kj_time' => date('Y-m-d H:i:s', strtotime('+1 minute', strtotime($v['kj_time']))),
                            'pre_draw_issue' => $next_qishu
                        ]);
                    }
                }
            }
        }
    }


    /**
     * 格式化开奖号码
     *
     */
    protected function format_pre_draw_code($pre_draw_code)
    {
        $pre_draw_code = explode(',', $pre_draw_code);
        if ($pre_draw_code) {
            foreach ($pre_draw_code as $k => $v) {
                $pre_draw_code[$k] = (int)$v;
            }
        }
        return $pre_draw_code;
    }


    /**
     * 获取相关的彩票类
     * @param $lottery_id
     * @return Cqssc
     * @throws Exception
     */
    protected function getLotteryClass($lottery_id)
    {
        switch ($lottery_id) {
            case 1:
                //重庆时时彩
                $lotteryClass = new Cqssc();
                break;
            case 2:
                //新疆时时彩
                $lotteryClass = new Xjssc();
                break;
            case 3:
                //奇趣1分彩
                $lotteryClass = new Qqyfc();
                break;
            case 4:
                //奇趣5分彩
                $lotteryClass = new Qqwfc();
                break;
            case 5:
                //奇趣10分彩
                $lotteryClass = new Qqsfc();
                break;
            case 6:
                //河内1分彩
                $lotteryClass = new Hlyfc();
                break;
            case 7:
                //河内5分彩
                $lotteryClass = new Hlwfc();
                break;
            case 8:
                //幸运飞艇
                $lotteryClass = new Xyft();
                break;
            case 9:
                //广东11选5
                $lotteryClass = new Gd11x5();
                break;
            case 10:
                //江西11选5
                $lotteryClass = new Jx11x5();
                break;
            case 11:
                //山东11选5
                $lotteryClass = new Sd11x5();
                break;
            case 12:
                //河北11选5
                $lotteryClass = new Hb11x5();
                break;
            case 13:
                //江苏快三
                $lotteryClass = new Jsk3();
                break;
            case 14:
                //广西快三
                $lotteryClass = new Gxk3();
                break;
            default:
                throw new \think\Exception('彩种不存在', 100006);
                break;
        }
        return $lotteryClass;
    }

}