<?php
namespace betting\calculation;


trait Calculation
{

    /**
     * 开奖 5个号码  每个号码 0-9
     * 前四/前四直选/复式
     * 游戏玩法：从万、千、百、十位各选一个号码组成一注。
     * 投注方案：2345*；开奖号码：2345*
     * 投注玩法：从万位、千位、百位、十位中选择一个4位数号码组成一注，所选号码与开奖号码全部相同，且顺序一致，即为中奖
     * @one_num 1,2,3 (万位)
     * @two_num 1,2,3 (千位)
     * @three_num 1,2,3 (百位)
     * @four_num 1,2,3 (十位)
     */
    public function q4zhixfs($param, $pre_draw_code)
    {
        //获取下注的参数
        $two_num = $this->formatNum(explode(',', $param['two_num']));
        $three_num = $this->formatNum(explode(',', $param['three_num']));
        $four_num = $this->formatNum(explode(',', $param['four_num']));
        $one_num = $this->formatNum(explode(',', $param['one_num']));
        if (count($two_num) == 0 || count($three_num) == 0 || count($four_num) == 0 || count($one_num) == 0) {
            return [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '前四/前四直选/复式'
            ];
        }

        if (in_array($pre_draw_code[1], $two_num) && in_array($pre_draw_code[2], $three_num) && in_array($pre_draw_code[3], $four_num) && in_array($pre_draw_code[0], $one_num)) {
            $re = [
                'count'=>1,
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '前四/前四直选/复式'
            ];
        } else {
            $re = [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '前四/前四直选/复式'
            ];
        }
        return $re;
    }
    /**
     * 开奖 5个号码  每个号码 0-9
     * 前四/前四直选/单式
     * 投注玩法：手动输入一个4位数号码组成一注，所选号码的万位、千位、百位、十位与开奖号码相同，且顺序一致，即为中奖（每个号码之间用,隔开，每注之间用;隔开）
     * 投注方案：2345*； 开奖号码：2345*，即中前四星直选一等奖
     * data_num 1,2,3,4;2,3,4,5
     */
    public function q4zhixds($param, $pre_draw_code)
    {
        $data_num = $param['data_num'];
        $data_num = explode(' ', $data_num);

        if (count($data_num) == 0) {
            return [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '前四/前四直选/单式'
            ];
        }


        if ($data_num) {
            foreach ($data_num as $k => $v) {
                $data_num[$k] = implode(',', $this->formatNum(str_split($v)));
            }
        }
        //中奖号码后4位
        $pre_draw_code = array_slice($pre_draw_code, 0, 4);
        $pre_draw_code = implode(',', $pre_draw_code);
        if (in_array($pre_draw_code, $data_num)) {
            $re = [
                'count'=>1,
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '前四/前四直选/单式'
            ];

        } else {
            $re = [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '前四/前四直选/单式'
            ];
        }
        return $re;
    }

    /**
     * 开奖 5个号码  每个号码 0-9
     * 前四/前四组选/组选24
     * 游戏玩法：至少选择四个号码组成一注。
     * 投注方案：选择1234（展开为1234*，1243*，1324*，1342*，1423*，1432*...）,开奖为4321*，即为中奖
     * 投注玩法：前四组选24：选4个或多个，所选号码与开奖万、千、百、十一致顺序不限，即为中奖
     * @data_num 1,2,3,4
     */
    public function q4zhux24($param, $pre_draw_code)
    {
        //获取下注的参数
        $data_num = $param['data_num'];
        $data_num =  $this->formatNum(explode(',', $data_num));
        $pre_draw_code = array_slice($pre_draw_code, 0, 4);
        if(count(array_unique($pre_draw_code)) !=4){
            $re = [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '前四/前四组选/组选24'
            ];
            return $re;
        }
        $data_num_jj = array_intersect($data_num,$pre_draw_code);
        if(count($data_num_jj) == 4){
            $re = [
                'count'=>1,
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '前四/前四组选/组选24'
            ];
        }else{
            $re = [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '前四/前四组选/组选24'
            ];
        }
        return $re;
    }
    /**
     * 开奖 5个号码  每个号码 0-9
     * 前四/前四直选/组选12
     * 投注玩法：从二重号中选择一个或多个，从单号中选择两个或多个，所选号码与开奖万、千、百、十一致顺序不限，即为中奖
     * 投注方案：如投注的二重号是1，单号是23，开奖为1123*，即为中奖
     * @data_num 5,6 (单号)
     * @double_num 5,6 (二重号)
     * {"double_num":"0,1,2,3,4,5,6,7,8,9","data_num":"0,1,2,3,4,5,6,7,8,9"}
     * 1,1,0,6,3
     */
    public function q4zhux12($param, $pre_draw_code)
    {
        //获取下注的参数
        $data_num = $this->formatNum(explode(',', $param['data_num']));
        $double_num = $this->formatNum(explode(',', $param['double_num']));
        if (count($data_num) < 2 || count($double_num) < 0) {
            return [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '前四/前四直选/组选12'
            ];
        }

        $pre_draw_code = array_slice($pre_draw_code, 0, 4);
        if(count(array_unique($pre_draw_code)) != 3){
            return [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '前四/前四直选/组选12'
            ];
        }
        //二重号
        foreach ($double_num as $k=>$v){
            if(array_count_values_of($v, $pre_draw_code) == 2){
                $data_num = array_diff($data_num, [$v]);
//                unset($data_num[array_search($v, $data_num)]);
                $re = array_intersect($data_num, $pre_draw_code);
                if(count($re) == 2){
                    return [
                        'count'=>1,
                        'status' => 2,
                        'remark' => '中奖',
                        'play_name' => '前四/前四直选/组选12'
                    ];
                }

            }
        }

        return [
            'count'=>0,
            'status' => 3,
            'remark' => '未中奖',
            'play_name' => '前四/前四直选/组选12'
        ];
    }
    /**
     * 开奖 5个号码  每个号码 0-9
     * 后四/后四直选/复式
     * 游戏玩法：从千、百、十、个位各选一个号码组成一注。
     * 投注方案：*2345；开奖号码：*2345
     * 投注玩法：从千位、百位、十位、个位中选择一个4位数号码组成一注，所选号码与开奖号码全部相同，且顺序一致，即为中奖
     *
     * @two_num 1,2,3 (千位)
     * @three_num 1,2,3 (百位)
     * @four_num 1,2,3 (十位)
     *  @five_num 1,2,3 (个位)
     */
    public function h4zhixfs($param, $pre_draw_code)
    {
        //获取下注的参数
        $two_num = $this->formatNum(explode(',', $param['two_num']));
        $three_num = $this->formatNum(explode(',', $param['three_num']));
        $four_num = $this->formatNum(explode(',', $param['four_num']));
        $five_num = $this->formatNum(explode(',', $param['five_num']));
        if (count($two_num) == 0 || count($three_num) == 0 || count($four_num) == 0 || count($five_num) == 0) {
            return [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '后四/后四直选/复式'
            ];
        }

        if (in_array($pre_draw_code[1], $two_num) && in_array($pre_draw_code[2], $three_num) && in_array($pre_draw_code[3], $four_num) && in_array($pre_draw_code[4], $five_num)) {
            $re = [
                'count'=>1,
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '后四/后四直选/复式'
            ];
        } else {
            $re =[
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '后四/后四直选/复式'
            ];
        }
        return $re;
    }
    /**
     * 开奖 5个号码  每个号码 0-9
     * 后四/后四直选/单式
     * 投注玩法：手动输入一个4位数号码组成一注，所选号码的从千位、百位、十位、个位与开奖号码相同，且顺序一致，即为中奖（每个号码之间用,隔开，每注之间用;隔开）
     * 投注方案：*2345； 开奖号码：*2345，即中前四星直选一等奖
     * data_num 1234 2345
     */
    public function h4zhixds($param, $pre_draw_code)
    {
        //获取下注的参数
        $data_num = $param['data_num'];
        $data_num = explode(' ', $data_num);
        if (count($data_num) == 0) {
            return [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '后四/后四直选/单式'
            ];
        }


        if ($data_num) {
            foreach ($data_num as $k => $v) {
                $data_num[$k] = implode(',', $this->formatNum(str_split($v)));
            }
        }
        //中奖号码后4位
        $pre_draw_code = array_slice($pre_draw_code, 1, 4);
        $pre_draw_code = implode(',', $pre_draw_code);
        if (in_array($pre_draw_code, $data_num)) {
            $re = [
                'count'=>1,
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '后四/后四直选/单式'
            ];

        } else {
            $re = [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '后四/后四直选/单式'
            ];
        }
        return $re;
    }
    /**
     * 开奖 5个号码  每个号码 0-9
     * 后四/后四组选/组选24
     * 游戏玩法：至少选择四个号码组成一注。
     * 投注方案：选择1234（展开为*1234，*1243，*1324，*1342，*1423，*1432...）,开奖为*4321，即为中奖
     * 投注玩法：后四组选24：选4个或多个，所选号码与开奖千、百、十、个一致顺序不限，即为中奖
     * @data_num 1,2,3,4
     * 1,0,4,9,5
     * {"data_num":"0,1,2,3,4,5,6,7,8,9"}
     */
    public function h4zhux24($param, $pre_draw_code)
    {
        //获取下注的参数
        $data_num = $param['data_num'];
        $data_num =  $this->formatNum(explode(',', $data_num));

        $pre_draw_code = array_slice($pre_draw_code, 1, 4);
        if(count(array_unique($pre_draw_code)) !=4){
            $re = [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '前四/后四直选/组选24'
            ];
            return $re;
        }
        $data_num_jj = array_intersect($data_num,$pre_draw_code);
        if(count($data_num_jj) == 4){
            $re = [
                'count'=>1,
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '后四/后四直选/组选24'
            ];
        }else{
            $re = [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '后四/后四直选/组选24'
            ];
        }
        return $re;
    }
    /**
     * 开奖 5个号码  每个号码 0-9
     * 后四/后四直选/组选12
     * 投注玩法：从二重号中选择一个或多个，从单号中选择两个或多个，所选号码与开奖千、百、十、个一致顺序不限，即为中奖
     * 投注方案：如投注的二重号是1，单号是23，开奖为*1123，即为中奖
     * @data_num 5,6 (单号)
     * @double_num 5,6 (二重号)
     */
    public function h4zhux12($param, $pre_draw_code)
    {
        //获取下注的参数
        $data_num = $this->formatNum(explode(',', $param['data_num']));
        $double_num = $this->formatNum(explode(',', $param['double_num']));
        if (count($data_num) < 2 || count($double_num) < 0) {
            return [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '后四/后四直选/组选12'
            ];
        }
        //取开奖号啊

        $pre_draw_code = array_slice($pre_draw_code, 1, 4);
        foreach ($double_num as $k => $v) {
            //获取$v 在 开奖号码中出现的次数
            if (array_count_values_of($v, $pre_draw_code) == 2) {
                $data_num = array_diff($data_num, [$v]);
                $re = array_intersect($data_num, $pre_draw_code);
                if (count($re) == 2 && !in_array($v, $re)) {
                    return [
                        'count'=>1,
                        'status' => 2,
                        'remark' => '中奖',
                        'play_name' => '后四/后四直选/组选12'
                    ];
                }
            }
        }
        return [
            'count'=>0,
            'status' => 3,
            'remark' => '未中奖',
            'play_name' => '后四/后四直选/组选12'
        ];
    }
    /**
     * 开奖 5个号码  每个号码 0-9
     * 前三/前三直选/复式
     * 游戏玩法：从万、千、百位各选一个号码组成一注。
     * 投注方案：234**；开奖号码：234**
     * 投注玩法：从万位、千位、百位中选择一个3位数号码组成一注，所选号码与开奖号码全部相同，且顺序一致，即为中奖
     * @one_num  第一位好嘛 （01,02,03）
     * @two_num  第二位好嘛 （01,02,03）
     * @three_num  第三位好嘛 （01,02,03）
     */
    public function q3zhixfs($param, $pre_draw_code)
    {
        //获取下注的参数
        $one_num = $param['one_num'];
        $two_num = $param['two_num'];
        $three_num = $param['three_num'];
        $one_num = $this->formatNum(explode(',', $one_num));
        $two_num = $this->formatNum(explode(',', $two_num));
        $three_num = $this->formatNum(explode(',', $three_num));
        if (count($one_num) == 0 || count($two_num) == 0 || count($three_num) == 0) {
            return [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '前三/前三直选/复式'
            ];
        }

        if (in_array($pre_draw_code[0], $one_num) && in_array($pre_draw_code[1], $two_num) && in_array($pre_draw_code[2], $three_num)) {
            $re = [
                'count'=>1,
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '前三/前三直选/复式'
            ];

        } else {
            $re = [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '前三/前三直选/复式'
            ];
        }
        return $re;
    }
    /**
     * 开奖 5个号码  每个号码 0-9
     * 前三/前三直选/单式   (玩法)
     * 投注玩法：手动输入一个3位数号码组成一注，所选号码的万、千、百与开奖号码相同，且顺序一致，即为中奖（每个号码之间用,隔开，每注之间用;隔开）
     * 投注方案：234**；开奖号码：234**
     * 投注玩法：手动输入一个3位数号码组成一注，所选号码的万、千、百位与开奖号码相同，且顺序一致，即为中奖
     * @data_num  下注号码123 213
     */
    public function q3zhixds($param, $pre_draw_code)
    {
        //获取下注的参数
        $data_num = $param['data_num'];
        $data_num = explode(' ', $data_num);
        if (count($data_num) == 0) {
            return [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '前三/前三直选/单式'
            ];
        }
        if ($data_num) {
            foreach ($data_num as $k => $v) {
                $data_num[$k] = implode(',', $this->formatNum(str_split($v)));
            }
        }
        //中奖号码前三位
        $pre_draw_code = array_slice($pre_draw_code, 0, 3);

        $pre_draw_code = implode(',', $pre_draw_code);
        if (in_array($pre_draw_code, $data_num)) {
            $re = [
                'count'=>1,
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '前三/前三直选/单式'
            ];

        } else {
            $re = [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '前三/前三直选/单式'
            ];
        }
        return $re;
    }
    /**
     * 开奖 5个号码  每个号码 0-9
     * 前三/前三组选 /组三
     * 游戏玩法：至少选择三个号码组成一注。
     * 投注方案：选择123（展开为123**，132**，231**，213**，312**，321**）,开奖为321**，即为中奖
     * 投注玩法：前三组六：选3个或多个，所选号码与开奖万、千、百一致顺序不限，即为中奖
     * @data_num   1,2,3
     */
    public function q3zhuxz3($param, $pre_draw_code)
    {
        $data_num = $param['data_num'];
        $data_num = explode(',', $data_num);
        $data_num = $this->formatNum($data_num);
        $data_num = array_unique($data_num);

        //前三的号码
        $pre_draw_code = [$pre_draw_code[0], $pre_draw_code[1], $pre_draw_code[2]];
        //去重
        $pre_draw_code = array_unique($pre_draw_code);
        if (count($pre_draw_code) != 2) {
            return [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '前三/前三组选/组三'
            ];
        }

        if (array_intersect($pre_draw_code, $data_num) == $pre_draw_code) {
            $re = [
                'count'=>1,
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '前三/前三组选/组三'
            ];
        } else {
            $re =[
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '前三/前三组选/组三'
            ];
        }
        return $re;
    }
    /**
     * 开奖 5个号码  每个号码 0-9
     * 前三/前三组选 /组六
     * 游戏玩法：至少选择三个号码组成一注。
     * 投注方案：选择123（展开为123**，132**，231**，213**，312**，321**）,开奖为321**，即为中奖
     * 投注玩法：前三组六：选3个或多个，所选号码与开奖万、千、百一致顺序不限，即为中奖
     * @data_num   1,2,3
     */
    public function q3zhuxz6($param, $pre_draw_code)
    {
        $data_num = $param['data_num'];
        $data_num = explode(',', $data_num);
        $data_num = $this->formatNum($data_num);
        $data_num = array_unique($data_num);


        //前三的号码
        $pre_draw_code = [$pre_draw_code[0], $pre_draw_code[1], $pre_draw_code[2]];
        //去重
        $pre_draw_code = array_unique($pre_draw_code);
        if (count($pre_draw_code) != 3) {
            return [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '前三/前三组选/组六'
            ];
        }
        if (array_intersect($pre_draw_code, $data_num) == $pre_draw_code) {
            $re = [
                'count'=>1,
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '前三/前三组选/组六'
            ];
        } else {
            $re = [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '前三/前三组选/组六  '
            ];
        }
        return $re;
    }
    /**
     * 前三/前三组选/组选包胆
     * 玩法示意：从0-9中任选1个号码。
     * 投注方案：包胆3；开奖号码后三位：(1)出现3xx或者33x,即中前三组三；(2)出现3xy，即中前三组六。
     * 从0-9中任意选择1个号码组成一注，出现后三组三或组六，即为中奖。
     * 从0-9中任选1个号码
     * @data_num  0 (任选1个号码)
     */
    public function q3zxbd($param,$pre_draw_code){
        //获取下注的参数
        $data_num = $param['data_num'];
        $data_num = explode(',', $data_num);
        $data_num = $this->formatNum($data_num);
        //后三的号码
        $pre_draw_code = [$pre_draw_code[0], $pre_draw_code[1], $pre_draw_code[2]];
        if (array_intersect($pre_draw_code, $data_num)) {
            $re = [
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '前三/前三组选/组选包胆'
            ];
        } else {
            $re = [
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '前三/前三组选/组选包胆'
            ];
        }
        return $re;
    }
    /**
     * 中三/中三组选/组选包胆
     * 玩法示意：从0-9中任选1个号码。
     * 投注方案：包胆3；开奖号码中三位：(1)出现3xx或者33x,即中前三组三；(2)出现3xy，即中前三组六。
     * 从0-9中任意选择1个号码组成一注，出现中三组三或组六，即为中奖。
     * 从0-9中任选1个号码
     * @data_num  0 (任选1个号码)
     */
    public function z3zubd($param,$pre_draw_code){
        $data_num = $param['data_num'];
        $data_num = explode(',', $data_num);
        $data_num = $this->formatNum($data_num);




        //后三的号码
        $pre_draw_code = [$pre_draw_code[1], $pre_draw_code[2], $pre_draw_code[3]];
        if (array_intersect($pre_draw_code, $data_num)) {
            $re = [
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '中三/中三组选/组选包胆'
            ];
        } else {
            $re = [
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '中三/中三组选/组选包胆'
            ];
        }
        return $re;
    }
    /**
     * 后三/后三组选/组选包胆
     * 玩法示意：从0-9中任选1个号码。
     * 投注方案：包胆3；开奖号码后三位：(1)出现3xx或者33x,即中后三组三；(2)出现3xy，即中后三组六。
     * 从0-9中任意选择1个号码组成一注，出现后三组三或组六，即为中奖。
     * 从0-9中任选1个号码
     * @data_num  0 (任选1个号码)
     */
    public function h3zxbd($param,$pre_draw_code){
        $data_num = $param['data_num'];
        $data_num = explode(',', $data_num);
        $data_num = $this->formatNum($data_num);
        //后三的号码
        $pre_draw_code = [$pre_draw_code[2], $pre_draw_code[3], $pre_draw_code[4]];
        if (array_intersect($pre_draw_code, $data_num)) {
            $re = [
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '后三/后三组选/组选包胆'
            ];
        } else {
            $re = [
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '后三/后三组选/组选包胆'
            ];
        }


        return $re;


    }
    /**
     * 前二/组选/包胆
     * 从0-9中任意选择1个包胆号码。
     * 投注方案：包胆号码8；开奖号码前二位：出现1个8（不包括2个8），即中前二组选。
     * 从0-9中任意选择1个包胆号码，开奖号码的万位、千位中任意1位包含所选的包胆号码相同（不含对子号），即为中奖。
     * @data_num 1
     *
     */
    public function q2zhuxbd($param, $pre_draw_code){
        $pre_draw_code = [$pre_draw_code[0], $pre_draw_code[1]];
        $pre_draw_code = array_unique($pre_draw_code);
        if (count($pre_draw_code) != 2) {
            $re = [
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '前二/组选/包胆'
            ];
            return $re;

        }

        $data_num = $param['data_num'];
        $data_num = sprintf("%02d", $data_num);
        if (in_array($data_num, $pre_draw_code)) {
            $re = [
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '前二/组选/包胆'
            ];

        } else {

            $re = [
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '前二/组选/包胆'
            ];

        }
        return $re;
    }
    /**
     * 后二/组选/包胆
     * 从0-9中任意选择1个包胆号码。
     * 投注方案：包胆号码8；开奖号码后二位：出现1个8（不包括2个8），即中后二组选。
     * 从0-9中任意选择1个包胆号码，开奖号码的万位、千位中任意1位包含所选的包胆号码相同（不含对子号），即为中奖。
     * @data_num 1
     *
     */
    public function h2zhuxbd($param, $pre_draw_code){
        $pre_draw_code = [$pre_draw_code[3], $pre_draw_code[4]];
        $pre_draw_code = array_unique($pre_draw_code);
        if (count($pre_draw_code) != 2) {
            $re = [
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '后二/组选/包胆'
            ];
            return $re;

        }


        $data_num = $param['data_num'];
        $data_num = sprintf("%02d", $data_num);
        if (in_array($data_num, $pre_draw_code)) {
            $re = [
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '后二/组选/包胆'
            ];

        } else {

            $re = [
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '后二/组选/包胆'
            ];

        }

    }


    /**
     * 开奖 5个号码  每个号码 0-9
     * 中三/中三直选/复式
     *   游戏玩法：从千、百、十位各选一个号码组成一注。
     * 投注玩法：从千、百、十中选择一个3位数号码组成一注，所选号码与开奖号码全部相同，且顺序一致，即为中奖
     * @two_num  第二位好嘛 1,2,3）
     * @three_num  第三位好嘛 1,2,3）
     *  @four_num  第三位好嘛 （1,2,3）
     */
    public function z3zhixfs($param, $pre_draw_code)
    {
        //获取下注的参数
        $four_num = $param['four_num'];
        $two_num = $param['two_num'];
        $three_num = $param['three_num'];
        $four_num = $this->formatNum(explode(',', $four_num));
        $two_num = $this->formatNum(explode(',', $two_num));
        $three_num = $this->formatNum(explode(',', $three_num));
        if (count($four_num) == 0 || count($two_num) == 0 || count($three_num) == 0) {
            return [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '中三/中三直选/复式'
            ];
        }

        if (in_array($pre_draw_code[1], $two_num) && in_array($pre_draw_code[2], $three_num) && in_array($pre_draw_code[3], $four_num)) {
            $re = [
                'count'=>1,
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '中三/中三直选/复式'
            ];

        } else {
            $re = [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '中三/中三直选/复式'
            ];
        }
        return $re;
    }
    /**
     * 开奖 5个号码  每个号码 0-9
     * 中三/中三直选/单式   (玩法)
     * 投注玩法：手动输入一个3位数号码组成一注，所选号码的万、千、百与开奖号码相同，且顺序一致，即为中奖（每个号码之间用,隔开，每注之间用;隔开）
     * 投注方案：234**；开奖号码：234**
     * 投注玩法：手动输入一个3位数号码组成一注，所选号码的万、千、百位与开奖号码相同，且顺序一致，即为中奖
     * @data_num  下注号码 123 456
     */
    public function z3zhixds($param, $pre_draw_code)
    {
        //获取下注的参数
        $data_num = $param['data_num'];
        $data_num = explode(' ', $data_num);
        if (count($data_num) == 0) {
            return [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '中三/中三直选/单式'
            ];
        }

        if ($data_num) {
            foreach ($data_num as $k => $v) {
                $data_num[$k] = implode(',', $this->formatNum(str_split($v)));
            }
        }
        //中奖号码前三位
        $pre_draw_code = array_slice($pre_draw_code, 1, 3);

        $pre_draw_code = implode(',', $pre_draw_code);
        if (in_array($pre_draw_code, $data_num)) {
            $re = [
                'count'=>1,
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '中三/中三直选/单式'
            ];

        } else {
            $re = [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '中三/中三直选/单式'
            ];
        }
        return $re;
    }
    /**
     * 开奖 5个号码  每个号码 0-9
     * 中三/中三组选/组三
     * 游戏玩法：至少选择三个号码组成一注。
     * 投注方案：选择123（展开为*123*，*132*，*231*，*213*，*312*，*321*）,开奖为*321*，即为中奖
     * 投注玩法：中三/中三组选/组三：选3个或多个，所选号码与开奖万、千、百一致顺序不限，即为中奖
     * @data_num   1,2,3
     */
    public function z3zhuxz3($param, $pre_draw_code)
    {
        $data_num = $param['data_num'];
        $data_num = explode(',', $data_num);
        $data_num = $this->formatNum($data_num);
        $data_num = array_unique($data_num);

        //前三的号码
        $pre_draw_code = [$pre_draw_code[1], $pre_draw_code[2], $pre_draw_code[3]];
        //去重
        $pre_draw_code = array_unique($pre_draw_code);
        if (count($pre_draw_code) != 2) {
            return [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '中三/中三组选/组三'
            ];
        }

        if (array_intersect($pre_draw_code, $data_num) == $pre_draw_code) {
            $re = [
                'count'=>1,
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '中三/中三组选/组三'
            ];
        } else {
            $re = [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '中三/中三组选/组三'
            ];
        }
        return $re;
    }
    /**
     * 开奖 5个号码  每个号码 0-9
     * 中三/中三组选/组六
     * 游戏玩法：至少选择三个号码组成一注。
     *投注方案：选择12（展开为*122*，*212*，*221* 和 *112*、*121*、*211*）,开奖为*212* 或 *121*，即为中奖
     * 投注玩法：中三组三：选2个或多个，所选号码与开奖千、百、十位一致顺序不限，即为中奖
     * @data_num   1,2,3
     */
    public function z3zhuxz6($param, $pre_draw_code)
    {
        $data_num = $param['data_num'];
        $data_num = explode(',', $data_num);
        $data_num = $this->formatNum($data_num);
        $data_num = array_unique($data_num);
        //前三的号码
        $pre_draw_code = [$pre_draw_code[1], $pre_draw_code[2], $pre_draw_code[3]];
        //去重
        $pre_draw_code = array_unique($pre_draw_code);
        if (count($pre_draw_code) != 3) {
            return [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '中三/中三组选/组六'
            ];
        }
        if (array_intersect($pre_draw_code, $data_num) == $pre_draw_code) {
            $re = [
                'count'=>1,
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '中三/中三组选/组六'
            ];
        } else {
            $re = [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '中三/中三组选/组六'
            ];
        }
        return $re;
    }
    /**
     * 开奖 5个号码  每个号码 0-9
     * 后三/后三直选/复式
     * 游戏玩法：从百、十、个位各选一个号码组成一注。
     * 投注方案：**345；开奖号码：**345
     * 投注玩法：从百位、十位、个位中选择一个3位数号码组成一注，所选号码与开奖号码全部相同，且顺序一致，即为中奖
     * @three_num  第三位好嘛 （1,2,3）
     * * @four_num  第一位好嘛 （1,2,3
     * @five_num  第二位好嘛 1,2,3
     */
    public function h3zhixfs($param, $pre_draw_code)
    {
        //获取下注的参数

        $three_num = $param['three_num'];
        $four_num = $param['four_num'];
        $five_num = $param['five_num'];
        $four_num = $this->formatNum(explode(',', $four_num));
        $five_num = $this->formatNum(explode(',', $five_num));
        $three_num = $this->formatNum(explode(',', $three_num));
        if (count($five_num) == 0 || count($four_num) == 0 || count($three_num) == 0) {
            return [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '后三/后三直选/复式'
            ];
        }

        if (in_array($pre_draw_code[2], $three_num) && in_array($pre_draw_code[3], $four_num) && in_array($pre_draw_code[4], $five_num)) {
            $re = [
                'count'=>1,
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '后三/后三直选/复式'
            ];

        } else {
            $re = [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '后三/后三直选/复式'
            ];
        }
        return $re;
    }
    /**
     * 开奖 5个号码  每个号码 0-9
     * 后三/后三直选/单式   (玩法)
     * 投注玩法：手动输入一个3位数号码组成一注，所选号码的万、千、百与开奖号码相同，且顺序一致，即为中奖（每个号码之间用,隔开，每注之间用;隔开）
     * 投注方案：**345； 开奖号码：**345，即中后三星直选一等奖
     * 投注玩法：手动输入一个3位数号码组成一注，所选号码的百位、十位、个位与开奖号码相同，且顺序一致，即为中奖
     * @data_num  下注号码  123 345
     */
    public function h3zhixds($param, $pre_draw_code)
    {
        //获取下注的参数
        $data_num = $param['data_num'];
        $data_num = explode(' ', $data_num);
        if (count($data_num) == 0) {
            return [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '后三/后三直选/单式'
            ];
        }

        if ($data_num) {
            foreach ($data_num as $k => $v) {
                $data_num[$k] = implode(',', $this->formatNum(str_split($v)));
            }
        }
        //中奖号码前三位
        $pre_draw_code = array_slice($pre_draw_code, 2, 3);

        $pre_draw_code = implode(',', $pre_draw_code);
        if (in_array($pre_draw_code, $data_num)) {
            $re = [
                'count'=>1,
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '后三/后三直选/单式'
            ];

        } else {
            $re = [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '后三/后三直选/单式'
            ];
        }
        return $re;
    }
    /**
     * 开奖 5个号码  每个号码 0-9
     * 后三/后三组选/组三
     * 游戏玩法：至少选择三个号码组成一注。
     * 投注方案：选择12（展开为**122，**212，**221 和 **112、**121、**211）,开奖为**212 或 **121，即为中奖
     * 投注玩法：后三组三：选2个或多个，所选号码与开奖百、十、个位一致顺序不限，即为中奖
     * @data_num   1,2,3
     */
    public function h3zhuxz3($param, $pre_draw_code)
    {
        $data_num = $param['data_num'];
        $data_num = explode(',', $data_num);
        $data_num = $this->formatNum($data_num);
        $data_num = array_unique($data_num);

        //前三的号码
        $pre_draw_code = [$pre_draw_code[2], $pre_draw_code[3], $pre_draw_code[4]];
        //去重
        $pre_draw_code = array_unique($pre_draw_code);
        if (count($pre_draw_code) != 2) {
            return [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '后三/后三组选/组三'
            ];
        }

        if (array_intersect($pre_draw_code, $data_num) == $pre_draw_code) {
            $re = [
                'count'=>1,
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '后三/后三组选/组三'
            ];
        } else {
            $re = [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '后三/后三组选/组三'
            ];
        }
        return $re;
    }
    /**
     * 开奖 5个号码  每个号码 0-9
     * 后三/后三组选/组六
     * 游戏玩法：至少选择三个号码组成一注。
     * 投注方案：选择123（展开为**123，**132，**231，**213，**312，**321）,开奖为**321，即为中奖
     * 投注玩法：后三组六：选3个或多个，所选号码与开奖百、十、个位一致顺序不限，即为中奖
     * @data_num   1,2,3
     */
    public function h3zhuxz6($param, $pre_draw_code)
    {
        $data_num = $param['data_num'];
        $data_num = explode(',', $data_num);
        $data_num = $this->formatNum($data_num);
        $data_num = array_unique($data_num);
        //前三的号码
        $pre_draw_code = [$pre_draw_code[2], $pre_draw_code[3], $pre_draw_code[4]];
        //去重
        $pre_draw_code = array_unique($pre_draw_code);
        if (count($pre_draw_code) != 3) {
            return [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '后三/后三组选/组六'
            ];
        }
        if (array_intersect($pre_draw_code, $data_num) == $pre_draw_code) {
            $re = [
                'count'=>1,
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '后三/后三组选/组六'
            ];
        } else {
            $re = [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '后三/后三组选/组六'
            ];
        }
        return $re;
    }
    /**
     * 开奖 5个号码  每个号码 0-9
     * 二星/直选/前二复式 前二/直选/复式
     * 游戏玩法：从万、千位各选一个号码组成一注。
     * 投注方案：23***；开奖号码：23***
     * 投注玩法：从万位、千位中选择一个2位数号码组成一注，所选号码与开奖号码全部相同，且顺序一致，即为中奖
     * @one_num 1,2
     * @two_num 1,2
     */
    public function q2zhixfs($param, $pre_draw_code)
    {
        $one_num = explode(',', $param['one_num']);
        $two_num = explode(',', $param['two_num']);
        //获取前二
        if (in_array($pre_draw_code[0], $one_num) && in_array($pre_draw_code[1], $two_num)) {

            $re = [
                'count'=>1,
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '二星/直选/前二复式'
            ];

        } else {
            $re = [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '二星/直选/前二复式'
            ];
        }
        return $re;
    }
    /**
     * 开奖 5个号码  每个号码 0-9
     * 二星/直选/前二单式
     * 游戏玩法：从万、千位各选一个号码组成一注。
     * 投注玩法：手动输入一个2位数号码组成一注，所选号码的万位、千位与开奖号码相同，且顺序一致，即为中奖（每个号码之间用,隔开，每注之间用;隔开）
     * 投注方案：23***； 开奖号码：23***，即中前二星直选一等奖
     * @data_num 01 12
     */
    public function q2zhixds($param, $pre_draw_code)
    {
        //获取前二
        $pre_draw_code = [(int)$pre_draw_code[0], (int)$pre_draw_code[1]];
        $pre_draw_code = implode('', $pre_draw_code);
        $data_num = explode(' ', $param['data_num']);
        if (count($data_num) == 0) {
            $re = [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '二星/直选/前二单式'
            ];
            return $re;
        }
        if (in_array($pre_draw_code, $data_num)) {
            $re = [
                'count'=>1,
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '二星/直选/前二单式'
            ];
        } else {
            $re =[
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '二星/直选/前二单式'
            ];
        }

        return $re;
    }
    /**
     * 开奖 5个号码  每个号码 0-9
     * 二星/直选/后二复式
     * 游戏玩法：从十、个位各选一个号码组成一注
     * 投注方案：***23；开奖号码：***23
     * 投注玩法：从十位、个���中选择一个2位数号码组成一注，所选号码与开奖号码全部相同，且顺序一致，即为中奖
     * @four_num 1,2,3 (十位)
     *  @five_num 1,2,3 (个位)
     */
    public function h2zhixfs($param, $pre_draw_code)
    {
        $four_num = explode(',', $param['four_num']);
        $five_num = explode(',', $param['five_num']);
        //获取后二
        if (in_array($pre_draw_code[3], $four_num) && in_array($pre_draw_code[4], $five_num)) {
            $re = [
                'count'=>1,
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '二星/直选/后二复式'
            ];

        } else {
            $re = [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '二星/直选/后二复式'
            ];
        }
        return $re;
    }
    /**
     * 开奖 5个号码  每个号码 0-9
     * 二星/直选/后二单式  后二/直选/复式
     * 游戏玩法：从万、千位各选一个号码组成一注。
     * 投注玩法：手动输入一个2位数号码组成一注，所选号码的十位、个位与开奖号码相同，且顺序一致，即为中奖（每个号码之间用,隔开，每注之间用;隔开）
     * 投注方案：***23； 开奖号码：***23，即中后二星直选一等奖
     * @data_num 0,1;1,2
     */
    public function h2zhixds($param, $pre_draw_code)
    {
        //获取前二
        $pre_draw_code = [$pre_draw_code[0], $pre_draw_code[1]];
        $pre_draw_code = implode(',', $pre_draw_code);
        $data_num = explode(';', $param['data_num']);
        if (count($data_num) == 0) {
            $re = [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '二星/直选/后二单式'
            ];
            return $re;
        }

        foreach ($data_num as $k => $v) {
            $data_num[$k] = implode(',', $this->formatNum(explode(',', $v)));
        }
        if (in_array($pre_draw_code, $data_num)) {
            $re = [
                'count'=>1,
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '二星/直选/后二单式'
            ];
        } else {
            $re = [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '二星/直选/后二单式'
            ];
        }

        return $re;
    }

    /**
     * 开奖 5个号码  每个号码 0-9
     * 二星/组选/前二复式  前二/组选/复试
     * 游戏玩法：至少选择两个号码组成一注。
     * 投注方案：选择12（展开为12***，21***）,开奖为12*** 或 21***，即为中奖
     * 投注玩法：前二组选复式：从0-9中选2个或多个号码对万位和千位投注，顺序不限，即为中奖
     * @data_num 1,2,3
     */
    public function q2zhuxfs($param, $pre_draw_code)
    {
        //获取前二
        $pre_draw_code = [$pre_draw_code[0], $pre_draw_code[1]];
        if (count(array_unique($pre_draw_code)) != 2) {
            $re = [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '二星/组选/前二复式'
            ];
            return $re;
        }
        $data_num = explode(',', $param['data_num']);
        $data_num = $this->formatNum($data_num);
        $data_num = array_unique($data_num);


        if (in_array($pre_draw_code[0], $data_num) && in_array($pre_draw_code[1], $data_num)) {

            $re = [
                'count'=>1,
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '二星/组选/前二复式'
            ];

        } else {
            $re = [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '二星/组选/前二复式'
            ];
        }
        return $re;


    }
    /**
     * 开奖 5个号码  每个号码 0-9
     * 二星/组选/前二单式 前二/组选/单试
     * 投注玩法：前二组选单式：键盘手动输入购买号码，2个数字为一注，与开奖号码的万位、千位符合且顺序不限，即为中奖（每个号码之间用,隔开；每注之间用;隔开）
     * 投注方案：手动输入12，开奖号码为12*** 或 21*** 即为中奖
     * @data_num 12 23
     */
    public function q2zhuxds($param, $pre_draw_code)
    {
        //获取前二
        $pre_draw_code = [$pre_draw_code[0], $pre_draw_code[1]];
        sort($pre_draw_code);
        $pre_draw_code = implode(',', $pre_draw_code);
        $data_num = explode(';', $param['data_num']);
        if (count($data_num) == 0) {
            $re = [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '二星/组选/前二单式'
            ];
            return $re;
        }
        foreach ($data_num as $k => $v) {
            $data = $this->formatNum(explode(',', $v));
            sort($data);
            $data_num[$k] = implode(',', $data);
        }
        if (in_array($pre_draw_code, $data_num)) {
            $re = [
                'count'=>1,
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '二星/组选/前二单式'
            ];
        } else {
            $re = [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '二星/组选/前二单式'
            ];
        }
        return $re;
    }
    /**
     * 开奖 5个号码  每个号码 0-9
     * 二星/组选/后二复式  后二/组选/复试
     * 游戏玩法：至少选择两个号码组成一注。
     * 投注方案：选择12（展开为***12，***21）,开奖为***12 或 ***21，即为中奖
     * 投注玩法：后二组选复式：从0-9中选2个或多个号码对十位和个位投注，顺序不限，即为中奖
     * @data_num 1,2,3
     */
    public function h2zhuxfs($param, $pre_draw_code)
    {
        //获取前二
        $pre_draw_code = [$pre_draw_code[3], $pre_draw_code[4]];
        if (count(array_unique($pre_draw_code)) != 2) {
            $re = [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '二星/组选/后二复式'
            ];
            return $re;
        }
        $data_num = explode(',', $param['data_num']);
        $data_num = $this->formatNum($data_num);
        $data_num = array_unique($data_num);


        if (in_array($pre_draw_code[0], $data_num) && in_array($pre_draw_code[1], $data_num)) {

            $re = [
                'count'=>1,
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '二星/组选/后二复式'
            ];

        } else {
            $re = [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '二星/组选/后二复式'
            ];
        }
        return $re;


    }
    /**
     * 开奖 5个号码  每个号码 0-9
     * 二星/组选/后二单式  后二/组选/单试
     * 投注玩法：后二组选单式：键盘手动输入购买号码，2个数字为一注，与开奖号码的十位、个位符合且顺序不限，即为中奖（每个号码之间用,隔开；每注之间用;隔开）
     * 投注方案：手动输入12，开奖号码为***12 或 ***21 即为中奖
     * @data_num 12 23
     */
    public function h2zhuxds($param, $pre_draw_code)
    {
        //获取后二
        $pre_draw_code = [$pre_draw_code[3], $pre_draw_code[4]];
        sort($pre_draw_code);
        $pre_draw_code = implode(',', $pre_draw_code);
        $data_num = explode(';', $param['data_num']);
        if (count($data_num) == 0) {
            $re = [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '二星/组选/后二单式'
            ];
            return $re;
        }
        foreach ($data_num as $k => $v) {
            $data = $this->formatNum(explode(',', $v));
            sort($data);
            $data_num[$k] = implode(',', $data);
        }
        if (in_array($pre_draw_code, $data_num)) {
            $re = [
                'count'=>1,
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '二星/组选/后二单式'
            ];
        } else {
            $re = [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '二星/组选/后二单式'
            ];
        }
        return $re;
    }
    /**
     * 开奖 5个号码  每个号码 0-9
     * 定位胆/定位胆/定位胆
     * 投注玩法：从万位、千位、百位、十位、个位任意位置上选择1个或1个以上号码，所选号码与相同位置上的开奖号码一致，即为中奖
     * 投注方案：定万位为1，开奖号码为1****即为中奖
     * @one_num 1,2,3 (万位)
     * @two_num 1,2,3 (千位)
     * @three_num 1,2,3 (百位)
     * @four_num 1,2,3 (十位)
     * @five_num 1,2,3 (个位)
     *
     */
    public function dwd($param, $pre_draw_code)
    {
        $one_num = isset($param['one_num'])?$param['one_num']:'';
        $two_num = isset($param['two_num'])?$param['two_num']:'';
        $three_num = isset($param['three_num'])?$param['three_num']:'';
        $four_num = isset($param['four_num'])?$param['four_num']:'';
        $five_num = isset($param['five_num'])?$param['five_num']:'';
        $one_num = $this->formatNum(explode(',', $one_num));
        $two_num = $this->formatNum(explode(',', $two_num));
        $three_num = $this->formatNum(explode(',', $three_num));
        $four_num = $this->formatNum(explode(',', $four_num));
        $five_num = $this->formatNum(explode(',', $five_num));
        $count = 0;
        if(in_array($pre_draw_code[0], $one_num)){
            $count++;
        }
        if(in_array($pre_draw_code[1], $two_num)){
            $count++;
        }
        if(in_array($pre_draw_code[2], $three_num)){
            $count++;
        }
        if(in_array($pre_draw_code[3], $four_num)){
            $count++;
        }
        if(in_array($pre_draw_code[4], $five_num)){
            $count++;
        }
        if($count > 0){
            $re = [
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '定位胆/定位胆/定位胆',
                'count' => $count
            ];
        }else{
            $re = [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '定位胆/定位胆/定位胆'
            ];
        }
        return $re;
    }
    /**
     * 不定胆/前四不定胆/一码不定位
     * 从0-9中任意选择1个以上号码。
     * 投注方案：1；
     * 开奖号码前四位：至少出现1个1，即中前四星一码不定位。
     * 从0-9中选择1个号码，每注由1个号码组成，只要开奖号码的万位、千位、百位、十位中包含所选号码，即为中奖。
     *
     */
    public function bddq4ym($param, $pre_draw_code)
    {
        $data_num = $param['data_num'];
        $data_num = $this->formatNum(explode(',', $data_num));
        $data_num = array_unique($data_num);
        $pre_draw_code = array_slice($pre_draw_code, 0, 4);
        $count = 0;
        if ($data_num) {
            foreach ($data_num as $k => $v) {
                if (in_array($v, $pre_draw_code)) {
                    $count++;
                }
            }
        }
        if ($count >= 1) {
            $re = [
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '不定胆/前四不定胆/一码不定位',
                'count'=>$count
            ];
        } else {
            $re = [
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '不定胆/前四不定胆/一码不定位'
            ];
        }
        return $re;
    }
    /**
     * 不定胆/前四不定胆/二码不定位
     * 0-9中任意选择2个以上号码。
     * 投注方案：1,2；
     * 开奖号码前四位：至少出现1和2各1个，即中前四星二码不定位。
     * 从0-9中选择2个号码，每注由2个不同的号码组成，开奖号码的万位、千位、百位、十位中同时包含所选的2个号码，即为中奖。
     * @data_num1，2，3
     */
    public function bddq4em($param, $pre_draw_code)
    {
        $data_num = $param['data_num'];
        $data_num = $this->formatNum(explode(',', $data_num));
        $data_num = array_unique($data_num);
        $pre_draw_code = array_slice($pre_draw_code, 0, 4);
        $pre_draw_code = array_unique($pre_draw_code);
        $count = 0;
        if ($data_num) {
            foreach ($data_num as $k => $v) {
                if (in_array($v, $pre_draw_code)) {
                    $count++;
                }
            }
        }
        if ($count >= 2) {
            $re = [
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '不定胆/前四不定胆/二码不定位'
            ];
            $input = [];
            for ($i=1;$i<=$count;$i++){
                $input[] = $i;

            }
            $c= comb($input,2);
            $re['count']=count($c);

        } else {
            $re = [
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '不定胆/前四不定胆/二码不定位'
            ];
        }
        return $re;
    }
    /**
     * 不定胆/后四不定胆/一码不定位
     * 从0-9中任意选择1个以上号码。
     * 投注方案：1；
     * 开奖号码后四位：至少出现1个1，即中后四星一码不定位。
     * 从0-9中选择1个号码，每注由1个号码组成，只要开奖号码的千位、百位、十位、个位中包含所选号码，即为中奖。
     * @data_num 1,2,3
     */
    public function bddh4ym($param, $pre_draw_code)
    {

        $data_num = $param['data_num'];
        $data_num = $this->formatNum(explode(',', $data_num));
        $data_num = array_unique($data_num);

        $pre_draw_code = array_slice($pre_draw_code, 1, 4);
        $count = 0;
        if ($data_num) {
            foreach ($data_num as $k => $v) {
                if (in_array($v, $pre_draw_code)) {
                    $count++;
                }


            }
        }
        if ($count >= 1) {

            $re = [
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '不定胆/后四不定胆/一码不定位',
                'count'=>$count
            ];


        } else {
            $re = [
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '不定胆/后四不定胆/一码不定位'
            ];
        }
        return $re;
    }
    /**
     * 不定胆/后四不定胆/二码不定位
     * 0-9中任意选择2个以上号码。
     * 投注方案：1,2；
     * 开奖号码前四位：至少出现1和2各1个，即中前四星二码不定位。
     * 从0-9中选择2个号码，每注由2个不同的号码组成，开奖号码的万位、千位、百位、十位中同时包含所选的2个号码，即为中奖。
     * @data_num1，2，3
     */
    public function bddh4em($param, $pre_draw_code)
    {

        $data_num = $param['data_num'];
        $data_num = $this->formatNum(explode(',', $data_num));
        $data_num = array_unique($data_num);

        $pre_draw_code = array_slice($pre_draw_code, 1, 4);
        $pre_draw_code = array_unique($pre_draw_code);
        $count = 0;
        if ($data_num) {
            foreach ($data_num as $k => $v) {
                if (in_array($v, $pre_draw_code)) {
                    $count++;
                }


            }
        }
        if ($count >= 2) {

            $re = [
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '不定胆/后四不定胆/二码不定位'
            ];
            $input = [];
            for ($i=1;$i<=$count;$i++){
                $input[] = $i;

            }
            $c= comb($input,2);
            $re['count']=count($c);

        } else {
            $re = [
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '不定胆/后四不定胆/二码不定位'
            ];
        }
        return $re;
    }


    /**
     * 开奖 5个号码  每个号码 0-9
     * 不定位/不定位/前三一码
     * 游戏玩法：至少选择一个号码组成一注。
     * 投注方案：选择不定位1，开出1****、*1***、**1**即为中奖
     * 投注玩法：从0-9中选择1个号码，每注由1个号码组成，只要开奖号码的万位、千位、百位中包含所选号码，即为中奖
     * @data_num 1,2,3
     */
    public function bddq3ym($param, $pre_draw_code){
        $data_num = $param['data_num'];
        $data_num = explode(',', $data_num);
        $data_num = $this->formatNum($data_num);
        $data_num = array_unique($data_num);


        //前三的号码
        $pre_draw_code = [$pre_draw_code[0], $pre_draw_code[1], $pre_draw_code[2]];
        $jj = array_intersect($data_num,$pre_draw_code);
        if(count($jj)>0){
            $re = [
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '不定位/不定位/前三一码',
                'count'=>count($jj)
            ];
        }else{
            $re = [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '不定位/不定位/前三一码'
            ];
        }
        return $re;
    }
    /**
     * 开奖 5个号码  每个号码 0-9
     * 不定位/不定位/中三一码
     * 游戏玩法：至少选择一个号码组成一注。
     * 投注方案：选择不定位1，开出*1***、**1**、***1*即为中奖
     * 投注玩法：从0-9中选择1个号码，每注由1个号码组成，只要开奖号码的千位、百位、十位中包含所选号码，即为中奖
     * @data_num 1,2,3
     */
    public function bddz3ym($param, $pre_draw_code){
        $data_num = $param['data_num'];
        $data_num = explode(',', $data_num);
        $data_num = $this->formatNum($data_num);
        $data_num = array_unique($data_num);
        //中三的号码
        $pre_draw_code = [$pre_draw_code[1], $pre_draw_code[2], $pre_draw_code[3]];
        $jj = array_intersect($data_num,$pre_draw_code);
        if(count($jj)>0){
            $re = [
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '不定位/不定位/中三一码',
                'count'=>count($jj)
            ];
        }else{
            $re =[
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '不定位/不定位/中三一码'
            ];
        }
        return $re;
    }
    /**
     * 开奖 5个号码  每个号码 0-9
     * 不定位/不定位/后三一码
     * 游戏玩法：至少选择一个号码组成一注。
     * 投注方案：选择不定位1，开出****1、***1*、**1**即为中奖
     * 投注玩法：从0-9中选择1个号码，每注由1个号码组成，只要开奖号码的百位、十位、个位中包含所选号码，即为中奖
     * @data_num 1,2,3
     */
    public function bddh3ym($param, $pre_draw_code){
        $data_num = $param['data_num'];
        $data_num = explode(',', $data_num);
        $data_num = $this->formatNum($data_num);
        $data_num = array_unique($data_num);


        $pre_draw_code = [$pre_draw_code[2], $pre_draw_code[3], $pre_draw_code[4]];
        $jj = array_intersect($data_num,$pre_draw_code);
        if(count($jj)>0){
            $re = [
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '不定位/不定位/后三一码',
                'count'=>count($jj)
            ];
        }else{
            $re = [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '不定位/不定位/后三一码'
            ];
        }
        return $re;
    }
    /**
     * 开奖 5个号码  每个号码 0-9
     * 不定胆/三星不定胆二码/前三
     * 从0-9中任意选择1个以上号码。
     *投注方案：1,2；
     * 开奖号码后三位：至少出现1和2各1个，即中前三二码不定位。
     * 从0-9中选择2个号码，每注由2个不同的号码组成，开奖号码的百位、十位、个位中同时包含所选的2个号码，即为中奖。
     * @data_num 1,2,3
     * @data_num 1,2,3
     *
     *
     */
    public function bdd_sx_qs($param, $pre_draw_code){
        $data_num = $param['data_num'];
        $data_num = $this->formatNum(explode(',', $data_num));
        $data_num = array_unique($data_num);
        $pre_draw_code = array_slice($pre_draw_code, 0, 3);
//        $pre_draw_code = array_unique($pre_draw_code);
        if(count(array_unique($pre_draw_code)) == 1){
            return [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '不定胆/三星不定胆二码 /后三'
            ];
        }
        $count = 0;
        if ($data_num) {
            foreach ($data_num as $k => $v) {
                if (in_array($v, $pre_draw_code)) {
                    $count++;
                }
            }
        }
        if ($count >= 2) {
            $re = [

                'status' => 2,
                'remark' => '中奖',
                'play_name' => '不定胆/三星不定胆二码 /后三'
            ];
            $input = [];
            for ($i=1;$i<=$count;$i++){
                $input[] = $i;
            }
            $c= comb($input,2);
            $re['count']=count($c);
        } else {
            $re = [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '不定胆/三星不定胆二码 /后三'
            ];
        }
        return $re;
    }
    /**
     * 开奖 5个号码  每个号码 0-9
     * 不定胆/三星不定胆二码 /后三
     * 从0-9中任意选择1个以上号码。
     *投注方案：1,2；
     * 开奖号码后三位：至少出现1和2各1个，即中后三二码不定位。
     * 从0-9中选择2个号码，每注由2个不同的号码组成，开奖号码的百位、十位、个位中同时包含所选的2个号码，即为中奖。
     * @data_num 1,2,3
     *
     *
     */
    public function bdd_sx_hs($param, $pre_draw_code){
        $data_num = $param['data_num'];
        $data_num = $this->formatNum(explode(',', $data_num));
        $data_num = array_unique($data_num);
        $pre_draw_code = array_slice($pre_draw_code, 2, 3);
        if(count(array_unique($pre_draw_code)) == 1){
            return [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '不定胆/三星不定胆二码 /后三'
            ];
        }
        $count = 0;
        if ($data_num) {
            foreach ($data_num as $k => $v) {
                if (in_array($v, $pre_draw_code)) {
                    $count++;
                }
            }
        }
        if ($count >= 2) {
            $re = [
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '不定胆/三星不定胆二码 /后三'
            ];
            $input = [];
            for ($i=1;$i<=$count;$i++){
                $input[] = $i;
            }
            $c= comb($input,2);
            $re['count']=count($c);
        } else {
            $re = [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '不定胆/三星不定胆二码 /后三'
            ];
        }
        return $re;
    }
    /**
     * 开奖 5个号码  每个号码 0-9
     * 不定位/不定位/五星一码
     * 游戏玩法：至少选择一个号码组成一注。
     * 投注方案：例如投注3，开奖号34535，中一注。（出现一次和出现两次都只算中一注）
     * 投注玩法：每注号码由一个数字组成，只要开奖号码任意一位出现了投注的号码，即为中奖
     * @data_num 1,2,3
     */
    public function bdw5x1m($param, $pre_draw_code){
        $data_num = $param['data_num'];
        $data_num = explode(',', $data_num);
        $data_num = $this->formatNum($data_num);
        $data_num = array_unique($data_num);


        $jj = array_intersect($data_num,$pre_draw_code);
        if(count($jj)>0){
            $re = [
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '不定位/不定位/五星一码',
                'count'=>count($jj)
            ];
        }else{
            $re = [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '不定位/不定位/五星一码'
            ];
        }
        return $re;
    }
    /**
     * 开奖 5个号码  每个号码 0-9
     * 不定位/不定位/五星二码
     * 游戏玩法：至少选择两个号码组成一注。
     * 投注方案：例如投注36，开奖号码32468，中一注。
     * 投注玩法：每注号码由两个数字组成，只要开奖号码包含投注号码即为中奖
     * @data_num 1,2,3
     */
    public function bdw5x2m($param, $pre_draw_code){
        $data_num = $param['data_num'];
        $data_num = explode(',', $data_num);
        $data_num = $this->formatNum($data_num);
        $data_num = array_unique($data_num);


        //交集个数大于2则中奖
        $jj = array_intersect($data_num,$pre_draw_code);
        if(count($jj)>=2){
            $re = [
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '不定位/不定位/五星二码'
            ];
            $input = [];
            for ($i=1;$i<=count($jj);$i++){
                $input[] = $i;

            }
            $c= comb($input,2);
            $re['count']=count($c);
        }else{
            $re = [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '不定位/不定位/五星二码'
            ];
        }
        return $re;
    }
    /**
     * 开奖 5个号码  每个号码 0-9
     * 不定位/不定位/五星三码
     * 游戏玩法：至少选择三个号码组成一注。
     * 投注方案：例如投注368，开奖号码32468，中一注。
     * 投注玩法：每注号码由三个数字组成，只要开奖号码中包含投注号码即为中奖
     * @data_num 1,2,3
     */
    public function bdw5x3m($param, $pre_draw_code){
        $data_num = $param['data_num'];
        $data_num = explode(',', $data_num);
        $data_num = $this->formatNum($data_num);
        $data_num = array_unique($data_num);


        //交集个数大于3则中奖
        $jj = array_intersect($data_num,$pre_draw_code);
        if(count($jj)>=3){
            $re = [
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '不定位/不定位/五星三码'
            ];
            $input = [];
            for ($i=1;$i<=count($jj);$i++){
                $input[] = $i;

            }
            $c= comb($input,3);
            $re['count']=count($c);
        }else{
            $re = [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '不定位/不定位/五星三码'
            ];
        }
        return $re;
    }
    /**
     * 开奖 5个号码  每个号码 0-9
     * 跨度/跨度/前三跨度
     * 游戏玩法：至少选择一个号码组成一注
     * 投注方案：选择5, 等于开奖号前三位2,5,7的最大数7与最小数字2的差值，即为中奖
     * 投注玩法：选择0-9，若所选号码与开奖号前三位的最大最小数字的差值相等，即为中奖
     */
    public function kdq3($param,$pre_draw_code){
        $data_num = $param['data_num'];
        $data_num = explode(',', $data_num);
        $data_num = $this->formatNum($data_num);
        $data_num = array_unique($data_num);


        //获取前三
        $pre_draw_code = [$pre_draw_code[0], $pre_draw_code[1], $pre_draw_code[2]];
        //跨度
        $kd = max($pre_draw_code) - min($pre_draw_code);
        $kd = (int)$kd;
        if (in_array($kd, $data_num)) {
            $re = [
                'count'=>1,
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '跨度/跨度/前三跨度'
            ];
        } else {
            $re = [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '跨度/跨度/前三跨度'
            ];
        }
        return $re;
    }
    /**
     * 开奖 5个号码  每个号码 0-9
     * 跨度/跨度/中三跨度
     * 游戏玩法：至少选择一个号码组成一注。
     * 投注方案：选择5, 等于开奖号中三位2,5,7的最大数7与最小数字2的差值，即为中奖
     * 投注玩法：选择0-9，若所选号码与开奖号中三位的最大最小数字的差值相等，即为中奖
     * @data_num 1,2,3
     */
    public function kdz3($param,$pre_draw_code){
        $data_num = $param['data_num'];
        $data_num = explode(',', $data_num);
        $data_num = $this->formatNum($data_num);
        $data_num = array_unique($data_num);
        //获取中三
        $pre_draw_code = [$pre_draw_code[1], $pre_draw_code[2], $pre_draw_code[3]];
        //跨度
        $kd = max($pre_draw_code) - min($pre_draw_code);
        $kd = (int)$kd;
        if (in_array($kd, $data_num)) {
            $re = [
                'count'=>1,
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '跨度/跨度/中三跨度'
            ];
        } else {
            $re = [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '跨度/跨度/中三跨度'
            ];
        }
        return $re;
    }
    /**
     * 开奖 5个号码  每个号码 0-9
     * 跨度/跨度/后三跨度
     * 游戏玩法：至少选择一个号码组成一注。
     * 投注方案：选择5, 等于开奖号后三位2,5,7的最大数7与最小数字2的差值，即为中奖
     * 投注玩法：选择0-9，若所选号码与开奖号后三位的最大最小数字的差值相等，即为中奖
     * @data_num 1,2,3
     */
    public function kdh3($param,$pre_draw_code){
        $data_num = $param['data_num'];
        $data_num = explode(',', $data_num);
        $data_num = $this->formatNum($data_num);
        $data_num = array_unique($data_num);
        //获取后三
        $pre_draw_code = [$pre_draw_code[2], $pre_draw_code[3], $pre_draw_code[4]];
        //跨度
        $kd = max($pre_draw_code) - min($pre_draw_code);
        $kd = (int)$kd;
        if (in_array($kd, $data_num)) {
            $re = [
                'count'=>1,
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '跨度/跨度/后三跨度'
            ];
        } else {
            $re = [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '跨度/跨度/后三跨度'
            ];
        }
        return $re;
    }
    /**
     * 开奖 5个号码  每个号码 0-9
     * 跨度/跨度/前二跨度 前二/直选/跨度
     * 游戏玩法：至少选择一个号码组成一注。
     * 投注方案：选择5, 等于开奖号前二位2,7的最大数7与最小数字2的差值，即为中奖
     * 投注玩法：选择0-9，若所选号码与开奖号前二位的最大最小数字的差值相等，即为中奖
     * @data_num 1,2,3
     */
    public function kdq2($param,$pre_draw_code){
        $data_num = $param['data_num'];
        $data_num = explode(',', $data_num);
        $data_num = $this->formatNum($data_num);
        $data_num = array_unique($data_num);


        //获取前二
        $pre_draw_code = [$pre_draw_code[0], $pre_draw_code[1]];
        //跨度
        $kd = max($pre_draw_code) - min($pre_draw_code);
        $kd = (int)$kd;
        if (in_array($kd, $data_num)) {
            $re = [
                'count'=>1,
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '跨度/跨度/前二跨度'
            ];
        } else {
            $re = [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '跨度/跨度/前二跨度'
            ];
        }
        return $re;
    }
    /**
     * 开奖 5个号码  每个号码 0-9
     * 跨度/跨度/后二跨度 后二/直选/跨度
     * 游戏玩法：至少选择一个号码组成一注。
     * 投注方案：选择5, 等于开奖号后二位2,7的最大数7与最小数字2的差值，即为中奖
     * 投注玩法：选择0-9，若所选号码与开奖号后二位的最大最小数字的差值相等，即为中奖
     * @data_num 1,2,3
     */
    public function kdh2($param,$pre_draw_code){
        $data_num = $param['data_num'];
        $data_num = explode(',', $data_num);
        $data_num = $this->formatNum($data_num);
        $data_num = array_unique($data_num);
        //获取后二
        $pre_draw_code = [$pre_draw_code[3], $pre_draw_code[4]];
        //跨度
        $kd = max($pre_draw_code) - min($pre_draw_code);
        $kd = (int)$kd;
        if (in_array($kd, $data_num)) {
            $re = [
                'count'=>1,
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '跨度/跨度/后二跨度'
            ];
        } else {
            $re = [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '跨度/跨度/后二跨度'
            ];
        }
        return $re;
    }
    /**
     * 开奖 5个号码  每个号码 0-9
     * 任选/任四/复式
     * 游戏玩法：从万、千、百、十、个任四位选一个号码组
     * 投注方案：2*456；开奖号码：2*456
     * 投注玩法：任选四位与开奖号分别对应相同且顺序一致即为中奖
     * @one_num 1,2,3 (万位)
     * @two_num 1,2,3 (千位)
     * @three_num 1,2,3 (百位)
     * @four_num 1,2,3 (十位)
     * @five_num 1,2,3 (个位)
     */
    public function r4fs($param, $pre_draw_code)
    {
        $one_num = isset($param['one_num']) ? $param['one_num'] : '';
        $two_num = isset($param['two_num']) ? $param['two_num'] : '';
        $three_num = isset($param['three_num']) ? $param['three_num'] : '';
        $four_num = isset($param['four_num']) ? $param['four_num'] : '';
        $five_num = isset($param['five_num']) ? $param['five_num'] : '';
        $one_num = $this->formatNum(explode(',', $one_num));
        $two_num = $this->formatNum(explode(',', $two_num));
        $three_num = $this->formatNum(explode(',', $three_num));
        $four_num = $this->formatNum(explode(',', $four_num));
        $five_num = $this->formatNum(explode(',', $five_num));
        $count = 0;

        if (in_array($pre_draw_code[0], $one_num)) {
            $count++;
        }
        if (in_array($pre_draw_code[1], $two_num)) {
            $count++;
        }
        if (in_array($pre_draw_code[2], $three_num)) {
            $count++;
        }
        if (in_array($pre_draw_code[3], $four_num)) {
            $count++;
        }
        if (in_array($pre_draw_code[4], $five_num)) {
            $count++;
        }
        if ($count == 4) {
            $re = [
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '任选/任四/复试',
                'count'=>1
            ];
        }elseif ($count == 5){
            $re = [
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '任选/任四/复试',
                'count'=>5
            ];
        } else {
            $re = [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '任选/任四/复试'
            ];
        }
        return $re;
    }
    /**
     * 任选/任四/单式
     *玩法示意： 手动输入号码，至少输入1个四位数号码和至少选择四个位置(每个号码之间用,隔开，每注号码之间用;隔开)
     *输入号码0123选择万、千、百、十位置，如开奖号码位0123*； 则中奖
     *手动输入一注或者多注的四个号码和至少四个位置，如果选中的号码与位置和开奖号码对应则中奖
     * @data_address 万,千,百,十,个
     * @data_num 1234 3456
     */
    public function r4ds($param, $pre_draw_code)
    {
        $data_address = explode(',', $param['data_address']);
        $data_address = array_unique($data_address);
        $address_all = ['万', '千', '百', '十', '个'];
        if ($data_address) {
            foreach ($data_address as $k => $v) {
                if (!in_array($v, $address_all)) {
                    unset($data_address[$k]);
                }
            }
        }
        $data_address = array_unique($data_address);
        if (count($data_address) < 4) {
            return [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '任选/任四/单式'
            ];
        };
        //位置组合
        $data_num_zh = getArrSet([$data_address, $data_address, $data_address, $data_address]);
        foreach ($data_num_zh as $k => $v) {
            // sort($v);
            if (count(array_unique($v)) != 4) {
                unset($data_num_zh[$k]);
            } else {
                $data_num_zh[$k] =$this->address_sort($v);
            }
        }
        // $data_num_zh = array_unique($data_num_zh);
        $data_num_zh = array_values($data_num_zh);
        $re_data_num_zh = [];
        if($data_num_zh){
            foreach ($data_num_zh as $k=>$v){
                if($k>=1){
                    if(count(array_intersect($v,$data_num_zh[$k-1]))!=4){
                        $re_data_num_zh[] = implode(',',$v);
                    }
                }else{
                    $re_data_num_zh[] = implode(',',$v);
                }
            }
        }
        $zh = [];
        foreach ($re_data_num_zh as $k=>$v){
            $code = explode(',',$v);
            $zh[] = (int)$this->address($code[0],$pre_draw_code).''. (int)$this->address($code[1],$pre_draw_code).''. (int)$this->address($code[2],$pre_draw_code).''.(int)$this->address($code[3],$pre_draw_code);
        }
        $data_num = $param['data_num'];
        $data_num =
        $data_num = explode(' ',$data_num);
        // dump($data_num);
        $data_num = array_unique($data_num);
        $count = count(array_intersect($data_num,$zh));
        if($count>0){
            $re = [
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '任选/任四/单式',
                'count'=>$count
            ];
        }else{
            $re = [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '任选/任四/单式'
            ];
        }
        return $re;
    }
    /**
     * 开奖 5个号码  每个号码 0-9
     * 任选/任三/复式
     * 游戏玩法：从万、千、百、十、个任三位选一个号码组
     * 投注方案：2*4*6；开奖号码：2*4*6
     * 投注玩法：任选三位与开奖号分别对应相同且顺序一致即为中奖
     * @one_num 1,2,3 (万位)
     * @two_num 1,2,3 (千位)
     * @three_num 1,2,3 (百位)
     * @four_num 1,2,3 (十位)
     * @five_num 1,2,3 (个位)
     */
    public function r3fs($param, $pre_draw_code)
    {
        $one_num = isset($param['one_num']) ? $param['one_num'] : '';
        $two_num = isset($param['two_num']) ? $param['two_num'] : '';
        $three_num = isset($param['three_num']) ? $param['three_num'] : '';
        $four_num = isset($param['four_num']) ? $param['four_num'] : '';
        $five_num = isset($param['five_num']) ? $param['five_num'] : '';
        $one_num = $this->formatNum(explode(',', $one_num));
        $two_num = $this->formatNum(explode(',', $two_num));
        $three_num = $this->formatNum(explode(',', $three_num));
        $four_num = $this->formatNum(explode(',', $four_num));
        $five_num = $this->formatNum(explode(',', $five_num));
        $count = 0;
        if (in_array($pre_draw_code[0], $one_num)) {
            $count++;
        }
        if (in_array($pre_draw_code[1], $two_num)) {
            $count++;
        }
        if (in_array($pre_draw_code[2], $three_num)) {
            $count++;
        }
        if (in_array($pre_draw_code[3], $four_num)) {
            $count++;
        }
        if (in_array($pre_draw_code[4], $five_num)) {
            $count++;
        }
        if ($count >= 3) {
            $input = [];
            for ($i=1;$i<=$count;$i++){
                $input[] = $i;
            }
            $c= comb($input,3);
            $re = [
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '任选/任三/复试',
                'count'=>count($c)
            ];
        } else {
            $re = [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '任选/任三/复试'
            ];
        }
        return $re;
    }
    /**
     * 任选/任三/单试
     * 手动输入号码，至少输入1个两位数号码和至少选择两个位置
     * 输入号码01并选择万、千位置位，如开奖号码位01***； 则中奖
     *  手动输入一注或者多注的两个号码和至少两个位置，如果选中的号码与位置和开奖号码对应则中奖
     * @data_num 1,2;3,4
     * @data_address 万，千
     *
     */
    public function r3ds($param, $pre_draw_code)
    {



        $data_address = explode(',', $param['data_address']);
        $data_address = array_unique($data_address);
        $address_all = ['万', '千', '百', '十', '个'];
        if ($data_address) {
            foreach ($data_address as $k => $v) {
                if (!in_array($v, $address_all)) {
                    unset($data_address[$k]);
                }
            }
        }

        $data_address = array_unique($data_address);

        if (count($data_address) < 3) {
            return [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '任选/任三/单式'
            ];
        };

        //位置组合
        $data_num_zh = getArrSet([$data_address, $data_address, $data_address]);

        foreach ($data_num_zh as $k => $v) {
            // sort($v);

            if (count(array_unique($v)) !=3) {
                unset($data_num_zh[$k]);
            } else {
                $data_num_zh[$k] =$this->address_sort($v);
            }

        }
        // $data_num_zh = array_unique($data_num_zh);
        $data_num_zh = array_values($data_num_zh);
        $re_data_num_zh = [];
        if($data_num_zh){
            foreach ($data_num_zh as $k=>$v){
                if($k>=1){
                    if(count(array_intersect($v,$data_num_zh[$k-1]))!=3){
                        $re_data_num_zh[] = implode(',',$v);
                    }
                }else{
                    $re_data_num_zh[] = implode(',',$v);
                }


            }
        }




        $zh = [];
        foreach ($re_data_num_zh as $k=>$v){
            $code = explode(',',$v);
            $zh[] = (int)$this->address($code[0],$pre_draw_code).''. (int)$this->address($code[1],$pre_draw_code).''. (int)$this->address($code[2],$pre_draw_code);

        }
        $data_num = $param['data_num'];

        $data_num = explode(' ',$data_num);
        // dump($data_num);
        $data_num = array_unique($data_num);
        $count = count(array_intersect($data_num,$zh));



        if($count>0){
            $re = [
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '任选/任三/单式',
                'count'=>$count
            ];
        }else{
            $re = [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '任选/任三/单式'
            ];
        }
        return $re;


    }

    /**
     * 开奖 5个号码  每个号码 0-9
     * 任选/任三/组三
     * 游戏玩法：至少选择两个号码组成一注，并选择三个对
     * 投注方案：选择12（展开为**122，**212，**221 和 **112、**121、**211）,开奖为**212 或 **121，即为中奖
     * 投注玩法：0-9中任意选择2个号码组成两注，所选号码与开奖号码的对应位相同，且顺序不限，即为中奖
     * @data_address 万,千,百,十,个
     * @data_num 1,2,4
     */
    public function r3z3($param, $pre_draw_code){
        $data_address = explode(',', $param['data_address']);
        $data_address = array_unique($data_address);
        $address_all = ['万', '千', '百', '十', '个'];
        if ($data_address) {
            foreach ($data_address as $k => $v) {
                if (!in_array($v, $address_all)) {
                    unset($data_address[$k]);
                }
            }
        }
        $data_address = array_unique($data_address);
        if (count($data_address) < 3) {
            return [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '任选/任三/组三'
            ];
        };
        //位置组合
        $data_num_zh = getArrSet([$data_address, $data_address, $data_address]);
        foreach ($data_num_zh as $k => $v) {
            // sort($v);
            if (count(array_unique($v)) != 3) {
                unset($data_num_zh[$k]);
            } else {
                sort($v);
                $data_num_zh[$k] = implode(',',$v);
            }
        }
        $data_num_zh = array_unique($data_num_zh);
        $data_num_zh = array_values($data_num_zh);
        $count =0;
        $data_num = $param['data_num'];
        $data_num = explode(',',$data_num);
        $zh = [];
        foreach ($data_num_zh as $k=>$v){
            $code = explode(',',$v);
            $re = [(int)$this->address($code[0],$pre_draw_code),(int)$this->address($code[1],$pre_draw_code),(int)$this->address($code[2],$pre_draw_code)];
            $re = array_unique($re);
            if(count($re) == 2){
                if(count(array_intersect($re,$data_num)) == 2){
                    $count ++ ;
                }
            }
        }
        if($count>0){
            $re = [
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '任选/任三/组三',
                'count'=>$count
            ];
        }else{
            $re = [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '任选/任三/组三'
            ];
        }
        return $re;
    }
    /**
     * 开奖 5个号码  每个号码 0-9
     * 任选/任三/组六
     * 玩法示意：从0-9中任意选择3个或3个以上号码和任意三个位置
     * 位置选择万、千、百，号码选择012；开奖号码为012**、则中奖
     * 从0-9中任意选择3个或3个以上号码和万、千、百、十、个任意的三个位置，如果组合的号码与开奖号码对应则中奖
     * @data_address 万,千,百,十,个
     * @data_num 1,2,4
     */
    public function r3z6($param, $pre_draw_code){
        $data_address = explode(',', $param['data_address']);
        $data_address = array_unique($data_address);
        $address_all = ['万', '千', '百', '十', '个'];
        if ($data_address) {
            foreach ($data_address as $k => $v) {
                if (!in_array($v, $address_all)) {
                    unset($data_address[$k]);
                }
            }
        }
        $data_address = array_unique($data_address);
        if (count($data_address) < 3) {
            return [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '任选/任三/组六'
            ];
        }
        //位置组合
        $data_num_zh = getArrSet([$data_address, $data_address, $data_address]);
        foreach ($data_num_zh as $k => $v) {
            // sort($v);
            if (count(array_unique($v)) != 3) {
                unset($data_num_zh[$k]);
            } else {
                sort($v);
                $data_num_zh[$k] = implode(',',$v);
            }
        }
        $data_num_zh = array_unique($data_num_zh);
        $data_num_zh = array_values($data_num_zh);
        $count =0;
        $data_num = $param['data_num'];
        $data_num = explode(',',$data_num);
        $zh = [];
        foreach ($data_num_zh as $k=>$v){
            $code = explode(',',$v);
            $re = [(int)$this->address($code[0],$pre_draw_code),(int)$this->address($code[1],$pre_draw_code),(int)$this->address($code[2],$pre_draw_code)];
            $re = array_unique($re);
            if(count($re) == 3){
                if(count(array_intersect($re,$data_num)) == 3){
                    $count ++ ;
                }
            }
        }
        if($count>0){
            $re = [
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '任选/任三/组六',
                'count'=>$count
            ];
        }else{
            $re = [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '任选/任三/组六'
            ];
        }
        return $re;
    }
    /**
     * 开奖 5个号码  每个号码 0-9
     * 任选/任二/复试
     * 万、千、百、十、个任意2位，开奖号分别对应且顺序一致即中奖
     * 万位买0，千位买1，百位买2，开奖01234，则中奖。
     * 从万、千、百、十、个中至少2个位置各选一个或多个号码，将各个位置的号码进行组合，所选号码的各位与开奖号码相同则中奖。
     * @one_num 0,1,2
     * @two_num 0,1,2
     * @three_num 0,1,2
     * @four_num 0,1,2
     * @five_num 0,1,2
     */
    public function r2fs($param, $pre_draw_code)
    {
        $one_num = isset($param['one_num']) ? $param['one_num'] : '';
        $two_num = isset($param['two_num']) ? $param['two_num'] : '';
        $three_num = isset($param['three_num']) ? $param['three_num'] : '';
        $four_num = isset($param['four_num']) ? $param['four_num'] : '';
        $five_num = isset($param['five_num']) ? $param['five_num'] : '';

        $one_num = $this->formatNum(explode(',', $one_num));
        $two_num = $this->formatNum(explode(',', $two_num));
        $three_num = $this->formatNum(explode(',', $three_num));
        $four_num = $this->formatNum(explode(',', $four_num));
        $five_num = $this->formatNum(explode(',', $five_num));
        $count = 0;

        if (in_array($pre_draw_code[0], $one_num)) {
            $count++;
        }
        if (in_array($pre_draw_code[1], $two_num)) {
            $count++;
        }
        if (in_array($pre_draw_code[2], $three_num)) {
            $count++;
        }
        if (in_array($pre_draw_code[3], $four_num)) {
            $count++;
        }
        if (in_array($pre_draw_code[4], $five_num)) {
            $count++;
        }

        if ($count >= 2) {
            $input = [];
            for ($i=1;$i<=$count;$i++){
                $input[] = $i;

            }
            $c= comb($input,2);

            $re = [
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '任选/任二/复试',
                'count'=>count($c)
            ];


        } else {
            $re = [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '任选/任二/复试'
            ];
        }
        return $re;
    }
    /**
     * 开奖 5个号码  每个号码 0-9
     * 任选/任二/单式
     * 游戏玩法：至少选择两个号码组成一注，并选择两个对（每个号码之间用,隔开；每注号码之间用;隔开）
     * 投注方案：选择12,对应位百、十，开奖为**21* 或 **12*，即为中奖
     * 投注玩法：从0-9中选2个号码组成一注，所择号码与开奖号码的对应位相同，顺序不限，即为中奖
     *
     * @data_num 1,2;3,4
     * @data_address 万,千,百,十,个
     *
     */
    public function r2ds($param, $pre_draw_code)
    {
        $data_address = explode(',', $param['data_address']);
        $data_address = array_unique($data_address);
        $address_all = ['万', '千', '百', '十', '个'];
        if ($data_address) {
            foreach ($data_address as $k => $v) {
                if (!in_array($v, $address_all)) {
                    unset($data_address[$k]);
                }
            }
        }
        $data_address = array_unique($data_address);
        if (count($data_address) <2) {
            return [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '任选/任二/单式'
            ];
        };

        //位置组合
        $data_num_zh = getArrSet([$data_address, $data_address]);

        foreach ($data_num_zh as $k => $v) {
            if (count(array_unique($v)) !=2) {
                unset($data_num_zh[$k]);
            } else {
                $data_num_zh[$k] =$this->address_sort($v);
            }

        }
        // $data_num_zh = array_unique($data_num_zh);
        $data_num_zh = array_values($data_num_zh);
        $re_data_num_zh = [];
        if($data_num_zh){
            foreach ($data_num_zh as $k=>$v){
                if($k>=1){
                    if(count(array_intersect($v,$data_num_zh[$k-1]))!=2){
                        $re_data_num_zh[] = implode(',',$v);
                    }
                }else{
                    $re_data_num_zh[] = implode(',',$v);
                }
            }
        }
        $zh = [];
        foreach ($re_data_num_zh as $k=>$v){
            $code = explode(',',$v);
            $zh[] = (int)$this->address($code[0],$pre_draw_code).''. (int)$this->address($code[1],$pre_draw_code);

        }
        $data_num = $param['data_num'];
        $data_num =
        $data_num = explode(' ',$data_num);
        // dump($data_num);
        $data_num = array_unique($data_num);
        $count = count(array_intersect($data_num,$zh));



        if($count>0){
            $re = [
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '任选/任二/单式',
                'count'=>$count
            ];
        }else{
            $re = [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '任选/任二/单式'
            ];
        }
        return $re;
    }
    /**
     * 开奖 5个号码  每个号码 0-9
     * 任选/任二/组选
     * 从0-9中任意选择2个或2个以上号码和任意两个位置
     * 位置选择万、千，号码选择01；开奖号码为01***、则中奖
     * 从0-9中任意选择2个或2个以上号码和万、千、百、十、个任意的两个位置，如果组合的号码与开奖号码对应则中奖
     *
     */
    public function rx_re_zx($param, $pre_draw_code){
        $data_num = $param['data_num'];
        $data_num = explode(',', $data_num);
        $data_num = $this->formatNum($data_num);
        $data_address = explode(',', $param['data_address']);
        $data_address = array_unique($data_address);
        $address_all = ['万', '千', '百', '十', '个'];
        if ($data_address) {
            foreach ($data_address as $k => $v) {
                if (!in_array($v, $address_all)) {
                    unset($data_address[$k]);
                }
            }
        }
        //位置组合
        $data_num_zh = getArrSet([$data_address, $data_address]);
        foreach ($data_num_zh as $k => $v) {
            if (count(array_unique($v)) !=2) {
                unset($data_num_zh[$k]);
            } else {
                $data_num_zh[$k] = implode(',',$this->address_sort($v));
            }

        }
        $data_num_zh = array_unique($data_num_zh);
        $count = 0;
//        $zh = [];
        foreach ($data_num_zh as $k=>$v) {
            $code = explode(',', $v);
            if ((int)$this->address($code[0], $pre_draw_code) != (int)$this->address($code[1], $pre_draw_code)) {
//                $zh[] = [$this->address($code[0], $pre_draw_code), $this->address($code[1], $pre_draw_code)];
                $rr = [$this->address($code[0], $pre_draw_code), $this->address($code[1], $pre_draw_code)];
                if (count(array_intersect($data_num, $rr)) == 2) {
                    $count++;
                }
            }
        }
        if ($count > 0) {
            $re = [
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '任选/任二/组选',
                'count'=>$count
            ];
        } else {
            $re = [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '任选/任二/组选'
            ];
        }
        return $re;
    }
    /**
     * 开奖 5个号码  每个号码 0-9
     *大小单双/大小单双/前二
     * 游戏玩法：从万、千位各选一个号码组成一注。
     * 投注方案：万位选择小，千位选择双，开出12***即为中奖
     * 投注玩法：大小单双:对万位和千位的“大（56789）小（01234）、单（13579）双（02468）”形态进行购买。
     * @w_status 大,小,单,双(万位)
     * @q_status 大,小,单,双（千位）
     */

    public function dxdsq2($param, $pre_draw_code){
        $w_status = explode(',', $param['w_status']);
        $w_status = array_unique($w_status);

        $q_status = explode(',', $param['q_status']);
        $q_status = array_unique($q_status);
        //获取万位
        $w_type = [];
        $w_num = $pre_draw_code[0];
        if($w_num>=5){
            $w_type[] = '大';
        }else{
            $w_type[] = '小';
        }
        if($w_num % 2 == 0){
            $w_type[] = '双';
        }else{
            $w_type[] = '单';
        }
        //千位
        $q_type = [];
        $q_num = $pre_draw_code[1];
        if($q_num>=5){
            $q_type[] = '大';
        }else{
            $q_type[] = '小';
        }
        if($q_num % 2 == 0){
            $q_type[] = '双';
        }else{
            $q_type[] = '单';
        }

        //交集
        $w_jj = array_intersect($w_status,$w_type);
        $q_jj = array_intersect($q_status,$q_type);

        if(count($w_jj)>0 && count($q_jj)>0){
            $re = [
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '大小单双/大小单双/前二',
                'count'=>count($w_jj)*count($q_jj)
            ];
        }else{
            $re = [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '大小单双/大小单双/前二'
            ];
        }
        return $re;

    }
    /**
     * 开奖 5个号码  每个号码 0-9
     *大小单双/大小单双/后二
     * 游戏玩法：从十、个位各选一个号码组成一注
     * 投注方案：十位选择小，个位选择双，开出***12即为中奖
     * 投注玩法：大小单双:对十位和个位的“大（56789）小（01234）、单（13579）双（02468）”形态进行购买。
     * @s_status 大,小,单,双(十位)
     * @g_status 大,小,单,双（个位）
     */
    public function dxdsh2($param, $pre_draw_code){
        $s_status = explode(',', $param['s_status']);
        $s_status = array_unique($s_status);

        $g_status = explode(',', $param['g_status']);
        $g_status = array_unique($g_status);
        //获取十位
        $s_type = [];
        $w_num = $pre_draw_code[3];
        if($w_num>=5){
            $s_type[] = '大';
        }else{
            $s_type[] = '小';
        }
        if($w_num % 2 == 0){
            $s_type[] = '双';
        }else{
            $s_type[] = '单';
        }
        //个位
        $g_type = [];
        $q_num = $pre_draw_code[4];
        if($q_num>=5){
            $g_type[] = '大';
        }else{
            $g_type[] = '小';
        }
        if($q_num % 2 == 0){
            $g_type[] = '双';
        }else{
            $g_type[] = '单';
        }

        //交集
        $s_jj = array_intersect($s_status,$s_type);
        $g_jj = array_intersect($g_status,$g_type);
        if(count($s_jj)>0 && count($g_jj)>0){
            $re = [
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '大小单双/大小单双/后二',
                'count'=>count($s_jj)*count($g_jj)
            ];
        }else{
            $re = [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '大小单双/大小单双/后二'
            ];
        }
        return $re;

    }
    /**
     * 开奖 5个号码  每个号码 0-9
     * 和值/直选/前三
     * 游戏玩法：至少选择一个号码组成一注。
     * 投注方案：选择01, 相当于前三位投注0,0,1/0,1,0/1,0,0三组号码，开奖号码前三位号码的和值为1（0,0,1/0,1,0/1,0,0），即为中奖。
     * 投注玩法：选择0-27，若所选和值号码与开奖号前三位的和值相等，即为中奖。
     * @data_num 1,2,3,4
     */
    public function hzzhixq3($param, $pre_draw_code)
    {
        //获取下注的参数
        $data_num = $param['data_num'];
        $data_num = explode(',', $data_num);
        $data_num = $this->formatNum($data_num,27);
        if (count($data_num) == 0) {
            return [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '和值/直选/前三'
            ];
        }
        //中奖号码前三位
        $sum = (int)$pre_draw_code[0] + (int)$pre_draw_code[1] + (int)$pre_draw_code[2];
        $sum = (int)$sum;
        if (in_array($sum, $data_num)) {

            $re = [
                'count'=>1,
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '和值/直选/前三'
            ];

        } else {
            $re =[
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '和值/直选/前三'
            ];

        }
        return $re;
    }
    /**
     * 开奖 5个号码  每个号码 0-9
     * 和值/直选/中三
     * 游戏玩法：至少选择一个号码组成一注。
     * 投注方案：选择01, 相当于中三位投注0,0,1/0,1,0/1,0,0三组号码，开奖号码中三位号码的和值为1（0,0,1/0,1,0/1,0,0），即为中奖。
     * 投注玩法：选择0-27，若所选和值号码与开奖号中三位的和值相等，即为中奖。
     * @data_num 1,2,3,4
     */
    public function hzzhixz3($param, $pre_draw_code)
    {
        //获取下注的参数
        $data_num = $param['data_num'];
        $data_num = explode(',', $data_num);
        $data_num = $this->formatNum($data_num,27);
        if (count($data_num) == 0) {
            return [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '和值/直选/中三'
            ];
        }
        //中奖号码中三位
        $sum = (int)$pre_draw_code[1] + (int)$pre_draw_code[2] + (int)$pre_draw_code[3];
        $sum = (int)$sum;
        if (in_array($sum, $data_num)) {

            $re = [
                'count'=>1,
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '和值/直选/中三'
            ];

        } else {
            $re = [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '和值/直选/中三'
            ];

        }
        return $re;
    }
    /**
     * 开奖 5个号码  每个号码 0-9
     * 和值/直选/后三
     * 游戏玩法：至少选择一个号码组成一注。
     * 投注方案：选择01, 相当于后三位投注0,0,1/0,1,0/1,0,0三组号码，开奖号码后三位号码的和值为1（0,0,1/0,1,0/1,0,0），即为中奖。
     * 投注玩法：选择0-27，若所选和值号码与开奖号后三位的和值相等，即为中奖。
     * @data_num 1,2,3,4
     */
    public function hzzhixh3($param, $pre_draw_code)
    {
        //获取下注的参数
        $data_num = $param['data_num'];
        $data_num = explode(',', $data_num);
        $data_num = $this->formatNum($data_num,27);
        if (count($data_num) == 0) {
            return [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '和值/直选/后三'
            ];
        }
        //中奖号码后三
        $sum = (int)$pre_draw_code[2] + (int)$pre_draw_code[3] + (int)$pre_draw_code[4];
        $sum = (int)$sum;
        if (in_array($sum, $data_num)) {

            $re = [
                'count'=>1,
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '和值/直选/后三'
            ];

        } else {
            $re = [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '和值/直选/后三'
            ];

        }
        return $re;
    }
    /**
     * 开奖 5个号码  每个号码 0-9
     * 和值/直选/前二 前二/直选/直选和值
     * 游戏玩法：至少选择一个号码组成一注。
     * 投注方案：选择1, 相当于前2位投注0,1/1,0两组号码，开奖号码前2位号码的和值为1（0,1/1,0），即为中奖。
     * 投注玩法：选择0-18，若所选和值号码与开奖号前二位的和值相等，即为中奖。
     * @data_num 1,2,3,4
     */
    public function hzzhixq2($param, $pre_draw_code)
    {
        //获取下注的参数
        $data_num = $param['data_num'];
        $data_num = explode(',', $data_num);
        $data_num = $this->formatNum($data_num,18);
        if (count($data_num) == 0) {
            return [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '和值/直选/前二'
            ];
        }
        //中奖号码前2位
        $sum = (int)$pre_draw_code[0] + (int)$pre_draw_code[1];
        $sum = (int)$sum;
        if (in_array($sum, $data_num)) {
            $re = [
                'count'=>1,
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '和值/直选/前二'
            ];
        } else {
            $re = [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '和值/直选/前二'
            ];
        }
        return $re;
    }
    /**
     * 开奖 5个号码  每个号码 0-9
     * 和值/直选/后二 后二/直选/直选和值
     * 游戏玩法：至少选择一个号码组成一注。
     * 投注方案：选择1, 相当于后2位投注0,1/1,0两组号码，开奖号码后2位号码的和值为1（0,1/1,0），即为中奖。
     * 投注玩法：选择0-18，若所选和值号码与开奖号后二位的和值相等，即为中奖。
     * @data_num 1,2,3,4
     */
    public function hzzhixh2($param, $pre_draw_code)
    {
        //获取下注的参数
        $data_num = $param['data_num'];
        $data_num = explode(',', $data_num);
        $data_num = $this->formatNum($data_num,18);
        if (count($data_num) == 0) {
            return [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '和值/直选/后二'
            ];
        }
        //中奖号码前2位
        $sum = (int)$pre_draw_code[3] + (int)$pre_draw_code[4];
        $sum = (int)$sum;
        if (in_array($sum, $data_num)) {
            $re = [
                'count'=>1,
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '和值/直选/后二'
            ];
        } else {
            $re = [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '和值/直选/后二'
            ];
        }
        return $re;
    }

    /**
     * 开奖 5个号码  每个号码 0-9
     * 和值/尾数/前三
     * 游戏玩法：至少选择一个号码组成一注
     * 投注方案：选择5, 等于开奖号前三位4,5,6和值的尾数，即为中奖
     * 投注玩法：选择0-9，若所选号码与开奖号前三位的和值的尾数相等，即为中奖
     * @data_num 1,2,3,4
     */
    public function hzwsq3($param, $pre_draw_code)
    {
        //中奖号码前三位
        $sum = (int)$pre_draw_code[0] + (int)$pre_draw_code[1] + (int)$pre_draw_code[2];
        $ws = $sum % 10;
        $ws = (int)$ws;
        $data_num = $param['data_num'];
        $data_num = explode(',', $data_num);
        $data_num = $this->formatNum($data_num);
        if (in_array($ws, $data_num)) {
            $re = [
                'count'=>1,
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '和值/尾选/前三'
            ];
        } else {
            $re = [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '和值/尾选/前三'
            ];
        }
        return $re;
    }
    /**
     * 开奖 5个号码  每个号码 0-9
     * 和值/尾选/中三
     * 游戏玩法：至少选择一个号码组成一注
     * 投注方案：选择5, 等于开奖号中三位4,5,6和值的尾数，即为中奖
     * 投注玩法：选择0-9，若所选号码与开奖号中三位的和值的尾数相等，即为中奖
     * @data_num 1,2,3,4
     */
    public function hzwsz3($param, $pre_draw_code)
    {
        //中奖号码中三位
        $sum = (int)$pre_draw_code[1] + (int)$pre_draw_code[2] + (int)$pre_draw_code[3];
        $ws = $sum % 10;
        $ws = (int)$ws;
        $data_num = $param['data_num'];
        $data_num = explode(',', $data_num);
        $data_num = $this->formatNum($data_num);
        if (in_array($ws, $data_num)) {
            $re = [
                'count'=>1,
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '和值/尾选/中三'
            ];
        } else {
            $re = [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '和值/尾选/中三'
            ];
        }
        return $re;
    }
    /**
     * 开奖 5个号码  每个号码 0-9
     * 和值/尾选/后三
     * 游戏玩法：至少选择一个号码组成一注
     * 投注方案：选择5, 等于开奖号后三位4,5,6和值的尾数，即为中奖
     * 投注玩法：选择0-9，若所选号码与开奖号后三位的和值的尾数相等，即为中奖
     * @data_num 1,2,3,4
     */
    public function hzwsh3($param, $pre_draw_code)
    {
        //中奖号码后三位
        $sum = (int)$pre_draw_code[2] + (int)$pre_draw_code[3] + (int)$pre_draw_code[4];
        $ws = $sum % 10;
        $ws = (int)$ws;
        $data_num = $param['data_num'];
        $data_num = explode(',', $data_num);
        $data_num = $this->formatNum($data_num);
        if (in_array($ws, $data_num)) {
            $re = [
                'count'=>1,
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '和值/尾选/后三'
            ];
        } else {
            $re = [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '和值/尾选/后三'
            ];
        }
        return $re;
    }
    /**
     * 开奖 5个号码  每个号码 0-9
     * 趣味/特殊/一帆风顺
     * 从0-9中任意选择1个以上号码。
     * 投注方案：8；开奖号码：至少出现1个8，即中一帆风顺。
     * 从0-9中任意选择1个号码组成一注，只要开奖号码的万位、千位、百位、十位、个位中包含所选号码，即为中奖。
     * @data_num 1,2,3
     */
    public function yffs($param,$pre_draw_code){
        $data_num = explode(',', $param['data_num']);
        $data_num = $this->formatNum($data_num);
        $data_num = array_unique($data_num);

        $jj = array_intersect($data_num,$pre_draw_code);
        if (count($jj)>=1) {
            $re = [
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '趣味/趣味/一帆风顺',
                'count'=>count($jj)
            ];
        } else {
            $re = [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '趣味/趣味/一帆风顺'
            ];
        }
        return $re;
    }
    /**
     * 开奖 5个号码  每个号码 0-9
     * 趣味/特殊/好事成双
     * 从0-9中任意选择1个以上的二重号码。
     * 投注方案：8；开奖号码：至少出现2个8，即中好事成双。
     * 从0-9中任意选择1个号码组成一注，只要所选号码在开奖号码的万位、千位、百位、十位、个位中出现2次，即为中奖。
     * @data_num 1,2,3
     */
    public function hscs($param,$pre_draw_code){
        $data_num = explode(',', $param['data_num']);
        $data_num = $this->formatNum($data_num);
        $data_num = array_unique($data_num);
        $count = 0;
        if($data_num){
            foreach($data_num as $k=>$v){
                if(array_count_values_of($v,$pre_draw_code)==2){
                    $count ++;
                }
            }
        }
        if ($count>=1) {
            $re = [
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '趣味/特殊/好事成双',
                'count'=>$count
            ];
        } else {
            $re = [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '趣味/特殊/好事成双'
            ];
        }
        return $re;
    }
    /**
     * 新龙虎/新龙虎/万百
     * 投注方案：例如投注龙，开奖号为64535，即为中奖
     * 投注玩法：开奖号码的万位数字大于百位数字则为龙；万位小于百位则为虎；号码相同则打和。
     * @data_num 龙,虎,和
     * $addres  [0,1]
     */
    public function lh($param,$pre_draw_code,$addess){
        $data_num = $param['data_num'];
        if($pre_draw_code[$addess[0]] > $pre_draw_code[$addess[1]]){
            $s = '龙';
        }elseif ($pre_draw_code[$addess[0]] < $pre_draw_code[$addess[1]]){
            $s = '虎';
        }else{
            $s = '和';
        }
        $data_num = explode(',',$data_num);
        if(in_array($s,$data_num)){
            $re = [
                'status' => 2,
                'remark' => '中奖',

                'count'=>1
            ];
        }else{
            $re = [
                'count'=>0,
                'status' => 3,
                'remark' => '未中奖',
            ];
        }
        return $re;



    }






}