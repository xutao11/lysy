<?php
namespace betting\calculation;


trait Cp11x5{
    /**
     * 三码/前三直选/复式   (玩法)
     * 从01-11共11个号码中选择3个不重复的号码组成一注，所选号码与当期顺序摇出的5个号码中的前3个号码相同，且顺序一致，即为中奖。
     * @pre_draw_code  开奖号码
     * @betting_num 购买号码
     *
     * @one_num  第一位好嘛 （01,02,03）
     * @two_num  第二位好嘛 （01,02,03）
     * @three_num  第三位好嘛 （01,02,03）
     *
     */
    protected function q3_zhix_fs($param, $pre_draw_code)
    {
        $pre_draw_code = $this->formatNum($pre_draw_code,11,1);

        $one_num = $this->formatNum(explode(',', $param['one_num']),11,1);
        $two_num = $this->formatNum(explode(',', $param['two_num']),11,1);
        $three_num = $this->formatNum(explode(',', $param['three_num']),11,1);
        //获取前三号码
        $pre_draw_code = array_slice($pre_draw_code,0,3);
        //去重
        $pre_draw_code = array_unique($pre_draw_code);
        if(count($pre_draw_code) !=3){
            $re = [
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '三码/前三直选/复式',
                'count'=>0
            ];
            return $re;
        }
        if(in_array($pre_draw_code[0],$one_num) && in_array($pre_draw_code[1],$two_num) && in_array($pre_draw_code[2],$three_num)){
            $re = [
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '三码/前三直选/复式',
                'count'=>1
            ];
        }else{
            $re = [
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '三码/前三直选/复式',
                'count'=>0
            ];
        }
        return $re;
    }


    /**
     * 三码/前三直选/单式   (玩法)
     * 手动输入3个号码组成一注，所输入的号码与当期顺序摇出的5个号码中的前3个号码相同，且顺序一致，即为中奖。
     * @pre_draw_code  开奖号码
     * @betting 购买号码
     *
     * @data_num  下注号码 01,02,03;02,01,03
     */
    protected function q3_zhix_ds($param, $pre_draw_code)
    {
        $pre_draw_code = $this->formatNum($pre_draw_code,11,1);
        
        $data_num = explode(';',$param['data_num']);
        if($data_num){
            foreach ($data_num as $k=>$v){

                $data_num[$k] = implode(',',$this->formatNum(explode(',',$v),11,1));

            }
        }
        //获取前三号码
        $pre_draw_code = array_slice($pre_draw_code,0,3);
        $pre_draw_code = implode(',',$pre_draw_code);
        if(in_array($pre_draw_code,$data_num)){
            $re = [
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '三码/前三直选/单式',
                'count'=>1
            ];
        }else{
            //未中奖
            $re = [
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '三码/前三直选/单式',
                'count'=>0
            ];
        }
        return $re;


    }


    /**
     * 三码/前三组合/复式   (玩法)
     * 从01-11中共11个号码中选择3个号码，所选号码与当期顺序摇出的5个号码中的前3个号码相同，顺序不限，即为中奖。
     * @pre_draw_code  开奖号码
     * @betting 购买号码
     * @data_num  所选号码 （01,02,03,04）
     */

    protected function q3_zhux_fs($param, $pre_draw_code)
    {
        $pre_draw_code = $this->formatNum($pre_draw_code,11,1);
        
        $data_num = $this->formatNum(explode(',',$param['data_num']),11,1);
        //取开将号码的前三位
        $pre_draw_code = array_slice($pre_draw_code, 0, 3);
        //取交集
        $jj = array_intersect($data_num,$pre_draw_code);
        if(count($jj) == 3){
            $re = [
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '三码/前三组合/复式',
                'count'=>1
            ];
        }else{
            $re = [
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '三码/前三组合/复式',
                'count'=>0
            ];
        }
        return $re;

    }

    /**
     * 三码/前三组合/单式   (玩法)
     * 手动输入3个号码组成一注，所输入的号码与当期顺序摇出的5个号码中的前3个号码相同，顺序不限，即为中奖。
     * @pre_draw_code  开奖号码
     * @betting 购买号码
     * @data_num  购买号码(1,2,3;1,2,4)
     */

    protected function q3_zhux_ds($param, $pre_draw_code)
    {
        $pre_draw_code = $this->formatNum($pre_draw_code,11,1);
        
        $data_num = explode(';',$param['data_num']);
        if($data_num){
            foreach ($data_num as $k=>$v){
                $data_num[$k] = $this->formatNum(explode(',',$v),11,1);
            }
        }
        //取开将号码的前三位
        $pre_draw_code = array_slice($pre_draw_code, 0, 3);
        foreach ($data_num as $k=>$v){
            $jj = array_intersect($v,$pre_draw_code);
            if(count($jj) == 3){
                return [
                    'status' => 2,
                    'remark' => '中奖',
                    'play_name' => '三码/前三组合/单式',
                    'count'=>1
                ];
            }
        }
        return [
            'status' => 3,
            'remark' => '未中奖',
            'play_name' => '三码/前三组合/单式',
            'count'=>0
        ];

    }

    /**
     * 二码/前二直选/复式   (玩法)
     * 从01-11共11个号码中选择2个不重复的号码组成一注，所选号码与当期顺序摇出的5个号码中的前2个号码相同，且顺序一致，即为中奖。
     * @pre_draw_code  开奖号码
     * @betting 购买号码
     * @one_num  第一位好嘛 （01,02,03）
     * @two_num  第二位好嘛 （01,02,03）
     */
    protected function q2_zhix_fs($param, $pre_draw_code)
    {
        $pre_draw_code = $this->formatNum($pre_draw_code,11,1);
        
        $one_num = $this->formatNum(explode(',', $param['one_num']),11,1);
        $two_num = $this->formatNum(explode(',', $param['two_num']),11,1);
        //获取前三号码
        $pre_draw_code = array_slice($pre_draw_code,0,2);
        //去重
        $pre_draw_code = array_unique($pre_draw_code);

        if(in_array($pre_draw_code[0],$one_num) && in_array($pre_draw_code[1],$two_num)){
            $re = [
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '二码/前二直选/复式',
                'count'=>1
            ];
        }else{
            $re = [
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '二码/前二直选/复式',
                'count'=>0
            ];
        }
        return $re;

    }

    /**
     * 二码/前二直选/单式   (玩法)
     * 手动输入2个号码组成一注，所输入的号码与当期顺序摇出的5个号码中的前2个号码相同，且顺序一致，即为中奖。
     * @pre_draw_code  开奖号码
     * @betting 购买号码
     * @data_num  下注号码 01,02;02,01
     */
    protected function q2_zhix_ds($param, $pre_draw_code)
    {
        $pre_draw_code = $this->formatNum($pre_draw_code,11,1);
        
        $data_num = explode(';',$param['data_num']);
        if($data_num){
            foreach ($data_num as $k=>$v){
                $data_num[$k] = $this->formatNum(explode(',',$v),11,1);
            }
        }
        //取开将号码的前二位
        $pre_draw_code = array_slice($pre_draw_code, 0, 2);
        foreach ($data_num as $k=>$v){
            $jj = array_intersect($v,$pre_draw_code);
            if(count($jj) == 2){
                return [
                    'status' => 2,
                    'remark' => '中奖',
                    'play_name' => '二码/前二直选/单式',
                    'count'=>1
                ];
            }
        }
        return [
            'status' => 3,
            'remark' => '未中奖',
            'play_name' => '二码/前二直选/单式',
            'count'=>0
        ];

    }


    /**
     * 二码/前二组合/复式   (玩法)
     * 从01-11中共11个号码中选择2个号码，所选号码与当期顺序摇出的5个号码中的前2个号码相同，顺序不限，即为中奖。
     * @pre_draw_code  开奖号码
     * @betting 购买号码
     * @data_num  开奖号码(01,02,03,04)
     */

    protected function q2_zhux_fs($param, $pre_draw_code)
    {
        $pre_draw_code = $this->formatNum($pre_draw_code,11,1);
        
        $data_num = $this->formatNum(explode(',',$param['data_num']),11,1);

        //取开将号码的前2位
        $pre_draw_code = array_slice($pre_draw_code, 0, 2);
        //取交集
        $jj = array_intersect($data_num,$pre_draw_code);
        if(count($jj) == 2){
            $re = [
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '二码/前二组合/复式',
                'count'=>1
            ];
        }else{
            $re = [
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '二码/前二组合/复式',
                'count'=>0
            ];
        }
        return $re;

    }

    /**
     * 二码/前二组合/单式   (玩法)
     * 手动输入2个号码组成一注，所输入的号码与当期顺序摇出的5个号码中的前2个号码相同，顺序不限，即为中奖。
     * @pre_draw_code  开奖号码
     * @betting 购买号码
     * @data_num  购买号码(1,2;1,4)
     */

    protected function q2_zhux_ds($param, $pre_draw_code)
    {
        $pre_draw_code = $this->formatNum($pre_draw_code,11,1);
        
        $data_num = explode(';',$param['data_num']);
        if($data_num){
            foreach ($data_num as $k=>$v){
                $data_num[$k] = $this->formatNum(explode(',',$v),11,1);
            }
        }
        //取开将号码的前2位
        $pre_draw_code = array_slice($pre_draw_code, 0, 2);
        foreach ($data_num as $k=>$v){
            $jj = array_intersect($v,$pre_draw_code);
            if(count($jj) == 2){
                return [
                    'status' => 2,
                    'remark' => '中奖',
                    'play_name' => '二码/前二组合/单式',
                    'count'=>1
                ];
            }
        }
        return [
            'status' => 3,
            'remark' => '未中奖',
            'play_name' => '二码/前二组合/单式',
            'count'=>0
        ];

    }

    /**
     * 不定胆/前三位   (玩法)
     * 从01-11中共11个号码中选择1个号码，每注由1个号码组成，只要当期顺序摇出的第一位、第二位、第三位开奖号码中包含所选号码，即为中奖。
     * @pre_draw_code  开奖号码
     * @betting 购买号码
     * @data_num  所选号码 01,02,03,02,04
     */

    protected function bdw_q3($param, $pre_draw_code)
    {
        $pre_draw_code = $this->formatNum($pre_draw_code,11,1);
        
        $data_num = $this->formatNum(explode(',',$param['data_num']),11,1);
        //取开将号码的前3位
        $pre_draw_code = array_slice($pre_draw_code, 0, 3);

        //交集
        $jj = array_intersect($data_num,$pre_draw_code);
//        dump($jj);
        if(count($jj)>0){
            $re = [
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '不定胆/前三位',
                'count'=>1
            ];
        }else{
            $re = [
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '不定胆/前三位',
                'count'=>0
            ];
        }
        return $re;




    }

    /**
     * 定位胆/定位胆   (玩法)
     * 从第一位，第二位，第三位任意1个位置或多个位置上选择1个号码，所选号码与相同位置上的开奖号码一致，即为中奖。
     *
     * @pre_draw_code  开奖号码 (01,08)  01表示第几位 08表示购买的好嘛
     * @betting 购买号码
     * @one_num  第一位好嘛 （01,02,03）
     * @two_num  第二位好嘛 （01,02,03）
     * @three_num  第三位好嘛 （01,02,03）
     */

    protected function dwd($param, $pre_draw_code)
    {
        $pre_draw_code = $this->formatNum($pre_draw_code,11,1);

        
        $one_num = $this->formatNum(explode(',', $param['one_num']),11,1);
        $two_num = $this->formatNum(explode(',', $param['two_num']),11,1);
        $three_num = $this->formatNum(explode(',', $param['three_num']),11,1);

        //获取前三号码
        $pre_draw_code = array_slice($pre_draw_code,0,3);
        $count = 0;
        if(in_array($pre_draw_code[0],$one_num)){
            $count++;
        }
        if(in_array($pre_draw_code[1],$two_num)){
            $count++;
        }
        if(in_array($pre_draw_code[2],$three_num)){
            $count++;
        }

        if($count>0){
            $re = [
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '定位胆/定位胆',
                'count'=>$count
            ];
        }else{
            $re = [
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '定位胆/定位胆',
                'count'=>0
            ];

        }
        return $re;


    }

    /**
     * 任选/任选复试/一中一  (玩法)
     * 从01-11共11个号码中选择1个号码进行购买，只要当期顺序摇出的5个开奖号码中包含所选号码，即为中奖。
     * @pre_draw_code  开奖号码
     * @betting 购买号码
     * @data_num  所选号码（01,02,03）
     */

    protected function rx_fs_1z1($param, $pre_draw_code)
    {
        $pre_draw_code = $this->formatNum($pre_draw_code,11,1);
        
        $data_num = $this->formatNum(explode(',',$param['data_num']),11,1);
        //取交集
        $jj = array_intersect($data_num,$pre_draw_code);
        if(count($jj) >= 1){
            $re = [
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '任选/任选复试/一中一',
                'count'=>count($jj)
            ];
        }else{
            $re = [
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '任选/任选复试/一中一',
                'count'=>0
            ];
        }
        return $re;



    }

    /**
     * 任选/任选复试/二中二  (玩法)
     * 从01-11共11个号码中选择2个号码进行购买，只要当期顺序摇出的5个开奖号码中包含所选号码，即为中奖。
     * @pre_draw_code  开奖号码
     * @betting 购买号码
     *  @data_num  所选号码 01,02,03,04
     */


    protected function rx_fs_2z2($param, $pre_draw_code)
    {
        $pre_draw_code = $this->formatNum($pre_draw_code,11,1);
        
        $data_num = $this->formatNum(explode(',',$param['data_num']),11,1);
        //取交集
        $jj = array_intersect($data_num,$pre_draw_code);
        if(count($jj) >= 2){
            $re = [
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '任选/任选复试/二中二',
                'count'=>1
            ];
            $input = [];
            for ($i=1;$i<=count($jj);$i++){
                $input[] = $i;

            }
            $c= comb($input,2);
            $re['count'] = count($c);
        }else{
            $re = [
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '任选/任选复试/二中二',
                'count'=>0
            ];
        }
        return $re;

    }

    /**
     * 任选/任选复试/三中三  (玩法)
     * 从01-11共11个号码中选择3个号码进行购买，只要当期顺序摇出的5个开奖号码中包含所选号码，即为中奖。
     * 从01-11共11个号码中选择3个号码进行购买，只要当期顺序摇出的5个开奖号码中包含所选号码，即为中奖。
     * @pre_draw_code  开奖号码
     * @betting 购买号码
     * @data_num  所选号码 01,02,03,04
     */

    protected function rx_fs_3z3($param, $pre_draw_code)
    {
        $pre_draw_code = $this->formatNum($pre_draw_code,11,1);
        
        $data_num = $this->formatNum(explode(',',$param['data_num']),11,1);
        //取交集
        $jj = array_intersect($data_num,$pre_draw_code);
        if(count($jj) >= 3){
            $re = [
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '任选/任选复试/三中三',
                'count'=>1
            ];
            $input = [];
            for ($i=1;$i<=count($jj);$i++){
                $input[] = $i;

            }
            $c= comb($input,3);
            $re['count'] = count($c);
        }else{
            $re = [
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '任选/任选复试/三中三',
                'count'=>0
            ];
        }
        return $re;

    }

    /**
     * 任选/任选复试/四中四  (玩法)
     * 从01-11共11个号码中选择4个号码进行购买，只要当期顺序摇出的5个开奖号码中包含所选号码，即为中奖。
     * @pre_draw_code  开奖号码
     * @betting 购买号码
     * @data_num  所选号码 01,02,03,04
     */


    protected function rx_fs_4z4($param, $pre_draw_code)
    {
        $pre_draw_code = $this->formatNum($pre_draw_code,11,1);
        
        $data_num = $this->formatNum(explode(',',$param['data_num']),11,1);
        //取交集
        $jj = array_intersect($data_num,$pre_draw_code);
        if(count($jj) >= 4){
            $re = [
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '任选/任选复试/四中四',

            ];
            $input = [];
            for ($i=1;$i<=count($jj);$i++){
                $input[] = $i;

            }
            $c= comb($input,4);
            $re['count'] = count($c);
        }else{
            $re = [
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '任选/任选复试/四中四',
                'count'=>0
            ];
        }
        return $re;

    }

    /**
     * 任选/任选复试/五中五  (玩法)
     * 从01-11共11个号码中选择5个号码进行购买，只要当期顺序摇出的5个开奖号码中包含所选号码，即为中奖。
     * @pre_draw_code  开奖号码
     * @betting_num 购买号码
     * @data_num  所选号码 01,02,03,04,05
     */

    protected function rx_fs_5z5($param, $pre_draw_code)
    {
        $pre_draw_code = $this->formatNum($pre_draw_code,11,1);
        
        $data_num = $this->formatNum(explode(',',$param['data_num']),11,1);
        //取交集
        $jj = array_intersect($data_num,$pre_draw_code);
        if(count($jj) >= 5){
            $re = [
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '任选/任选复试/五中五',
                'count'=>1
            ];

        }else{
            $re = [
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '任选/任选复试/五中五',
                'count'=>0
            ];
        }
        return $re;


    }

    /**
     * 任选/任选复试/六中五  (玩法)
     * 从01-11共11个号码中选择6个号码进行购买，只要当期顺序摇出的5个开奖号码中包含所选号码，即为中奖。
     * @pre_draw_code  开奖号码
     * @data_num  所选号码 01,02,03,04,05,06
     */

    protected function rx_fs_6z5($param, $pre_draw_code)
    {
        $pre_draw_code = $this->formatNum($pre_draw_code,11,1);
        
        $data_num = $this->formatNum(explode(',',$param['data_num']),11,1);

        //取交集
        $jj = array_intersect($data_num,$pre_draw_code);

        if(count($jj) >= 5){
            $re = [
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '任选/任选复试/六中五',
                'count'=>count($data_num)-5
            ];
        }else{
            $re = [
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '任选/任选复试/六中五',
                'count'=>0
            ];
        }
        return $re;

    }

    /**
     * 任选/任选复试/七中五  (玩法)
     * 从01-11共11个号码中选择7个号码进行购买，只要当期顺序摇出的5个开奖号码中包含所选号码，即为中奖。
     * @pre_draw_code  开奖号码
     * @betting 购买号码
     * @data_num  所选号码 01,02,03,04,05,06
     */

    protected function rx_fs_7z5($param, $pre_draw_code)
    {
        $pre_draw_code = $this->formatNum($pre_draw_code,11,1);
        
        $data_num = $this->formatNum(explode(',',$param['data_num']),11,1);
        //取交集
        $jj = array_intersect($data_num,$pre_draw_code);
        if(count($jj) == 5){
            $re = [
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '任选/任选复试/七中五',
            ];


            $input = [];
            for ($i=1;$i<=count($data_num)-5;$i++){
                $input[] = $i;

            }
            $c= comb($input,2);
            $re['count'] = count($c);

        }else{
            $re = [
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '任选/任选复试/七中五',
                'count'=>0
            ];
        }
        return $re;

    }

    /**
     * 任选/任选复试/八中五  (玩法)
     * 从01-11共11个号码中选择8个号码进行购买，只要当期顺序摇出的5个开奖号码中包含所选号码，即为中奖。
     * @pre_draw_code  开奖号码
     * @betting_num 购买号码
     * @data_num  所选号码 01,02,03,04,05,06,7,8
     */

    protected function rx_fs_8z5($param, $pre_draw_code)
    {
        $pre_draw_code = $this->formatNum($pre_draw_code,11,1);
        
        $data_num = $this->formatNum(explode(',',$param['data_num']),11,1);
        //取交集
        $jj = array_intersect($data_num,$pre_draw_code);
        if(count($jj) == 5){
            $re = [
                'status' => 2,
                'remark' => '中奖',
                'play_name' => '任选/任选复试/八中五',
                'count'=>1
            ];
            $input = [];
            for ($i=1;$i<=count($data_num)-5;$i++){
                $input[] = $i;

            }
            $c= comb($input,3);
            $re['count'] = count($c);
        }else{
            $re = [
                'status' => 3,
                'remark' => '未中奖',
                'play_name' => '任选/任选复试/八中五',
                'count'=>0
            ];
        }
        return $re;


    }

}
