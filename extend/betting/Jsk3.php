<?php


namespace betting;

use betting\calculation\K3;

/**
 * 江苏快3 开奖
 * Class Jsk3
 * @package betting
 */
class Jsk3
{
    use K3;
    /**
     * 和值/和值/和值
     * 玩法介绍：和值是指对三个号码的和值进行投注包括“和值3”至“和值18”投注
     * 投注方案：如和值选6 开奖号码之和为6，即视为中奖
     * @data_num 2,3,4
     *
     */
    public function play_1711($param, $pre_draw_code)
    {
        return $this->hz($param,$pre_draw_code);
    }
    /**
     * 二同号/二同号/二同号单选
     * 玩法介绍：二同号单选是指对三个号码中两个指定的相同号码和一个指定的不同号码进行投注
     * 投注方案：投注号码为113，当期开奖结果为113，视为中奖，如果开奖号为三同号则不中奖
     * @data_num   (单号) 1,2,3,4
     * @double_num  （重复号) 1,2,3,4
     */
    public function play_1714($param, $pre_draw_code)
    {
        return $this->ertonghao_dx($param,$pre_draw_code);
    }
    /**
     * 二同号/二同号/二同号复选
     * 玩法介绍：二同号复选是指对三个号码中两个指定的相同号码和一个任意号码进行投注
     * 投注方案：投注号码为11？，当期开奖结果包含11，视为中奖，如果开奖号为三同号则不中奖
     * @double_num( 重复号) 1,2,3,4
     */
    public function play_1715($param, $pre_draw_code){
        return $this->ertonghao_fx($param,$pre_draw_code);
    }
    /**
     * /二不同号/二不同号/二不同号
     * 玩法介绍：二不同号投注是指对三个号码中两个指定的不同号码和一个任意号码进行投注
     * 投注方案：投注号码为1，2，当期开奖结果包含1，2，视为中奖。
     * @data_num 1,2,3,4
     */
    public function play_1719($param,$pre_draw_code){
        return $this->erbutonghao($param,$pre_draw_code);
    }
    /**
     * 三同号/三同号/三同号
     * 玩法介绍：三同号是指从所有相同的三个号码（111，222、…、666）中任意选择一组号码进行投注
     * 中奖举例：投注方案：投注号码为111，当期开奖结果为111，视为中奖。
     * @sc_num 三重号 1,2,3
     */
    public function play_1722($param,$pre_draw_code){
        return $this->three_th($param,$pre_draw_code);
    }
    /**
     * 三不同号/三不同号/标准选号
     * 玩法介绍：从1-6中任意选择3个（或以上）不相同号码组成一注，顺序不限，若其中三位与开奖号码相同即为中奖。
     * 中奖举例：投注方案：2,5,6；开奖号码中出现：1个2、1个5、1个6 (顺序不限)，即为中奖。
     * @data_num 1,2,3,4
     */
    public function play_1725($param,$pre_draw_code){
        return $this->three_bt_bz($param,$pre_draw_code);
    }

    /**
     * 三不同号/三不同号/手动输入
     * 玩法介绍：从1-6中任意选择3个（或以上）不相同号码组成一注，顺序不限，若其中三位与开奖号码相同即为中奖。
     * 投注方案：2,5,6；开奖号码中出现：1个2、1个5、1个6 (顺序不限)，即为中奖。
     * @data_num 123 321 456
     */
    public function play_1726($param,$pre_draw_code){
        return $this->three_bt_sd($param,$pre_draw_code);
    }
    /**
     * 三连号/三连号/三连号
     * 玩法介绍：开奖号码为三连号视为中奖
     * 投注方案：当期开奖号码中出现：123,234,345,456中的任意一组并且与选号一致即为中奖。
     * @data_num 123,234,345,456
     *
     */
    public function play_1729($param,$pre_draw_code){
        return $this->three_lh($param,$pre_draw_code);
    }
    /**
     * 单挑一骰/单挑一骰/单挑一骰
     * 玩法介绍：从1-6中任意选择1个号码组成一注，只要开奖号码出现所选号码，即为中奖。
     * 投注方案：当期开奖号码中包含选号即为中奖。
     * @data_num 1,2,3
     */
    public function play_1732($param,$pre_draw_code){
        return $this->dtys($param,$pre_draw_code);
    }
    /**
     * 格式化数据
     */
    public function formatNum($data, $max = 6, $min = 1)
    {

        foreach ($data as $k => $v) {
            if ($v > $max || $v < $min || $v == '') {
                unset($data[$k]);
            } else {
                $data[$k] = (int)$v;
            }
        }
        return $data;
    }



}