<?php


namespace lottery;
/**
 * 幸运飞艇
 * Class Xyft
 * @package lottery
 */

class Xyft
{
    /**
     * 获取下期开奖时间和期数
     *幸运飞艇 开奖规则：每日180期,从北京时间13:09分起,每5分钟一期。结束时间次日凌晨04:04
     */

    public function gettime()
    {

        $time = time()+60;
        $time = $time - (((13 * 60) + 9) * 60);
        $h = date('H', $time);
        $i = date('i', $time);
        $ss = (60 * $h) + $i;
        $c = $ss / 5;
        if ($c < 180) {
            $c = ceil($c) + 1;
            $c = sprintf("%03d", $c);
            $re['qishu'] = date('Ymd', $time) . $c;
            $re['kj_time'] = date('Y-m-d') . ' ' . date('H:i:s', strtotime(date('Y-m-d',$time)) + (($c - 1) * 5 * 60) + (13 * 60 * 60) + (9 * 60));

        } else {
            $re['qishu'] = date('Ymd',time()) . '001';
            $re['kj_time'] = date('Y-m-d',time()) . " 13:09:00";

        }
        $re['service_time']=date('Y-m-d H:i:s',time());
        return $re;
    }
    /**
     * 前一/前一/复试
     * 游戏玩法：冠军 投注的1个号码与开奖号码的第一位号
     * 如：第一位选择01，开奖号码为01,*,*,*,*,*,*,*,*,*即为中奖
     * 冠军 投注的1个号码与开奖号码的第一位号码相同且顺序一致，即为中奖
     * @data_num 1,2,3
     */
    public function bet_124($param)
    {
        
        
        $re_data = [
            'count' => 0,
            'play_name' => '前一/前一/复试'
        ];
        if (!isset($param['data_num'])) return $re_data;
        $data_num = explode(',', $param['data_num']);
        $data_num = $this->formatNum($data_num, 10, 1);
        $data_num = array_unique($data_num);
        $re_data = [
            'count' => count($data_num),
            'play_name' => '前一/前一/复试'
        ];
        return $re_data;
    }

    /**
     * 前二/前二/复试
     * 如：第一位选择01，第二位选择02.开奖号码为：01,02,*,*,*,*,*,*,*,*即为中奖
     * 冠军，亚军 投注的2个号码与开奖号码的第一位、第二位号码相同且顺序一致，即为中奖。
     * @one_num 1,2,3 (冠军)
     * @two_num 1,2,3 (亚军)
     */
    public function bet_127($param)
    {
        
        
        if (!isset($param['one_num']) || !isset($param['two_num'])) {
            return [
                'count' => 0,
                'play_name' => '前二/前二/复试'
            ];
        }
        //冠军号码格式化
        $one_num = explode(',', $param['one_num']);
        $one_num = $this->formatNum($one_num, 10, 1);
        $one_num = array_unique($one_num);
        //亚军号码格式化
        $two_num = explode(',', $param['two_num']);
        $two_num = $this->formatNum($two_num, 10, 1);
        $two_num = array_unique($two_num);
        $count = 0;
        if ($one_num) {
            foreach ($one_num as $k => $v) {
                if (in_array($v, $two_num)) {
                    $count += count($two_num) - 1;
                } else {
                    $count += count($two_num);
                }
            }
        }
        $re_data = [
            'count' => $count,
            'play_name' => '前二/前二/复试'
        ];
        return $re_data;
    }

    /**
     * 前二/前二/单式
     * 游戏玩法：手动输入号码，从01-10中任意输入2个号码（每个号码之间用,隔开 每注之间用;隔开）
     * 投注方案：0102； 开奖号码为：01,02,*,*,*,*,*,*,*,*即为中奖
     * 手动输入号码，从01-10中任意输入2个号码组成一注，所选号码与开奖号码的第一位、第二位相同，且顺序一致，即为中奖
     * @data_num 1,2,3 (冠军)
     */
    public function bet_128($param)
    {
        
        
        if (!isset($param['data_num'])) {
            return [
                'count' => 0,
                'play_name' => '前二/前二/单式'
            ];
        }
        $data_num = $param['data_num'];
        $data_num = explode(';', $data_num);
        $count = 0;
        foreach ($data_num as $k => $v) {
            $v = $this->formatNum(explode(',', $v), 10, 1);
            $v = array_unique($v);
            if (count($v) == 2) {
                $count++;
            }
        }
        return [
            'count' => $count,
            'play_name' => '前二/前二/单式'
        ];
    }

    /**
     * 定位胆/定位胆/定位胆
     * 冠军 如：第一位为01，开奖号码为01,*,*,*,*,*,*,*,*,*即为中奖亚军 如：第二位为02，开奖号码为*,02,*,*,*,*,*,*,*,*即为中奖季军 如：第三位为03，开奖号码为*,*,03,*,*,*,*,*,*,*即为中奖第四名 如：第四名位为04，开奖号码为*,*,*,04,*,*,*,*,*,*即为中奖第五名 如：第五名位为05，开奖号码为*,*,*,*,05,*,*,*,*,*即为中奖第六名 如：第六名位为06，开奖号码为*,*,*,*,*,06,*,*,*,*即为中奖第七名 如：第七名位为07，开奖号码为*,*,*,*,*,*,07,*,*,*即为中奖第八名 如：第八名位为08，开奖号码为*,*,*,*,*,*,*,08,*,*即为中奖第九名 如：第九名位为09，开奖号码为*,*,*,*,*,*,*,*,09,*即为中奖第十名 如：第十名位为10，开奖号码为*,*,*,*,*,*,*,*,*,10即为中奖
     * 冠军 从一号位置开始，选择最少一个、最多十个位置，任意1个位置或多个位置上选择1个号码，所选号码与相同位置上的开奖号码一致，即为中奖
     * @one_num 1,2,3 (冠军)
     * @two_num 1,2,3 (亚军)
     * @three_num 1,2,3 (季军)
     * @four_num 1,2,3 (第四名)
     * @five_num 1,2,3 (第5名)
     * @six_num 1,2,3 (第6名)
     * @seven_num 1,2,3 (第7名)
     * @eight_num 1,2,3 (第8名)
     * @nine_num 1,2,3 (第9名)
     * @ten_num 1,2,3 (第10名)
     */
    public function bet_1531($param)
    {

        $one_num = !isset($param['one_num']) ? [] : $this->formatNum(explode(',', $param['one_num']), 10, 1);
        $two_num =!isset($param['two_num'])? [] : $this->formatNum(explode(',', $param['two_num']), 10, 1);
        $three_num =!isset($param['three_num'])? [] : $this->formatNum(explode(',', $param['three_num']), 10, 1);
        $four_num =!isset($param['four_num'])? [] : $this->formatNum(explode(',', $param['four_num']), 10, 1);
        $five_num =!isset($param['five_num'])? [] : $this->formatNum(explode(',', $param['five_num']), 10, 1);
        $six_num =!isset($param['six_num'])? [] : $this->formatNum(explode(',', $param['six_num']), 10, 1);
        $seven_num =!isset($param['seven_num'])? [] : $this->formatNum(explode(',', $param['seven_num']), 10, 1);
        $eight_num =!isset($param['eight_num'])? [] : $this->formatNum(explode(',', $param['eight_num']), 10, 1);
        $nine_num =!isset($param['nine_num'])? [] : $this->formatNum(explode(',', $param['nine_num']), 10, 1);
        $ten_num =!isset($param['ten_num'])? [] : $this->formatNum(explode(',', $param['ten_num']), 10, 1);
        return [
            'count' => count($one_num) + count($two_num) + count($three_num) + count($four_num) + count($five_num) + count($six_num) + count($seven_num) + count($eight_num) + count($nine_num) + count($ten_num),
            'play_name' => '前二/前二/单式'
        ];

    }

    /**
     * 大小/大小/第一名
     * 玩法示意： 选择第一名车号大小为一注。
     * 选择的号码与开奖号码相对应，即为中奖，如第一位购买号码为大，开奖号码为大（6,7,8,9,10）即为中奖。
     * 选择第一名车号大小为一注。
     * @num_type  大,小
     */
    public function bet_142($param)
    {
        
        
        $re = [
            'count' => 0,
            'play_name' => '大小/大小/第一名'
        ];
        if (!isset($param['data_num']) || $param['data_num'] == '') return $re;

        $num_type = explode(',', $param['data_num']);
        if ($num_type) {
            foreach ($num_type as $k => $v) {
                if (!in_array($v, ['大', '小'])) {
                    unset($num_type[$k]);
                }
            }
        }
        $re['count'] = count($num_type);
        return $re;
    }

    /**
     * 大小/大小/第二名
     * 玩法示意： 选择第二名车号大小为一注。
     * 选择的号码与开奖号码相对应，即为中奖，如第二位购买号码为大，开奖号码为大（6,7,8,9,10）即为中奖。
     * 选择第二名车号大小为一注。
     * @data_num  大,小
     */
    public function bet_143($param)
    {
        
        
        $re = [
            'count' => 0,
            'play_name' => '大小/大小/第二名'
        ];
        if (!isset($param['data_num']) || $param['data_num']=='') return $re;

        $num_type = explode(',', $param['data_num']);
        if ($num_type) {
            foreach ($num_type as $k => $v) {
                if (!in_array($v, ['大', '小'])) {
                    unset($num_type[$k]);
                }
            }
        }
        $re['count'] = count($num_type);
        return $re;
    }

    /**
     * 大小/大小/第三名
     * 玩法示意： 选择第三名车号大小为一注。
     * 选择的号码与开奖号码相对应，即为中奖，如第三位购买号码为大，开奖号码为大（6,7,8,9,10）即为中奖。
     * 选择第三名车号大小为一注。
     * @data_num  大,小
     */
    public function bet_144($param)
    {
        
        
        $re = [
            'count' => 0,
            'play_name' => '大小/大小/第三名'
        ];
        if (!isset($param['data_num']) || $param['data_num']=='') return $re;

        $num_type = explode(',', $param['data_num']);
        if ($num_type) {
            foreach ($num_type as $k => $v) {
                if (!in_array($v, ['大', '小'])) {
                    unset($num_type[$k]);
                }
            }
        }
        $re['count'] = count($num_type);
        return $re;
    }

    /**
     * 单双/单双/第一名
     * 如：选择单，开奖号码的第一位为01或03或05或07或09，即为中奖
     * 选择第一名车号的单双，1 3 5 7 9为单，2 4 6 8 10为双，若相符即为中奖
     * @data_num 单,双
     */
    public function bet_1535($param)
    {
        
        
        $re = [
            'count' => 0,
            'play_name' => '单双/单双/第一名'
        ];
        if (!isset($param['data_num']) || $param['data_num'] == '') return $re;

        $num_type = explode(',', $param['data_num']);
        if ($num_type) {
            foreach ($num_type as $k => $v) {
                if (!in_array($v, ['单', '双'])) {
                    unset($num_type[$k]);
                }
            }
        }
        $re['count'] = count($num_type);
        return $re;
    }

    /**
     * 单双/单双/第二名
     * 如：选择单，开奖号码的第二位为01或03或05或07或09，即为中奖
     * 选择第二名车号的单双，1 3 5 7 9为单，2 4 6 8 10为双，若相符即为中奖
     * @data_num 单,双
     */
    public function bet_1536($param)
    {
        
        
        $re = [
            'count' => 0,
            'play_name' => '单双/单双/第二名'
        ];
        if (!isset($param['data_num']) || $param['data_num']=='') return $re;

        $num_type = explode(',', $param['data_num']);
        if ($num_type) {
            foreach ($num_type as $k => $v) {
                if (!in_array($v, ['单', '双'])) {
                    unset($num_type[$k]);
                }
            }
        }
        $re['count'] = count($num_type);
        return $re;
    }

    /**
     * 单双/单双/第三名
     * 如：选择单，开奖号码的第三位为01或03或05或07或09，即为中奖
     * 选择第三名车号的单双，1 3 5 7 9为单，2 4 6 8 10为双，若相符即为中奖
     * @data_num 单,双
     */
    public function bet_1537($param)
    {
        $re = [
            'count' => 0,
            'play_name' => '单双/单双/第三名'
        ];
        if (!isset($param['data_num']) || $param['data_num'] == '') return $re;

        $num_type = explode(',', $param['data_num']);
        if ($num_type) {
            foreach ($num_type as $k => $v) {
                if (!in_array($v, ['单', '双'])) {
                    unset($num_type[$k]);
                }
            }
        }
        $re['count'] = count($num_type);
        return $re;
    }

    /**
     * 龙虎/龙虎/第一名
     * 如：选择龙，开奖号码的第一位为03，第十位为01，即为中奖
     * 龙：冠军号码大于第十名号码视为“龙”中奖，如冠军开出07，第十名开出03。否则是虎。
     * @data_num 龙,虎
     */
    public function bet_1540($param)
    {
        
        
        $re = [
            'count' => 0,
            'play_name' => '龙虎/龙虎/第一名'
        ];
        if (!isset($param['data_num']) || $param['data_num'] == '') return $re;

        $num_type = explode(',', $param['data_num']);
        if ($num_type) {
            foreach ($num_type as $k => $v) {
                if (!in_array($v, ['龙', '虎'])) {
                    unset($num_type[$k]);
                }
            }
        }
        $re['count'] = count($num_type);
        return $re;
    }

    /**
     * 龙虎/龙虎/第二名
     * 如：选择龙，开奖号码的第二位为03，第九位为01，即为中奖
     * 龙：亚军号码大于第九名号码视为“龙”中奖，如亚军开出07，第九名开出03。否则是虎。
     * @data_num 龙,虎
     */
    public function bet_1541($param)
    {
        
        
        $re = [
            'count' => 0,
            'play_name' => '龙虎/龙虎/第二名'
        ];
        if (!isset($param['data_num']) || $param['data_num'] == '') return $re;

        $num_type = explode(',', $param['data_num']);
        if ($num_type) {
            foreach ($num_type as $k => $v) {
                if (!in_array($v, ['龙', '虎'])) {
                    unset($num_type[$k]);
                }
            }
        }
        $re['count'] = count($num_type);
        return $re;
    }

    /**
     * 龙虎/龙虎/第三名
     * 如：选择龙，开奖号码的第三位为03，第八位为01，即为中奖
     * 龙：季军号码大于第八名号码视为“龙”中奖，如季军开出07，第八名开出03。否则是虎。
     * @data_num 龙,虎
     */
    public function bet_1542($param)
    {
        $re = [
            'count' => 0,
            'play_name' => '龙虎/龙虎/第三名'
        ];
        if (!isset($param['data_num']) || $param['data_num'] == '') return $re;

        $num_type = explode(',', $param['data_num']);
        if ($num_type) {
            foreach ($num_type as $k => $v) {
                if (!in_array($v, ['龙', '虎'])) {
                    unset($num_type[$k]);
                }
            }
        }
        $re['count'] = count($num_type);
        return $re;
    }
    /**
     * 格式化数据
     */
    public function formatNum($data,$max=10,$min=1)
    {

        foreach ($data as $k => $v) {
            if ($v > $max || $v < $min ||$v == '') {
                unset($data[$k]);
            } else {
                $data[$k] = (int)$v;
            }
        }
        return $data;
    }

}