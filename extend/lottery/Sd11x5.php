<?php


namespace lottery;

use lottery\calculation\Cp11x5;

/**
 * 山东11选5
 * Class Sd11x5
 * @package lottery
 */
class Sd11x5
{
    use Cp11x5;
    /**
     * 获取开奖时间
     * 山东11选5 每日43期 09:00-23:00 20分钟一期
     */
    public function gettime()
    {
        $time = time()+180;

        $h = date('H', $time);
        $i = date('i', $time);
        $all_i = (int)($h*60 + $i);
        $re = [];
        if($all_i< 540){
            //9点之前
            $qishu = 1;
            $re['qishu'] = date('Ymd').sprintf("%03d", $qishu);
            $re['kj_time'] = date('Y-m-d',$time)." 09:00:00";
        }else if($all_i>=540 && $all_i<1380){
            $qishu = (int)(($all_i-540)/20);
            $qishu = $qishu + 2;
            $re['qishu'] = date('Ymd').sprintf("%03d", $qishu);
            $re['kj_time'] = date('Y-m-d H:i:s',strtotime(date('Y-m-d',$time))+(((($qishu-1)*20)+540)*60));
        }else if($all_i>=1380){

            $qishu = 1;
            $re['qishu'] = date('Ymd',strtotime("+1 day",$time)).sprintf("%03d", $qishu);

            $re['kj_time'] = date('Y-m-d',strtotime("+1 day",$time))." 09:30:00";
        }
        $re['service_time']=date('Y-m-d H:i:s',$time);
        return $re;
    }

    /**
     * 三码/前三直选/复式   (玩法)
     * 从01-11共11个号码中选择3个不重复的号码组成一注，所选号码与当期顺序摇出的5个号码中的前3个号码相同，且顺序一致，即为中奖。
     * @one_num  第一位好嘛 （01,02,03）
     * @two_num  第二位好嘛 （01,02,03）
     * @three_num  第三位好嘛 （01,02,03）
     */
    public function bet_1686($param)
    {
        return $this->q3_zhix_fs($param);
    }


    /**
     * 三码/前三直选/单式   (玩法)
     * 手动输入3个号码组成一注，所输入的号码与当期顺序摇出的5个号码中的前3个号码相同，且顺序一致，即为中奖。
     * @data_num  下注号码 01,02,03;02,01,03
     */
//    public function bet_5($param)
//    {
//        return $this->q3_zhix_ds($param);
//    }


    /**
     * 三码/前三组合/复式   (玩法)
     * 从01-11中共11个号码中选择3个号码，所选号码与当期顺序摇出的5个号码中的前3个号码相同，顺序不限，即为中奖。
     * @data_num  所选号码 （01,02,03,04）
     */

    public function bet_1688($param)
    {
        return $this->q3_zhux_fs($param);
    }

    /**
     * 三码/前三组合/单式   (玩法)
     * 手动输入3个号码组成一注，所输入的号码与当期顺序摇出的5个号码中的前3个号码相同，顺序不限，即为中奖。
     * @data_num  购买号码(1,2,3;1,2,4)
     */

//    public function bet_6($param)
//    {
//        return $this->q3_zhux_ds($param);
//    }

    /**
     * 二码/前二直选/复式   (玩法)
     * 从01-11共11个号码中选择2个不重复的号码组成一注，所选号码与当期顺序摇出的5个号码中的前2个号码相同，且顺序一致，即为中奖。
     * @one_num  第一位好嘛 （01,02,03）
     * @two_num  第二位好嘛 （01,02,03）
     */
    public function bet_1691($param)
    {

        return $this->q2_zhix_fs($param);
    }




    /**
     * 二码/前二直选/单式   (玩法)
     * 手动输入2个号码组成一注，所输入的号码与当期顺序摇出的5个号码中的前2个号码相同，且顺序一致，即为中奖。
     * @data_num  下注号码 01,02;02,01
     */
//    public function bet_12($param)
//    {
//        return $this->q2_zhix_ds($param);
//
//    }


    /**
     * 二码/前二组合/复式   (玩法)
     * 从01-11中共11个号码中选择2个号码，所选号码与当期顺序摇出的5个号码中的前2个号码相同，顺序不限，即为中奖。
     * @data_num  开奖号码(01,02,03,04)
     */

    public function bet_1693($param)
    {
        return $this->q2_zhux_fs($param);
    }

    /**
     * 二码/前二组合/单式   (玩法)
     * 手动输入2个号码组成一注，所输入的号码与当期顺序摇出的5个号码中的前2个号码相同，顺序不限，即为中奖。
     * @data_num  购买号码(1,2;1,4)
     */
//    public function bet_40($param)
//    {
//        return $this->q2_zhux_ds($param);
//    }

    /**
     * 不定胆/前三位   (玩法)
     * 从01-11中共11个号码中选择1个号码，每注由1个号码组成，只要当期顺序摇出的第一位、第二位、第三位开奖号码中包含所选号码，即为中奖。
     * @data_num  所选号码 01,02,03,02,04
     */

//    public function bet_15($param)
//    {
//        return $this->bdd_q3($param);
//    }

    /**
     * 定位胆/定位胆   (玩法)
     * 从第一位，第二位，第三位任意1个位置或多个位置上选择1个号码，所选号码与相同位置上的开奖号码一致，即为中奖。
     * @one_num  第一位好嘛 （01,02,03）
     * @two_num  第二位好嘛 （01,02,03）
     * @three_num  第三位好嘛 （01,02,03）
     */

    public function bet_1696($param)
    {

        return $this->dwd($param);
    }

    /**
     * 任选/任选复试/一中一  (玩法)
     * 从01-11共11个号码中选择1个号码进行购买，只要当期顺序摇出的5个开奖号码中包含所选号码，即为中奖。
     * @data_num  所选号码（01,02,03）
     */

    public function bet_1699($param)
    {
        return $this->rx_fs_1z1($param);
    }

    /**
     * 任选/任选复试/二中二  (玩法)
     * 从01-11共11个号码中选择2个号码进行购买，只要当期顺序摇出的5个开奖号码中包含所选号码，即为中奖。
     * @data_num  所选号码 01,02,03,04
     */

    public function bet_1700($param)
    {
        return $this->rx_fs_2z2($param);
    }

    /**
     * 任选/任选复试/三中三  (玩法)
     * 从01-11共11个号码中选择3个号码进行购买，只要当期顺序摇出的5个开奖号码中包含所选号码，即为中奖。
     * @data_num  所选号码 01,02,03,04
     */

    public function bet_1701($param)
    {
        return $this->rx_fs_3z3($param);
    }


    /**
     * 任选/任选复试/四中四  (玩法)
     * 从01-11共11个号码中选择4个号码进行购买，只要当期顺序摇出的5个开奖号码中包含所选号码，即为中奖。
     * @data_num  所选号码 01,02,03,04
     */

    public function bet_1702($param)
    {
        return $this->rx_fs_4z4($param);
    }

    /**
     * 任选/任选复试/五中五  (玩法)
     * 从01-11共11个号码中选择5个号码进行购买，只要当期顺序摇出的5个开奖号码中包含所选号码，即为中奖。
     * @data_num  所选号码 01,02,03,04,05
     */

    public function bet_1703($param)
    {

        return $this->rx_fs_5z5($param);

    }

    /**
     * 任选/任选复试/六中五  (玩法)
     * 从01-11共11个号码中选择6个号码进行购买，只要当期顺序摇出的5个开奖号码中包含所选号码，即为中奖。
     * @data_num  所选号码 01,02,03,04,05,06
     */

    public function bet_1704($param)
    {
        return $this->rx_fs_6z5($param);
    }

    /**
     * 任选/任选复试/七中五  (玩法)
     * 从01-11共11个号码中选择7个号码进行购买，只要当期顺序摇出的5个开奖号码中包含所选号码，即为中奖。
     * @data_num  所选号码 01,02,03,04,05,06,7
     */
    public function bet_1705($param)
    {
        return $this->rx_fs_7z5($param);
    }

    /**
     * 任选/任选复试/八中五  (玩法)
     * 从01-11共11个号码中选择8个号码进行购买，只要当期顺序摇出的5个开奖号码中包含所选号码，即为中奖。
     * @data_num  所选号码 01,02,03,04,05,06,7,8
     */

    public function bet_1706($param)
    {
        return $this->rx_fs_8z5($param);
    }

    /**
     * 任选/任选单式/一中一  (玩法)
     * 从01-11共11个号码中选择1个号码进行购买，只要当期顺序摇出的5个开奖号码中包含所选号码，即为中奖。
     * @data_num 所选号码 01,02,03
     */
//
//    public function bet_21($param)
//    {
//        return $this->rx_ds_1z1($param);
//    }

    /**
     * 任选/任选单式/二中二  (玩法)
     * 从01-11共11个号码中选择2个号码进行购买，只要当期顺序摇出的5个开奖号码中包含所选号码，即为中奖。
     * @data_num  所选号码 01,02;03,04
     */

//    public function bet_22($param)
//    {
//        return $this->rx_ds_2z2($param);
//    }

    /**
     * 任选/任选单式/三中三  (玩法)
     * 从01-11共11个号码中选择3个号码进行购买，只要当期顺序摇出的5个开奖号码中包含所选号码，即为中奖。
     * @data_num  所选号码 01,02,03;03,04,05
     */

//    public function bet_23($param)
//    {
//        return $this->rx_ds_3z3($param);
//    }

    /**
     * 任选/任选单式/四中四  (玩法)
     * 从01-11共11个号码中选择4个号码进行购买，只要当期顺序摇出的5个开奖号码中包含所选号码，即为中奖。
     * @data_num  所选号码 01,02,03,04;03,04,05,06
     */

//    public function bet_24($param)
//    {
//        return $this->rx_ds_4z4($param);
//    }

    /**
     * 任选/任选单式/五中五  (玩法)
     * 从01-11共11个号码中选择5个号码进行购买，只要当期顺序摇出的5个开奖号码中包含所选号码，即为中奖。
     * @data_num  所选号码 01,02,03,04,05;03,04,05,06,07
     */

//    public function bet_25($param)
//    {
//
//        return $this->rx_ds_5z5($param);
//    }

    /**
     * 任选/任选单式/六中五  (玩法)
     * 从01-11共11个号码中选择6个号码进行购买，只要当期顺序摇出的5个开奖号码中包含所选号码，即为中奖。
     * @data_num  所选号码 01,02,03,04,05,06;03,04,05,06,07,08
     */

//    public function bet_26($paly)
//    {
//        return $this->rx_ds_6z5($param);
//    }

    /**
     * 任选/任选单式/七中五  (玩法)
     * 从01-11共11个号码中选择7个号码进行购买，只要当期顺序摇出的5个开奖号码中包含所选号码，即为中奖。
     * @data_num  所选号码 01,02,03,04,05,06,07;03,04,05,06,07,08,09
     */

//    public function bet_27($param)
//    {
//        return $this->rx_ds_7z5($param);
//    }

    /**
     * 任选/任选单式/八中五  (玩法)
     * 从01-11共11个号码中选择8个号码进行购买，只要当期顺序摇出的5个开奖号码中包含所选号码，即为中奖。
     * @data_num  所选号码 01,02,03,04,05,06,07,08;03,04,05,06,07,08,09,10
     */

//    public function bet_28($param)
//    {
//        return $this->rx_ds_8z5($param);
//
//    }
    /**
     * 格式化数据
     */
    public function formatNum($data, $max = 11, $min = 1)
    {

        foreach ($data as $k => $v) {
            if ($v > $max || $v < $min || $v == '') {
                unset($data[$k]);
            } else {
                $data[$k] = (int)$v;
            }
        }
        return $data;
    }


}