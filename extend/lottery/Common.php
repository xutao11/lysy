<?php


namespace lottery;


class Common
{
    public function __construct()
    {
    }


    /**
     * 前四/前四直选/复式
     * 游戏玩法：从万、千、百、十位各选一个号码组成一注。
     * 投注方案：2345*；开奖号码：2345*
     * 投注玩法：从万位、千位、百位、十位中选择一个4位数号码组成一注，所选号码与开奖号码全部相同，且顺序一致，即为中奖
     * @one_num 1,2,3 (万位)
     * @two_num 1,2,3 (千位)
     * @three_num 1,2,3 (百位)
     * @four_num 1,2,3 (十位)
     */
    protected function qszxfs($param){
        $one_num = isset($param['one_num']) ? $param['one_num'] : '';
        $two_num = isset($param['two_num']) ? $param['two_num'] : '';
        $three_num = isset($param['three_num']) ? $param['three_num'] : '';
        $four_num = isset($param['four_num']) ? $param['four_num'] : '';
        $one_num = $one_num !== '' ? explode(',', $one_num) : [];
        $two_num = $two_num !== '' ? explode(',', $two_num) : [];
        $three_num = $three_num !== '' ? explode(',', $three_num) : [];
        $four_num = $four_num !== '' ? explode(',', $four_num) : [];
        $one_num = $this->formatNum($one_num);
        $two_num = $this->formatNum($two_num);
        $three_num = $this->formatNum($three_num);
        $four_num = $this->formatNum($four_num);

        $re_data = [
//            'data_num' => [],
            'count' => count($two_num)*count($three_num)*count($four_num)*count($one_num),
            'play_name' => '前四/前四直选/复式'
        ];
        return $re_data;


    }
    /**
     * 前四/前四直选/单式
     * 投注玩法：手动输入一个4位数号码组成一注，所选号码的万位、千位、百位、十位与开奖号码相同，且顺序一致，即为中奖（每注之间用 隔开）
     * 投注方案：2345*； 开奖号码：2345*，即中前四星直选一等奖
     * data_num 1234 2345
     */
    protected function qszxds($param){

        $re_data = [
            'count' => 0,
            'play_name' => '前四/前四直选/单式'
        ];
        if(!isset($param['data_num'])) return $re_data;
        $data_num = explode(' ',$param['data_num']);
        $data_num = array_unique($data_num);
        if(!$data_num) return $re_data;
        foreach ($data_num as $k=>$v){
            $v = str_split($v);
            if(count($v) != 4){
                unset($data_num[$k]);
            }
        }
        $re_data['count'] = count($data_num);
        return $re_data;
    }


    /**
     * 前四/前四直选/组选24
     * 游戏玩法：至少选择四个号码组成一注。
     * 投注方案：选择1234（展开为1234*，1243*，1324*，1342*，1423*，1432*...）,开奖为4321*，即为中奖
     * 投注玩法：前四组选24：选4个或多个，所选号码与开奖万、千、百、十一致顺序不限，即为中奖
     * @data_num 1,2,3,4
     */
    protected function qszx24($param){
        $re_data = [
            'count' => 0,
            'play_name' => '前四/前四直选/组选24'
        ];
        if(!isset($param['data_num'])) return $re_data;
        $data_num = $this->formatNum(explode(',',$param['data_num']));
        $data_num = array_unique($data_num);
        if(count($data_num)<4) return $re_data;
        $data_num_zh = getArrSet([$data_num,$data_num,$data_num,$data_num]);
        foreach ($data_num_zh as $k=>$v){
            if (count($v) != count(array_unique($v))) {
                unset($data_num_zh[$k]);
            }else{
                sort($v);
                $data_num_zh[$k] = implode(',',$v);
            }
        }
        $data_num_zh = array_unique($data_num_zh);
        $re_data['count'] = count($data_num_zh);
        return $re_data;
    }
    /**
     * 前四/前四直选/组选12
     * 投注玩法：从二重号中选择一个或多个，从单号中选择两个或多个，所选号码与开奖万、千、百、十一致顺序不限，即为中奖
     * 投注方案：如投注的二重号是1，单号是23，开奖为1123*，即为中奖
     * @data_num 5,6 (单号)
     * @double_num 5,6 (二重号)
     */
    protected function qszx12($param){
        $re_data = [
            'count' => 0,
            'play_name' => '前四/前四直选/组选12'
        ];
        if (!isset($param['double_num']) || !isset($param['data_num'])) return $re_data;

        $double_num = explode(',', $param['double_num']);
        $double_num = $this->formatNum($double_num);
        $double_num = array_unique($double_num);
        $data_num = $this->formatNum(explode(',', $param['data_num']));
        $data_num = array_unique($data_num);
        //参数不足
        if (count($double_num) == 0 || count($data_num) == 0) {
            return $re_data;
        }
        $data_num_zh = getArrSet([$data_num, $data_num]);
        foreach ($data_num_zh as $k => $v) {
            if (count($v) != count(array_unique($v))) {
                unset($data_num_zh[$k]);
            }
        }
        $data = [];
        foreach ($double_num as $k => $v) {
            foreach ($data_num_zh as $k1 => $v1) {
                if (!in_array($v, $v1)) {
                    sort($v1);
                    $data[$v][] = implode(',', $v1);
                }

            }
        }
        foreach ($data as $k => $v) {
            $re_data['count'] += count(array_unique($v));

        }
        return $re_data;
    }

    /**
     * 后四/后四直选/复式
     * 游戏玩法：从千、百、十、个位各选一个号码组成一注。
     * 投注方案：*2345；开奖号码：*2345
     * 投注玩法：从千位、百位、十位、个位中选择一个4位数号码组成一注，所选号码与开奖号码全部相同，且顺序一致，即为中奖
     * @two_num 1,2,3 (千位)
     * @three_num 1,2,3 (百位)
     * @four_num 1,2,3 (十位)
     *  @five_num 1,2,3 (个位)
     */
    protected function hszxfs($param){
        $two_num = isset($param['two_num']) ? $param['two_num'] : '';
        $three_num = isset($param['three_num']) ? $param['three_num'] : '';
        $four_num = isset($param['four_num']) ? $param['four_num'] : '';
        $five_num = isset($param['five_num']) ? $param['five_num'] : '';
        $five_num = $five_num !== '' ? explode(',', $five_num) : [];
        $two_num = $two_num !== '' ? explode(',', $two_num) : [];
        $three_num = $three_num !== '' ? explode(',', $three_num) : [];
        $four_num = $four_num !== '' ? explode(',', $four_num) : [];
        $five_num = $this->formatNum($five_num);
        $two_num = $this->formatNum($two_num);
        $three_num = $this->formatNum($three_num);
        $four_num = $this->formatNum($four_num);

        $re_data = [
            'count' => count($two_num)*count($three_num)*count($four_num)*count($five_num),
            'play_name' => '后四/后四直选/复式'
        ];
        return $re_data;


    }
    /**
     * 后四/后四直选/单式
     * 投注玩法：手动输入一个4位数号码组成一注，所选号码的从千位、百位、十位、个位与开奖号码相同，且顺序一致，即为中奖（每个号码之间用,隔开，每注之间用;隔开）
     * 投注方案：*2345； 开奖号码：*2345，即中前四星直选一等奖
     * data_num 1234 2345
     */
    protected function hszxds($param){
        $re_data = [
            'count' => 0,
            'play_name' => '后四/后四直选/单式'
        ];
        if(!isset($param['data_num'])) return $re_data;

        $data_num = explode(' ',$param['data_num']);
        $data_num = array_unique($data_num);

        if(!$data_num) return $re_data;
        foreach ($data_num as $k=>$v){
//            $v = explode(',',$v);
            $v = str_split($v);
            if(count($v) != 4){
                unset($data_num[$k]);
            }
        }
        $re_data['count'] = count($data_num);
        return $re_data;
    }

    /**
     * 后四/后四直选/组选24
     * 游戏玩法：至少选择四个号码组成一注。
     * 投注方案：选择1234（展开为*1234，*1243，*1324，*1342，*1423，*1432...）,开奖为*4321，即为中奖
     * 投注玩法：后四组选24：选4个或多个，所选号码与开奖千、百、十、个一致顺序不限，即为中奖
     * @data_num 1,2,3,4
     */
    protected function hszx24($param){
        $re_data = [
            'count' => 0,
            'play_name' => '后四/后四直选/组选24'
        ];
        if(!isset($param['data_num'])) return $re_data;

        $data_num = $this->formatNum(explode(',',$param['data_num']));
        $data_num = array_unique($data_num);
        if(count($data_num)<4) return $re_data;
        $data_num_zh = getArrSet([$data_num,$data_num,$data_num,$data_num]);
        foreach ($data_num_zh as $k=>$v){
            if (count($v) != count(array_unique($v))) {
                unset($data_num_zh[$k]);
            }else{
                sort($v);
                $data_num_zh[$k] = implode(',',$v);
            }
        }
        $data_num_zh = array_unique($data_num_zh);
        $re_data['count'] = count($data_num_zh);
        return $re_data;
    }
    /**
     * 后四/后四直选/组选12
     * 投注玩法：从二重号中选择一个或多个，从单号中选择两个或多个，所选号码与开奖千、百、十、个一致顺序不限，即为中奖
     * 投注方案：如投注的二重号是1，单号是23，开奖为*1123，即为中奖
     * @data_num 5,6 (单号)
     * @double_num 5,6 (二重号)
     */
    protected function hszx12($param){
        $re_data = [
            'count' => 0,
            'play_name' => '后四/后四直选/组选12'
        ];
        if (!isset($param['double_num']) || !isset($param['data_num'])) return $re_data;


        $double_num = explode(',', $param['double_num']);
        $double_num = $this->formatNum($double_num);
        $double_num = array_unique($double_num);
        $data_num = $this->formatNum(explode(',', $param['data_num']));
        $data_num = array_unique($data_num);
        //参数不足
        if (count($double_num) == 0 || count($data_num) == 0) {
            return $re_data;
        }
        $data_num_zh = getArrSet([$data_num, $data_num]);

        foreach ($data_num_zh as $k => $v) {
            if (count($v) != count(array_unique($v))) {
                unset($data_num_zh[$k]);
            }
        }
        $data = [];
        foreach ($double_num as $k => $v) {
            foreach ($data_num_zh as $k1 => $v1) {
                if (!in_array($v, $v1)) {
                    sort($v1);
                    $data[$v][] = implode(',', $v1);
                }

            }
        }
        foreach ($data as $k => $v) {
            $re_data['count'] += count(array_unique($v));

        }
        return $re_data;
    }
    /**
     * 前三/前三直选/复式
     * 游戏玩法：从万、千、百位各选一个号码组成一注。
     * 投注方案：234**；开奖号码：234**
     * 投注玩法：从万位、千位、百位中选择一个3位数号码组成一注，所选号码与开奖号码全部相同，且顺序一致，即为中奖
     * @one_num  第一位好嘛 （01,02,03）
     * @two_num  第二位好嘛 （01,02,03）
     * @three_num  第三位好嘛 （01,02,03）
     */
    protected function q3zxfs($param)
    {
        $re_data = [
            'count' => 0,
            'play_name' => '前三/前三直选/复式'
        ];
        if (!isset($param['one_num']) || !isset($param['two_num']) || !isset($param['three_num'])) return $re_data;
        if ($param['one_num']=='' || $param['two_num']=='' || $param['three_num']=='') return $re_data;
        //分割好嘛
        $one_num = explode(',', $param['one_num']);
        $two_num = explode(',', $param['two_num']);
        $three_num = explode(',', $param['three_num']);
        if (count($one_num) == 0 || count($two_num) == 0 || count($three_num) == 0) return $re_data;
        //格式化数据
        $one_num = $this->formatNum($one_num);
        $two_num = $this->formatNum($two_num);
        $three_num = $this->formatNum($three_num);
        //排列组合
        $data_num = getArrSet([$one_num, $two_num, $three_num]);
        foreach ($data_num as $k => $v) {
            $data_num[$k] = implode(',', $v);
        }
        $data_num = array_values($data_num);

        $re_data = [
            'count' => count($data_num),
            'play_name' => '前三/前三直选/复式'
        ];
        return $re_data;
    }
    /**
     * 前三/前三直选/单式   (玩法)
     * 投注玩法：手动输入一个3位数号码组成一注，所选号码的万、千、百与开奖号码相同，且顺序一致，即为中奖（每个号码之间用,隔开，每注之间用;隔开）
     * 投注方案：234**；开奖号码：234**
     * 投注玩法：手动输入一个3位数号码组成一注，所选号码的万、千、百位与开奖号码相同，且顺序一致，即为中奖
     * @data_num  下注号码 123 234
     */
    protected function q3zxds($param)
    {
        $re_data = [
            'count' => 0,
            'play_name' => '前三/前三直选/单式 '
        ];
        if ($param['data_num'] == '') return $re_data;
        //分割
        $data_num = explode(' ', $param['data_num']);
        foreach ($data_num as $k => $v) {
            if ($v == '') {
                unset($data_num[$k]);
            } else {
//                $dat = explode(',', $v);
                $dat = str_split($v);
                //位数不满住3位
                if (count($dat) != 3) {
                    unset($data_num[$k]);
                } else {
                    //所选之不能小于1  大于11
                    if (max($dat) > 9 || min($dat) < 0) {
                        unset($data_num[$k]);
                    } else {
                        foreach ($dat as $k_d => $v_d) {
                            //补0 成为2位数
                            $dat[$k_d] = (int)$v_d;
                        }
                        $data_num[$k] = implode(',', $dat);
                    }
                }
            }
        }
        $data_num = array_unique($data_num);
        $data_num = array_values($data_num);
        $re_data = [
            'count' => count($data_num),
            'play_name' => '前三/前三直选/单式 '
        ];
        return $re_data;
    }

    /**
     * 前三/前三组选 /组三
     * 游戏玩法：至少选择三个号码组成一注。
     * 投注方案：选择123（展开为123**，132**，231**，213**，312**，321**）,开奖为321**，即为中奖
     * 投注玩法：前三组六：选3个或多个，所选号码与开奖万、千、百一致顺序不限，即为中奖
     * @data_num   1,2,3
     */
    protected function q3zxzs($param)
    {
        $re_data = [
            'count' => 0,
            'play_name' => '前三/前三组选/组三 '
        ];
        if (!isset($param['data_num'])) return $re_data;
        $data_num = explode(',', $param['data_num']);
        foreach ($data_num as $k => $v) {
            if ($v > 9 || $v < 0) {
                unset($data_num[$k]);
            }
        }
        if (count($data_num) < 2) return $re_data;
        $data_new = [];
        $data_num_zh = getArrSet([$data_num, $data_num]);
        foreach ($data_num_zh as $k => $v) {
            if (count($v) != count(array_unique($v))) {
                unset($data_num_zh[$k]);
            } else {
                $data_new[] = implode(',', $v);
            }
        }
        $re_data = [
            'count' => count($data_new),
            'play_name' => '前三/前三组选/组三 '
        ];
        return $re_data;
    }
    /**
     * 前三/前三组选 /组六
     * 游戏玩法：至少选择三个号码组成一注。
     * 投注方案：选择123（展开为123**，132**，231**，213**，312**，321**）,开奖为321**，即为中奖
     * 投注玩法：前三组六：选3个或多个，所选号码与开奖万、千、百一致顺序不限，即为中奖
     * @data_num   1,2,3
     */
    protected function q3zxzl($param)
    {
        $re_data = [
            'count' => 0,
            'play_name' => '前三/前三组选/组六 '
        ];
        if (!isset($param['data_num'])) return $re_data;
        $data_num = explode(',', $param['data_num']);
        foreach ($data_num as $k => $v) {
            if ($v > 9 || $v < 0) {
                unset($data_num[$k]);
            }
        }
        if (count($data_num) < 2) return $re_data;
        $data_new = [];
        $data_num_zh = getArrSet([$data_num, $data_num, $data_num]);
        foreach ($data_num_zh as $k => $v) {
            if (count($v) != count(array_unique($v))) {
                unset($data_num_zh[$k]);
            } else {
                sort($v);
                $data_new[] = implode(',', $v);
            }
        }
        $data_new = array_unique($data_new);
        $data_new = array_values($data_new);
        $re_data = [
            'count' => count($data_new),
            'play_name' => '前三/前三组选/组六 '
        ];
        return $re_data;
    }

    /**
     * 中三/中三直选/复式
     *   游戏玩法：从千、百、十位各选一个号码组成一注。
     * 投注方案：234**；开奖号码：234**
     * 投注玩法：从千、百、十中选择一个3位数号码组成一注，所选号码与开奖号码全部相同，且顺序一致，即为中奖
     * @one_num  第一位好嘛 （01,02,03）
     * @two_num  第二位好嘛 （01,02,03）
     * @three_num  第三位好嘛 （01,02,03）
     */
    protected function z3zxfs($param)
    {
        $re_data = [
            'count' => 0,
            'play_name' => '中三/中三直选/复式'
        ];
        if (!isset($param['one_num']) || !isset($param['two_num']) || !isset($param['three_num'])) return $re_data;
        if ($param['one_num']=='' || $param['two_num']=='' || $param['three_num']=='') return $re_data;
        //分割好嘛
        $one_num = explode(',', $param['one_num']);
        $two_num = explode(',', $param['two_num']);
        $three_num = explode(',', $param['three_num']);
        if (count($one_num) == 0 || count($two_num) == 0 || count($three_num) == 0) return $re_data;
        //格式化数据
        $one_num = $this->formatNum($one_num);
        $two_num = $this->formatNum($two_num);
        $three_num = $this->formatNum($three_num);
        //排列组合
        $data_num = getArrSet([$one_num, $two_num, $three_num]);
        foreach ($data_num as $k => $v) {
            $data_num[$k] = implode(',', $v);
        }
        $data_num = array_values($data_num);

        $re_data = [
            'count' => count($data_num),
            'play_name' => '中三/中三直选/复式'
        ];
        return $re_data;
    }
    /**
     * 中三/中三直选/单式   (玩法)
     * 投注玩法：手动输入一个3位数号码组成一注，所选号码的万、千、百与开奖号码相同，且顺序一致，即为中奖
     * 投注方案：234**；开奖号码：234**
     * 投注玩法：手动输入一个3位数号码组成一注，所选号码的万、千、百位与开奖号码相同，且顺序一致，即为中奖
     * @data_num  下注号码 123 124
     */
    protected function z3zxds($param)
    {
        $re_data = [
            'count' => 0,
            'play_name' => '中三/中三直选/单式'
        ];
        if ($param['data_num'] == '') return $re_data;
        //分割
        $data_num = explode(' ', $param['data_num']);
        foreach ($data_num as $k => $v) {
            if ($v == '') {
                unset($data_num[$k]);
            } else {
//                $dat = explode('', $v);
                $dat = str_split($v);
                //位数不满住3位
                if (count($dat) != 3) {
                    unset($data_num[$k]);
                } else {
                    //所选之不能小于1  大于11
                    if (max($dat) > 9 || min($dat) < 0) {
                        unset($data_num[$k]);
                    } else {
                        foreach ($dat as $k_d => $v_d) {
                            //补0 成为2位数
                            $dat[$k_d] = (int)$v_d;
                        }
                        $data_num[$k] = implode(',', $dat);
                    }
                }
            }
        }
        $data_num = array_unique($data_num);
        $data_num = array_values($data_num);
        $re_data = [
            'count' => count($data_num),
            'play_name' => '中三/中三直选/单式'
        ];
        return $re_data;
    }
    /**
     * 中三/中三组选/组三
     * 游戏玩法：至少选择三个号码组成一注。
     * 投注方案：选择123（展开为*123*，*132*，*231*，*213*，*312*，*321*）,开奖为321**，即为中奖
     * 投注玩法：前三组六：选3个或多个，所选号码与开奖千、百、十一致顺序不限，即为中奖
     * @data_num   1,2,3
     */
    protected function z3zxz3($param)
    {
        $re_data = [
            'count' => 0,
            'play_name' => '中三/中三组选/组三'
        ];
        if (!isset($param['data_num'])) return $re_data;
        $data_num = explode(',', $param['data_num']);
        foreach ($data_num as $k => $v) {
            if ($v > 9 || $v < 0) {
                unset($data_num[$k]);
            }
        }
        if (count($data_num) < 2) return $re_data;
        $data_new = [];
        $data_num_zh = getArrSet([$data_num, $data_num]);
        foreach ($data_num_zh as $k => $v) {

            if (count($v) != count(array_unique($v))) {
                unset($data_num_zh[$k]);
            } else {
                $data_new[] = implode(',', $v);
            }
        }
        $re_data = [
            'count' => count($data_new),
            'play_name' => '中三/中三组选/组三'
        ];
        return $re_data;
    }
    /**
     * 中三/中三组选/组六
     * 游戏玩法：至少选择三个号码组成一注。
     *投注方案：选择12（展开为*122*，*212*，*221* 和 *112*、*121*、*211*）,开奖为*212* 或 *121*，即为中奖
     * 投注玩法：中三组三：选2个或多个，所选号码与开奖千、百、十位一致顺序不限，即为中奖
     * @data_num   1,2,3
     */
    protected function z3zxz6($param)
    {
        $re_data = [
            'count' => 0,
            'play_name' => '中三/中三组选/组六'
        ];
        if (!isset($param['data_num'])) return $re_data;
        $data_num = explode(',', $param['data_num']);
        foreach ($data_num as $k => $v) {
            if ($v > 9 || $v < 0) {
                unset($data_num[$k]);
            }
        }
        if (count($data_num) < 2) return $re_data;
        $data_new = [];
        $data_num_zh = getArrSet([$data_num, $data_num, $data_num]);
        foreach ($data_num_zh as $k => $v) {
            if (count($v) != count(array_unique($v))) {
                unset($data_num_zh[$k]);
            } else {
                sort($v);
                $data_new[] = implode(',', $v);
            }
        }
        $data_new = array_unique($data_new);
        $data_new = array_values($data_new);
        $re_data = [
            'count' => count($data_new),
            'play_name' => '中三/中三组选/组六'
        ];
        return $re_data;
    }
    /**
     * 后三/后三直选/复式
     * 游戏玩法：从百、十、个位各选一个号码组成一注。
     * 投注方案：**345；开奖号码：**345
     * 投注玩法：从百位、十位、个位中选择一个3位数号码组成一注，所选号码与开奖号码全部相同，且顺序一致，即为中奖
     * @one_num  第一位好嘛 （01,02,03）
     * @two_num  第二位好嘛 （01,02,03）
     * @three_num  第三位好嘛 （01,02,03）
     */
    protected function h3zxfs($param)
    {
        $re_data = [
            'count' => 0,
            'play_name' => '后三/后三直选/复式'
        ];
        if (!isset($param['one_num']) || !isset($param['two_num']) || !isset($param['three_num'])) return $re_data;
        if ($param['one_num']=='' || $param['two_num']=='' || $param['three_num']=='') return $re_data;
        //分割好嘛
        $one_num = explode(',', $param['one_num']);
        $two_num = explode(',', $param['two_num']);
        $three_num = explode(',', $param['three_num']);
        if (count($one_num) == 0 || count($two_num) == 0 || count($three_num) == 0) return $re_data;
        //格式化数据
        $one_num = $this->formatNum($one_num);
        $two_num = $this->formatNum($two_num);
        $three_num = $this->formatNum($three_num);
        //排列组合
        $data_num = getArrSet([$one_num, $two_num, $three_num]);
        foreach ($data_num as $k => $v) {
            $data_num[$k] = implode(',', $v);
        }
        $data_num = array_values($data_num);

        $re_data = [
            'count' => count($data_num),
            'play_name' => '后三/后三直选/复式'
        ];
        return $re_data;
    }
    /**
     * 后三/后三直选/单式   (玩法)
     * 投注玩法：手动输入一个3位数号码组成一注，所选号码的万、千、百与开奖号码相同，且顺序一致，即为中奖（每个号码之间用,隔开，每注之间用;隔开）
     * 投注方案：**345； 开奖号码：**345，即中后三星直选一等奖
     * 投注玩法：手动输入一个3位数号码组成一注，所选号码的百位、十位、个位与开奖号码相同，且顺序一致，即为中奖
     * @data_num  下注号码 01,02,03;02,01,03
     */
    protected function h3zxds($param)
    {
        $re_data = [
            'count' => 0,
            'play_name' => '后三/后三直选/单式'
        ];
        if ($param['data_num']=='') return $re_data;
        //分割
        $data_num = explode(' ', $param['data_num']);
        foreach ($data_num as $k => $v) {
            if ($v == '') {
                unset($data_num[$k]);
            } else {
//                $dat = explode(',', $v);
                $dat = str_split($v);
                //位数不满住3位
                if (count($dat) != 3) {
                    unset($data_num[$k]);
                } else {
                    //所选之不能小于1  大于11
                    if (max($dat) > 9 || min($dat) < 0) {
                        unset($data_num[$k]);
                    } else {
                        foreach ($dat as $k_d => $v_d) {
                            //补0 成为2位数
                            $dat[$k_d] = (int)$v_d;
                        }
                        $data_num[$k] = implode(',', $dat);
                    }
                }
            }
        }
        $data_num = array_unique($data_num);
        $data_num = array_values($data_num);
        $re_data = [
            'count' => count($data_num),
            'play_name' => '后三/后三直选/单式'
        ];
        return $re_data;
    }
    /**
     * 后三/后三组选/组三
     * 游戏玩法：至少选择三个号码组成一注。
     * 投注方案：选择12（展开为**122，**212，**221 和 **112、**121、**211）,开奖为**212 或 **121，即为中奖
     * 投注玩法：后三组三：选2个或多个，所选号码与开奖百、十、个位一致顺序不限，即为中奖
     * @data_num   1,2,3
     */
    protected function h3zxz3($param)
    {
        $re_data = [
            'count' => 0,
            'play_name' => '后三/后三组选/组三'
        ];
        if (!isset($param['data_num'])) return $re_data;
        $data_num = explode(',', $param['data_num']);
        foreach ($data_num as $k => $v) {
            if ($v > 9 || $v < 0) {
                unset($data_num[$k]);
            }
        }
        if (count($data_num) < 2) return $re_data;
        $data_new = [];
        $data_num_zh = getArrSet([$data_num, $data_num]);
        foreach ($data_num_zh as $k => $v) {
            if (count($v) != count(array_unique($v))) {
                unset($data_num_zh[$k]);
            } else {
                $data_new[] = implode(',', $v);
            }
        }
        $re_data = [
            'count' => count($data_new),
            'play_name' => '后三/后三组选/组三'
        ];
        return $re_data;
    }
    /**
     * 后三/后三组选/组六
     * 游戏玩法：至少选择三个号码组成一注。
     * 投注方案：选择123（展开为**123，**132，**231，**213，**312，**321）,开奖为**321，即为中奖
     * 投注玩法：后三组六：选3个或多个，所选号码与开奖百、十、个位一致顺序不限，即为中奖
     * @data_num   1,2,3
     */
    protected function h3zxz6($param)
    {
        $re_data = [
            'count' => 0,
            'play_name' => '后三/后三组选/组六'
        ];
        if (!isset($param['data_num'])) return $re_data;
        $data_num = explode(',', $param['data_num']);
        foreach ($data_num as $k => $v) {
            if ($v > 9 || $v < 0) {
                unset($data_num[$k]);
            }
        }
        if (count($data_num) < 2) return $re_data;
        $data_new = [];
        $data_num_zh = getArrSet([$data_num, $data_num, $data_num]);
        foreach ($data_num_zh as $k => $v) {
            if (count($v) != count(array_unique($v))) {
                unset($data_num_zh[$k]);
            } else {
                sort($v);
                $data_new[] = implode(',', $v);
            }
        }
        $data_new = array_unique($data_new);
        $data_new = array_values($data_new);
        $re_data = [
            'count' => count($data_new),
            'play_name' => '后三/后三组选/组六'
        ];
        return $re_data;
    }
    /**
     * 二星/直选/前二复式
     * 游戏玩法：从万、千位各选一个号码组成一注。
     * 投注方案：23***；开奖号码：23***
     * 投注玩法：从万位、千位中选择一个2位数号码组成一注，所选号码与开奖号码全部相同，且顺序一致，即为中奖
     * @one_num 1,2
     * @two_num 1,2
     */
    protected function qezxfs($param)
    {
        $re_data = [
            'count' => 0,
            'play_name' => '二星/直选/前二复式'
        ];
        if (!isset($param['one_num']) || !isset($param['two_num'])) return $re_data;
        if ($param['one_num']=='' || $param['two_num'] == '') return $re_data;
        //分割好嘛
        $one_num = explode(',', $param['one_num']);
        $two_num = explode(',', $param['two_num']);
        if (count($one_num) == 0 || count($two_num) == 0) return $re_data;
        //格式化数据
        $one_num = $this->formatNum($one_num);
        $two_num = $this->formatNum($two_num);
        $re_data = [
            'count' => count($one_num)*count($two_num),
            'play_name' => '二星/直选/前二复式'
        ];
        return $re_data;
    }
    /**
     * 二星/直选/前二单式
     * 游戏玩法：从万、千位各选一个号码组成一注。
     * 投注玩法：手动输入一个2位数号码组成一注，所选号码的万位、千位与开奖号码相同，且顺序一致，即为中奖（每个号码之间用,隔开，每注之间用;隔开）
     * 投注方案：23***； 开奖号码：23***，即中前二星直选一等奖
     * @data_num 01 23
     */
    protected function qezxds($param)
    {
        $re_data = [
            'count' => 0,
            'play_name' => '二星/直选/前二单式'
        ];
        if ($param['data_num'] == '') return $re_data;
        //分割
        $data_num = explode(' ', $param['data_num']);
        foreach ($data_num as $k => $v) {
            if ($v =='') {
                unset($data_num[$k]);
            } else {
                $dat = str_split($v);
                //位数不满住3位
                if (count($dat) != 2) {
                    unset($data_num[$k]);
                } else {
                    //所选之不能小于1  大于11
                    if (max($dat) > 9 || min($dat) < 0) {
                        unset($data_num[$k]);
                    } else {
                        foreach ($dat as $k_d => $v_d) {
                            //补0 成为2位数
                            $dat[$k_d] =(int) $v_d;
                        }
                        $data_num[$k] = implode(',', $dat);
                    }
                }
            }
        }
        $data_num = array_unique($data_num);
        $data_num = array_values($data_num);
        $re_data = [
            'count' => count($data_num),
            'play_name' => '二星/直选/前二单式'
        ];
        return $re_data;
    }
    /**
     * 二星/直选/后二复式
     * 游戏玩法：从十、个位各选一个号码组成一注
     * 投注方案：***23；开奖号码：***23
     * 投注玩法：从十位、个位中选择一个2位数号码组成一注，所选号码与开奖号码全部相同，且顺序一致，即为中奖
     * @four_num 1,2,3 (十位)
     *  @five_num 1,2,3 (个位)
     */
    protected function hezxfs($param)
    {
        $re_data = [
            'count' => 0,
            'play_name' => '二星/直选/后二复式'
        ];
        if (!isset($param['four_num']) || !isset($param['four_num'])) return $re_data;
        if ($param['five_num']=='' || $param['five_num'] == '') return $re_data;
        //分割好嘛
        $four_num = explode(',', $param['four_num']);
        $five_num = explode(',', $param['five_num']);
        if (count($four_num) == 0 || count($five_num) == 0) return $re_data;

        //格式化数据
        $four_num = $this->formatNum($four_num);
        $five_num = $this->formatNum($five_num);
        $re_data = [
            'count' => count($four_num)*count($five_num),
            'play_name' => '二星/直选/后二复式'
        ];
        return $re_data;
    }
    /**
     * 二星/直选/后二单式
     * 游戏玩法：从万、千位各选一个号码组成一注。
     * 投注玩法：手动输入一个2位数号码组成一注，所选号码的十位、个位与开奖号码相同，且顺序一致，即为中奖（每个号码之间用,隔开，每注之间用;隔开）
     * 投注方案：***23； 开奖号码：***23，即中后二星直选一等奖
     * @data_num 0,1;1,2
     */
    protected function hezxds($param)
    {
        $re_data = [
            'count' => 0,
            'play_name' => '二星/直选/后二单式'
        ];
        if ($param['data_num'] == '') return $re_data;
        //分割
        $data_num = explode(' ', $param['data_num']);
        foreach ($data_num as $k => $v) {
            if ($v == '') {
                unset($data_num[$k]);
            } else {
                $dat = str_split($v);
                //位数不满住3位
                if (count($dat) != 2) {
                    unset($data_num[$k]);
                } else {
                    //所选之不能小于1  大于11
                    if (max($dat) > 9 || min($dat) < 0) {
                        unset($data_num[$k]);
                    } else {
                        foreach ($dat as $k_d => $v_d) {
                            //补0 成为2位数
                            $dat[$k_d] = (int)$v_d;
                        }
                        $data_num[$k] = implode(',', $dat);
                    }
                }
            }
        }
        $data_num = array_unique($data_num);
        $data_num = array_values($data_num);
        $re_data = [
            'count' => count($data_num),
            'play_name' => '二星/直选/后二单式'
        ];
        return $re_data;
    }
    /**
     * 二星/组选/前二复式
     * 游戏玩法：至少选择两个号码组成一注。
     * 投注方案：选择12（展开为12***，21***）,开奖为12*** 或 21***，即为中奖
     * 投注玩法：前二组选复式：从0-9中选2个或多个号码对万位和千位投注，顺序不限，即为中奖
     * @data_num 1,2,3
     */
    protected function q2zhuxfs($param)
    {
        $re_data = [
            'count' => 0,
            'play_name' => '二星/组选/前二复式'
        ];
        if (!isset($param['data_num'])) return $re_data;
        //分割好嘛
        $data_num = explode(',', $param['data_num']);
        if (count($data_num) < 2 || count($data_num) > 10) return $re_data;
        //格式化数据
        foreach ($data_num as $k => $v) {
            //补0 成为2位数
            $data_num[$k] = (int)$v;
        }
        //去重
        $data_num = array_unique($data_num);
        $data_num = array_values($data_num);
        $re_num = numTree($data_num, 2);
        //数据格式化
        foreach ($re_num as $k => $v) {
            $re_num[$k] = implode(',', $v);
        }
        $re_data = [
            'count' => count($re_num),
            'play_name' => '二星/组选/前二复式'
        ];
        return $re_data;
    }
    /**
     * 二星/组选/前二单式
     * 投注玩法：前二组选单式：键盘手动输入购买号码，2个数字为一注，与开奖号码的万位、千位符合且顺序不限，即为中奖（每个号码之间用,隔开；每注之间用;隔开）
     * 投注方案：手动输入12，开奖号码为12*** 或 21*** 即为中奖
     * @data_num 12 23
     */
    protected function q2zhuxds($param)
    {
        $re_data = [
            'count' => 0,
            'play_name' => '二星/组选/前二单式'
        ];
        if (!isset($param['data_num'])) return $re_data;
        //分割好嘛
        $data_num = explode(' ', $param['data_num']);
        $re_num = [];
        foreach ($data_num as $k => $v) {
            if ($v == '') {
                unset($data_num[$k]);
            } else {
                $dat = str_split($v);
                //判断是否两位数字   或判断是否有重复值
                if (count($dat) != 2 || count($dat) != count(array_unique($dat)) || max($dat) > 9 || min($dat) < 0) {
                    // 判断个数 是否有重复值  所选号码 >0 <=11
                    unset($data_num[$k]);
                } else {
                    foreach ($dat as $k1 => $v1) {
                        $dat[$k1] = (int)$v1;
                    }
                    $re_num[] = $dat;
                }
            }
        }
        //判断所选号码是否重复(去除元素之相同的组合)
        foreach ($re_num as $k => $v) {
            foreach ($re_num as $k1 => $v1) {
                if ($k1 == $k) continue;
                $is_del = true;
                foreach ($v as $k3 => $v3) {
                    if (!in_array($v3, $v1)) {
                        $is_del = false;
                    }
                }
                if ($is_del) {
                    unset($re_num[$k]);
                }
            }
        }
        //组合数字以,连接
        foreach ($re_num as $k => $v) {
            $re_num[$k] = implode(',', $v);
        }
        $re_data = [
            'count' => count($re_num),
            'play_name' => '二星/组选/前二单式'
        ];
        return $re_data;
    }
    /**
     * 二星/组选/后二复式
     * 游戏玩法：至少选择两个号码组成一注。
     * 投注方案：选择12（展开为***12，***21）,开奖为***12 或 ***21，即为中奖
     * 投注玩法：后二组选复式：从0-9中选2个或多个号码对十位和个位投注，顺序不限，即为中奖
     * @data_num 1,2,3
     */
    protected function h2zhuxfs($param)
    {
        $re_data = [
            'count' => 0,
            'play_name' => '二星/组选/后二复式'
        ];
        if (!isset($param['data_num'])) return $re_data;
        //分割好嘛
        $data_num = explode(',', $param['data_num']);
        if (count($data_num) < 2 || count($data_num) > 10) return $re_data;
        //格式化数据
        foreach ($data_num as $k => $v) {
            //补0 成为2位数
            $data_num[$k] = (int)$v;
        }
        //去重
        $data_num = array_unique($data_num);
        $data_num = array_values($data_num);
        $re_num = numTree($data_num, 2);
        //数据格式化
        foreach ($re_num as $k => $v) {
            $re_num[$k] = implode(',', $v);
        }
        $re_data = [
            'count' => count($re_num),
            'play_name' => '二星/组选/后二复式'
        ];
        return $re_data;
    }
    /**
     * 二星/组选/后二单式
     * 投注玩法：后二组选单式：键盘手动输入购买号码，2个数字为一注，与开奖号码的十位、个位符合且顺序不限，即为中奖（每个号码之间用,隔开；每注之间用;隔开）
     * 投注方案：手动输入12，开奖号码为***12 或 ***21 即为中奖
     * @data_num 1,2;2,3
     */
    protected function h2zhuxds($param)
    {
        $re_data = [
            'count' => 0,
            'play_name' => '二星/组选/后二单式'
        ];
        if (!isset($param['data_num'])) return $re_data;
        //分割好嘛
        $data_num = explode(' ', $param['data_num']);
        $re_num = [];
        foreach ($data_num as $k => $v) {
            if ($v == '') {
                unset($data_num[$k]);
            } else {
                $dat = str_split($v);
                //判断是否两位数字   或判断是否有重复值
                if (count($dat) != 2 || count($dat) != count(array_unique($dat)) || max($dat) > 9 || min($dat) < 0) {
                    // 判断个数 是否有重复值  所选号码 >0 <=11
                    unset($data_num[$k]);
                } else {
                    foreach ($dat as $k1 => $v1) {
                        $dat[$k1] = (int)$v1;
                    }
                    $re_num[] = $dat;
                }
            }
        }
        //判断所选号码是否重复(去除元素之相同的组合)
        foreach ($re_num as $k => $v) {
            foreach ($re_num as $k1 => $v1) {
                if ($k1 == $k) continue;
                $is_del = true;
                foreach ($v as $k3 => $v3) {
                    if (!in_array($v3, $v1)) {
                        $is_del = false;
                    }
                }
                if ($is_del) {
                    unset($re_num[$k]);
                }
            }
        }
        //组合数字以,连接
        foreach ($re_num as $k => $v) {
            $re_num[$k] = implode(',', $v);
        }
        $re_data = [
            'count' => count($re_num),
            'play_name' => '二星/组选/后二单式'
        ];
        return $re_data;
    }
    /**
     * 定位胆/定位胆/定位胆
     * 投注玩法：从万位、千位、百位、十位、个位任意位置上选择1个或1个以上号码，所选号码与相同位置上的开奖号码一致，即为中奖
     * 投注方案：定万位为1，开奖号码为1****即为中奖
     * @one_num 1,2,3 (万位)
     * @two_num 1,2,3 (千位)
     * @three_num 1,2,3 (百位)
     * @four_num 1,2,3 (十位)
     * @five_num 1,2,3 (个位)
     *
     */
    protected function dwd($param){
        $one_num = isset($param['one_num']) ? $param['one_num'] : '';
        $two_num = isset($param['two_num']) ? $param['two_num'] : '';
        $three_num = isset($param['three_num']) ? $param['three_num'] : '';
        $four_num = isset($param['four_num']) ? $param['four_num'] : '';
        $five_num = isset($param['five_num']) ? $param['five_num'] : '';
        $one_num = $one_num !== '' ? explode(',', $one_num) : '';
        $two_num = $two_num !== '' ? explode(',', $two_num) : '';
        $three_num = $three_num !== '' ? explode(',', $three_num) : '';
        $four_num = $four_num !== '' ? explode(',', $four_num) : '';
        $five_num = $five_num !== '' ? explode(',', $five_num) : '';
//        $num_ys = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
//        $data_num_zh = getArrSet([$num_ys, $num_ys, $num_ys, $num_ys, $num_ys]);
//        $data_num = [];
//        foreach ($data_num_zh as $k => $v) {
//            if ($one_num != '' && in_array($v[0], $one_num)) {
//                $data_num[] = implode(',', $v);
//            } else if ($two_num != '' && in_array($v[1], $two_num)) {
//                $data_num[] = implode(',', $v);
//            } else if ($three_num != '' && in_array($v[2], $three_num)) {
//                $data_num[] = implode(',', $v);
//            } else if ($four_num != '' && in_array($v[3], $four_num)) {
//                $data_num[] = implode(',', $v);
//            } else if ($five_num != '' && in_array($v[4], $five_num)) {
//                $data_num[] = implode(',', $v);
//            }
//        }
        $count = 0;
        if ($one_num) {
            $count += count($one_num);
        }
        if ($two_num) {
            $count += count($two_num);
        }
        if ($three_num) {
            $count += count($three_num);
        }
        if ($four_num) {
            $count += count($four_num);
        }
        if ($five_num) {
            $count += count($five_num);
        }

        $re_data = [
//            'data_num' => $data_num,
            'count' => $count,
            'play_name' => '定位胆/定位胆/定位胆'
        ];
        return $re_data;
    }
    /**
     * 不定位/不定位/前三一码
     * 游戏玩法：至少选择一个号码组成一注。
     * 投注方案：选择不定位1，开出1****、*1***、**1**即为中奖
     * 投注玩法：从0-9中选择1个号码，每注由1个号码组成，只要开奖号码的万位、千位、百位中包含所选号码，即为中奖
     * @data_num 1,2,3
     */
    protected function bddq3ym($param){
        $re_data = [
            'count' => 0,
            'play_name' => '不定位/不定位/前三一码'
        ];
        if (!isset($param['data_num'])) return $re_data;
        $data_num = explode(',', $param['data_num']);
        $data_num = array_unique($this->formatNum($data_num));
        $re_data = [
            'count' => count($data_num),
            'play_name' => '不定位/不定位/前三一码'
        ];
        return $re_data;
    }
    /**
     * 不定位/不定位/中三一码
     * 游戏玩法：至少选择一个号码组成一注。
     * 投注方案：选择不定位1，开出*1***、**1**、***1*即为中奖
     * 投注玩法：从0-9中选择1个号码，每注由1个号码组成，只要开奖号码的千位、百位、十位中包含所选号码，即为中奖
     * @data_num 1,2,3
     */
    protected function bddz3ym($param){
        $re_data = [
            'count' => 0,
            'play_name' => '不定位/不定位/中三一码'
        ];
        if (!isset($param['data_num'])) return $re_data;
        $data_num = explode(',', $param['data_num']);
        $data_num = array_unique($this->formatNum($data_num));
        $re_data = [
            'count' => count($data_num),
            'play_name' => '不定位/不定位/中三一码'
        ];
        return $re_data;
    }
    /**
     * 不定位/不定位/后三一码
     * 游戏玩法：至少选择一个号码组成一注。
     * 投注方案：选择不定位1，开出****1、***1*、**1**即为中奖
     * 投注玩法：从0-9中选择1个号码，每注由1个号码组成，只要开奖号码的百位、十位、个位中包含所选号码，即为中奖
     * @data_num 1,2,3
     */
    protected function bddh3ym($param){
        $re_data = [
            'count' => 0,
            'play_name' => '不定位/不定位/后三一码'
        ];
        if (!isset($param['data_num'])) return $re_data;
        $data_num = explode(',', $param['data_num']);
        $data_num = array_unique($this->formatNum($data_num));
        $re_data = [
            'count' => count($data_num),
            'play_name' => '不定位/不定位/后三一码'
        ];
        return $re_data;
    }
    /**
     * 不定位/不定位/五星一码
     * 游戏玩法：至少选择一个号码组成一注。
     * 投注方案：例如投注3，开奖号34535，中一注。（出现一次和出现两次都只算中一注）
     * 投注玩法：每注号码由一个数字组成，只要开奖号码任意一位出现了投注的号码，即为中奖
     * @data_num 1,2,3
     */
    protected function bddwxym($param){
        $re_data = [
            'count' => 0,
            'play_name' => '不定位/不定位/五星一码'
        ];
        if (!isset($param['data_num'])) return $re_data;
        $data_num = explode(',', $param['data_num']);
        $data_num = array_unique($this->formatNum($data_num));
        $re_data = [
            'count' => count($data_num),
            'play_name' => '不定位/不定位/五星一码'
        ];
        return $re_data;
    }
    /**
     * 不定位/不定位/五星二码
     * 游戏玩法：至少选择两个号码组成一注。
     * 投注方案：例如投注36，开奖号码32468，中一注。
     * 投注玩法：每注号码由两个数字组成，只要开奖号码包含投注号码即为中奖
     * @data_num 1,2,3
     */
    protected function bddwx2m($param){
        $re_data = [
            'count' => 0,
            'play_name' => '不定位/不定位/五星二码'
        ];
        if (!isset($param['data_num'])) return $re_data;
        $data_num = explode(',', $param['data_num']);
        $data_num = array_unique($this->formatNum($data_num));
        $count = (count($data_num) -1) /2*count($data_num);

//        $re_num = numTree($data_num, 2);
        $re_data = [
            'count' => $count,
            'play_name' => '不定位/不定位/五星二码'
        ];
        return $re_data;
    }
    /**
     * 不定位/不定位/五星三码
     * 游戏玩法：至少选择三个号码组成一注。
     * 投注方案：例如投注368，开奖号码32468，中一注。
     * 投注玩法：每注号码由三个数字组成，只要开奖号码中包含投注号码即为中奖
     * @data_num 1,2,3
     */
    protected function bddwx3m($param){
        $re_data = [
            'count' => 0,
            'play_name' => '不定位/不定位/五星三码'
        ];
        if (!isset($param['data_num'])) return $re_data;
        $data_num = explode(',', $param['data_num']);
        $data_num = array_unique($this->formatNum($data_num));
        $re_num = numTree($data_num, 3);
        //数据格式化
        foreach ($re_num as $k => $v) {
            $re_num[$k] = implode(',', $v);
        }
        $re_data = [
            'count' => count($re_num),
            'play_name' => '不定位/不定位/五星三码'
        ];
        return $re_data;
    }
    /**
     * 跨度/跨度/前三跨度
     * 游戏玩法：至少选择一个号码组成一注
     * 投注方案：选择5, 等于开奖号前三位2,5,7的最大数7与最小数字2的差值，即为中奖
     * 投注玩法：选择0-9，若所选号码与开奖号前三位的最大最小数字的差值相等，即为中奖
     * @data_num 1,2,3
     */
    protected function kdq3($param){
        $re_data = [
            'count' => 0,
            'play_name' => '跨度/跨度/前三跨度'
        ];
        if (!isset($param['data_num'])) return $re_data;
        $data_num = explode(',', $param['data_num']);
        $data_num = array_unique($this->formatNum($data_num));
        $data_num_zh = getArrSet([[0, 1, 2, 3, 4, 5, 6, 7, 8, 9], [0, 1, 2, 3, 4, 5, 6, 7, 8, 9], [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]]);
        foreach ($data_num_zh as $k => $v) {
            $kd = max($v) - min($v);
            if (in_array($kd, $data_num)) {
                $data[] = implode(',', $v);
            }
        }
        $re_data = [
            'count' => count($data),
            'play_name' => '跨度/跨度/前三跨度'
        ];
        return $re_data;

    }
    /**
     * 跨度/跨度/中三跨度
     * 游戏玩法：至少选择一个号码组成一注。
     * 投注方案：选择5, 等于开奖号中三位2,5,7的最大数7与最小数字2的差值，即为中奖
     * 投注玩法：选择0-9，若所选号码与开奖号中三位的最大最小数字的差值相等，即为中奖
     * @data_num 1,2,3
     */
    protected function kdz3($param){
        $re_data = [
            'count' => 0,
            'play_name' => '跨度/跨度/中三跨度'
        ];
        if (!isset($param['data_num'])) return $re_data;
        $data_num = explode(',', $param['data_num']);
        $data_num = array_unique($this->formatNum($data_num));
        $data_num_zh = getArrSet([[0, 1, 2, 3, 4, 5, 6, 7, 8, 9], [0, 1, 2, 3, 4, 5, 6, 7, 8, 9], [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]]);
        foreach ($data_num_zh as $k => $v) {
            $kd = max($v) - min($v);
            if (in_array($kd, $data_num)) {
                $data[] = implode(',', $v);
            }
        }
        $re_data = [
            'count' => count($data),
            'play_name' => '跨度/跨度/中三跨度'
        ];
        return $re_data;

    }
    /**
     * 跨度/跨度/后三跨度
     * 游戏玩法：至少选择一个号码组成一注。
     * 投注方案：选择5, 等于开奖号后三位2,5,7的最大数7与最小数字2的差值，即为中奖
     * 投注玩法：选择0-9，若所选号码与开奖号后三位的最大最小数字的差值相等，即为中奖
     * @data_num 1,2,3
     */
    protected function kdh3($param){
        $re_data = [
            'count' => 0,
            'play_name' => '跨度/跨度/后三跨度'
        ];
        if (!isset($param['data_num'])) return $re_data;
        $data_num = explode(',', $param['data_num']);
        $data_num = array_unique($this->formatNum($data_num));
        $data_num_zh = getArrSet([[0, 1, 2, 3, 4, 5, 6, 7, 8, 9], [0, 1, 2, 3, 4, 5, 6, 7, 8, 9], [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]]);
        foreach ($data_num_zh as $k => $v) {
            $kd = max($v) - min($v);
            if (in_array($kd, $data_num)) {
                $data[] = implode(',', $v);
            }
        }
        $re_data = [
            'count' => count($data),
            'play_name' => '跨度/跨度/后三跨度'
        ];
        return $re_data;

    }

    /**
     * 跨度/跨度/前二跨度
     * 游戏玩法：至少选择一个号码组成一注。
     * 投注方案：选择5, 等于开奖号前二位2,7的最大数7与最小数字2的差值，即为中奖
     * 投注玩法：选择0-9，若所选号码与开奖号前二位的最大最小数字的差值相等，即为中奖
     * @data_num 1,2,3
     */
    protected function kdq2($param){
        $re_data = [
            'count' => 0,
            'play_name' => '跨度/跨度/前二跨度'
        ];
        if (!isset($param['data_num'])) return $re_data;
        $data_num = explode(',', $param['data_num']);
        $data_num = array_unique($this->formatNum($data_num));
        $data_num_zh = getArrSet([[0, 1, 2, 3, 4, 5, 6, 7, 8, 9], [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]]);
        foreach ($data_num_zh as $k => $v) {
            $kd = max($v) - min($v);
            if (in_array($kd, $data_num)) {
                $data[] = implode(',', $v);
            }
        }
        $re_data = [
            'count' => count($data),
            'play_name' => '跨度/跨度/前二跨度'
        ];
        return $re_data;

    }
    /**
     * 跨度/跨度/后二跨度
     * 游戏玩法：至少选择一个号码组成一注。
     * 投注方案：选择5, 等于开奖号后二位2,7的最大数7与最小数字2的差值，即为中奖
     * 投注玩法：选择0-9，若所选号码与开奖号后二位的最大最小数字的差值相等，即为中奖
     * @data_num 1,2,3
     */
    protected function kdh2($param){
        $re_data = [
            'count' => 0,
            'play_name' => '跨度/跨度/后二跨度'
        ];
        if (!isset($param['data_num'])) return $re_data;
        $data_num = explode(',', $param['data_num']);
        $data_num = array_unique($this->formatNum($data_num));
        $data_num_zh = getArrSet([[0, 1, 2, 3, 4, 5, 6, 7, 8, 9], [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]]);
        foreach ($data_num_zh as $k => $v) {
            $kd = max($v) - min($v);
            if (in_array($kd, $data_num)) {
                $data[] = implode(',', $v);
            }
        }
        $re_data = [
            'count' => count($data),
            'play_name' => '跨度/跨度/后二跨度'
        ];
        return $re_data;
    }
    /**
     * 任选/任四/复式
     * 游戏玩法：从万、千、百、十、个任四位选一个号码组
     * 投注方案：2*456；开奖号码：2*456
     * 投注玩法：任选四位与开奖号分别对应相同且顺序一致即为中奖
     * @one_num 1,2,3 (万位)
     * @two_num 1,2,3 (千位)
     * @three_num 1,2,3 (百位)
     * @four_num 1,2,3 (十位)
     * @five_num 1,2,3 (个位)
     */
    protected function r4fs($param){
        $re_data = [
            'count' => 0,
            'play_name' => '任选/任四/复式'
        ];
        $one_num = isset($param['one_num']) ? $param['one_num'] : [];
        $two_num = isset($param['two_num']) ? $param['two_num'] : [];
        $three_num = isset($param['three_num']) ? $param['three_num'] : [];
        $four_num = isset($param['four_num']) ? $param['four_num'] : [];
        $five_num = isset($param['five_num']) ? $param['five_num'] : [];
        $one_num = !is_array($one_num) ? explode(',', $one_num) : [];
        $two_num = !is_array($two_num) ? explode(',', $two_num) : [];
        $three_num = !is_array($three_num) ? explode(',', $three_num) : [];
        $four_num = !is_array($four_num) ? explode(',', $four_num) : [];
        $five_num = !is_array($five_num) ? explode(',', $five_num) : [];
        $data = [
            $this->formatNum($one_num),
            $this->formatNum($two_num),
            $this->formatNum($three_num),
            $this->formatNum($four_num),
            $this->formatNum($five_num),
        ];
        $address_all = [];
        if (count($data[0]) > 0) {
            $address_all[] = '万';
        }
        if (count($data[1]) > 0) {
            $address_all[] = '千';
        }
        if (count($data[2]) > 0) {
            $address_all[] = '百';
        }
        if (count($data[3]) > 0) {
            $address_all[] = '十';
        }
        if (count($data[4]) > 0) {
            $address_all[] = '个';
        }
        if (count($address_all) < 4) return $re_data;
        $data_num_zh = getArrSet([$address_all, $address_all, $address_all,$address_all]);
        foreach ($data_num_zh as $k => $v) {
            sort($v);
            if (count(array_unique($v)) != 4) {
                unset($data_num_zh[$k]);
            } else {
                $data_num_zh[$k] = implode(',', $v);
            }
        }
        $data_num_zh = array_unique($data_num_zh);
        $data_num_zh = array_values($data_num_zh);
        $all_count = 0;
        if ($data_num_zh) {
            foreach ($data_num_zh as $k => $v) {
                $v = explode(',', $v);
                $f_count = 1;
                foreach ($v as $k1 => $v1) {
                    $count = 0;
                    switch ($v1) {
                        case "万":
                            $count = count($data[0]);
                            break;
                        case "千":
                            $count = count($data[1]);
                            break;
                        case "百":
                            $count = count($data[2]);
                            break;
                        case "十":
                            $count = count($data[3]);
                            break;
                        case "个":
                            $count = count($data[4]);
                            break;
                    }
                    $f_count *= $count;
                }
                $all_count += $f_count;
            }
        }

        $re_data = [
            'count' => $all_count,
            'play_name' => '任选/任四/复式'
        ];
        return $re_data;
    }
    /**
     * 任选/任三/复式
     * 游戏玩法：从万、千、百、十、个任三位选一个号码组
     * 投注方案：2*4*6；开奖号码：2*4*6
     * 投注玩法：任选三位与开奖号分别对应相同且顺序一致即为中奖
     * @one_num 1,2,3 (万位)
     * @two_num 1,2,3 (千位)
     * @three_num 1,2,3 (百位)
     * @four_num 1,2,3 (十位)
     * @five_num 1,2,3 (个位)
     */
    protected function r3fs($param)
    {
        $re_data = [
            'count' => 0,
            'play_name' => '任选/任三/复式'
        ];
        $one_num = isset($param['one_num']) ? $param['one_num'] : [];
        $two_num = isset($param['two_num']) ? $param['two_num'] : [];
        $three_num = isset($param['three_num']) ? $param['three_num'] : [];
        $four_num = isset($param['four_num']) ? $param['four_num'] : [];
        $five_num = isset($param['five_num']) ? $param['five_num'] : [];
        $one_num = !is_array($one_num) ? explode(',', $one_num) : [];
        $two_num = !is_array($two_num) ? explode(',', $two_num) : [];
        $three_num = !is_array($three_num) ? explode(',', $three_num) : [];
        $four_num = !is_array($four_num) ? explode(',', $four_num) : [];
        $five_num = !is_array($five_num) ? explode(',', $five_num) : [];

        $data = [
            $this->formatNum($one_num),
            $this->formatNum($two_num),
            $this->formatNum($three_num),
            $this->formatNum($four_num),
            $this->formatNum($five_num),
        ];
        $address_all = [];
        if (count($data[0]) > 0) {
            $address_all[] = '万';
        }
        if (count($data[1]) > 0) {
            $address_all[] = '千';
        }
        if (count($data[2]) > 0) {
            $address_all[] = '百';
        }
        if (count($data[3]) > 0) {
            $address_all[] = '十';
        }
        if (count($data[4]) > 0) {
            $address_all[] = '个';
        }
        if (count($address_all) < 3) return $re_data;
        $data_num_zh = getArrSet([$address_all, $address_all, $address_all]);
        foreach ($data_num_zh as $k => $v) {
            sort($v);
            if (count(array_unique($v)) != 3) {
                unset($data_num_zh[$k]);
            } else {
                $data_num_zh[$k] = implode(',', $v);
            }
        }
        $data_num_zh = array_unique($data_num_zh);
        $data_num_zh = array_values($data_num_zh);
        $all_count = 0;
        if ($data_num_zh) {
            foreach ($data_num_zh as $k => $v) {
                $v = explode(',', $v);
                $f_count = 1;
                foreach ($v as $k1 => $v1) {
                    $count = 0;
                    switch ($v1) {
                        case "万":
                            $count = count($data[0]);
                            break;
                        case "千":
                            $count = count($data[1]);
                            break;
                        case "百":
                            $count = count($data[2]);
                            break;
                        case "十":
                            $count = count($data[3]);
                            break;
                        case "个":
                            $count = count($data[4]);
                            break;
                    }
                    $f_count *= $count;
                }
                $all_count += $f_count;
            }
        }

        $re_data = [
            'count' => $all_count,
            'play_name' => '任选/任三/复式'
        ];
        return $re_data;


    }
    /**
     * 任选/任三/单式
     * 游戏玩法：从万、千、百、十、个任三位选一个号码组(每个号码之间用,隔开，每注之间用;隔开)
     * 投注方案：**345； 选择对应位：百、十、个，开奖号码：**345，即为中奖
     * 投注玩法：手动输入一个3位数号码组成一注，所选号码与开奖号码的对应位相同，且顺序一致，即为中奖
     * @data_address 万,千,百,十,个
     * @data_num 123 456
     */
    protected function r3ds($param)
    {
        $re_data = [
            'count' => 0,
            'play_name' => '任选/任三/单式'
        ];
        $address_all = ['万', '千', '百', '十', '个'];
        $data_num = $param['data_num'];
        $data_address = $param['data_address'];
        //号码
        $data_num = explode(' ', $data_num);
        if ($data_num) {
            foreach ($data_num as $k => $v) {
//                $r_v = explode(',', $v);
                $r_v = str_split($v);
                if (count($r_v) != 3 || max($r_v) > 9 || min($r_v) < 0) {
                    unset($data_num[$k]);
                }
            }
        }
        $data_num = array_unique($data_num);
        //位置
        $data_address = explode(',', $data_address);
        if ($data_address) {
            foreach ($data_address as $k => $v) {
                if (!in_array($v, $address_all)) {
                    unset($data_address[$k]);
                }
            }
        }
        $data_address = array_unique($data_address);
        if (count($data_address) < 3) return $re_data;


        $data_num_zh = getArrSet([$data_address, $data_address, $data_address]);
        foreach ($data_num_zh as $k => $v) {
            sort($v);
            if (count(array_unique($v)) != 3) {
                unset($data_num_zh[$k]);
            } else {
                $data_num_zh[$k] = implode(',', $v);
            }
        }
        $data_num_zh = array_unique($data_num_zh);
        $data_num_zh = array_values($data_num_zh);
        $re_data = [
            'count' => count($data_num_zh) * count($data_num),
            'play_name' => '任选/任三/单式'
        ];
        return $re_data;
    }
    /**
     * 任选/任三/组三
     * 游戏玩法：至少选择两个号码组成一注，并选择三个对
     * 投注方案：选择12（展开为**122，**212，**221 和 **112、**121、**211）,开奖为**212 或 **121，即为中奖
     * 投注玩法：0-9中任意选择2个号码组成两注，所选号码与开奖号码的对应位相同，且顺序不限，即为中奖
     * @data_address 万,千,百,十,个
     * @data_num 1,2,4
     */
    protected function r3zhu3($param)
    {
        $address_all = ['万', '千', '百', '十', '个'];
        $re_data = [
            'count' => 0,
            'play_name' => '任选/任三/组三'
        ];
        $data_num = $param['data_num'];
        $data_address = $param['data_address'];
        //号码
        $data_num = explode(',', $data_num);
        $data_num = array_unique($data_num);
        if ($data_num) {
            foreach ($data_num as $k => $v) {
                if ($v > 9 || $v < 0) {
                    unset($data_num[$k]);
                }
            }
        }
        if (count($data_num) < 2) return $re_data;

        $data_num_hm = getArrSet([$data_num, $data_num, $data_num]);
        foreach ($data_num_hm as $k => $v) {
            sort($v);
            if (count(array_unique($v)) != 2) {
                unset($data_num_hm[$k]);
            } else {
                $data_num_hm[$k] = implode(',', $v);
            }
        }
        $data_num_hm = array_unique($data_num_hm);
        //位置
        $data_address = explode(',', $data_address);
        if ($data_address) {
            foreach ($data_address as $k => $v) {
                if (!in_array($v, $address_all)) {
                    unset($data_address[$k]);
                }
            }
        }
        $data_address = array_unique($data_address);
        if (count($data_address) < 3) return $re_data;
        $data_num_zh = getArrSet([$data_address, $data_address, $data_address]);
        foreach ($data_num_zh as $k => $v) {
            sort($v);
            if (count(array_unique($v)) != 3) {
                unset($data_num_zh[$k]);
            } else {
                $data_num_zh[$k] = implode(',', $v);
            }
        }
        $data_num_zh = array_unique($data_num_zh);
        $data_num_zh = array_values($data_num_zh);

        $re_data = [
            'count' => count($data_num_zh) * count($data_num_hm),
            'play_name' => '任选/任三/组三'
        ];
        return $re_data;
    }
    /**
     * 任选/任三/组六
     * 玩法示意：从0-9中任意选择3个或3个以上号码和任意三个位置
     * 位置选择万、千、百，号码选择012；开奖号码为012**、则中奖
     * 从0-9中任意选择3个或3个以上号码和万、千、百、十、个任意的三个位置，如果组合的号码与开奖号码对应则中奖
     * @data_address 万,千,百,十,个
     * @data_num 1,2,4
     */
    protected function r3zhu6($param)
    {
        $address_all = ['万', '千', '百', '十', '个'];
        $data_num = $param['data_num'];
        $re_data = [
            'count' => 0,
            'play_name' => '任选/任三/组六'
        ];

        $data_address = $param['data_address'];
        //号码
        $data_num = explode(',', $data_num);

        $data_num = array_unique($data_num);
        if ($data_num) {
            foreach ($data_num as $k => $v) {
                if ($v > 9 || $v < 0) {
                    unset($data_num[$k]);
                }
            }
        }
        if (count($data_num) < 3) return $re_data;

        $data_num_hm = getArrSet([$data_num, $data_num, $data_num]);
        foreach ($data_num_hm as $k => $v) {
            sort($v);
            if (count(array_unique($v)) != 3) {
                unset($data_num_hm[$k]);
            } else {
                $data_num_hm[$k] = implode(',', $v);
            }
        }
        $data_num_hm = array_unique($data_num_hm);
        //位置
        $data_address = explode(',', $data_address);
        if ($data_address) {
            foreach ($data_address as $k => $v) {
                if (!in_array($v, $address_all)) {
                    unset($data_address[$k]);
                }
            }
        }
        $data_address = array_unique($data_address);
        if (count($data_address) < 3) return $re_data;
        $data_num_zh = getArrSet([$data_address, $data_address, $data_address]);
        foreach ($data_num_zh as $k => $v) {
            sort($v);
            if (count(array_unique($v)) != 3) {
                unset($data_num_zh[$k]);
            } else {
                $data_num_zh[$k] = implode(',', $v);
            }
        }
        $data_num_zh = array_unique($data_num_zh);
        $data_num_zh = array_values($data_num_zh);

        $re_data = [
            'count' => count($data_num_zh) * count($data_num_hm),
            'play_name' => '任选/任三/组六'
        ];
        return $re_data;
    }

    /**
     * 任选/任二/复式
     * 玩法示意：万、千、百、十、个任意2位，开奖号分别对应且顺序一致即中奖
     * 万位买0，千位买1，百位买2，开奖01234，则中奖。
     * 从万、千、百、十、个中至少2个位置各选一个或多个号码，将各个位置的号码进行组合，所选号码的各位与开奖号码相同则中奖。
     *
     * @one_num 1,2,3 (万位)
     * @two_num 1,2,3 (千位)
     * @three_num 1,2,3 (百位)
     * @four_num 1,2,3 (十位)
     * @five_num 1,2,3 (个位)
     *
     */
    protected function r2fs($param)
    {
        $one_num = isset($param['one_num']) ? $param['one_num'] : [];
        $two_num = isset($param['two_num']) ? $param['two_num'] : [];
        $three_num = isset($param['three_num']) ? $param['three_num'] : [];
        $four_num = isset($param['four_num']) ? $param['four_num'] : [];
        $five_num = isset($param['five_num']) ? $param['five_num'] : [];
        $one_num = !is_array($one_num) ? explode(',', $one_num) : [];
        $two_num = !is_array($two_num) ? explode(',', $two_num) : [];
        $three_num = !is_array($three_num) ? explode(',', $three_num) : [];
        $four_num = !is_array($four_num) ? explode(',', $four_num) : [];
        $five_num = !is_array($five_num) ? explode(',', $five_num) : [];
        $data = [
            $this->formatNum($one_num),
            $this->formatNum($two_num),
            $this->formatNum($three_num),
            $this->formatNum($four_num),
            $this->formatNum($five_num),
        ];
        $sun = 0;
        for ($i = 0; $i < count($data); $i++) {
            $ii = count($data[$i]);
            for ($j = $i + 1; $j < count($data); $j++) {
                $sun += $ii * count($data[$j]);
            }
        }
        $re_data = [
            'count' => $sun,
            'play_name' => '任选/任二/复式'
        ];
        return $re_data;


    }
    /**
     * 任选/任二/单式
     * 游戏玩法：至少选择两个号码组成一注，并选择两个对（每个号码之间用,隔开；每注号码之间用;隔开）
     * 投注方案：选择12,对应位百、十，开奖为**21* 或 **12*，即为中奖
     * 投注玩法：从0-9中选2个号码组成一注，所择号码与开奖号码的对应位相同，顺序不限，即为中奖
     *
     * @data_num 12 34
     * @data_address 万,千,百,十,个
     *
     */
    protected function r2ds($param)
    {
        $address_all = ['万', '千', '百', '十', '个'];
        $re_data = [
            'count' => 0,
            'play_name' => '任选/任二/单式'
        ];
        $data_num = $param['data_num'];
        $data_address = $param['data_address'];
        //号码
        $data_num = explode(' ', $data_num);
        if ($data_num) {
            foreach ($data_num as $k => $v) {
//                $r_v = explode(',', $v);
                $r_v = str_split($v);
                if (count($r_v) != 2) {
                    unset($data_num[$k]);
                }
            }
        }
        //位置
        $data_address = explode(',', $data_address);
        if ($data_address) {
            foreach ($data_address as $k => $v) {
                if (!in_array($v, $address_all)) {
                    unset($data_address[$k]);
                }
            }
        }
        if (count($data_address) < 2) return $re_data;
        $sun = 0;
        for ($i = 0; $i < count($data_address); $i++) {
            $ii = count($data_address[$i]);
            for ($j = $i + 1; $j < count($data_address); $j++) {
                $sun += $ii * count($data_address[$j]);
            }
        }
        //注数
        $count = count($data_num) * $sun;
        $re_data = [
            'count' => $count,
            'play_name' => '任选/任二/单式'
        ];
        return $re_data;

    }
    /**
     * 任选/任二/组选
     * 玩法示意： 从0-9中任意选择2个或2个以上号码和任意两个位置
     * 位置选择万、千，号码选择01；开奖号码为01***、则中奖
     * 从0-9中任意选择2个或2个以上号码和万、千、百、十、个任意的两个位置，如果组合的号码与开奖号码对应则中奖
     * @data_num 1,2,3
     * @data_address 万,千,百,十,个
     *
     * data_num 两两组合的个数  *   data_address 两两组合的个数
     *
     */
    protected function r2zhux($param)
    {
        $address_all = ['万', '千', '百', '十', '个'];
        $re_data = [
            'count' => 0,
            'play_name' => '任选/任二/组选'
        ];
        $data_num = $param['data_num'];
        $data_address = $param['data_address'];
        //位置
        $data_address = explode(',', $data_address);
        if ($data_address) {
            foreach ($data_address as $k => $v) {
                if (!in_array($v, $address_all)) {
                    unset($data_address[$k]);
                }
            }
        }
        if (count($data_address) < 2) return $re_data;

        $data_num = explode(',', $data_num);
        $data_num = $this->formatNum($data_num);

        $sun = 0;
        for ($i = 0; $i < count($data_address); $i++) {
            $ii = count($data_address[$i]);
            for ($j = $i + 1; $j < count($data_address); $j++) {
                $sun += $ii * count($data_address[$j]);
            }
        }
        $sun1 = 0;
        for ($i = 0; $i < count($data_num); $i++) {
            $ii = count($data_num[$i]);
            for ($j = $i + 1; $j < count($data_num); $j++) {
                $sun1 += $ii * count($data_num[$j]);
            }
        }
        $re_data = [
            'count' => $sun * $sun1,
            'play_name' => '任选/任二/组选'
        ];
        return $re_data;
    }
    /**
     *大小单双/大小单双/前二
     * 游戏玩法：从万、千位各选一个号码组成一注。
     * 投注方案：万位选择小，千位选择双，开出12***即为中奖
     * 投注玩法：大小单双:对万位和千位的“大（56789）小（01234）、单（13579）双（02468）”形态进行购买。
     * @w_status 大,小,单,双(万位)
     * @q_status 大,小,单,双（千位）
     */
    protected function dxdsq2($param)
    {

        if($param['w_status'] == '' || $param['q_status'] == ''){
            return [
                'count' => 0,
                'play_name' => '大小单双/大小单双/前二'
            ];
        }

        $w_status = explode(',', $param['w_status']);
        $w_status = array_unique($w_status);


        $q_status = explode(',', $param['q_status']);
        $q_status = array_unique($q_status);

        $re_data = [
            'count' => count($w_status)*count($q_status),
            'play_name' => '大小单双/大小单双/前二'
        ];
        return $re_data;
    }
    /**
     *大小单双/大小单双/后二
     * 游戏玩法：从十、个位各选一个号码组成一注
     * 投注方案：十位选择小，个位选择双，开出***12即为中奖
     * 投注玩法：大小单双:对十位和个位的“大（56789）小（01234）、单（13579）双（02468）”形态进行购买。
     * @s_status 大,小,单,双(十位)
     * @g_status 大,小,单,双（个位）
     */
    protected function dxdsh2($param)
    {
        if($param['s_status'] == '' || $param['g_status'] == ''){
            return [
                'count' => 0,
                'play_name' => '大小单双/大小单双/前二'
            ];
        }
        $s_status = explode(',', $param['s_status']);
        $s_status = array_unique($s_status);

        $g_status = explode(',', $param['g_status']);
        $g_status = array_unique($g_status);
        $re_data = [
            'count' => count($s_status)*count($g_status),
            'play_name' => '大小单双/大小单双/后二'
        ];
        return $re_data;
    }
    /**
     * 和值/直选/前三
     * 游戏玩法：至少选择一个号码组成一注。
     * 投注方案：选择01, 相当于前三位投注0,0,1/0,1,0/1,0,0三组号码，开奖号码前三位号码的和值为1（0,0,1/0,1,0/1,0,0），即为中奖。
     * 投注玩法：选择0-27，若所选和值号码与开奖号前三位的和值相等，即为中奖。
     * @data_num 1,2,3,4
     */
    protected function hzzhixq3($param){
        $re_data = [
            'count' => 0,
            'play_name' => '和值/直选/前三'
        ];
        if (!isset($param['data_num'])) return $re_data;
        if (trim($param['data_num']) == '' || $param['data_num']==NULL) return $re_data;
        $data_num = explode(',', $param['data_num']);
        if (count($data_num) == 0) return $re_data;
        $data_num = $this->formatNum($data_num,27);

        $data_num_zh = getArrSet([[0, 1, 2, 3, 4, 5, 6, 7, 8, 9], [0, 1, 2, 3, 4, 5, 6, 7, 8, 9], [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]]);
        if ($data_num_zh) {
            foreach ($data_num_zh as $k => $v) {
                $sum = array_sum($v);
                if (!in_array($sum, $data_num)) {
                    unset($data_num_zh[$k]);
                } else {
                    $data_num_zh[$k] = implode(',', $v);
                }
            }
        }
        $data_num_zh = array_values($data_num_zh);
        $re_data = [
            'count' => count($data_num_zh),
            'play_name' => '和值/直选/前三'
        ];
        return $re_data;
    }
    /**
     * 和值/直选/中三
     * 游戏玩法：至少选择一个号码组成一注。
     * 投注方案：选择01, 相当于中三位投注0,0,1/0,1,0/1,0,0三组号码，开奖号码中三位号码的和值为1（0,0,1/0,1,0/1,0,0），即为中奖。
     * 投注玩法：选择0-27，若所选和值号码与开奖号中三位的和值相等，即为中奖。
     * @data_num 1,2,3,4
     */
    protected function hzzhixz3($param){
        $re_data = [
            'count' => 0,
            'play_name' => '和值/直选/中三'
        ];
        if (!isset($param['data_num'])) return $re_data;
        if (trim($param['data_num']) == '' || $param['data_num']==NULL) return $re_data;
        $data_num = explode(',', $param['data_num']);
        if (count($data_num) == 0) return $re_data;
        $data_num = $this->formatNum($data_num,27);

        $data_num_zh = getArrSet([[0, 1, 2, 3, 4, 5, 6, 7, 8, 9], [0, 1, 2, 3, 4, 5, 6, 7, 8, 9], [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]]);
        if ($data_num_zh) {
            foreach ($data_num_zh as $k => $v) {
                $sum = array_sum($v);
                if (!in_array($sum, $data_num)) {
                    unset($data_num_zh[$k]);
                } else {
                    $data_num_zh[$k] = implode(',', $v);
                }
            }
        }
        $data_num_zh = array_values($data_num_zh);
        $re_data = [
            'count' => count($data_num_zh),
            'play_name' => '和值/直选/中三'
        ];
        return $re_data;
    }
    /**
     * 和值/直选/后三
     * 游戏玩法：至少选择一个号码组成一注。
     * 投注方案：选择01, 相当于后三位投注0,0,1/0,1,0/1,0,0三组号码，开奖号码后三位号码的和值为1（0,0,1/0,1,0/1,0,0），即为中奖。
     * 投注玩法：选择0-27，若所选和值号码与开奖号后三位的和值相等，即为中奖。
     * @data_num 1,2,3,4
     */
    protected function hzzhixh3($param){
        $re_data = [
            'count' => 0,
            'play_name' => '和值/直选/后三'
        ];
        if (!isset($param['data_num'])) return $re_data;
        if (trim($param['data_num']) == '' || $param['data_num']==NULL) return $re_data;
        $data_num = explode(',', $param['data_num']);
        if (count($data_num) == 0) return $re_data;
        $data_num = $this->formatNum($data_num,27);
        $data_num_zh = getArrSet([[0, 1, 2, 3, 4, 5, 6, 7, 8, 9], [0, 1, 2, 3, 4, 5, 6, 7, 8, 9], [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]]);
        if ($data_num_zh) {
            foreach ($data_num_zh as $k => $v) {
                $sum = array_sum($v);
                if (!in_array($sum, $data_num)) {
                    unset($data_num_zh[$k]);
                } else {
                    $data_num_zh[$k] = implode(',', $v);
                }
            }
        }
        $data_num_zh = array_values($data_num_zh);
        $re_data = [
            'count' => count($data_num_zh),
            'play_name' => '和值/直选/后三'
        ];
        return $re_data;
    }
    /**
     * 和值/直选/前二
     * 游戏玩法：至少选择一个号码组成一注。
     * 投注方案：选择1, 相当于前2位投注0,1/1,0两组号码，开奖号码前2位号码的和值为1（0,1/1,0），即为中奖。
     * 投注玩法：选择0-18，若所选和值号码与开奖号前二位的和值相等，即为中奖。
     * @data_num 1,2,3,4
     */
    protected function hzzhixq2($param){
        $re_data = [
            'count' => 0,
            'play_name' => '和值/直选/前二'
        ];
        if (!isset($param['data_num'])) return $re_data;
        if (trim($param['data_num']) == '' || $param['data_num']==NULL) return $re_data;
        $data_num = explode(',', $param['data_num']);
        if (count($data_num) == 0) return $re_data;
        $data_num = $this->formatNum($data_num,18);
        $data_num_zh = getArrSet([[0, 1, 2, 3, 4, 5, 6, 7, 8, 9], [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]]);
        if ($data_num_zh) {
            foreach ($data_num_zh as $k => $v) {
                $sum = array_sum($v);
                if (!in_array($sum, $data_num)) {
                    unset($data_num_zh[$k]);
                } else {
                    $data_num_zh[$k] = implode(',', $v);
                }
            }
        }
        $data_num_zh = array_values($data_num_zh);
        $re_data = [
            'count' => count($data_num_zh),
            'play_name' => '和值/直选/前二'
        ];
        return $re_data;
    }
    /**
     * 和值/直选/后二
     * 游戏玩法：至少选择一个号码组成一注。
     * 投注方案：选择1, 相当于后2位投注0,1/1,0两组号码，开奖号码后2位号码的和值为1（0,1/1,0），即为中奖。
     * 投注玩法：选择0-18，若所选和值号码与开奖号后二位的和值相等，即为中奖。
     * @data_num 1,2,3,4
     */
    protected function hzzhixh2($param){
        $re_data = [
            'count' => 0,
            'play_name' => '和值/直选/后二'
        ];
        if (!isset($param['data_num'])) return $re_data;
        if (trim($param['data_num']) == '' || $param['data_num']==NULL) return $re_data;
        $data_num = explode(',', $param['data_num']);
        if (count($data_num) == 0) return $re_data;
        $data_num = $this->formatNum($data_num,18);
        $data_num_zh = getArrSet([[0, 1, 2, 3, 4, 5, 6, 7, 8, 9], [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]]);
        if ($data_num_zh) {
            foreach ($data_num_zh as $k => $v) {
                $sum = array_sum($v);
                if (!in_array($sum, $data_num)) {
                    unset($data_num_zh[$k]);
                } else {
                    $data_num_zh[$k] = implode(',', $v);
                }
            }
        }
        $data_num_zh = array_values($data_num_zh);
        $re_data = [
            'count' => count($data_num_zh),
            'play_name' => '和值/直选/后二'
        ];
        return $re_data;
    }
    /**
     * 和值/尾选/前三
     * 游戏玩法：至少选择一个号码组成一注
     * 投注方案：选择5, 等于开奖号前三位4,5,6和值的尾数，即为中奖
     * 投注玩法：选择0-9，若所选号码与开奖号前三位的和值的尾数相等，即为中奖
     * @data_num 1,2,3,4
     */
    protected function hzwsq3($param){
        $re_data = [
            'count' =>0,
            'play_name' => '和值/尾选/前三'
        ];
        if (!isset($param['data_num'])) return $re_data;
        if (trim($param['data_num']) == '' || $param['data_num']==NULL) return $re_data;
        $data_num = explode(',', $param['data_num']);
        if (count($data_num) == 0) return $re_data;
        $data_num = $this->formatNum($data_num);

        $re_data = [
            'count' => count($data_num),
            'play_name' => '和值/尾选/前三'
        ];
        return $re_data;
    }
    /**
     * 和值/尾选/中三
     * 游戏玩法：至少选择一个号码组成一注
     * 投注方案：选择5, 等于开奖号前中位4,5,6和值的尾数，即为中奖
     * 投注玩法：选择0-9，若所选号码与开奖号前中位的和值的尾数相等，即为中奖
     * @data_num 1,2,3,4
     */
    protected function hzwsz3($param){
        $re_data = [
            'count' => 0,
            'play_name' => '和值/尾选/中三'
        ];
        if (!isset($param['data_num'])) return $re_data;
        if (trim($param['data_num']) == '' || $param['data_num']==NULL) return $re_data;
        $data_num = explode(',', $param['data_num']);
        if (count($data_num) == 0) return $re_data;
        $data_num = $this->formatNum($data_num);

        $re_data = [
            'count' => count($data_num),
            'play_name' => '和值/尾选/中三'
        ];
        return $re_data;
    }
    /**
     * 和值/尾选/后三
     * 游戏玩法：至少选择一个号码组成一注
     * 投注方案：选择5, 等于开奖号后三位4,5,6和值的尾数，即为中奖
     * 投注玩法：选择0-9，若所选号码与开奖号后三位的和值的尾数相等，即为中奖
     * @data_num 1,2,3,4
     */
    protected function hzwsh3($param){
        $re_data = [
            'count' => 0,
            'play_name' => '和值/尾选/后三'
        ];
        if (!isset($param['data_num'])) return $re_data;
        if (trim($param['data_num']) == '' || $param['data_num']==NULL) return $re_data;
        $data_num = explode(',', $param['data_num']);
        if (count($data_num) == 0) return $re_data;
        $data_num = $this->formatNum($data_num);

        $re_data = [
            'count' => count($data_num),
            'play_name' => '和值/尾选/后三'
        ];
        return $re_data;
    }
    /**
     * 趣味/趣味/一帆风顺
     * 游戏玩法：从0-9中任意选择1个以上号码。
     * 投注方案：例如投注3，开奖号34535，中一注。（出现一次和出现两次都只算中一注）
     * 投注玩法：每注号码由一个数字组成，只要开奖号码任意一位出现了投注的号码，即为中奖
     * @data_num 1,2,3
     */
    protected function qwyffs($param)
    {
        $data_num = $param['data_num'];
        $data_num = explode(',', $data_num);
        $data_num = $this->formatNum($data_num);
        $re_data = [
            'count' => count($data_num),
            'play_name' => '趣味/趣味/一帆风顺'
        ];
        return $re_data;
    }
    /**
     * 趣味/趣味/好事成双
     * 游戏玩法：从0-9中任意选择1个以上的二重号码。
     * 投注方案：例如投注3，开奖号34573，中一注。
     * 投注玩法：从0-9中任意一个号码组成一注，只要所选号码在开奖号码任意一位出现了2次，即为中奖
     * @data_num 1,2,3
     */
    protected function qwhscs($param)
    {

        $data_num = $param['data_num'];
        $data_num = explode(',', $data_num);
        $data_num = $this->formatNum($data_num);
        $re_data = [
            'count' => count($data_num),
            'play_name' => '趣味/趣味/好事成双'
        ];
        return $re_data;
    }
    /**
     * 新龙虎

     * @data_num 龙,虎
     */
    protected function lh($param)
    {
        $data_num = $param['data_num'];
        $data_num = explode(',', $data_num);
        $data_num = array_unique($data_num);
        $count = 0;
        if($data_num){
            foreach ($data_num as $k=>$v){
                if(in_array($v,['龙','虎'])){
                    $count++;
                }
            }
        }

        $re_data = [
            'count' => $count,
        ];
        return $re_data;
    }
    /**
     * 新龙虎  和

     * @data_num 和
     */
    protected function lhh($param)
    {
        $data_num = $param['data_num'];

        $count = 0;
        if($data_num == '和'){
            $count++;
        }

//        $data_num = explode(',', $data_num);
//        $data_num = array_unique($data_num);
//        if($data_num){
//            foreach ($data_num as $k=>$v){
//                if(in_array($v,['龙','虎'])){
//                    $count++;
//                }
//            }
//        }

        $re_data = [
            'count' => $count,
        ];
        return $re_data;
    }

}