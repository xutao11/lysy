<?php


namespace lottery;

use lottery\calculation\Calculation;

/**
 * 河内5分彩
 * Class Hlwfc
 * @package lottery
 */
class Hlwfc
{
    use Calculation;
     /**
      * 获取开奖时间
      */
    public function gettime(){
        //但钱时间
        $time = time()+90;

        $h = date('H',$time);
        $i = date('i',$time);
        //当前开奖期数
        $qishu = (int)(($h * 60 + $i)/5);
        //反推最近一次的开奖时间
        $next_time= strtotime(date('Y-m-d',$time))+($qishu*5*60)+(5*60);
        $kj_time = date('Y-m-d H:i:s',$next_time);
        //期数
        $next_qishu = (date('H',$next_time)*60 + date('i',$next_time))/5;
        $next_qishu =  sprintf("%03d", $next_qishu);
        $re = [
            'qishu' => $next_qishu==0?date('Ymd',$time).'288':date('Ymd',$time).$next_qishu,
            'kj_time' => $kj_time,
            'service_time'=>date('Y-m-d H:i:s',$time)
        ];
        return $re;
    }
    /**
     * 前三/前三直选/复式   玩法示意： 从万、千、百位各选一个号码组成一注。
     * 投注方案：345； 开奖号码：345，即中前三直选一等奖从万、千、百位中选择一个3位号码组成一注，所选号码与开奖号码的前3位相同，且顺序一致，即为中奖。
     * @one_num  第一位好嘛  1,2,3
     * @two_num  第二位好嘛  1,2,3
     * @three_num  第三位好嘛 1,2,3
     */
    public function bet_308($param)
    {
        $re = $this->q3zxfs($param);
        return $re;

    }


    /**
     * 前三/前三直选/单式   (玩法)
     * 玩法示意： 手动输入号码，至少输入1个三位数号码组成一注。
     * 投注方案：345； 开奖号码：345，即中前三直选一等奖手动输入一个3位数号码组成一注，所选号码的万位、千位、百位与开奖号码相同，且顺序一致，即为中奖。
     * @data_num  下注号码 123 234
     */
    public function bet_309($param)
    {
        $re = $this->q3zxds($param);
        return $re;

    }

    /**
     * 前三/前三直选/直选和值
     *玩法示意： 从0-27中任意选择1个或1个以上号码
     * 投注方案：和值1；开奖号码前三位：001,010,100,即中前三直选一等奖所选数值等于开奖号码的万位、千位、百位三个数字相加之和，即为中奖。
     * @data_num   和值（0-27） 0,2,27
     */
    public function bet_310($param)
    {
        $re = $this->hzzhixq3($param);
        return $re;

    }

    /**
     *前三/前三直选/直选跨度
     * (最大-最小)
     * 玩法示意：从0-9中任意选择1个或1个以上号码
     * 投注方案：跨度8；开出的三个数字包括0,8,x，其中x≠9，即可中前三直选；开出的三个数字包括1,9,x，其中x≠0，即可中前三直选跨度。
     * @data_num 1,2,3
     */
    public function bet_311($param)
    {
        $re = $this->kdq3($param);
        $re['play_name'] = '前三/前三直选/直选跨度';
        return $re;
    }


    /**
     * 前三/前三组选 /组三
     * 玩法示意： 从0-9中任意选择2个或2个以上号码。
     * 投注方案：5,8；开奖号码前三位：1个5，2个8或1个8，2个5 (顺序不限)，即中奖。
     * 从0-9中选择2个数字组成两注，所选号码与开奖号码的万位、千位、百位相同，且顺序不限，即为中奖。
     * @data_num   1,2,3
     */
    public function bet_312($param)
    {
        $re = $this->q3zxzs($param);
        return $re;
    }

    /**
     * 前三/前三组选 /组六
     * 玩法示意从0-9中任意选择3个或3个以上号码。
     * 投注方案：2,5,8；开奖号码前三位：1个2、1个5、1个8 (顺序不限)，即中前三组选六一等奖。
     * 从0-9中任意选择3个号码组成一注，所选号码与开奖号码的万位、千位、百位相同，顺序不限，即为中奖。。
     * @data_num   1,2,3
     */
    public function bet_313($param)
    {
        $re = $this->q3zxzl($param);
        return $re;
    }

    /**
     * 前三/前三组选/组选和值
     * 玩法示意：从1-26中任意选择1个或1个以上号码
     * 投注方案：和值1；开奖号码前三位：001,010,100,即中前三组选和值
     * 所选数值等于开奖号码的万位、千位、百位三个数字相加之和，即为中奖。
     * 从1-26中任意选择1个或1个以上号码
     *
     * @data_num  1,2,3
     *
     *
     */
//    public function bet_488($param)
//    {
//        $param = $this->param;
//        $re_data = [
//            'data_num' => [],
//            'count' => 0,
//            'play_name' => '前三/前三组选/组选和值 '
//        ];
//        if (!isset($param['data_num'])) return $re_data;
//        if (trim($param['data_num']) == '' || $param['data_num']==NULL) return $re_data;
//        //分割
//        $data_num = explode(',', $param['data_num']);
//        if (count($data_num) == 0) return $re_data;
//        foreach ($data_num as $k => $v) {
//            $num = (int)$v;
//            if ($v > 26 || $v < 1) {
//                unset($data_num[$k]);
//            } else {
//                $data_num[$k] = $num;
//            }
//
//
//        }
//        //获取前三的所有组合
//        $data_num_zh = getArrSet([[0, 1, 2, 3, 4, 5, 6, 7, 8, 9], [0, 1, 2, 3, 4, 5, 6, 7, 8, 9], [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]]);
//        foreach ($data_num_zh as $k => $v) {
//
//            $sum = array_sum($v);
//            if (!in_array($sum, $data_num) || count(array_unique($v)) == 1) {
//                unset($data_num_zh[$k]);
//            } else {
//
//                sort($v);
//                $data_num_zh[$k] = implode(',', $v);
//
//
//            }
//        }
//
//        $data_num_zh = array_unique($data_num_zh);
//
//        $data_num_zh = array_values($data_num_zh);
//        $re_data = [
//            'data_num' => $data_num_zh,
//            'count' => count($data_num_zh),
//            'play_name' => '前三/前三组选/组选和值 '
//        ];
//        return $re_data;
//
//    }

    /**
     * 前三/前三其他 /和值尾数
     * 玩法示意：从0-9中选择1个号码。
     * 投注方案：和值尾数8；开奖号码：前三位和值尾数为8，即中得和值尾数。
     * 从下方中选择1个号码组成1注，所选号码与开奖号码前三位和值的尾数相同，即为中奖。
     * 从0-9中选择1个号码。
     * @data_num  0,1,2
     */
    public function bet_315($param)
    {
        $re = $this->hzwsq3($param);
        $re['play_name'] = '前三/前三其他/和值尾数';
        return $re;
    }

    /**
     * 中三/中三直选/复式   玩法示意： 从千、百、十位各选一个号码组成一注。
     * 投注方案：345； 开奖号码：345，即中前三直选一等奖从千、百、十位中选择一个3位号码组成一注，所选号码与开奖号码的前3位相同，且顺序一致，即为中奖。
     * @one_num  第一位好嘛 1,2,3
     * @two_num  第二位好嘛  1,2,3
     * @three_num  第三位好嘛  1,2,3
     */
    public function bet_320($param)
    {
        $re = $this->z3zxfs($param);
        return $re;
    }

    /**
     * 中三/中三直选/单式   (玩法)
     * 玩法示意： 手动输入号码，至少输入1个三位数号码组成一注。
     * 投注方案：345； 开奖号码：345，即中前三直选一等奖手动输入一个3位数号码组成一注，所选号码的千位、百位、十位与开奖号码相同，且顺序一致，即为中奖。。
     * @data_num  下注号码  123 345
     */
    public function bet_321($param)
    {
        $re = $this->z3zxds($param);
        return $re;
    }


    /**
     * 中三/中三直选/直选和值
     *玩法示意： 从0-27中任意选择1个或1个以上号码
     * 投注方案：和值1；开奖号码前三位：001,010,100,即中前三直选一等奖所选数值等于开奖号码的千位、百位、十位三个数字相加之和，即为中奖。
     * @data_num   和值（0-27） 0,2,27
     */
    public function bet_322($param)
    {
        $re = $this->hzzhixz3($param);
        return $re;
    }

    /**
     *中三/中三直选/直选跨度
     * (最大-最小)
     * 玩法示意：从0-9中任意选择1个或1个以上号码
     * 投注方案：跨度8；开出的三个数字包括0,8,x，其中x≠9，即可中前三直选；开出的三个数字包括1,9,x，其中x≠0，即可中前三直选跨度。
     * @data_num 1,2,3
     */
    public function bet_323($param)
    {
        $re = $this->kdz3($param);
        $re['play_name'] = '中三/中三直选/直选跨度';
        return $re;
    }

    /**
     * 中三/中三组选 /组三
     * 玩法示意： 从0-9中任意选择2个或2个以上号码。
     * 投注方案：5,8；开奖号码中三位：1个5，2个8或1个8，2个5 (顺序不限)，即中奖。
     * 从0-9中选择2个数字组成两注，所选号码与开奖号码的千位、百位、十位相同，且顺序不限，即为中奖。
     * @data_num   1,2,3
     */
    public function bet_324($param)
    {
        $re = $this->z3zxz3($param);
        return $re;
    }

    /**
     * 中三/中三组选/组六
     * 玩法示意从0-9中任意选择3个或3个以上号码。
     * 投注方案：2,5,8；开奖号码前三位：1个2、1个5、1个8 (顺序不限)，即中前三组选六一等奖。
     * 从0-9中任意选择3个号码组成一注，所选号码与开奖号码的千位、百位、十位相同，顺序不限，即为中奖。。
     * @data_num   1,2,3
     */
    public function bet_325($param)
    {
        $re = $this->z3zxz6($param);
        return $re;
    }

    /**
     * 中三/中三其他/和值尾数
     * 玩法示意：从0-9中选择1个号码。
     * 投注方案：和值尾数8；开奖号码：前三位和值尾数为8，即中得和值尾数。
     * 从下方中选择1个号码组成1注，所选号码与开奖号码前三位和值的尾数相同，即为中奖。
     * 从0-9中选择1个号码。
     * @data_num  0,1,2
     */
    public function bet_326($param)
    {
        $re = $this->hzwsz3($param);
        return $re;
    }

    /**
     * 后三/后三直选/复式   玩法示意： 从百、十、个位各选一个号码组成一注。
     * 投注方案：345； 开奖号码：345，即中前三直选一等奖从百、十、个位中选择一个3位号码组成一注，所选号码与开奖号码的前3位相同，且顺序一致，即为中奖。
     * @one_num  第一位好嘛 （1,2,3
     * @two_num  第二位好嘛 1,2,3
     * @three_num  第三位好嘛  1,2,3
     */
    public function bet_331($param)
    {
        $re = $this->h3zxfs($param);
        return $re;
    }

    /**
     * 后三/后三直选/单式   (玩法)
     * 玩法示意： 手动输入号码，至少输入1个三位数号码组成一注。
     * 投注方案：345； 开奖号码：345，即中前三直选一等奖手动输入一个3位数号码组成一注，所选号码的百、十、个与开奖号码相同，且顺序一致，即为中奖。。
     * @data_num  下注号码 123 234
     */
    public function bet_332($param)
    {
        $re = $this->h3zxds($param);
        return $re;
    }

    /**
     * 后三/后三直选/直选和值
     *玩法示意： 从0-27中任意选择1个或1个以上号码
     * 投注方案：和值1；开奖号码前三位：001,010,100,即中前三直选一等奖所选数值等于开奖号码的百、十、个三个数字相加之和，即为中奖。
     * @data_num   和值（0-27） 0,2,27
     */
    public function bet_333($param)
    {
        $re = $this->hzzhixh3($param);
        $re['play_name'] = '后三/后三直选/直选和值';
        return $re;
    }


    /**
     *后三/后三直选/直选跨度
     * (最大-最小)
     * 玩法示意：从0-9中任意选择1个或1个以上号码
     * 投注方案：跨度8；开出的三个数字包括0,8,x，其中x≠9，即可中前三直选；开出的三个数字包括1,9,x，其中x≠0，即可中前三直选跨度。
     * @data_num 1,2,3
     */
    public function bet_334($param)
    {
        $re = $this->kdh3($param);
        $re['play_name'] = '后三/后三直选/直选跨度';
        return $re;
    }


    /**
     * 后三/后三组选 /组三
     * 玩法示意： 从0-9中任意选择2个或2个以上号码。
     * 投注方案：5,8；开奖号码中三位：1个5，2个8或1个8，2个5 (顺序不限)，即中奖。
     * 从0-9中选择2个数字组成两注，所选号码与开奖号码的百、十、个相同，且顺序不限，即为中奖。
     * @data_num   1,2,3
     */
    public function bet_335($param)
    {
        $re = $this->h3zxz3($param);
        return $re;
    }

    /**
     * 后三/后三组选 /组六
     * 玩法示意从0-9中任意选择3个或3个以上号码。
     * 投注方案：2,5,8；开奖号码前三位：1个2、1个5、1个8 (顺序不限)，即中前三组选六一等奖。
     * 从0-9中任意选择3个号码组成一注，所选号码与开奖号码的百、十、个相同，顺序不限，即为中奖。。
     * @data_num   1,2,3
     */
    public function bet_336($param)
    {
        $re = $this->h3zxz6($param);
        return $re;
    }


    /**
     * 后三/后三组选/组选和值
     * 玩法示意：从1-26中任意选择1个或1个以上号码
     * 投注方案：和值1；开奖号码前三位：001,010,100,即中前三组选和值
     * 所选数值等于开奖号码的百、十、个三个数字相加之和，即为中奖。
     * 从1-26中任意选择1个或1个以上号码
     *
     * @data_num  1,2,3
     *
     *
     */
//    public function bet_511($param)
//    {
//        $param = $this->param;
//        $re_data = [
//            'data_num' => [],
//            'count' => 0,
//            'play_name' => '后三/后三组选/组选和值 '
//        ];
//        if (!isset($param['data_num'])) return $re_data;
//        if (trim($param['data_num']) == '' || $param['data_num']==NULL) return $re_data;
//        //分割
//        $data_num = explode(',', $param['data_num']);
//        if (count($data_num) == 0) return $re_data;
//        foreach ($data_num as $k => $v) {
//            $num = (int)$v;
//            if ($v > 26 || $v < 1) {
//                unset($data_num[$k]);
//            } else {
//                $data_num[$k] = $num;
//            }
//        }
//        //获取前三的所有组合
//        $data_num_zh = getArrSet([[0, 1, 2, 3, 4, 5, 6, 7, 8, 9], [0, 1, 2, 3, 4, 5, 6, 7, 8, 9], [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]]);
//        foreach ($data_num_zh as $k => $v) {
//
//            $sum = array_sum($v);
//            //去除 111，333 等
//            if (!in_array($sum, $data_num) || count(array_unique($v)) == 1) {
//                unset($data_num_zh[$k]);
//            } else {
//                sort($v);
//                $data_num_zh[$k] = implode(',', $v);
//            }
//        }
//        $data_num_zh = array_unique($data_num_zh);
//        $data_num_zh = array_values($data_num_zh);
//        $re_data = [
//            'data_num' => $data_num_zh,
//            'count' => count($data_num_zh),
//            'play_name' => '后三/后三组选/组选和值 '
//        ];
//        return $re_data;
//    }


    /**
     * 后三/后三组选/组选包胆
     * 玩法示意：从0-9中任选1个号码。
     * 投注方案：包胆3；开奖号码后三位：(1)出现3xx或者33x,即中后三组三；(2)出现3xy，即中后三组六。
     * 从0-9中任意选择1个号码组成一注，出现后三组三或组六，即为中奖。
     * 从0-9中任选1个号码
     * @data_num  0 (任选1个号码)
     */
//    public function bet_180($param)
//    {
//        $re = $this->q3zxbd($param);
//        $re['play_name'] = '后三/后三组选/组选包胆';
//        return $re;
//
//    }


    /**
     * 后三/后三其他/和值尾数
     *
     * 玩法示意：从0-9中选择1个号码。
     * 投注方案：和值尾数8；开奖号码：后三位和值尾数为8，即中得和值尾数。
     * 从下方中选择1个号码组成1注，所选号码与开奖号码后三位和值的尾数相同，即为中奖。
     * 从0-9中选择1个号码。
     * @data_num 0,1
     *
     */
    public function bet_338($param)
    {
        $re = $this->hzwsh3($param);
        $re['play_name'] = '后三/后三其他/和值尾数';
        return $re;
    }

    /**
     * 前二/直选/复试
     *玩法示意：从万、千位各选一个号码组成一注。
     * 投注方案：58；开奖号码前二位：58，即中前二直选一等奖。
     * 从万位、千位中选择一个2位数号码组成一注，所选号码与开奖号码的前2位相同，且顺序一致，即为中奖。
     * 从万、千位各选一个号码组成一注。
     * @one_num 0,1
     * @two_num 0,1
     */
    public function bet_342($param)
    {
        $re = $this->qezxfs($param);
        $re['play_name'] = '前二/直选/复试';
        return $re;
    }

    /**
     * 前二/直选/单试
     *玩法示意：手动输入号码，至少输入1个两位数号码。
     * 投注方案：58；开奖号码前二位：58，即中前二直选一等奖。
     * 从万位、千位中选择一个2位数号码组成一注，所选号码与开奖号码的前2位相同，且顺序一致，即为中奖。
     * 从万、千位各选一个号码组成一注。
     * @data_num 0,1;1,2
     */
    public function bet_343($param)
    {
        $re = $this->qezxds($param);
        $re['play_name'] = '前二/直选/单试';
        return $re;
    }

    /**
     * 前二/直选/直选和值
     * 玩法示意：从0-18中任意选择1个或1个以上的和值号码。
     * 投注方案：和值1；开奖号码前二位：01,10，即中前二直选。
     * 所选数值等于开奖号码的万位、千位二个数字相加之和，即为中奖。
     * 从0-18中任意选择1个或1个以上的和值号码
     * @data_num = 0,1
     */
    public function bet_344($param)
    {
        $re = $this->hzzhixq2($param);
        $re['play_name'] = '前二/直选/直选和值';
        return $re;
    }

    /**
     * 前二/直选/跨度 （）
     * 玩法示意：从0-9中选择1个号码。
     * 投注方案：跨度9；开奖号码为9,0,-,-,-或0,9,-,-,-，即中前二直选跨度。
     * 所选数值等于开奖号码的前2位最大与最小数字相减之差，即为中奖。
     *
     * @data_num 0,1
     */
    public function bet_345($param)
    {

        $re = $this->kdq2($param);
        $re['play_name'] = '前二/直选/跨度';
        return $re;
    }

    /**
     * 前二/组选/复试
     * 玩法示意：从0-9中任意选择2个或2个以上号码。
     * 投注方案：5,8；开奖号码前二位：1个5，1个8 (顺序不限)，即中前二组选一等奖。
     * 从0-9中选2个号码组成一注，所选号码与开奖号码的万位、千位相同，顺序不限，即中奖。
     * @data_num
     *
     */
    public function bet_346($param)
    {
        $re = $this->q2zhuxfs($param);
        $re['play_name'] = '前二/组选/复试';
        return $re;
    }

    /**
     * 前二/组选/单式
     *
     * 玩法示意：手动输入号码，至少输入1个两位数号码。
     * 投注方案：5,8；开奖号码前二位：1个5，1个8 (顺序不限)，即中前二组选一等奖。
     * 手动输入一个2位数号码组成一注，所选号码的万位、千位与开奖号码相同，顺序不限，即为中奖。
     * @data_num 1,2;1,3
     */
    public function bet_347($param)
    {
        $re = $this->q2zhuxds($param);
        $re['play_name'] = '前二/组选/单式';
        return $re;
    }


    /**
     * 前二/组选/和值
     * 玩法示意：从1-17中任意选择1个或1个以上号码。
     * 投注方案：和值1；开奖号码前二位：10或01 (顺序不限，不含对子号)，即中前二组选。
     * 从1-17中任意选择1个或1个以上号码。所选数值等于开奖号码的万位、千位二个数字相加之和（不含对子号），即为中奖。
     * @data_num 1,2
     */
//    public function bet_535($param)
//    {
//        $param = $this->param;
//        $re_data = [
//            'data_num' => [],
//            'count' => 0,
//            'play_name' => '前二/组选/和值'
//        ];
//        if (!isset($param['data_num'])) return $re_data;
//        if (trim($param['data_num']) == '' || $param['data_num']==NULL) return $re_data;
//        //分割
//        $data_num = explode(',', $param['data_num']);
//        if (count($data_num) == 0) return $re_data;
//        foreach ($data_num as $k => $v) {
//            $num = (int)$v;
//            if ($v > 17 || $v < 1) {
//                unset($data_num[$k]);
//            } else {
//                $data_num[$k] = $num;
//            }
//        }
//        //获取前三的所有组合
//        $data_num_zh = getArrSet([[0, 1, 2, 3, 4, 5, 6, 7, 8, 9], [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]]);
//
//        foreach ($data_num_zh as $k => $v) {
//
//            $sum = array_sum($v);
//            if (!in_array($sum, $data_num) || count(array_unique($v)) == 1) {
//                unset($data_num_zh[$k]);
//            } else {
//                sort($v);
//                $data_num_zh[$k] = implode(',', $v);
//            }
//        }
//        $data_num_zh = array_unique($data_num_zh);
//        $data_num_zh = array_values($data_num_zh);
//        $re_data = [
//            'data_num' => $data_num_zh,
//            'count' => count($data_num_zh),
//            'play_name' => '前二/组选/和值'
//        ];
//        return $re_data;
//
//    }

    /**
     * 前二/组选/包胆
     * 玩法示意： 从0-9中任意选择1个包胆号码。
     * 投注方案：包胆号码8；开奖号码前二位：出现1个8（不包括2个8），即中前二组选。
     * 从0-9中任意选择1个包胆号码，开奖号码的万位、千位中任意1位包含所选的包胆号码相同（不含对子号），即为中奖。
     * @data_num 1(任选1个号码)
     */
    public function bet_349($param)
    {
        $re = $this->q2zhuxbd($param);
        return $re;
    }


    /**
     * 后二/直选/复试
     *玩法示意：从十、个位各选一个号码组成一注
     * 投注方案：58；开奖号码前二位：58，即中前二直选一等奖。
     * 从万十、个中选择一个2位数号码组成一注，所选号码与开奖号码的前2位相同，且顺序一致，即为中奖。
     * 从十、个位各选一个号码组成一注。
     * @one_num 0,1
     * @two_num 0,1
     */
    public function bet_353($param)
    {
        $re = $this->hezxfs($param);
        $re['play_name'] = '后二/直选/复试';
        return $re;
    }

    /**
     * 后二/直选/单试
     *玩法示意：手动输入号码，至少输入1个两位数号码。
     * 投注方案：58；开奖号码前二位：58，即中前二直选一等奖。
     * 从万位、千位中选择一个2位数号码组成一注，所选号码与开奖号码的前2位相同，且顺序一致，即为中奖。
     * 从万、千位各选一个号码组成一注。
     * @data_num 12 34
     */
    public function bet_354($param)
    {
        $re = $this->hezxds($param);
        $re['play_name'] = '后二/直选/单试';
        return $re;
    }

    /**
     * 后二/直选/直选和值
     * 玩法示意：从0-18中任意选择1个或1个以上的和值号码。
     * 投注方案：和值1；开奖号码前二位：01,10，即中前二直选。
     * 所选数值等于开奖号码的万位、千位二个数字相加之和，即为中奖。
     * 从0-18中任意选择1个或1个以上的和值号码
     * @data_num = 0,1
     */
    public function bet_355($param)
    {
        $re = $this->hzzhixh2($param);
        $re['play_name'] = '后二/直选/直选和值';
        return $re;
    }

    /**
     * 后二/直选/跨度 （）
     * 玩法示意：从0-9中选择1个号码。
     * 投注方案：跨度9；开奖号码为9,0,-,-,-或0,9,-,-,-，即中前二直选跨度。
     * 所选数值等于开奖号码的前2位最大与最小数字相减之差，即为中奖。
     *
     * @data_num 0,1
     */
    public function bet_356($param)
    {
        $re = $this->kdh2($param);
        $re['play_name'] = '后二/直选/跨度';
        return $re;
    }

    /**
     * 后二/组选/复试
     * 玩法示意：从0-9中任意选择2个或2个以上号码。
     * 投注方案：5,8；开奖号码前二位：1个5，1个8 (顺序不限)，即中前二组选一等奖。
     * 从0-9中选2个号码组成一注，所选号码与开奖号码的万位、千位相同，顺序不限，即中奖。
     * @data_num
     *
     */
    public function bet_357($param)
    {
        $re = $this->h2zhuxfs($param);
        $re['play_name'] = '后二/组选/复试';
        return $re;
    }

    /**
     * 后二/组选/单式
     *
     * 玩法示意：手动输入号码，至少输入1个两位数号码。
     * 投注方案：5,8；开奖号码前二位：1个5，1个8 (顺序不限)，即中前二组选一等奖。
     * 手动输入一个2位数号码组成一注，所选号码的万位、千位与开奖号码相同，顺序不限，即为中奖。
     * @data_num 1,2;1,3
     */
    public function bet_358($param)
    {
        $re = $this->h2zhuxds($param);
        $re['play_name'] = '后二/组选/单式';
        return $re;
    }


    /**
     * 后二/组选/和值
     * 玩法示意：从1-17中任意选择1个或1个以上号码。
     * 投注方案：和值1；开奖号码前二位：10或01 (顺序不限，不含对子号)，即中前二组选。
     * 从1-17中任意选择1个或1个以上号码。所选数值等于开奖号码的万位、千位二个数字相加之和（不含对子号），即为中奖。
     * @data_num 1,2
     */
//    public function bet_524($param)
//    {
//        $param = $this->param;
//        $re_data = [
//            'data_num' => [],
//            'count' =>0,
//            'play_name' => '后二/组选/和值'
//        ];
//        if (!isset($param['data_num'])) return $re_data;
//        if (trim($param['data_num']) == '' || $param['data_num']==NULL) return $re_data;
//        //分割
//        $data_num = explode(',', $param['data_num']);
//        if (count($data_num) == 0) return $re_data;
//        foreach ($data_num as $k => $v) {
//            $num = (int)$v;
//            if ($v > 17 || $v < 1) {
//                unset($data_num[$k]);
//            } else {
//                $data_num[$k] = $num;
//            }
//        }
//        //获取后二的所有组合
//        $data_num_zh = getArrSet([[0, 1, 2, 3, 4, 5, 6, 7, 8, 9], [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]]);
//
//        foreach ($data_num_zh as $k => $v) {
//
//            $sum = array_sum($v);
//            if (!in_array($sum, $data_num) || count(array_unique($v)) == 1) {
//                unset($data_num_zh[$k]);
//            } else {
//                sort($v);
//                $data_num_zh[$k] = implode(',', $v);
//            }
//        }
//        $data_num_zh = array_unique($data_num_zh);
//        $data_num_zh = array_values($data_num_zh);
//        $re_data = [
//            'data_num' => $data_num_zh,
//            'count' => count($data_num_zh),
//            'play_name' => '后二/组选/和值'
//        ];
//        return $re_data;
//
//    }

    /**
     * 后二/组选/包胆
     * 玩法示意： 从0-9中任意选择1个包胆号码。
     * 投注方案：包胆号码8；开奖号码前二位：出现1个8（不包括2个8），即中前二组选。
     * 从0-9中任意选择1个包胆号码，开奖号码的万位、千位中任意1位包含所选的包胆号码相同（不含对子号），即为中奖。
     * @data_num 1(任选1个号码)
     */
    public function bet_478($param)
    {
        $re = $this->q2zhuxbd($param);
        $re['play_name'] = '后二/组选/包胆';
        return $re;
    }


    /**
     * 定位胆/定位胆/定位胆
     * 玩法示意：在万千百十个位任意位置上任意选择1个或1个以上号码。
     * 投注方案：1；开奖号码万位：1，即中定位胆万位一等奖。
     * 从万、千、百、十、个位任意位置上至少选择1个以上号码，所选号码与相同位置上的开奖号码一致，即为中奖。
     * @one_num 1,2,3 (万位)
     * @two_num 1,2,3 (千位)
     * @three_num 1,2,3 (百位)
     * @four_num 1,2,3 (十位)
     * @five_num 1,2,3 (个位)
     *
     */
    public function bet_361($param)
    {
        $re = $this->dwd($param);
        return $re;
    }

    /**
     * 不定胆/五星不定胆/二码不定位
     *玩法示意  从0-9中任意选择2个以上号码
     * 投注方案：1,2；
     * 开奖号码：至少出现1和2各1个，即中五星二码不定位。
     * 从0-9中选择2个号码，每注由2个不同的号码组成，开奖号码的万位、千位、百位、十位、个位中同时包含所选的2个号码，即为中奖。
     * @data_num 1,2,3
     */

//    public function bet_542($param)
//    {
//        $param = $this->param;
//        $re_data = [
//            'data_num' => [],
//            'count' => 0,
//            'play_name' => '不定胆/五星不定胆/二码不定位'
//        ];
//        if (!isset($param['data_num'])) return $re_data;
//        $data_num = explode(',', $param['data_num']);
//        $data_num = $this->formatNum($data_num);
//        //获取的所有组合
//        $data_num_zh = getArrSet([$data_num, $data_num]);
//        foreach ($data_num_zh as $k => $v) {
//            sort($v);
//            if (count(array_unique($v)) == 1) {
//                //去除三个一样的数  0，0，0   1，1，1  2，2，2
//                unset($data_num_zh[$k]);
//            } else {
//                $data_num_zh[$k] = implode(',', $v);
//            }
//
//        }
//        $data_num_zh = array_unique($data_num_zh);
//        $data_num_zh = array_values($data_num_zh);
//
//
//        $re_data = [
//            'data_num' => $data_num_zh,
//            'count' => count($data_num_zh),
//            'play_name' => '不定胆/五星不定胆/二码不定位'
//        ];
//        return $re_data;
//    }

    /**
     * 不定胆/五星不定胆/三码不定位
     * 玩法示意：从0-9中任意选择3个以上号码。
     * 投注方案：1,2,3；
     * 开奖号码：至少出现1、2、3各1个，即中五星三码不定位。
     * 从0-9中选择3个号码，每注由3个不同的号码组成，开奖号码的万位、千位、百位、十位、个位中同时包含所选的3个号码，即为中奖。
     * @data_num 1,2,3
     */
//    public function bet_543($param)
//    {
//        $param = $this->param;
//        $re_data = [
//            'data_num' => [],
//            'count' => 0,
//            'play_name' => '不定胆/五星不定胆/二码不定位'
//        ];
//        if (!isset($param['data_num'])) return $re_data;
//        $data_num = explode(',', $param['data_num']);
//        $data_num = $this->formatNum($data_num);
//        //获取的所有组合
//        $data_num_zh = getArrSet([$data_num, $data_num, $data_num]);
//        foreach ($data_num_zh as $k => $v) {
//            sort($v);
//            if (count(array_unique($v)) != 3) {
//                //去除三个一样的数  0，0，0   1，1，1  2，2，2 223  332
//                unset($data_num_zh[$k]);
//            } else {
//                $data_num_zh[$k] = implode(',', $v);
//            }
//        }
//        $data_num_zh = array_unique($data_num_zh);
//        $data_num_zh = array_values($data_num_zh);
//        $re_data = [
//            'data_num' => $data_num_zh,
//            'count' => count($data_num_zh),
//            'play_name' => '不定胆/五星不定胆/二码不定位'
//        ];
//        return $re_data;
//    }

    /**
     * 不定胆/前四不定胆 /一码不定位
     * 玩法示意：从0-9中任意选择1个以上号码。
     * 投注方案：1；
     * 开奖号码前四位：至少出现1个1，即中前四星一码不定位。
     * 从0-9中选择1个号码，每注由1个号码组成，只要开奖号码的万位、千位、百位、十位中包含所选号码，即为中奖。
     * @data_num 1,2,3
     */
    public function bet_370($param)
    {
        $re = $this->bddq4ym($param);
        return $re;
    }

    /**
     * 不定胆/前四不定胆 /二码不定位
     *  玩法示意：从0-9中任意选择2个以上号码。
     * 投注方案：1,2；
     * 开奖号码前四位：至少出现1和2各1个，即中前四星二码不定位。
     * 从0-9中选择2个号码，每注由2个不同的号码组成，开奖号码的万位、千位、百位、十位中同时包含所选的2个号码，即为中奖。
     *
     * @data_num 1,2,3
     */
//    public function bet_546($param)
//    {
//        $param = $this->param;
//        $re_data = [
//            'data_num' => [],
//            'count' => 0,
//            'play_name' => '不定胆/前四不定胆 /二码不定位'
//        ];
//        if (!isset($param['data_num']) ||$param['data_num'] == '') return $re_data;
//        $data_num = explode(',', $param['data_num']);
//        $data_num = $this->formatNum($data_num);
//        $data_num_zh = getArrSet([$data_num, $data_num]);
//        foreach ($data_num_zh as $k => $v) {
//            sort($v);
//            if (count(array_unique($v)) != 2) {
//                unset($data_num_zh[$k]);
//            } else {
//                $data_num_zh[$k] = implode(',', $v);
//            }
//        }
//        //去重
//        $data_num_zh = array_unique($data_num_zh);
//        $data_num_zh = array_values($data_num_zh);
//        $re_data = [
//            'data_num' => $data_num_zh,
//            'count' => count($data_num_zh),
//            'play_name' => '不定胆/前四不定胆 /二码不定位'
//        ];
//        return $re_data;
//    }

    /**
     * 不定胆/后四不定胆/一码不定位
     * 玩法示意：从0-9中任意选择1个以上号码。
     * 投注方案：1；
     * 开奖号码后四位：至少出现1个1，即中后四星一码不定位。
     * 从0-9中选择1个号码，每注由1个号码组成，只要开奖号码的千位、百位、十位、个位中包含所选号码，即为中奖。
     * @data_num 1,2,3
     *
     */
    public function bet_372($param)
    {
        $re = $this->bddq4ym($param);
        $re['play_name'] = '不定胆/后四不定胆/一码不定位';
        return $re;
    }

    /**
     * 不定胆/后四不定胆/二码不定位
     *  玩法示意： 从0-9中任意选择2个以上号码。
     * 投注方案：1,2；
     * 开奖号码后四位：至少出现1和2各1个，即中后四星二码不定位。
     * 从0-9中选择2个号码，每注由2个不同的号码组成，开奖号码的千位、百位、十位、个位中同时包含所选的2个号码，即为中奖。
     * @data_num 1,2,3
     */
//    public function bet_549($param)
//    {
//        $param = $this->param;
//        $re_data = [
//            'data_num' => [],
//            'count' => 0,
//            'play_name' => '不定胆/后四不定胆/二码不定位'
//        ];
//        if (!isset($param['data_num'])) return $re_data;
//        $data_num = explode(',', $param['data_num']);
//        $data_num = $this->formatNum($data_num);
//        $data_num_zh = getArrSet([$data_num, $data_num]);
//        foreach ($data_num_zh as $k => $v) {
//            sort($v);
//            if (count(array_unique($v)) != 2) {
//                unset($data_num_zh[$k]);
//            } else {
//                $data_num_zh[$k] = implode(',', $v);
//            }
//        }
//        //去重
//        $data_num_zh = array_unique($data_num_zh);
//        $data_num_zh = array_values($data_num_zh);
//        $re_data = [
//            'data_num' => $data_num_zh,
//            'count' => count($data_num_zh),
//            'play_name' => '不定胆/后四不定胆/二码不定位'
//        ];
//        return $re_data;
//    }

    /**
     * 不定胆/三星不定胆一码/后三
     * 玩法示意: 从0-9中任意选择1个以上号码。
     * 投注方案：1；
     * 开奖号码后三位：至少出现1个1，即中后三一码不定位。
     * 从0-9中选择1个号码，每注由1个不同的号码组成，开奖号码的百位、十位、个位中同时包含所选的1个号码，即为中奖。
     * @data_num 1,2,3
     */
    public function bet_374($param)
    {
        $re = $this->bddh3ym($param);
        $re['play_name'] = '不定胆/三星不定胆一码/后三';
        return $re;
    }

    /**
     * 不定胆/三星不定胆一码/前三
     * 玩法示意: 从0-9中任意选择1个以上号码。
     * 投注方案：1；
     * 开奖号码前三位：至少出现1个1，即中前三一码不定位。
     * 从0-9中选择1个号码，每注由1个号码组成，只要开奖号码的万位、千位、百位中包含所选号码，即为中奖。
     * @data_num 1,2,3
     */
    public function bet_375($param)
    {
        $re = $this->bddq3ym($param);
        $re['play_name'] = '不定胆/三星不定胆一码/前三';
        return $re;
    }


    /**
     * 不定胆/三星不定胆二码/后三
     * 玩法示意：从0-9中任意选择2个以上号码。
     * 投注方案：1,2；
     * 开奖号码后三位：至少出现1和2各1个，即中后三二码不定位。
     * 从0-9中选择2个号码，每注由2个不同的号码组成，开奖号码的百位、十位、个位中同时包含所选的2个号码，即为中奖。
     * @data_num 1,2,3
     */
    public function bet_376($param)
    {
        $re = $this->bdd3x2mh3($param);
        return $re;
    }

    /**
     * 不定胆/三星不定胆二码/前三
     * 玩法示意：从0-9中任意选择2个以上号码。
     * 投注方案：1,2；
     * 开奖号码后三位：至少出现1和2各1个，即中后三二码不定位。
     * 从0-9中选择2个号码，每注由2个不同的号码组成，开奖号码的百位、十位、个位中同时包含所选的2个号码，即为中奖。
     * @data_num 1,2,3
     */
    public function bet_377($param)
    {
        $re = $this->bdd3x2mh3($param);
        $re['play_name'] = '不定胆/三星不定胆二码/前三';
        return $re;
    }


    /**
     * 任选/任二/复式
     * 玩法示意：万、千、百、十、个任意2位，开奖号分别对应且顺序一致即中奖
     * 万位买0，千位买1，百位买2，开奖01234，则中奖。
     * 从万、千、百、十、个中至少2个位置各选一个或多个号码，将各个位置的号码进行组合，所选号码的各位与开奖号码相同则中奖。
     *
     * @one_num 1,2,3 (万位)
     * @two_num 1,2,3 (千位)
     * @three_num 1,2,3 (百位)
     * @four_num 1,2,3 (十位)
     * @five_num 1,2,3 (个位)
     *
     */
    public function bet_382($param)
    {
        $re = $this->r2fs($param);
        return $re;
    }

    /**
     * 任选/任二/单式
     * 玩法示意：手动输入号码，至少输入1个两位数号码和至少选择两个位置
     * 万位买0，千位买1，百位买2，开奖01234，则中奖。
     * 从万、千、百、十、个中至少2个位置各选一个或多个号码，将各个位置的号码进行组合，所选号码的各位与开奖号码相同则中奖。
     *
     * @data_num 12 34
     * @data_address 万,千,百,十,个
     *
     */
    public function bet_383($param)
    {
        $re= $this->r2ds($param);
        return $re;
    }

    /**
     * 任选/任二/组选
     * 玩法示意： 从0-9中任意选择2个或2个以上号码和任意两个位置
     * 位置选择万、千，号码选择01；开奖号码为01***、则中奖
     * 从0-9中任意选择2个或2个以上号码和万、千、百、十、个任意的两个位置，如果组合的号码与开奖号码对应则中奖
     * @data_num 1,2,3
     * @data_address 万,千,百,十,个
     *
     * data_num 两两组合的个数  *   data_address 两两组合的个数
     *
     */
    public function bet_384($param)
    {
        $re= $this->r2zhux($param);
        return $re;
    }

    /**
     * 任选/任三/复式
     *玩法示意：万、千、百、十、个任意3位，开奖号分别对应且顺序一致即中奖
     *万位买0，千位买1，百位买2，十位买3，开奖01234，则中奖。
     * 从万、千、百、十、个中至少3个位置各选一个或多个号码，将各个位置的号码进行组合，所选号码的各位与开奖号码相同则中奖
     *
     * @one_num 1,2,3 (万位)
     * @two_num 1,2,3 (千位)
     * @three_num 1,2,3 (百位)
     * @four_num 1,2,3 (十位)
     * @five_num 1,2,3 (个位)
     *
     */
    public function bet_385($param)
    {
        $re = $this->r3fs($param);
        return $re;
    }

    /**
     * 任选/任三/单式
     *  玩法示意：手动输入号码，至少输入1个三位数号码和至少选择三个位置
     * 输入号码012选择万、千、百位置，如开奖号码位012**； 则中奖
     * 手动输入一注或者多注的三个号码和至少三个位置，如果选中的号码与位置和开奖号码对应则中奖
     * @data_address 万,千,百,十,个
     * @data_num 123 345
     */
    public function bet_386($param)
    {
        $re = $this->r3ds($param);
        return $re;
    }
    /**
     * 任选/任三/组三
     * 玩法示意：从0-9中任意选择2个或2个以上号码和任意三个位置
     * 位置选择万、千、百，号码选择01；开奖号码为110**、则中奖
     * 从0-9中任意选择2个或2个以上号码和万、千、百、十、个任意的三个位置，如果组合的号码与开奖号码对应则中奖
     * @data_address 万,千,百,十,个
     * @data_num 1,2,4
     */
    public function bet_387($param)
    {
        $re = $this->r3zhu3($param);
        return $re;
    }

    /**
     * 任选/任三/组六
     * 玩法示意：从0-9中任意选择3个或3个以上号码和任意三个位置
     * 位置选择万、千、百，号码选择012；开奖号码为012**、则中奖
     * 从0-9中任意选择3个或3个以上号码和万、千、百、十、个任意的三个位置，如果组合的号码与开奖号码对应则中奖
     * @data_address 万,千,百,十,个
     * @data_num 1,2,4
     */
    public function bet_388($param)
    {
        $re = $this->r3zhu6($param);
        return $re;
    }


    /**
     * 趣味/特殊/一帆风顺
     * 玩法示意：从0-9中任意选择1个以上号码。
     * 投注方案：8；开奖号码：至少出现1个8，即中一帆风顺。
     * 从0-9中任意选择1个号码组成一注，只要开奖号码的万位、千位、百位、十位、个位中包含所选号码，即为中奖。
     * @data_num 1,2,3
     *
     */
    public function bet_393($param)
    {
        $re = $this->qwyffs($param);
        $re['play_name'] = '趣味/特殊/一帆风顺';
        return $re;
    }

    /**
     * 趣味/特殊/好事成双
     * 玩法示意：从0-9中任意选择1个以上的二重号码。
     * 投注方案：8；开奖号码：至少出现2个8，即中好事成双。
     * 从0-9中任意选择1个号码组成一注，只要所选号码在开奖号码的万位、千位、百位、十位、个位中出现2次，即为中奖。
     * @data_num 1,2,3
     *
     */
    public function bet_394($param)
    {
        $re = $this->qwhscs($param);
        $re['play_name'] = '趣味/特殊/好事成双';
        return $re;
    }

    /**
     * 趣味/特殊/三星报喜
     * 玩法示意：从0-9中任意选择1个以上的三重号码。
     * 投注方案：8；开奖号码：至少出现3个8，即中三星报喜。
     * 从0-9中任意选择1个号码组成一注，只要所选号码在开奖号码的万位、千位、百位、十位、个位中出现3次，即为中奖。
     * @data_num 1,2,3
     *
     */
//    public function bet_570($param)
//    {
//
//        $param = $this->param;
//        $data_num = $param['data_num'];
//        $data_num = explode(',', $data_num);
//        $data_num = $this->formatNum($data_num);
//        $re_data = [
//            'data_num' => [],
//            'count' => count($data_num),
//            'play_name' => '趣味/特殊/三星报喜'
//        ];
//        return $re_data;
//    }


    /**
     * 新龙虎/新龙虎/1v2
     * 投注方案：例如投注龙，开奖号为64535，即为中奖
     * 投注玩法：开奖号码的万位数字大于千位数字则为龙；万位小于千位则为虎；号码相同则打和。
     * @data_num 龙,虎
     */
    public function bet_573($param)
    {
        $re_data = $this->lh($param);

        return $re_data;
    }
    /**
     * 新龙虎/新龙虎/万百
     * 投注方案：例如投注龙，开奖号为64535，即为中奖
     * 投注玩法：开奖号码的万位数字大于百位数字则为龙；万位小于百位则为虎；号码相同则打和。
     * @data_num 龙,虎
     */
    public function bet_577($param)
    {
        $re_data = $this->lh($param);
        return $re_data;
    }
    /**
     * 新龙虎/新龙虎/万十
     * 投注方案：例如投注龙，开奖号为64535，即为中奖
     * 投注玩法：开奖号码的万位数字大于十位数字则为龙；万位小于十位则为虎；号码相同则打和。
     * @data_num 龙,虎
     */
    public function bet_581($param)
    {

        $re_data = $this->lh($param);
        return $re_data;
    }
    /**
     * 新龙虎/新龙虎/万个
     * 投注方案：例如投注龙，开奖号为64535，即为中奖
     * 投注玩法：开奖号码的万位数字大于个位数字则为龙；万位小于个位则为虎；号码相同则打和。
     * @data_num 龙,虎
     */
    public function bet_585($param)
    {
        $re_data = $this->lh($param);
        return $re_data;
    }
    /**
     * 新龙虎/新龙虎/千百
     * 投注方案：例如投注龙，开奖号为64535，即为中奖
     * 投注玩法：开奖号码的千位数字大于百位数字则为龙；千位小于百位则为虎；号码相同则打和。
     * @data_num 龙,虎
     */
    public function bet_589($param)
    {
        $re_data = $this->lh($param);
        return $re_data;
    }
    /**
     * 新龙虎/新龙虎/千十
     * 投注方案：例如投注龙，开奖号为64535，即为中奖
     * 投注玩法：开奖号码的千位数字大于十位数字则为龙；千位小于十位则为虎；号码相同则打和。
     * @data_num 龙,虎
     */
    public function bet_593($param)
    {
        $re_data = $this->lh($param);
        return $re_data;
    }
    /**
     * 新龙虎/新龙虎/千个
     * 投注方案：例如投注龙，开奖号为64535，即为中奖
     * 投注玩法：开奖号码的千位数字大于个位数字则为龙；千位小于个位则为虎；号码相同则打和。
     * @data_num 龙,虎
     */
    public function bet_597($param)
    {
        $re_data = $this->lh($param);
        return $re_data;
    }
    /**
     * 新龙虎/新龙虎/百十
     * 投注方案：例如投注龙，开奖号为64535，即为中奖
     * 投注玩法：开奖号码的百位数字大于十位数字则为龙；百位小于十位则为虎；号码相同则打和。
     * @data_num 龙,虎
     */
    public function bet_601($param)
    {
        $re_data = $this->lh($param);
        return $re_data;
    }
    /**
     * 新龙虎/新龙虎/百个
     * 投注方案：例如投注龙，开奖号为64535，即为中奖
     * 投注玩法：开奖号码的百位数字大于个位数字则为龙；百位小于个位则为虎；号码相同则打和。
     * @data_num 龙,虎
     */
    public function bet_605($param)
    {
        $re_data = $this->lh($param);
        return $re_data;
    }
    /**
     * 新龙虎/新龙虎/十个
     * 投注方案：例如投注龙，开奖号为64535，即为中奖
     * 投注玩法：开奖号码的十位数字大于个位数字则为龙；十位小于个位则为虎；号码相同则打和。
     * @data_num 龙,虎
     */
    public function bet_609($param)
    {
        $re_data = $this->lh($param);
        return $re_data;
    }




    /**
     * 新龙虎/和/万千
     * 投注方案：例如投注龙，开奖号为64535，即为中奖
     * 投注玩法：开奖号码的万位数字大于千位数字则为龙；万位小于千位则为虎；号码相同则打和。
     * @data_num 和
     */
    public function bet_1847($param)
    {
        $re_data = $this->lhh($param);
        return $re_data;
    }
    /**
     * 新龙虎/和/万百
     * 投注方案：例如投注龙，开奖号为64535，即为中奖
     * 投注玩法：开奖号码的万位数字大于百位数字则为龙；万位小于百位则为虎；号码相同则打和。
     * @data_num 和
     */
    public function bet_1848($param)
    {
        $re_data = $this->lhh($param);
        return $re_data;
    }
    /**
     * 新龙虎/和/万十
     * 投注方案：例如投注龙，开奖号为64535，即为中奖
     * 投注玩法：开奖号码的万位数字大于十位数字则为龙；万位小于十位则为虎；号码相同则打和。
     * @data_num 和
     */
    public function bet_1849($param)
    {

        $re_data = $this->lhh($param);
        return $re_data;
    }
    /**
     * 新龙虎/和/万个
     * 投注方案：例如投注龙，开奖号为64535，即为中奖
     * 投注玩法：开奖号码的万位数字大于个位数字则为龙；万位小于个位则为虎；号码相同则打和。
     * @data_num 和
     */
    public function bet_1850($param)
    {
        $re_data = $this->lhh($param);
        return $re_data;
    }
    /**
     * 新龙虎/和/千百
     * 投注方案：例如投注龙，开奖号为64535，即为中奖
     * 投注玩法：开奖号码的千位数字大于百位数字则为龙；千位小于百位则为虎；号码相同则打和。
     * @data_num 和
     */
    public function bet_1851($param)
    {
        $re_data = $this->lhh($param);
        return $re_data;
    }
    /**
     * 新龙虎/和/千十
     * 投注方案：例如投注龙，开奖号为64535，即为中奖
     * 投注玩法：开奖号码的千位数字大于十位数字则为龙；千位小于十位则为虎；号码相同则打和。
     * @data_num 和
     */
    public function bet_1852($param)
    {
        $re_data = $this->lhh($param);
        return $re_data;
    }
    /**
     * 新龙虎/和/千个
     * 投注方案：例如投注龙，开奖号为64535，即为中奖
     * 投注玩法：开奖号码的千位数字大于个位数字则为龙；千位小于个位则为虎；号码相同则打和。
     * @data_num 和
     */
    public function bet_1853($param)
    {
        $re_data = $this->lhh($param);
        return $re_data;
    }
    /**
     * 新龙虎/和/百十
     * 投注方案：例如投注龙，开奖号为64535，即为中奖
     * 投注玩法：开奖号码的百位数字大于十位数字则为龙；百位小于十位则为虎；号码相同则打和。
     * @data_num 和
     */
    public function bet_1854($param)
    {
        $re_data = $this->lhh($param);
        return $re_data;
    }
    /**
     * 新龙虎/和/百个
     * 投注方案：例如投注龙，开奖号为64535，即为中奖
     * 投注玩法：开奖号码的百位数字大于个位数字则为龙；百位小于个位则为虎；号码相同则打和。
     * @data_num 和
     */
    public function bet_1855($param)
    {
        $re_data = $this->lhh($param);
        return $re_data;
    }
    /**
     * 新龙虎/和/十个
     * 投注方案：例如投注龙，开奖号为64535，即为中奖
     * 投注玩法：开奖号码的十位数字大于个位数字则为龙；十位小于个位则为虎；号码相同则打和。
     * @data_num 和
     */
    public function bet_1856($param)
    {
        $re_data = $this->lhh($param);
        return $re_data;
    }

    /**
     * 元素排列组合
     * @param $arr
     * @param $size
     * @return array
     */
    public function numTree($arr, $size)
    {
        $len = count($arr);
        $max = pow(2, $len);
        $min = pow(2, $size) - 1;
        $r_arr = array();
        for ($i = $min; $i < $max; $i++) {
            $count = 0;
            $t_arr = array();
            for ($j = 0; $j < $len; $j++) {
                $a = pow(2, $j);
                $t = $i & $a;
                if ($t == $a) {
                    $t_arr[] = $arr[$j];
                    $count++;
                }
            }
            if ($count == $size) {
                $r_arr[] = $t_arr;
            }
        }
        return $r_arr;
    }
    /**
     * 格式化数据
     */
    public function formatNum($data,$max=9,$min=0)
    {

        foreach ($data as $k => $v) {
            if ($v > $max || $v < $min ||$v == '') {
                unset($data[$k]);
            } else {
                $data[$k] = (int)$v;
            }
        }
        return $data;
    }


}