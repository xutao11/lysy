<?php

namespace lottery;


use think\Db;
use think\Exception;

class Lottery
{
    /**
     * @param $lottery_id  彩种id
     * @param $param  号码参数
     *
     */
    public function getBet($lottery_id, $param, $play_id, $fd)
    {



        //查询彩种是否存在
        $lottery = Db::name('lottery')->where('id', $lottery_id)->field('id,name,status')->find();
        if (!$lottery) throw new \think\Exception('彩种不存在', 100006);
        if ($lottery['status'] == '0') throw new \think\Exception('彩种已禁止', 100006);
        //查询玩法
        $play = Db::name('play')->where('id', $play_id)->where('lottery_id', $lottery_id)->field('status,paramjson,is_ygj,createtime,updatetime,remark,money,min_money_award,max_money_award')->find();


        if (!$play) throw new \think\Exception('玩法不存在', 100006);
        if ($play['status'] == '0') throw new \think\Exception('玩法已禁止', 100006);
        //获取相关的彩票类
        $lotteryClass = $this->getLotteryClass($lottery_id);
        $action = 'bet_' . $play_id;

        $bet = $lotteryClass->$action($param);

        //彩种名
        $bet['lottery'] = $lottery['name'];
        //单注下注金额
        $bet['money'] = $play['money'];
        //单倍奖金
        $bet['money_award'] = $this->jgMoney($fd, $play['max_money_award'], $play['min_money_award']);
        //返点
        $bet['fd'] = $fd;

        //玩法
        $play_name = getPlay($play_id);
        $bet['play_name'] = implode("/", array_reverse($play_name));

        return $bet;

    }

    /**
     * 获取下期开奖时间和期数
     * @param $lottery_id
     */
    public function getKjtime($lottery_id){
        $lotteryClass = $this->getLotteryClass($lottery_id);
        $re = $lotteryClass->gettime();
        return $re;
    }


    /**
     * 获取相关的彩票类
     * @param $lottery_id
     * @return Cqssc
     * @throws Exception
     */
    protected function getLotteryClass($lottery_id)
    {
        switch ($lottery_id) {
            case 1:
                //重庆时时彩
                $lotteryClass = new Cqssc();
                break;
            case 2:
                //新疆时时彩
                $lotteryClass = new Xjssc();
                break;
            case 3:
                //奇趣分分彩
                $lotteryClass = new Qqyfc();
                break;
            case 4:
                //奇趣5分彩
                $lotteryClass = new Qqwfc();
                break;
            case 5:
                //奇趣10 分彩
                $lotteryClass = new Qqsfc();
                break;
            case 6:
                //河内1分彩
                $lotteryClass = new Hlyfc();
                break;
            case 7:
                //河内5分彩
                $lotteryClass = new Hlwfc();
                break;
            case 8:
                //幸运飞艇
                $lotteryClass = new Xyft();
                break;
            case 9:
                //广东11选5
                $lotteryClass = new Gd11x5();
                break;
            case 10:
                //江西11选5
                $lotteryClass = new Jx11X5();
                break;
            case 11:
                //山东11选5
                $lotteryClass = new Sd11x5();
                break;
            case 12:
                //河北11选5
                $lotteryClass = new Hb11x5();
                break;
            case 13:
                //江苏快三
                $lotteryClass = new Jsk3();
                break;
            case 14:
                //广西快三
                $lotteryClass = new Gxk3();
                break;
            default:
                throw new \think\Exception('彩种不存在', 100006);
                break;
        }
        return $lotteryClass;
    }


    /**
     * @param $fd //反点
     * @param $max_money_award //最高奖金
     * @param $min_money_award //最低奖金
     * @param int $multiple 倍数
     * @return string
     */
    public function jgMoney($fd, $max_money_award, $min_money_award, $multiple = 1)
    {

        //计算奖金(最高奖金-最低奖金)/9.7% * 自身返点 + 最低奖金
        //   (9.97-9)/9.7%*9.6% +9
        $money_award = bcadd(bcmul(bcdiv(bcsub($max_money_award, $min_money_award, 10), 9.7, 10), $fd, 10), $min_money_award, 3);
        $money_award = bcmul($money_award, $multiple, 3);
        return $money_award;
    }


}