<?php


namespace lottery\calculation;


trait K3
{

    /**
     * 和值/和值/和值
     * 玩法介绍：和值是指对三个号码的和值进行投注包括“和值3”至“和值18”投注
     * 投注方案：如和值选6 开奖号码之和为6，即视为中奖
     * @data_num 2,3,4
     *
     */
    public function hz($param){
        
        $data_num = $param['data_num'];
        //分割号码
        $data_num = explode(',',$data_num);
        $data_num = $this->formatNum($data_num,18,3);
        $data_num = array_unique($data_num);
        $num = [1,2,3,4,5,6];
        $hmzh = getArrSet([$num,$num,$num]);
        $count = 0;
        if($hmzh){
            foreach ($hmzh as $k=>$v){
                $sum = array_sum($v);
                if(in_array($sum,$data_num)){
                    $count++;
                }
            }
        }
        $re_data = [
            // 'data_num' => '',
            'count' => $count,
            'play_name' => '和值/和值/和值'
        ];
        return $re_data;
    }
    /**
     * 二同号/二同号/二同号单选
     * 玩法介绍：二同号单选是指对三个号码中两个指定的相同号码和一个指定的不同号码进行投注
     * 投注方案：投注号码为113，当期开奖结果为113，视为中奖，如果开奖号为三同号则不中奖
     * @data_num   (单号) 1,2,3,4
     * @double_num  （重复号) 1,2,3,4
     */
    public function ertonghao_dx($param){
        
        //单号处理
        $data_num = $param['data_num'];
        $data_num = $this->formatNum(explode(',',$data_num),6,1);
        $data_num = array_unique($data_num);
        //二同号处理
        $double_num = $param['double_num'];
        $double_num = $this->formatNum(explode(',',$double_num),6,1);
        $double_num = array_unique($double_num);
        if(count($double_num) == 0 || count($data_num) == 0){
        }else{
            foreach ($data_num as $k=>$v){
                foreach ($double_num as $k1=>$v1){
                    if($v == $v1){
                        unset($double_num[$k1]);
                    }
                }
            }
        }
        $re_data = [
            // 'data_num' => '',
            'count' => count($data_num) * count($double_num),
            'play_name' => '二同号/二同号/二同号单选'
        ];
        return $re_data;
    }
    /**
     * 二同号/二同号/二同号复选
     * 玩法介绍：二同号复选是指对三个号码中两个指定的相同号码和一个任意号码进行投注
     * 投注方案：投注号码为11？，当期开奖结果包含11，视为中奖，如果开奖号为三同号则不中奖
     * @double_num( 重复号) 1,2,3,4
     */
    public function ertonghao_fx($param){
        
        $double_num = $param['double_num'];
        $double_num = $this->formatNum(explode(',',$double_num),6,1);
        $double_num = array_unique($double_num);
        $re_data = [
            // 'data_num' => '',
            'count' => count($double_num),
            'play_name' => '二同号/二同号/二同号复选'
        ];
        return $re_data;
    }
    /**
     * /二不同号/二不同号/二不同号
     * 玩法介绍：二不同号投注是指对三个号码中两个指定的不同号码和一个任意号码进行投注
     * 投注方案：投注号码为1，2，当期开奖结果包含1，2，视为中奖。
     * @data_num 1,2,3,4
     */
    public function erbutonghao($param){
        
        $data_num = $param['data_num'];
        $data_num = $this->formatNum(explode(',',$data_num),6,1);
        $hmzh = getArrSet([$data_num,$data_num]);

        $hmzh_new = [];
        if($hmzh){
            foreach ($hmzh as $k=>$v){
                if(count(array_unique($v)) != 2){
                    unset($hmzh[$k]);
                }else{
                    sort($v);
                    $hmzh_new[] = implode(',',$v);
                }
            }
        }
        $hmzh_new = array_unique($hmzh_new);
        $re_data = [
            // 'data_num' => '',
            'count' => count($hmzh_new),
            'play_name' => '二不同号/二不同号/二不同号'
        ];
        return $re_data;
    }
    /**
     * 三同号/三同号/三同号
     * 玩法介绍：三同号是指从所有相同的三个号码（111，222、…、666）中任意选择一组号码进行投注
     * 中奖举例：投注方案：投注号码为111，当期开奖结果为111，视为中奖。
     * @sc_num 三重号 1,2,3
     */
    public function three_th($param){
        
        $sc_num = $param['sc_num'];
        $sc_num = $this->formatNum(explode(',',$sc_num),6,1);
        $sc_num = array_unique($sc_num);
        $re_data = [
            // 'data_num' => '',
            'count' => count($sc_num),
            'play_name' => '三同号/三同号/三同号'
        ];
        return $re_data;
    }
    /**
     * 三不同号/三不同号/标准选号
     * 玩法介绍：从1-6中任意选择3个（或以上）不相同号码组成一注，顺序不限，若其中三位与开奖号码相同即为中奖。
     * 中奖举例：投注方案：2,5,6；开奖号码中出现：1个2、1个5、1个6 (顺序不限)，即为中奖。
     * @data_num 1,2,3,4
     */
    public function three_bt_bz($param){
        
        $data_num = $param['data_num'];
        $data_num = $this->formatNum(explode(',',$data_num),6,1);
        $hmzh = getArrSet([$data_num,$data_num,$data_num]);

        $hmzh_new = [];
        if($hmzh){
            foreach ($hmzh as $k=>$v){
                if(count(array_unique($v)) != 3){
                    unset($hmzh[$k]);
                }else{
                    sort($v);
                    $hmzh_new[] = implode(',',$v);
                }
            }
        }
        $hmzh_new = array_unique($hmzh_new);
        $re_data = [
            // 'data_num' => '',
            'count' => count($hmzh_new),
            'play_name' => '三不同号/三不同号/标准选号'
        ];
        return $re_data;



    }
    /**
     * 三不同号/三不同号/手动输入
     * 玩法介绍：从1-6中任意选择3个（或以上）不相同号码组成一注，顺序不限，若其中三位与开奖号码相同即为中奖。
     * 投注方案：2,5,6；开奖号码中出现：1个2、1个5、1个6 (顺序不限)，即为中奖。
     * @data_num 123 321 456
     */
    public function three_bt_sd($param){
        
        $data_num = explode(' ',$param['data_num']);
        if($data_num) {
            foreach ($data_num as $k=>$v){
                $v = (int)$v;
                if($v<111 || $v>666){
                    unset($data_num[$k]);
                }else{
                    $t = max(array_count_values(str_split($v)));
                    if($t>1){
                        unset($data_num[$k]);
                    }
                }
            }
        }
        $re_data = [
            // 'data_num' => '',
            'count' => count($data_num),
            'play_name' => '三不同号/三不同号/手动输入'
        ];
        return $re_data;
    }
    /**
     * 三连号/三连号/三连号
     * 玩法介绍：开奖号码为三连号视为中奖
     * 投注方案：当期开奖号码中出现：123,234,345,456中的任意一组并且与选号一致即为中奖。
     * @data_num 123,234,345,456
     *
     */
    public function three_lh($param){
        $lh = [123,234,345,456];
        
        $data_num = $param['data_num'];
        $data_num = explode(',',$data_num);
        $data_num = array_unique($data_num);
        $count = 0;
        if($data_num){
            foreach ($data_num as $k=>$v){
                if(in_array($v,$lh)){
                    $count++;
                }
            }
        }
        $re_data = [
            // 'data_num' => '',
            'count' => $count,
            'play_name' => '三连号/三连号/三连号'
        ];
        return $re_data;
    }
    /**
     * 单挑一骰/单挑一骰/单挑一骰
     * 玩法介绍：从1-6中任意选择1个号码组成一注，只要开奖号码出现所选号码，即为中奖。
     * 投注方案：当期开奖号码中包含选号即为中奖。
     * @data_num 1,2,3
     */
    public function dtys($param){
        
        $data_num = $param['data_num'];
        $data_num = $this->formatNum(explode(',',$data_num),6,1);
        $data_num = array_unique($data_num);
        $re_data = [
            // 'data_num' => '',
            'count' => count($data_num),
            'play_name' => '单挑一骰/单挑一骰/单挑一骰'
        ];
        return $re_data;


    }

}