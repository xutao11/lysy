<?php


namespace lottery\calculation;


trait Cp11x5
{
    /**
     * 三码/前三直选/复式   (玩法)
     * 从01-11共11个号码中选择3个不重复的号码组成一注，所选号码与当期顺序摇出的5个号码中的前3个号码相同，且顺序一致，即为中奖。
     * @one_num  第一位好嘛 （01,02,03）
     * @two_num  第二位好嘛 （01,02,03）
     * @three_num  第三位好嘛 （01,02,03）
     */
    public function q3_zhix_fs($param){


        $re_data = [
            // 'data_num' => [],
            'count' => 0,
            'play_name' => '三码/前三直选/复式'
        ];
        if (!isset($param['one_num']) || !isset($param['two_num']) || !isset($param['three_num'])) return $re_data;

        //分割好嘛
        $one_num = $this->formatNum(explode(',', $param['one_num']),11,1);
        $two_num = $this->formatNum(explode(',', $param['two_num']),11,1);
        $three_num = $this->formatNum(explode(',', $param['three_num']),11,1);
        if (count($one_num) == 0 || count($two_num) == 0 || count($three_num) == 0) return $re_data;

        //格式化数据
        foreach ($one_num as $k=>$v){
            if($v > 11 || $v <= 0){
                unset($one_num[$k]);
            }else{

                $one_num[$k] = sprintf("%02d",$v);
            }
        }
        foreach ($two_num as $k=>$v){
            if($v > 11 || $v <= 0){
                unset($two_num[$k]);
            }else{
                $two_num[$k] = sprintf("%02d",$v);
            }
        }
        foreach ($three_num as $k=>$v){
            if($v > 11 || $v <= 0){
                unset($three_num[$k]);
            }else{
                $three_num[$k] = sprintf("%02d",$v);
            }
        }
        //判断最大值和最小值
        //排列组合
        $data_num = getArrSet([$one_num, $two_num, $three_num]);
        //去重
        foreach ($data_num as $k => $v) {
            if (count($v) != count(array_unique($v))) {
                //'该数组有重复值';
                unset($data_num[$k]);
            } elseif (max($v) > 11 || min($v) <= 0) {
                unset($data_num[$k]);
            } else {
                $data_num[$k] = implode(',', $v);
            }
        }

        $data_num = array_values($data_num);

        $re_data = [
        //    'data_num' => $data_num,
            'count' => count($data_num),
            'play_name' => '三码/前三直选/复式'
        ];
        return $re_data;
    }
    /**
     * 三码/前三组合/复式   (玩法)
     * 从01-11中共11个号码中选择3个号码，所选号码与当期顺序摇出的5个号码中的前3个号码相同，顺序不限，即为中奖。
     * @data_num  所选号码 （01,02,03,04）
     */

    protected function q3_zhux_fs($param)
    {
            // $param = $this->param;
        $re_data = [
            // 'data_num' => [],
            'count' => 0,
            'play_name' => '三码/前三组合/复式'
        ];
        if (!isset($param['data_num'])) return $re_data;
        //分割好嘛
        $data_num = explode(',', $param['data_num']);
        if (count($data_num) < 3 || count($data_num) > 11) return $re_data;
        //格式化数据
        foreach ($data_num as $k => $v) {
            //补0 成为2位数
            $data_num[$k] = sprintf("%02d",$v);
        }

        //去重
        $data_num = array_unique($data_num);
        $data_num = array_values($data_num);

        $re_num = numTree($data_num,3);
        //数据格式化
        foreach ($re_num as $k=>$v){
            $re_num[$k] = implode(',',$v);
        }
        $re_data = [
            // 'data_num' => $re_num,
            'count' => count($re_num),
            'play_name' => '三码/前三组合/复式'
        ];
        return $re_data;


    }
    /**
     * 三码/前三组合/单式   (玩法)
     * 手动输入3个号码组成一注，所输入的号码与当期顺序摇出的5个号码中的前3个号码相同，顺序不限，即为中奖。
     * @data_num  购买号码(1,2,3;1,2,4)
     */

//    protected function q3_zhux_ds($param)
//    {
//        // $param = $this->param;
//        $re_data = [
//            // 'data_num' => [],
//            'count' => 0,
//            'play_name' => '三码/前三组合/单式'
//
//        ];
//        if (!isset($param['data_num'])) return $re_data;
//        //分割好嘛
//        $data_num = explode(';', $param['data_num']);
//        $re_num = [];
//        foreach ($data_num as $k => $v) {
//            if ($v == '') {
//                unset($data_num[$k]);
//            } else {
//                $dat = explode(',', $v);
//                if (count($dat) != 3 || count($dat) != count(array_unique($dat)) || max($dat) > 11 || min($dat) <= 0) {
//                    // 判断个数 是否有重复值  所选号码 >0 <=11
//                    unset($data_num[$k]);
//                } else {
//                    foreach ($dat as $k1 => $v1) {
//                        $dat[$k1] = sprintf("%02d",$v1);
//                    }
//                    $re_num[] = $dat;
//                }
//            }
//        }
//        //判断所选号码是否重复(去除元素之相同的组合)
//        foreach ($re_num as $k => $v) {
//            foreach ($re_num as $k1 => $v1) {
//                if ($k1 == $k) continue;
//                $is_del = true;
//                foreach ($v as $k3 => $v3) {
//                    if (!in_array($v3, $v1)) {
//                        $is_del = false;
//                    }
//                }
//                if ($is_del) {
//                    unset($re_num[$k]);
//                }
//            }
//        }
//        foreach ($re_num as $k => $v) {
//            $re_num[$k] = implode(',', $v);
//        }
//        $re_data = [
//            // 'data_num' => $re_num,
//            'count' => count($re_num),
//            'play_name' => '三码/前三组合/单式'
//        ];
//        return $re_data;
//    }
    /**
     * 二码/前二直选/复式   (玩法)
     * 从01-11共11个号码中选择2个不重复的号码组成一注，所选号码与当期顺序摇出的5个号码中的前2个号码相同，且顺序一致，即为中奖。
     * @one_num  第一位好嘛 （01,02,03）
     * @two_num  第二位好嘛 （01,02,03）
     */
    protected function q2_zhix_fs($param)
    {
        $re_data = [
            // 'data_num' => [],
            'count' => 0,
            'play_name' => '二码/前二直选/复式'
        ];
        // $param = $this->param;
        if (!isset($param['one_num']) || !isset($param['two_num'])) return $re_data;
        //分割好嘛
        $one_num = explode(',', $param['one_num']);

        $two_num = explode(',', $param['two_num']);
        if (count($one_num) == 0 || count($two_num) == 0) return $re_data;

        //格式化数据
        foreach ($one_num as $k=>$v){
            if($v > 11 || $v <= 0){
                unset($one_num[$k]);
            }else{
                $one_num[$k] =sprintf("%02d",$v);
            }
        }
        foreach ($two_num as $k=>$v){
            if($v > 11 || $v <= 0){
                unset($two_num[$k]);
            }else{
                $two_num[$k] = sprintf("%02d",$v);
            }
        }
        //判断最大值和最小值
        //排列组合
        $data_num = getArrSet([$one_num, $two_num]);
        //去重
        foreach ($data_num as $k => $v) {
            if (count($v) != count(array_unique($v))) {
                //'该数组有重复值';
                unset($data_num[$k]);
            } elseif (max($v) > 11 || min($v) <= 0) {
                unset($data_num[$k]);
            } else {
                $data_num[$k] = implode(',', $v);
            }
        }
        $data_num = array_values($data_num);
        $re_data = [
            // 'data_num' => $data_num,
            'count' => count($data_num),
            'play_name' => '二码/前二直选/复式'
        ];
        return $re_data;
    }




    /**
     * 二码/前二直选/单式   (玩法)
     * 手动输入2个号码组成一注，所输入的号码与当期顺序摇出的5个号码中的前2个号码相同，且顺序一致，即为中奖。
     * @data_num  下注号码 01,02;02,01
     */
//    protected function q2_zhix_ds($param)
//    {
//        $re_data = [
//            // 'data_num' => [],
//            'count' => 0,
//            'play_name' => '二码/前二直选/单式'
//        ];
//        // $param = $this->param;
//        if ($param['data_num'] == '') return $re_data;
//        //分割
//        $data_num = explode(';', $param['data_num']);
//        foreach ($data_num as $k => $v) {
//            if ($v == '') {
//                unset($data_num[$k]);
//            } else {
//                $dat = explode(',', $v);
//                //位数不满住3位
//                if (count($dat) != 2 || count($dat) != count(array_unique($dat))) {
//                    unset($data_num[$k]);
//                } else {
//                    //所选之不能小于1  大于11
//                    if (max($dat) > 11 || min($dat) <= 0) {
//                        unset($data_num[$k]);
//                    } else {
//                        foreach ($dat as $k_d => $v_d) {
//                            //补0 成为2位数
//                            $dat[$k_d] = sprintf("%02d",$v_d);
//                        }
//                        $data_num[$k] = implode(',', $dat);
//                    }
//                }
//            }
//        }
//        $data_num = array_unique($data_num);
//        $data_num = array_values($data_num);
//        $re_data = [
//            // 'data_num' => $data_num,
//            'count' => count($data_num),
//            'play_name' => '二码/前二直选/单式'
//        ];
//        return $re_data;
//
//    }
    /**
     * 二码/前二组合/复式   (玩法)
     * 从01-11中共11个号码中选择2个号码，所选号码与当期顺序摇出的5个号码中的前2个号码相同，顺序不限，即为中奖。
     * @data_num  开奖号码(01,02,03,04)
     */

    protected function q2_zhux_fs($param)
    {
        // $param = $this->param;
        $re_data = [
            // 'data_num' => [],
            'count' => 0,
            'play_name' => '二码/前二组合/复式'
        ];
        if (!isset($param['data_num'])) return $re_data;
        //分割好嘛
        $data_num = $this->formatNum(explode(',', $param['data_num']),11,1);
        if (count($data_num) < 2 || count($data_num) > 11) return $re_data;
        //格式化数据
        foreach ($data_num as $k => $v) {
            //补0 成为2位数
            $data_num[$k] = sprintf("%02d",$v);
        }
        //去重
        $data_num = array_unique($data_num);
        $data_num = array_values($data_num);
        $re_num = [];
        for ($i = 0; $i < count($data_num); $i++) {
            for ($j = $i + 1; $j < count($data_num); $j++) {
                $re_num[] = $data_num[$i] . ',' . $data_num[$j];
            }
        }
        $re_data = [
            // 'data_num' => $re_num,
            'count' => count($re_num),
            'play_name' => '二码/前二组合/复式'

        ];
        return $re_data;
    }

    /**
     * 二码/前二组合/单式   (玩法)
     * 手动输入2个号码组成一注，所输入的号码与当期顺序摇出的5个号码中的前2个号码相同，顺序不限，即为中奖。
     * @data_num  购买号码(1,2;1,4)
     */
//    protected function q2_zhux_ds($param)
//    {
//
//        // $param = $this->param;
//        $re_data = [
//            // 'data_num' => [],
//            'count' => 0,
//            'play_name' => '二码/前二组合/单式 '
//
//        ];
//        if (!isset($param['data_num'])) return $re_data;
//        //分割好嘛
//        $data_num = explode(';', $param['data_num']);
//        $re_num = [];
//        foreach ($data_num as $k => $v) {
//            if ($v == '') {
//                unset($data_num[$k]);
//            } else {
//                $dat = explode(',', $v);
//                //判断是否两位数字   或判断是否有重复值
//                if (count($dat) != 2 || count($dat) != count(array_unique($dat)) || max($dat) > 11 || min($dat) <= 0) {
//                    // 判断个数 是否有重复值  所选号码 >0 <=11
//                    unset($data_num[$k]);
//                } else {
//                    foreach ($dat as $k1 => $v1) {
//                        $dat[$k1] = sprintf("%02d",$v1);
//                    }
//                    $re_num[] = $dat;
//                }
//            }
//        }
//        //判断所选号码是否重复(去除元素之相同的组合)
//        foreach ($re_num as $k => $v) {
//            foreach ($re_num as $k1 => $v1) {
//                if ($k1 == $k) continue;
//                $is_del = true;
//                foreach ($v as $k3 => $v3) {
//                    if (!in_array($v3, $v1)) {
//                        $is_del = false;
//                    }
//                }
//                if ($is_del) {
//                    unset($re_num[$k]);
//                }
//            }
//        }
//        //组合数字以,连接
//        foreach ($re_num as $k => $v) {
//            $re_num[$k] = implode(',', $v);
//        }
//        $re_data = [
//            // 'data_num' => $re_num,
//            'count' => count($re_num),
//            'play_name' => '二码/前二组合/单式 '
//
//        ];
//        return $re_data;
//    }
    /**
     * 不定胆/前三位   (玩法)
     * 从01-11中共11个号码中选择1个号码，每注由1个号码组成，只要当期顺序摇出的第一位、第二位、第三位开奖号码中包含所选号码，即为中奖。
     * @data_num  所选号码 01,02,03,02,04
     */

//    protected function bdd_q3($param)
//    {
//        // $param = $this->param;
//        $re_data = [
//            // 'data_num' => [],
//            'count' => 0,
//            'play_name' => '不定胆/前三位'
//
//        ];
//        if($param['data_num'] == '') return $re_data;
//
//        //分割所选号码
//        $data_num = explode(',',$param['data_num']);
//
//        //格式化数据
//        foreach ($data_num as $k=>$v){
//
//            if($v<1 || $v > 11){
//                unset($data_num[$k]);
//            }else{
//                $data_num[$k] =sprintf("%02d",$v);
//            }
//        }
//        //去重
//        $data_num = array_unique($data_num);
//        $data_num = array_values($data_num);
//        $re_data = [
//            // 'data_num' => $data_num,
//            'count' => count($data_num),
//            'play_name' => '不定胆/前三位'
//
//        ];
//        return $re_data;
//
//
//
//
//
//    }

    /**
     * 定位胆/定位胆   (玩法)
     * 从第一位，第二位，第三位任意1个位置或多个位置上选择1个号码，所选号码与相同位置上的开奖号码一致，即为中奖。
     * @one_num  第一位好嘛 （01,02,03）
     * @two_num  第二位好嘛 （01,02,03）
     * @three_num  第三位好嘛 （01,02,03）
     */

    protected function dwd($param)
    {
        // $param = $this->param;
        $re_data = [
            // 'data_num' => [],
            'count' => 0,
            'play_name' => '定位胆/定位胆',
            'msg'=>'第一位数字为，第几位，第二个数字为所选号码'
        ];
        if (!isset($param['one_num']) || !isset($param['two_num']) || !isset($param['three_num'])) return $re_data;
        $one_num = explode(',',$param['one_num']);
        $two_num = explode(',',$param['two_num']);
        $three_num = explode(',',$param['three_num']);
        //格式化数据
        $one_num = $this->numPre($one_num);
        $two_num = $this->numPre($two_num);
        $three_num = $this->numPre($three_num);
        //去重
        $one_num = array_unique($one_num);
        $one_num = array_values($one_num);
        $two_num = array_unique($two_num);
        $two_num = array_values($two_num);
        $three_num = array_unique($three_num);
        $three_num = array_values($three_num);
        $re_num = [];
        //第一位
        foreach ($one_num as $k=>$v){
            $re_num[] = '1,'.$v;
        }
        //第二位
        foreach ($two_num as $k=>$v){
            $re_num[] = '2,'.$v;
        }
        //第三位
        foreach ($three_num as $k=>$v){
            $re_num[] = '3,'.$v;
        }
        $re_data = [
            // 'data_num' => $re_num,
            'count' => count($re_num),
            'play_name' => '定位胆/定位胆',
            'msg'=>'第一位数字为，第几位，第二个数字为所选号码'
        ];
        return $re_data;
    }

    /**
     * 任选/任选复试/一中一  (玩法)
     * 从01-11共11个号码中选择1个号码进行购买，只要当期顺序摇出的5个开奖号码中包含所选号码，即为中奖。
     * @data_num  所选号码（01,02,03）
     */

    protected function rx_fs_1z1($param)
    {
        // $param = $this->param;
        $re_data = [
            // 'data_num' => [],
            'count' => 0,
            'play_name' => '任选/任选复试/一中一',
//            'msg'=>'第一位数字为，第几位，第二个数字为所选号码'

        ];
        if($param['data_num'] == '') return $re_data;
        $data_num = $param['data_num'];
        $data_num = explode(',',$data_num);
        //格式化数据
        $data_num = $this->numPre($data_num);
        //去重
        $data_num = array_unique($data_num);
        $data_num = array_values($data_num);

        $re_data = [
            // 'data_num' => $data_num,
            'count' => count($data_num),
            'play_name' => '任选/任选复试/一中一',
//            'msg'=>'第一位数字为，第几位，第二个数字为所选号码'

        ];
        return $re_data;
    }

    /**
     * 任选/任选复试/二中二  (玩法)
     * 从01-11共11个号码中选择2个号码进行购买，只要当期顺序摇出的5个开奖号码中包含所选号码，即为中奖。
     * @data_num  所选号码 01,02,03,04
     */

    protected function rx_fs_2z2($param)
    {
        // $param = $this->param;
        $re_data = [
            // 'data_num' => [],
            'count' => 0,
            'play_name' => '任选/任选复试/二中二',
        ];
        if($param['data_num']=='') return $re_data;
        $data_num = explode(',',$param['data_num']);
        //格式化
        $data_num = $this->numPre($data_num);
        //去重
        $data_num = array_unique($data_num);
        //索性重置
        $data_num = array_values($data_num);
        if(count($data_num)<2) return $re_data;
        $re_num = numTree($data_num,2);
        //数据格式化
        foreach ($re_num as $k=>$v){
            $re_num[$k] = implode(',',$v);

        }
        $re_data = [
            // 'data_num' => $re_num,
            'count' => count($re_num),
            'play_name' => '任选/任选复试/二中二',
        ];
        return $re_data;
    }

    /**
     * 任选/任选复试/三中三  (玩法)
     * 从01-11共11个号码中选择3个号码进行购买，只要当期顺序摇出的5个开奖号码中包含所选号码，即为中奖。
     * @data_num  所选号码 01,02,03,04
     */

    protected function rx_fs_3z3($param)
    {
        // $param = $this->param;
        $re_data = [
            // 'data_num' => [],
            'count' => 0,
            'play_name' => '任选/任选复试/三中三',
        ];
        if($param['data_num'] == '') return $re_data;
        $data_num = explode(',',$param['data_num']);
        //格式化
        $data_num = $this->numPre($data_num);
        //去重
        $data_num = array_unique($data_num);
        //索性重置
        $data_num = array_values($data_num);
        if(count($data_num)<3) return $re_data;

        $re_num = numTree($data_num,3);
        //数据格式化
        foreach ($re_num as $k=>$v){
            $re_num[$k] = implode(',',$v);

        }
        $re_data = [
            // 'data_num' => $re_num,
            'count' => count($re_num),
            'play_name' => '任选/任选复试/三中三',
        ];
        return $re_data;


    }




    /**
     * 任选/任选复试/四中四  (玩法)
     * 从01-11共11个号码中选择4个号码进行购买，只要当期顺序摇出的5个开奖号码中包含所选号码，即为中奖。
     * @data_num  所选号码 01,02,03,04
     */

    protected function rx_fs_4z4($param)
    {
        // $param = $this->param;
        $re_data = [
            // 'data_num' => [],
            'count' => 0,
            'play_name' => '任选/任选复试/四中四',
        ];
        if($param['data_num'] == '') return $re_data;
        $data_num = explode(',',$param['data_num']);
        //格式化
        $data_num = $this->numPre($data_num);
        //去重
        $data_num = array_unique($data_num);
        //索性重置
        $data_num = array_values($data_num);
        if(count($data_num)<4) return $re_data;

        $re_num = numTree($data_num,4);
        //数据格式化
        foreach ($re_num as $k=>$v){
            $re_num[$k] = implode(',',$v);

        }
        $re_data = [
            // 'data_num' => $re_num,
            'count' => count($re_num),
            'play_name' => '任选/任选复试/四中四',
        ];
        return $re_data;
    }

    /**
     * 任选/任选复试/五中五  (玩法)
     * 从01-11共11个号码中选择5个号码进行购买，只要当期顺序摇出的5个开奖号码中包含所选号码，即为中奖。
     * @data_num  所选号码 01,02,03,04,05
     */

    protected function rx_fs_5z5($param)
    {

        // $param = $this->param;
        $re_data = [
            // 'data_num' => [],
            'count' => 0,
            'play_name' => '任选/任选复试/五中五',
        ];
        if($param['data_num'] == '') return $re_data;
        $data_num = explode(',',$param['data_num']);
        //格式化
        $data_num = $this->numPre($data_num);
        //去重
        $data_num = array_unique($data_num);
        //索性重置
        $data_num = array_values($data_num);
        if(count($data_num)<5) return $re_data;

        $re_num = numTree($data_num,5);
        //数据格式化
        foreach ($re_num as $k=>$v){
            $re_num[$k] = implode(',',$v);

        }
        $re_data = [
            // 'data_num' => $re_num,
            'count' => count($re_num),
            'play_name' => '任选/任选复试/五中五',
        ];
        return $re_data;

    }

    /**
     * 任选/任选复试/六中五  (玩法)
     * 从01-11共11个号码中选择6个号码进行购买，只要当期顺序摇出的5个开奖号码中包含所选号码，即为中奖。
     * @data_num  所选号码 01,02,03,04,05,06
     */

    protected function rx_fs_6z5($param)
    {
        // $param = $this->param;
        $re_data = [
            // 'data_num' => [],
            'count' => 0,
            'play_name' => '任选/任选复试/六中五',
        ];
        if($param['data_num'] == '') return $re_data;
        $data_num = explode(',',$param['data_num']);
        //格式化
        $data_num = $this->numPre($data_num);
        //去重
        $data_num = array_unique($data_num);
        //索性重置
        $data_num = array_values($data_num);
        if(count($data_num)<6) return $re_data;

        $re_num = numTree($data_num,6);
        //数据格式化
        foreach ($re_num as $k=>$v){
            $re_num[$k] = implode(',',$v);

        }
        $re_data = [
            // 'data_num' => $re_num,
            'count' => count($re_num),
            'play_name' => '任选/任选复试/六中五',
        ];
        return $re_data;


    }

    /**
     * 任选/任选复试/七中五  (玩法)
     * 从01-11共11个号码中选择7个号码进行购买，只要当期顺序摇出的5个开奖号码中包含所选号码，即为中奖。
     * @data_num  所选号码 01,02,03,04,05,06,7
     */
    protected function rx_fs_7z5($param)
    {
        // $param = $this->param;
        $re_data = [
            // 'data_num' => [],
            'count' => 0,
            'play_name' => '任选/任选复试/七中五',
        ];
        if($param['data_num'] == '') return $re_data;
        $data_num = explode(',',$param['data_num']);
        //格式化
        $data_num = $this->numPre($data_num);
        //去重
        $data_num = array_unique($data_num);
        //索性重置
        $data_num = array_values($data_num);
        if(count($data_num)<7) return $re_data;

        $re_num = numTree($data_num,7);
        //数据格式化
        foreach ($re_num as $k=>$v){
            $re_num[$k] = implode(',',$v);

        }
        $re_data = [
            // 'data_num' => $re_num,
            'count' => count($re_num),
            'play_name' => '任选/任选复试/七中五',
        ];
        return $re_data;


    }

    /**
     * 任选/任选复试/八中五  (玩法)
     * 从01-11共11个号码中选择8个号码进行购买，只要当期顺序摇出的5个开奖号码中包含所选号码，即为中奖。
     * @data_num  所选号码 01,02,03,04,05,06,7,8
     */

    protected function rx_fs_8z5($param)
    {
        // $param = $this->param;
        $re_data = [
            // 'data_num' => [],
            'count' => 0,
            'play_name' => '任选/任选复试/八中五',
        ];
        if($param['data_num'] =='') return $re_data;
        $data_num = explode(',',$param['data_num']);
        //格式化
        $data_num = $this->numPre($data_num);
        //去重
        $data_num = array_unique($data_num);
        //索性重置
        $data_num = array_values($data_num);
        if(count($data_num)<8) return $re_data;

        $re_num = numTree($data_num,8);
        //数据格式化
        foreach ($re_num as $k=>$v){
            $re_num[$k] = implode(',',$v);

        }
        $re_data = [
            // 'data_num' => $re_num,
            'count' => count($re_num),
            'play_name' => '任选/任选复试/八中五',
        ];
        return $re_data;




    }
    protected function numPre($arr){
        $re_arr = [];
        foreach ($arr as $k=>$v){
            if($v<1 || $v > 11){
                unset($arr[$k]);
            }else{

                $re_arr[] = (int)$v;
            }
        }
        return $re_arr;
    }
}