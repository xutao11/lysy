<?php

namespace game;
/**
 * 沙巴体育
 * Class SabaSports
 * @package game
 */
class SabaSports
{

    public $url = 'http://a0281.lishengyl88.com';
    public $vendor_id = '3s6ubfva9g'; //厂商识别码, 最大长度 = 50

    /**
     * /api/CreateMember 此 API 方法为在 Sportsbook 建立新会员资料。当创建会员后，FundTransfer 才能进行下注。
     * 测试环境建立帐号请加上"_test".
     * 呼叫API方法: http://{domain name}/api/CreateMember/
     * 请求格式: application/x-www-form-urlencoded
     *
     * 错误讯息
     * 代码    內容    描述
     * 0    OK    执行成功
     * 1    Failed    执行失败
     * 2    Failed    会员名称（Username）重复
     * 3    Failed    OperatorId错误
     * 4    Failed    赔率类型格式错误
     * 5    Failed    币别格式错误
     * 6    Failed    厂商会员识别码重复
     * 7    Failed    最小限制转帐金额大于最大限制转帐金额
     * 8    Failed    无效的前缀字元
     * 9    Failed    厂商识别码失效
     * 10    Failed    系统维护中
     * 11    Failed    最小限制转帐金额小于1
     * 12    Failed    長度限制30 ( Vendor_Member_ID or UserName)
     *
     * 参数
     * @param vendor_id * string (query)     厂商识别码, 最大长度 = 50
     *
     * Vendor_Member_ID *string (query)     厂商会员识别码（建议跟 Username 一样）, 支援 ASCII Table 33-126, 最大长度 = 30
     *
     * OperatorId * string (query)              厂商 ID。请带入sitename, 最大长度 = 50
     *
     * FirstName string (query)     会员姓氏, 最大长度 = 50
     *
     * LastName string (query)        会员名字, 最大长度 = 50
     *
     * UserName *    string    (query)        会员登入名称, 最大长度 = 30
     *
     * OddsType *    string    (query)        为此会员设置赔率类型。请参考附件"赔率类型表"
     *
     * Currency *    integer($int32)    (query)        为此会员设置币别。请参考附件中"币别表"
     *
     * MaxTransfer *    number($double)    (query)        于 Sportsbook 系统与厂商间的最大限制转帐金额
     *
     * MinTransfer *    number($double)    (query)        于 Sportsbook 系统与厂商间的最小限制转帐金额
     *
     * CustomInfo1    string    (query)        厂商备注, 最大长度 = 200
     *
     * CustomInfo2    string    (query)        厂商备注, 最大长度 = 200
     *
     * CustomInfo3    string    (query)        厂商备注, 最大长度 = 200
     *
     * CustomInfo4    string    (query)        厂商备注, 最大长度 = 200
     *
     * CustomInfo5    string    (query)        厂商备注, 最大长度 = 200
     *
     * 回应    回应Content Type
     *
     * 代码    描述
     * 200
     * 成功
     * {
     * "error_code": "0",
     * "message": ""
     * }
     * 204
     * 呼叫API失败
     *
     *
     */
    public function createMember($Vendor_Member_ID)
    {
        $url = $this->url . '/api/CreateMember';

        $vendor_id = $this->vendor_id;  //厂商识别码, 最大长度 = 50
//        $Vendor_Member_ID = 'cs_test'; //厂商会员识别码（建议跟 Username 一样）, 支援 ASCII Table 33-126, 最大长度 = 30
        $OperatorId = ''; //厂商 ID。请带入sitename, 最大长度 = 50
//        $FirstName = ''; //会员姓氏, 最大长度 = 50
//        $LastName = ''; //会员名字, 最大长度 = 50
        $UserName = $Vendor_Member_ID;  //会员登入名称, 最大长度 = 30
        $OddsType = '2';  //为此会员设置赔率类型。请参考附件"赔率类型表"
        $Currency = '13'; //为此会员设置币别。请参考附件中"币别表"
        $MaxTransfer = '1000'; //于 Sportsbook 系统与厂商间的最大限制转帐金额
        $MinTransfer = '0'; // 于 Sportsbook 系统与厂商间的最小限制转帐金额
        $post_data = [
            'vendor_id' => $vendor_id,
            'Vendor_Member_ID' => $Vendor_Member_ID,
            'OperatorId' => $OperatorId,
//            'FirstName' => $FirstName,
//            'LastName' => $LastName,
            'UserName' => $Vendor_Member_ID,
            'OddsType' => $OddsType,
            'Currency' => $Currency,
            'MaxTransfer' => $MaxTransfer,
            'MinTransfer' => $MinTransfer,
        ];
        $re = getHttpContent($url, 'POST', $post_data);
        $re = json_decode($re, true);
        if ($re['error_code'] != 0) {
            throw new \think\Exception($re['message'], 100007);

        }
        return $re;
    }

    /**
     * /api/Login    用于取得API登入金钥
     * 错误讯息
     * 代码    內容    说明
     * 0    OK    执行成功
     * 1    Failed    系统错误
     * 2    Failed    会员不存在
     * 9    Failed    厂商识别码失效
     * 10    Failed    系统维护中
     * @return token （string）
     */
    public function login()
    {
        //厂商会员识别码, 最大长度 = 30
        $vendor_member_id = 'cs_test';

        $url = $this->url . '/api/Login';
        $post_data = [
            'vendor_id' => $this->vendor_id,
            'vendor_member_id' => $vendor_member_id
        ];
        //请求数据
        $re = getHttpContent($url, 'POST', $post_data);

        $re = json_decode($re, true);
        //没有该用户
        if ($re['error_code'] == 2) {
            //创建用户
            $this->CreateMember($vendor_member_id);
            //请求数据
            $re = getHttpContent($url, 'POST', $post_data);
        }


        if ($re['error_code'] != 0) {
            throw new \think\Exception($re['message'], 100007);
        }
        return $re['Data'];
    }


    /**
     * 此为验证APP token.
     * 错误讯息
     * Code    Text    Description
     * 0    OK    执行成功
     * 1    Failed    执行过程中失败/ Token不存在
     * 2    Failed    找不到用户
     * 9    Failed    厂商识别码失效
     * 10    Failed    系统维护中
     */
    public function verifyToken($token)
    {
        $url = $this->url . '/api/VerifyToken ';

        $post_data = [
            'vendor_id' => $this->vendor_id,
            'token' => $token
        ];
        //请求
        $re = getHttpContent($url, "POST", $post_data);
        if($re['error_code'] != 0) {
            throw new \think\Exception($re['message'], 100007);

        }
        return $re;


    }


}

