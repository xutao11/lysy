<?php
/**
 * This file is part of workerman.
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the MIT-LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author walkor<walkor@workerman.net>
 * @copyright walkor<walkor@workerman.net>
 * @link http://www.workerman.net/
 * @license http://www.opensource.org/licenses/mit-license.php MIT License
 */

/**
 * 用于检测业务代码死循环或者长时间阻塞等问题
 * 如果发现业务卡死，可以将下面declare打开（去掉//注释），并执行php start.php reload
 * 然后观察一段时间workerman.log看是否有process_timeout异常
 */
//declare(ticks=1);
//namespace Im;
use \GatewayWorker\Lib\Gateway;
use Workerman\MySQL\Connection;

/**
 * 主逻辑
 * 主要是处理 onConnect onMessage onClose 三个方法
 * onConnect 和 onClose 如果不需要可以不用实现并删除
 */
class Events
{
    public static $db = null;
    public static $user = null;

    /**
     * 开启服务
     */
    public static function onWorkerStart($businessWorker)
    {

        echo "====================聊天室服务器开启====================\n";
        echo "====================连接数据库====================\n";
        $db1 = new Connection('127.0.0.1',3306,'lsyl','root','lsyl');
        self::$db = $db1;
        //实例用户类
        self::$user = new User(self::$db);
    }

    /**
     * 当客户端连接时触发
     * 如果业务不需此回调可以删除onConnect
     * 
     * @param int $client_id 连接id
     */
    public static function onConnect($client_id)
    {
        // 向当前client_id发送数据 
        Gateway::sendToClient($client_id,self::sendData('connect','','连接成功'));
    }
    
   /**
    * 当客户端发来消息时触发
    * @param int $client_id 连接id
    * @param mixed $message 具体消息
    */
   public static function onMessage($client_id, $param_data)
   {
       $param_data = json_decode($param_data,true);
       switch ($param_data['type']){
           case 'token':
               //获取用户信息
                $user_token= self::$user->verificationToken($param_data['data']);
                if($user_token['re_type'] == false){
                    Gateway::sendToClient($client_id,self::sendData('token_error','',$user_token['re_data']));
                }else{
//                    '{"type":"token","data":"00a36a3b30e450002907df519cb38b53"}';   348
                    //                    '{"type":"token","data":"01428b58e924310204d79a8379247b73"}';   347
                    //绑定用户
                    Gateway::bindUid($client_id,$user_token['re_data']);
                    $_SESSION[$client_id] = $user_token['re_data'];
                    //上线通知
                    self::online($user_token['re_data']);

                    //获取用户的上级和下级
                    $friends  = self::$user->getFriends($user_token['re_data']);
//                    var_dump($friends);
                    //上级
                    if($friends['parent_data']){
                        foreach ($friends['parent_data'] as $k=>$v){
                            //判断用户是否在线
                            if(Gateway::isUidOnline($v['id'])){
                                //在线
                                $friends['parent_data'][$k]['isonline'] = 1;
                            }else{
                                //离线
                                $friends['parent_data'][$k]['isonline'] = 0;
                            }
                        }
                    }
                    //下级用户处理
                    if($friends['child_data']){
                        foreach ($friends['child_data'] as $k=>$v){
                            //判断用户是否在线
                            if(Gateway::isUidOnline($v['id'])){
                                //在线
                                $friends['child_data'][$k]['isonline'] = 1;
                            }else{
                                //离线
                                $friends['child_data'][$k]['isonline'] = 0;
                            }
                        }
                    }
                    Gateway::sendToClient($client_id,self::sendData('friends',$friends,'好友列表'));
                }
               break;
           case "sendmsg":
               //获取当前发送者的uid
               $sender_id = Gateway::getUidByClientId($client_id);
               echo "发送者id:".$sender_id."\n";
               //发送消息
//               {"type":"sendmsg","data":"发送消息","receive_id":"347","msg_type":"txt","file":"文件名"}
               if(trim($param_data['data']) == ''){
                   Gateway::sendToClient($client_id,self::sendData('error','','发送消息为空'));
               }else{

                   //查询是否为好友关系
                   $sql = "select id from lsyl_user where (user_id = {$sender_id} or id = (select user_id from lsyl_user where id = {$sender_id})) and id={$param_data['receive_id']}";
                   echo "sql:".$sql."\n";
                   $is_friends = self::$db->query($sql);
                   echo "好友";
                   var_dump($is_friends);
                   echo "发送者id:".$sender_id."======接收者id".$param_data['receive_id']."===========发送类容:".$param_data['data']."==========消息类型:".$param_data['msg_type']."\n";

                   if(count($is_friends)>0){
                       //发送给好友
                       if(!isset($param_data['file_name'])){
                           $param_data['file_name'] = '';
                       }
                        Gateway::sendToUid($param_data['receive_id'],self::sendData('sendmsg',$param_data['data'],$param_data['msg_type'],$param_data['file_name'],$param_data['receive_id'],$sender_id));
                       //记录发送消息
             
                       self::$user->createMsg($sender_id,$param_data['receive_id'],$param_data['data'],time(),$param_data['msg_type'],$param_data['file_name']);
                   }else{
                       Gateway::sendToClient($client_id,self::sendData('error','','该用户不是你好友'));
                   }
               }
               break;
       }

   }
   
   /**
    * 当用户断开连接时触发
    * @param int $client_id 连接id
    */
   public static function onClose($client_id)
   {
       //获取下线人uid
       $uid = $_SESSION[$client_id];
       if($uid){
           unset($_SESSION[$client_id]);
           //离线通知
           self::offIine($uid);
       }
      echo $client_id."====uid:".$uid."====离线";


   }

    /**
     * 下线通知
     * @param $user_id  用户id
     */
    public static function offIine($user_id){
        $friends_ids = self::$user->getFriendIds($user_id);
        if($friends_ids){
            foreach ($friends_ids as $k=>$v){
                Gateway::sendToUid($v['id'],self::sendData('offline',$user_id,'用户离线'));
            }
        }
    }
    /**
     * 上线通知
     * @param $user_id  用户id
     */
    public static function online($user_id){
        $friends_ids = self::$user->getFriendIds($user_id);
        if($friends_ids){
            foreach ($friends_ids as $k=>$v){
                Gateway::sendToUid($v['id'],self::sendData('online',$user_id,'用户上线'));
            }
        }


    }




    /**
     * 发送数据
     * @param $type 类型 connect(连接成功)
     * @param string $data  数据
     * @param string $msg
     */
    /**
     * 发送数据
     * @param $type 类型 connect(连接成功)
     * @param string $data  数据
     * @param string $msg
     */
    public static function sendData($type,$data='',$msg='',$file_name='',$receive_id = 0,$send_id=0){

        $data = [
            'type'=>$type,
            'data'=>$data,
            'msg'=>$msg,
            'time'=>date('Y-m-d H:i:s',time()),
            'file_name'=>$file_name,
            'receive_id'=>$receive_id, //接收者
            'send_id'=>$send_id  //发送者
        ];
        if($file_name == ''){
            unset($data['file_name']);
        }
        if($receive_id == 0){
            unset($data['receive_id']);
        }
        if($send_id == 0){
            unset($data['send_id']);
        }

        return json_encode($data,JSON_UNESCAPED_UNICODE);
    }

}
