<?php



class User
{
    //数据库
    public $db = null;
    public function __construct($db)
    {
        if(!$db) return[
            're_type'=>false,
            're_data'=>'请连接数据库'
        ];
        $this->db=$db;
    }

    /**
     * 更具token验证当前token值
     */
    public function verificationToken($token){
        //查询token
        $sql = "select * from lsyl_user_token where token ='".$token."' limit 1";
        echo $sql."\n";
        $re = $this->db->query($sql);
        if(count($re) == 0){
            return [
                're_type'=>false,
                're_data'=>'没有查询到该token值'
            ];
        }else{
            if($re[0]['expiretime']< time()){
                return [
                    're_type'=>false,
                    're_data'=>'token已过期'
                ];
            }else{
                return [
                    're_type'=>true,
                    're_data'=>$re[0]['user_id']
                ];
            }
        }
    }

    //获取用户的上下级
    public function getFriends($user_id){
        //获取上级 Parent
        $parent_sql = "select id,avatar,account from lsyl_user where id = (select user_id from lsyl_user where id={$user_id})";
        $parent_data = $this->db->query($parent_sql);
        if($parent_data){
            foreach ($parent_data as $k=>$v){
                $look_f_sql = "select count(*)as count from lsyl_chatlog where recipient_uid={$user_id} and send_uid = {$v['id']} and is_look='0'";
                $look_f_sql_num = $this->db->query($look_f_sql);
                $parent_data[$k]['unread_num'] = $look_f_sql_num[0]['count'];
                $parent_data[$k]['account']="上级";
            }
        }


        //获取下级
        $child_sql = "select id,avatar,account from lsyl_user where user_id = {$user_id}";
        $child_data = $this->db->query($child_sql);
        //查询未读消息个数
        if($child_data){
            foreach ($child_data as $k=>$v){
                $look_f_sql = "select count(*)as count from lsyl_chatlog where recipient_uid={$user_id} and send_uid = {$v['id']} and is_look='0'";
                $look_f_sql_num = $this->db->query($look_f_sql);
                $child_data[$k]['unread_num'] = $look_f_sql_num[0]['count'];
            }
        }

        $data = [
            'parent_data'=>$parent_data,
            'child_data'=>$child_data
        ];
        return $data;
    }

    /**
     * 记录发送消息
     * @param $sender_id 发送者id
     * @param $receive_id 接收者id
     * @param $msg  发送消息
     * @param $time 发送时间
     * @param $msg_type 消息类型  txt image file
     * @param $file_name 文件名
     *
     */
    public function createMsg($sender_id,$receive_id,$msg,$time,$msg_type,$file_name){

        $sql = "insert into lsyl_chatlog (send_uid,recipient_uid,msg,is_look,createtime,msg_type,file_name) values ({$sender_id},{$receive_id},'{$msg}','0',{$time},'{$msg_type}','{$file_name}')";
        echo $sql;
        $this->db->query($sql);

    }

    /**
     * 获取用户好友
     * @param $user_id 用户id
     */
    public function getFriendIds($user_id){
        $friends_sql = "select id from lsyl_user where user_id = {$user_id} or id = (select user_id from lsyl_user where id={$user_id})";
        $friends_ids = $this->db->query($friends_sql);
        return $friends_ids;


    }

}