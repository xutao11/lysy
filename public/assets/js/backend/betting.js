define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'betting/index' + location.search,
                    add_url: 'betting/add',
                    edit_url: 'betting/edit',
                    del_url: 'betting/del',
                    multi_url: 'betting/multi',
                    table: 'betting',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                search:false,
                searchFormVisible: true,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'ordercode', title: __('Ordercode')},
                        // {field: 'user_id', title: __('User_id')},
                        {field: 'account', title:'下注人账号'},
                        {field: 'pre_draw_issue', title: __('Pre_draw_issue')},
                        {field: 'bettingLottery.name', title: '彩种',operate:false},
                        {field: 'param', title: '所选号码',operate:false},
                        {field: 'hangplan_id', title: __('Hangplan_id'), operate:false},
                        // {field: 'bt_num', title: __('Bt_num')},
                        // {field: 'gjbt_num', title: __('Gjbt_num')},
                        // {field: 'data_js', title: __('Data_js')},
                        {field: 'is_jz', title: __('Is_jz'), searchList: {"1":__('Is_jz 1'),"0":__('Is_jz 0')}, formatter: Table.api.formatter.normal},
                        // {field: 'is_max', title: __('Is_max')},
                        {field: 'play', title: '玩法',operate:false},
                        {field: 'jg_money', title: __('Jg_money'), operate:false},
                        {field: 'jg_num', title: __('Jg_num'), operate:false},
                        {field: 'xz_money', title: __('Xz_money'), operate:false},
                        {field: 'xz_num', title: __('Xz_num'), operate:false},
                        {field: 'multiple', title: __('Multiple'), operate:false},
                        {field: 'unit', title: __('Unit'), operate:false},
                        {field: 'kj_time', title: __('Kj_time'), operate:'RANGE', addclass:'datetimerange'},
                        {field: 'remark', title: __('Remark'), operate:false},
                        {field: 'status', title: __('Status'), searchList: {"1":__('Status 1'),"2":__('Status 2'),"3":__('Status 3'),"4":__('Status 4'),"5":__('Status 5')}, formatter: Table.api.formatter.status},
                        {field: 'createtime', title: __('Createtime'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'updatetime', title: __('Updatetime'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime,operate:false},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});