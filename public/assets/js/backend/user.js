define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'user/index' + location.search,
                    add_url: 'user/add',
                    edit_url: 'user/edit',
                    del_url: 'user/del',
                    resetpwd:'user/resetpwd',
                    baobiao:'user/userbb',
                    zfqd:'user/ptpay',
                    userlogin:'user/userlogin',
                    money:'user/moneyxz',
                    letter:'user/letter',
                    multi_url: 'user/multi',
                    editLevel:'user/edit_level',
                    table: 'user',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                search:false,
                searchFormVisible: true,
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id'),operate:false},
                        {field: 'avatar', title: __('Avatar'), events: Table.api.events.image, formatter: Table.api.formatter.image,operate:false},
                        {field: 'is_dali', title: __('Is_dali'), searchList: {"0":__('Is_dali 0'),"1":__('Is_dali 1')}, formatter: Table.api.formatter.normal},
                        {field: 'is_pay', title: __('Is_pay'), searchList: {"0":__('Is_pay 0'),"1":__('Is_pay 1')}, formatter: Table.api.formatter.normal},
                        {field: 'is_zz', title: __('Is_zz'), searchList: {"0":__('Is_zz 0'),"1":__('Is_zz 1')}, formatter: Table.api.formatter.normal},
                        {field: 'account', title: __('Account')},
                        {field: 'ratio', title: __('Ratio'), operate:false},
                        {field: 'money', title: __('Money'), operate:false},
                        {field: 'level', title: __('Level'),operate:false, table: table,events: Table.api.events.operate, formatter: Table.api.formatter.buttons,buttons:[
                                {
                                    name: 'cj',
                                    text: __('层级'),
                                    title: __('层级'),
                                    classname: 'btn btn-xs btn-primary btn-dialog',
                                    icon: 'fa fa-list',
                                    url: '/admin/user/user_level?{$ids}',
                                    callback: function (data) {
                                        Layer.alert(data);
                                        table.bootstrapTable('refresh');

                                    },
                                    visible: function (row) {
                                        //返回true时按钮显示,返回false隐藏
                                        return true;
                                    }
                                }

                            ]},
                        {field: 'identity', title: '身份',operate:false},
                        {field: 'status', title: __('Status'), searchList: {"1":__('Status 1'),"0":__('Status 0'),"2":__('Status 2')}, formatter: Table.api.formatter.status},
                        {field: 'is_jz', title: __('Is_jz'), searchList: {"1":__('Is_jz 1'),"0":__('Is_jz 0')}, formatter: Table.api.formatter.normal},
                        {field: 'p_user', title: __('User_id'),operate:false},
                        {field: 'login_ip', title: __('Login_ip'),operate:false},
                        {field: 'login_time', title: __('Login_time'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime,operate:false},
                        {field: 'createtime', title: __('Createtime'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime,operate:false},
                         {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate,buttons:[
                                 {
                                     name: 'editLevel',
                                     text: __('移线'),
                                     title: __('移线'),
                                     classname: 'btn btn-xs btn-primary btn-dialog',
                                     icon: 'fa fa-list',
                                     url: '/admin/user/edit_level?user_id={ids}',
                                     callback: function (data) {
                                         Layer.alert("接收到回传数据：" + JSON.stringify(data), {title: "回传数据"});
                                     },
                                     visible: function (row) {
                                         //返回true时按钮显示,返回false隐藏
                                         return true;
                                     }
                                 },
                                 {
                                     name: 'letter',
                                     text: __('私信'),
                                     title: __('私信'),
                                     classname: 'btn btn-xs btn-primary btn-dialog',
                                     icon: 'fa fa-list',
                                     url: 'user/letter?user_id={ids}',
                                     callback: function (data) {
                                         Layer.alert("接收到回传数据：" + JSON.stringify(data), {title: "回传数据"});
                                     },
                                     visible: function (row) {
                                         //返回true时按钮显示,返回false隐藏
                                         return true;
                                     }
                                 },
                                 {
                                     name: 'baobiao',
                                     text: __('报表'),
                                     title: __('报表'),
                                     classname: 'btn btn-xs btn-primary btn-dialog',
                                     icon: 'fa fa-list',
                                     url: '/admin/user/userbb?{$ids}',
                                     callback: function (data) {
                                         // if(data == '修正成功'){
                                         //     table.bootstrapTable('refresh');
                                         // }
                                         // Layer.alert(data);

                                     },
                                     visible: function (row) {
                                         //返回true时按钮显示,返回false隐藏
                                         return true;
                                     }
                                 },
                                {
                                    name: 'zfqd',
                                    text: __('支付渠道'),
                                    title: __('支付渠道'),
                                    classname: 'btn btn-xs btn-primary btn-dialog',
                                    icon: 'fa fa-list',
                                    url: '/admin/user/ptpay?{$ids}',
                                    callback: function (data) {
                                        Layer.alert(data);
                                        table.bootstrapTable('refresh');
                                    },
                                    visible: function (row) {
                                        //返回true时按钮显示,返回false隐藏
                                        return true;
                                    }
                                },
                                 {
                                     name: 'userlogin',
                                     text: __('登录日志'),
                                     title: __('登录日志'),
                                     classname: 'btn btn-xs btn-primary btn-dialog',
                                     icon: 'fa fa-list',
                                     url: '/admin/user/userlogin?{$ids}',
                                     callback: function (data) {
                                         Layer.alert(data);
                                         table.bootstrapTable('refresh');
                                     },
                                     visible: function (row) {
                                         //返回true时按钮显示,返回false隐藏
                                         return true;
                                     }
                                 },
                                 {
                                     name: 'money',
                                     text: __('资金调整'),
                                     title: __('资金调整'),
                                     classname: 'btn btn-xs btn-primary btn-dialog',
                                     icon: 'fa fa-list',
                                     url: '/admin/user/moneyxz?{$ids}',
                                     callback: function (data) {
                                         if(data == '修正成功'){
                                             table.bootstrapTable('refresh');
                                         }
                                         Layer.alert(data);

                                     },
                                     visible: function (row) {
                                         //返回true时按钮显示,返回false隐藏
                                         return true;
                                     }
                                 },
                                 {
                                     name: 'resetpwd',
                                     text: __('重置密码'),
                                     title: __('重置密码'),
                                     classname: 'btn btn-xs btn-success btn-magic btn-ajax',
                                     icon: 'fa fa-magic',
                                     url: '/admin/user/resetpwd?{$ids}',
                                     confirm: '重置密码:ls123456',
                                     success: function (data, ret) {
                                         Layer.alert(ret.msg);
                                         //如果需要阻止成功提示，则必须使用return false;
                                         //return false;
                                     },
                                     error: function (data, ret) {
                                         console.log(data, ret);
                                         Layer.alert(ret.msg);
                                         return false;
                                     }
                                 },


                            ]}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});