define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'gdbonus/index' + location.search,
                    add_url: 'gdbonus/add',
                    edit_url: 'gdbonus/edit',
                    del_url: 'gdbonus/del',
                    multi_url: 'gdbonus/multi',
                    table: 'gdbonus',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'user_id', title: __('User_id')},
                        {field: 'account', title: '用户',operate:false},
                        {field: 'date', title: __('Date')},
                        {field: 'xz_num', title: __('Xz_num')},
                        {field: 'xz_money', title: __('Xz_money'), operate:'BETWEEN'},
                        {field: 'hd_money', title: __('Hd_money'), operate:'BETWEEN'},
                        {field: 'jg_money', title: __('Jg_money'), operate:'BETWEEN'},
                        {field: 'fd_money', title: __('Fd_money'), operate:'BETWEEN'},
                        {field: 'yk_money', title: __('Yk_money'), operate:'BETWEEN'},
                        {field: 'ratio', title: __('Ratio')},
                        {field: 'money', title: __('Money'), operate:'BETWEEN'},
                        {field: 'createtime', title: __('Createtime'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});