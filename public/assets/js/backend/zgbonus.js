define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'zgbonus/index' + location.search,
                    add_url: 'zgbonus/add',
                    edit_url: 'zgbonus/edit',
                    del_url: 'zgbonus/del',
                    multi_url: 'zgbonus/multi',
                    examine:'zgbonus/examine',
                    table: 'zgbonus',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'cycle_id', title: __('Cycle_id')},
                        {field: 'user_id', title: __('User_id')},
                        {field: 'account', title: '用户',operate:false},
                        {field: 'startdate', title: __('Startdate'), operate:'RANGE', addclass:'datetimerange'},
                        {field: 'enddate', title: __('Enddate'), operate:'RANGE', addclass:'datetimerange'},
                        {field: 'xz_money', title: __('Xz_money'), operate:'BETWEEN'},
                        {field: 'fd_money', title: __('Fd_money'), operate:'BETWEEN'},
                        {field: 'hd_money', title: __('Hd_money'), operate:'BETWEEN'},
                        {field: 'jg_money', title: __('Jg_money'), operate:'BETWEEN'},
                        {field: 'yx_count', title: __('Yx_count')},
                        {field: 'yk_money', title: __('Yk_money'), operate:'BETWEEN'},
                        {field: 'money', title: __('Money'), operate:'BETWEEN'},
                        {field: 'status', title: __('Status'), searchList: {"0":__('Status 0'),"1":__('Status 1'),"2":__('Status 2')}, formatter: Table.api.formatter.status},
                        {field: 'remark', title: __('Remark')},
                        {field: 'ratio', title: __('Ratio')},
                        {field: 'createtime', title: __('Createtime'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate,buttons:[

                                {
                                    name: 'examine',
                                    text: __('审核'),
                                    title: __('审核'),
                                    classname: 'btn btn-xs btn-primary btn-dialog',
                                    icon: 'fa fa-list',
                                    url: '/admin/zgbonus/examine?id={ids}',
                                    callback: function (data) {
                                        if(data == '审核成功'){
                                            table.bootstrapTable('refresh');
                                        }
                                        Layer.alert(data);
                                    },
                                    visible: function (row) {
                                        //返回true时按钮显示,返回false隐藏
                                        return true;
                                    }
                                },


                            ]}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});