define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'kjlog/index' + location.search,
                    add_url: 'kjlog/add',
                    edit_url: 'kjlog/edit',
                    del_url: 'kjlog/del',
                    multi_url: 'kjlog/multi',
                    table: 'kjlog',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'lottery_id', title: __('Lottery_id')},
                        {field: 'lottery', title: '彩票名',operate:false},
                        {field: 'pre_draw_issue', title: __('Pre_draw_issue')},
                        {field: 'pre_draw_time', title: __('Pre_draw_time'), operate:'RANGE', addclass:'datetimerange'},
                        {field: 'pre_draw_code', title: __('Pre_draw_code')},
                        {field: 'createtime', title: __('Createtime'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});