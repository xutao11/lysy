define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'hangplan/index' + location.search,
                    add_url: 'hangplan/add',
                    edit_url: 'hangplan/edit',
                    del_url: 'hangplan/del',
                    multi_url: 'hangplan/multi',
                    table: 'hangplan',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'name', title: __('Name')},
                        {field: 'gjtype_id', title: __('Gjtype_id')},
                        {field: 'user_id', title: __('User_id')},
                        {field: 'status', title: __('Status'), searchList: {"1":__('Status 1'),"0":__('Status 0')}, formatter: Table.api.formatter.status},
                        {field: 'lottery_id', title: __('Lottery_id')},
                        {field: 'play_id', title: __('Play_id')},
                        {field: 'unit', title: __('Unit'), searchList: {"元":__('Unit 元'),"角":__('Unit 角'),"分":__('Unit 分'),"厘":__('Unit 厘')}, formatter: Table.api.formatter.normal},
                        {field: 'betting_jk', title: __('Betting_jk'), searchList: {"0":__('Betting_jk 0'),"1":__('Betting_jk 1')}, formatter: Table.api.formatter.normal},
                        {field: 'jk_gz', title: __('Jk_gz')},
                        {field: 'data_num', title: __('Data_num')},
                        {field: 'pt_type', title: __('Pt_type'), searchList: {"1":__('Pt_type 1'),"2":__('Pt_type 2')}, formatter: Table.api.formatter.normal},
                        {field: 'zx_bt', title: __('Zx_bt')},
                        {field: 'gjbt_id', title: __('Gjbt_id')},
                        {field: 'fp_type', title: __('Fp_type'), searchList: {"1":__('Fp_type 1'),"2":__('Fp_type 2')}, formatter: Table.api.formatter.normal},
                        {field: 'zsyk_y', title: __('Zsyk_y'), searchList: {"1":__('Zsyk_y 1'),"0":__('Zsyk_y 0')}, formatter: Table.api.formatter.normal},
                        {field: 'zsyk_y_value', title: __('Zsyk_y_value')},
                        {field: 'zsyk_k', title: __('Zsyk_k'), searchList: {"1":__('Zsyk_k 1'),"0":__('Zsyk_k 0')}, formatter: Table.api.formatter.normal},
                        {field: 'zsyk_k_value', title: __('Zsyk_k_value')},
                        {field: 'remark', title: __('Remark')},
                        {field: 'createtime', title: __('Createtime'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'updatetime', title: __('Updatetime'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});