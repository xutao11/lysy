/*
Navicat MySQL Data Transfer

Source Server         : 103.218.240.15(利胜正式库)
Source Server Version : 50729
Source Host           : 103.218.240.15:3306
Source Database       : ls_net

Target Server Type    : MYSQL
Target Server Version : 50729
File Encoding         : 65001

Date: 2020-07-30 10:28:39
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for cp_lottery
-- ----------------------------
DROP TABLE IF EXISTS `cp_lottery`;
CREATE TABLE `cp_lottery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) DEFAULT '' COMMENT '图片',
  `lotterytype_id` int(11) DEFAULT '0' COMMENT '分类',
  `name` varchar(255) DEFAULT '' COMMENT '彩票名',
  `status` enum('0','1') DEFAULT '1' COMMENT '状态:1=正常,0=禁用',
  `url_b` varchar(255) DEFAULT '' COMMENT '备用',
  `url` varchar(255) DEFAULT '' COMMENT '接口',
  `createtime` int(11) DEFAULT NULL COMMENT '创建时间',
  `updatetime` int(11) DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COMMENT='彩票种类表';

-- ----------------------------
-- Records of cp_lottery
-- ----------------------------
INSERT INTO `cp_lottery` VALUES ('1', '/uploads/20200413/10acaebbc12b8632ef5b330cd35914b1.png', '1', '重庆时时彩', '1', 'http://api.82p.net/api?p=json&t=cqssc&token=130A148DE7767351&limit=5', 'https://www.zhuyingtj.com/api?t=cqssc&limit=5&token=xFNiG7yCTZpT7x7c', '1575339267', '1586877169');
INSERT INTO `cp_lottery` VALUES ('2', '/uploads/20200323/8d246d8d1323e1f36518a5b161f645bf.png', '3', '奇趣分分彩', '1', 'http://api.82p.net/api?p=json&t=txffc&token=130A148DE7767351&limit=5', 'https://www.zhuyingtj.com/api?t=qqffc&limit=5&token=aqN6XllbcKgPsdxd', '1575340331', '1586421381');
INSERT INTO `cp_lottery` VALUES ('3', '', '3', '奇趣3分彩', '0', '', '', '1575340350', '1575340350');
INSERT INTO `cp_lottery` VALUES ('4', '/uploads/20200311/51efdcfc2a291a5a2a3f994be7f9152d.png', '3', '奇趣5分彩', '1', 'http://api.82p.net/api?p=json&t=tx5fc&token=130A148DE7767351&limit=5', 'https://www.zhuyingtj.com/api?t=qq5fc&limit=5&token=eTXA6yfzeaKmvTRn', '1575340366', '1586421379');
INSERT INTO `cp_lottery` VALUES ('5', '/uploads/20200311/896c19bfb96f00e3d4c27caf04883993.png', '3', '奇趣10分彩', '1', 'http://api.82p.net/api?p=json&t=tx10fc&token=130A148DE7767351&limit=5', 'https://www.zhuyingtj.com/api?t=qqsfc&limit=5&token=eTXA6yfzeaKmvTRn', '1575340383', '1586421386');
INSERT INTO `cp_lottery` VALUES ('7', '/uploads/20200322/473f9e9bf56f74e10ae990f2a3f12fc3.png', '3', '腾讯1分彩', '0', '', '', '1575340421', '1585413976');
INSERT INTO `cp_lottery` VALUES ('8', '/uploads/20200311/6d6c1ed650b6f6a34acf7a336fbbee70.png', '3', '河内5分彩', '1', 'http://api.82p.net/api?p=json&t=hn5fc&token=130A148DE7767351&limit=5', 'https://www.zhuyingtj.com/api?t=hn5fc&limit=5&token=xFNiG7yCTZpT7x7c', '1575340439', '1586421478');
INSERT INTO `cp_lottery` VALUES ('9', '/uploads/20200311/1fc2a09991c0d961164a750bdf12cf5e.png', '2', '广东11选5', '1', 'http://api.82p.net/api?p=json&t=gd115&token=130A148DE7767351&limit=5', 'https://www.zhuyingtj.com/api?t=gd11x5&limit=5&token=qqRCnkDRfns2Ahzw', '1575340454', '1586542282');
INSERT INTO `cp_lottery` VALUES ('10', '/uploads/20200322/c9e08b67755f3a918fb568ff2bfc4c07.png', '2', '江西11选5', '1', 'http://api.82p.net/api?p=json&t=jx115&token=130A148DE7767351&limit=5', 'https://www.zhuyingtj.com/api?t=jx11x5&limit=5&token=qqRCnkDRfns2Ahzw', '1575340465', '1588182100');
INSERT INTO `cp_lottery` VALUES ('11', '/uploads/20200429/d2147f80adfc9cfe43f9c36a7ab0274d.png', '2', '山东11选5', '1', 'http://api.82p.net/api?p=json&t=sd115&token=130A148DE7767351&limit=5', 'https://www.zhuyingtj.com/api?t=sd11x5&limit=5&token=xFNiG7yCTZpT7x7c', '1575340497', '1588182097');
INSERT INTO `cp_lottery` VALUES ('12', '/uploads/20200322/92fd2e1420eedf34af682f60515ab24e.png', '4', '北京PK10', '0', '', '', '1575427534', '1588173648');
INSERT INTO `cp_lottery` VALUES ('13', '/uploads/20200311/b46e085fbe258b95a743d8448418ea32.png', '4', '幸运飞艇', '1', 'http://api.82p.net/api?p=json&t=xyft&token=130A148DE7767351&limit=5', 'https://www.zhuyingtj.com/api?t=mlaft&limit=5&token=xFNiG7yCTZpT7x7c', '1575427707', '1586421391');
INSERT INTO `cp_lottery` VALUES ('14', '/uploads/20200429/2bdea9873e9aab5d8f7c0bbad6892717.png', '4', '北京PK10', '0', '', '', '1575512545', '1588173425');
INSERT INTO `cp_lottery` VALUES ('15', '/uploads/20200311/84d9f05f99a4a81ae456d8301085ec2f.png', '3', '河内1分彩', '1', 'http://api.82p.net/api?p=json&t=hn1fc&token=130A148DE7767351&limit=5', 'https://www.zhuyingtj.com/api?t=hnffc&limit=5&token=70TJILZC7cYS5vy7', '1578118650', '1586421393');
INSERT INTO `cp_lottery` VALUES ('16', '', '3', '腾讯5分彩', '0', '', '', '1581477129', '1585413987');
INSERT INTO `cp_lottery` VALUES ('17', '', '3', '腾讯10分彩', '0', '', '', '1581501255', '1585413996');
INSERT INTO `cp_lottery` VALUES ('18', '/uploads/20200327/b612ca66811b29aaf2858974b851ad52.png', '3', '排列三', '0', '', '', '1585248802', '1586421013');
INSERT INTO `cp_lottery` VALUES ('19', '/uploads/20200327/f0579a868244115f308195a0275d5d3f.png', '3', '排列五', '1', '', '', '1585248829', '1585248829');
INSERT INTO `cp_lottery` VALUES ('20', '/uploads/20200429/5bac81f9c14ab578ac97b4ef0b3f8804.png', '1', '新疆时时彩', '1', 'http://api.82p.net/api?p=json&t=xjssc&token=130A148DE7767351&limit=5', 'https://www.zhuyingtj.com/api?t=xjssc&limit=5&token=xFNiG7yCTZpT7x7c', '1587371233', '1588182103');
INSERT INTO `cp_lottery` VALUES ('21', '/uploads/20200429/b276d41d4a53aa9d802d419cb59d3b99.png', '5', '江苏快三', '1', 'http://api.82p.net/api?p=json&t=jsk3&token=130A148DE7767351&limit=5', 'https://www.zhuyingtj.com/api?t=jsk3&limit=5&token=xFNiG7yCTZpT7x7c', '1588126332', '1588962099');
INSERT INTO `cp_lottery` VALUES ('22', '/uploads/20200429/82fbf4499f93469f233d56bbff2858d8.png', '5', '广西快三', '1', 'http://api.82p.net/api?p=json&t=gxk3&token=130A148DE7767351&limit=5', 'https://www.zhuyingtj.com/api?t=gxk3&limit=5&token=xFNiG7yCTZpT7x7c', '1588126381', '1588962094');
INSERT INTO `cp_lottery` VALUES ('23', '/uploads/20200604/2d14a7ab4c1b00998412aee9ca1e88d2.png', '2', '河北11选5', '1', 'http://api.82p.net/api?p=json&t=heb115&token=130A148DE7767351&limit=5', 'http://api.81p.net/api?p=json&t=heb115&token=130A148DE7767351&limit=5', '1591193252', '1596051717');
